 
<script src="/fakturace/js/superselect/chosen.jquery.js" type="text/javascript"></script>
<link rel="stylesheet" href="/fakturace/js/superselect/chosen.css">    
<style>
 a.projectlink {
         background: #b3d1f7;
         -webkit-border-radius: 5px;
         -moz-border-radius: 5px;
         border-radius: 5px;
         display: inline-block;
         float: left;
         padding: 5px 12px;
         margin-right: 3px;
         color: black;
         margin-top: 0;
        }
        a.projectlink2 {
          background: #b3d1f7;
         -webkit-border-radius: 2px;
         -moz-border-radius: 2px;
         border-radius: 2px;
         display: inline-block;
         width: auto;
         padding: 5px 12px;
         margin-right: 3px;
        }
        .projecbox  {
          width: auto!important;
          background: #b3d1f7;
         -webkit-border-radius: 2px!important;
         -moz-border-radius: 2px!important;
         border-radius: 2px!important;
         display: inline-block;
         width: auto;
         padding: 3px 12px!important;
         margin: 0 5px 5px 0 !important;
          text-align: left !important;
        }
        a.wait, a.ok { color: #404040 !important; }

 input.correct[type=checkbox]{
	height: 0;
	width: 0;
	visibility: block;
}
label.correct {
  cursor: pointer;
    text-indent: -9999px;
    width: 20px;
    height: 14px;
    background: grey;
    background: #28a745;
    display: inline-block;
    border-radius: 100px;
    position: relative;
    float: none;
    top: 2px;
}
label.bytextarea {
  cursor: pointer;
    text-indent: -9999px;
    width: 20px;
    height: 14px;
    background: grey;
    background: #28a745;
    display: inline-block;
    border-radius: 100px;
    position: relative;
    float: none;
    top: 10px;
    margin-left: 10px;
}
label.correct:after {
	content: '';
	position: absolute;
	top: 1px;
	left: 1px;
	width: 12px;
	height: 12px;
	background: #fff;
	border-radius: 12px;
  transition: 0.3s;

}
input.correct:checked + label.correct {
  background: red;
}
input.correct:checked + label.correct:after {
	left: calc(100% - 1px);
	transform: translateX(-100%);
}
label.correct:active:after {
	width: 20px;
}
 input.faincorrect[type=checkbox]{
	height: 0;
	width: 0;
	visibility: hidden;
}
label.faincorrect {
	cursor: pointer;
	text-indent: -9999px;
	width: 20px;
	height: 14px;
  background: red;
	display: inline-block;
	border-radius: 100px;
  position: relative;
  float: none;
 top: 7px;
}
label.faincorrect:after {
	content: '';
	position: absolute;
	top: 1px;
	left: 1px;
	width: 12px;
	height: 12px;
	background: #fff;
	border-radius: 12px;
  transition: 0.3s;

}
input.faincorrect:checked + label.faincorrect {
  background: #bada55;
}
input.faincorrect:checked + label.faincorrect:after {
	left: calc(100% - 1px);
	transform: translateX(-100%);
}
label.faincorrect:active:after {
	width: 20px;
}
 #correctionmail {
   display: inline-block ;
 background: #efefef;
 margin-left: 5px;
 padding: 7px 15px;
 cursor: pointer;

}
#correctionmail:hover{
  background: #c0c0c0;
  color: #fff;
}
#mailhere {
  display: inline-block ;
 margin-left: 5px;
}

#mailhere a {
  display: block;
  background: red;
  color: #fff;
  padding: 7px 15px;
}
textarea {
  width: 254px;
    float: left;
}

.h5, h5 {
    width: 100%;
    font-size: 1.25rem;
    border-bottom: 1px dotted #17a2b8;
    margin-bottom: 0.5em;
    padding-bottom: 0.5em;
    margin-top: 1em;
    clear: left;
    display: inline-block;
    color: #17a2b8;
}

h5 i {
  color: #17a2b8;
  margin-right: 10px;
}
.QRcodes {
  padding: 5px;
  border: 1px solid rgb(206, 212, 218);
  margin-bottom: 20px;
}
</style>


<link rel="stylesheet" href="./js/superselect/prism.css">
   <link rel="stylesheet" href="./js/superselect/chosen.css">
<?php
$table_name = "project_online";
$page_name = "projekty";

if (isset($_REQUEST["newsid"]) && $_REQUEST["newsid"] > 0) {
    $newsid = $_REQUEST["newsid"];
} else {
    $newsid = false;
}

if (@$newsid) {
    $query = "SELECT * FROM " . $table_name . " WHERE id =" . $newsid;
    //echo $query;
    $res = mysql_query($query);
    if ($res && @mysql_num_rows($res) > 0) {
        $row = mysql_fetch_array($res);
    }
}

$checkquery = "Select * From " . $table_name . " Where ID =" . $newsid;
$checkresult = mysql_query($checkquery);
if (($checkresult) && (mysql_numrows($checkresult) > 0)) {
    while ($row = @mysql_fetch_array($checkresult)) {

        $id = $row['newsid'];
        $name = $row['name'];
        $organization_id = $row['organization_id'];
        $newKeywords = $row['newKeywords'];
        $description = $row['description'];
        $newFinalMoney = $row['newFinalMoney'];
        $newWaitCount = $row['newWaitCount'];
        $newOtherMoney = $row['newOtherMoney'];
        $newUcel = $row['newUcel'];
        $webPage = $row['webPage'];
        $newSbirkaOd = $row['newSbirkaOd'];
        $newSbirkaDo = $row['newSbirkaDo'];
        $accountDrawing = $row['accountDrawing'];
        $account = $row['account'];
        $newContactName = $row['newContactName'];
        $newContactPhone = $row['newContactPhone'];
        $newContactMobile = $row['newContactMobile'];
        $newContactEmail = $row['newContactEmail'];
        $newAktivace = $row['newAktivace'];
        $newCommunication = $row['newCommunication'];
        $newTV = $row['newTV'];
        $newPOS = $row['newPOS'];
        $newRadio = $row['newRadio'];
        $newNET = $row['newNET'];
        $newPrint = $row['newPrint'];
        $newPropagace = $row['newPropagace'];
        $newBoards = $row['newBoards'];
        $newEvents = $row['newEvents'];
        $newOtherComm = $row['newOtherComm'];
        $newRef = $row['newRef'];
        $file2 = $row['file2'];
        $file3 = $row['file3'];
        $projectStatut = $row['projectStatut'];
        $projectOfflineNr = $row['projectOfflineNr'];
        if ($row['forever'] == 'yes') {$forever = '<div class="input-group mb-3">Na dobu neurčitou, tedy do 31.12.2030</div>';}

        $edited = $row['edited'];

        $comquery = "Select name From organization_online Where ID = " . $organization_id;
        $comresult = mysql_query($comquery);
        if (($comresult) && (mysql_numrows($comresult) > 0)) {
            while ($comrow = @mysql_fetch_array($comresult)) {
                $mycompany = $comrow['name'];
            }
        }

    }
}

function mkChbox($fld, $nr, $post, $fromDB, $txt, $projectnr)
{

 
    $return = '<div class="custom-control custom-checkbox my-1 mr-sm-2">';
    $return .= '<input ';
    if (($post == $nr) || ($fromDB == 1)) {$return .= ' checked ';}
    $return .= ' type="checkbox" name="cats['. $nr .']"  value="'. $nr .'"  class="custom-control-input" id="' . $fld . $nr . '">';
    $return .= '<label class="custom-control-label" for="' . $fld . $nr . '">'.$txt.'</label>';
    $return .= '</div>';
    return $return;

}

function makeForm($field, $label, $type, $switch, $value, $required = '')
{

    if($type == 'textarea'){

      $html = '<div class="form-group txtarea">';
      $html .= '<label class="txtarea" for="'.$field.'">'.$label;
      $html .= '   <i><input type="checkbox" id="switch' . $switch . '" class="correct"  value="' . $label . '" /><label class="correct" onclick="clearMail()" for="switch' . $switch . '"></label></i>';
      $html .= '</label><textarea name="'.$field.'" class="form-control" rows="3" id="'.$field.'">';
      $html .=  $value;
      $html .= '</textarea></div>';

    }
    else{

    $html = '<div class="input-group mb-3">';
    $html .= ' <div class="input-group-prepend">';
    $html .= '   <span class="input-group-text" id="inputGroup-sizing-default">';
    $html .= $label;
    $html .= '   </span>';
    $html .= ' </div>';
    $html .= ' <input type="' . $type . '" required="required" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" value="';
    $html .= $value;
    $html .= ' " name="';
    $html .= $field;
    $html .= '" />';
    $html .= ' <div class="input-group-append">';
    $html .= ' <span class="input-group-text">';
    $html .= '   <input type="checkbox" id="switch' . $switch . '" class="correct"  value="' . $label . '" /><label class="correct" onclick="clearMail()" for="switch' . $switch . '"></label>';
    $html .= ' </span>';
    $html .= ' </div> ';
    $html .= ' </div> ';
    }

    return $html;

}

?>
<h1><?php echo $name; ?> (Poslední změna: <?php echo getHourDateFromSQL($edited); ?> )</h1>

<form ENCTYPE="multipart/form-data" action="index.php?id=<?php echo $page_name; ?><?php if ($newsid > 0) {echo "&newsid=", $newsid;}?>" method="post" name="noname" id="kontakt" class="row">

<div  class="<?php echo $resp2; ?>">

<fieldset>

<h5>Základní údaje</h5>
<?php $switchNR = 1; ?>
<?php echo makeForm('name', 'Název projektu:', 'text', $switchNR, $name);  $switchNR++;   ?>
<hr/>
<?php echo makeForm('newKeywords', 'Návrh DMS hesla:', 'text', $switchNR, $newKeywords); $switchNR++;   ?>

 

 
 

<?php echo makeForm('description', 'Popis projektu:', 'textarea', $switchNR, $description);  $switchNR++;   ?>
 
<h5>Financování projektu </h5>

<?php echo makeForm('newFinalMoney', 'Cílová částka projektu:', 'text', $switchNR, $newFinalMoney);  $switchNR++;   ?>
<?php echo makeForm('newOtherMoney', 'Specifikace dalších zdrojů financování projektu:', 'textarea', $switchNR, $newOtherMoney);  $switchNR++;   ?>
<?php echo makeForm('newUcel', 'Účel využití finančních prostředků z DMS:', 'textarea', $switchNR, $newUcel);  $switchNR++;   ?>
<?php echo makeForm('webPage', 'Webová adresa:', 'text', $switchNR, $webPage);  $switchNR++;   ?>
<?php echo makeForm('newSbirkaOd', 'Sbírka OD:', 'text', $switchNR, $newSbirkaOd);  $switchNR++;   ?>
<?php echo makeForm('$newSbirkaDo', 'Povolena do::', 'text', $switchNR, $newSbirkaDo);  $switchNR++;   ?>
<?php echo $forever; ?>
<?php echo makeForm('accountDrawing', 'Číslo sbírkového účtu:', 'text', $switchNR, $accountDrawing);  $switchNR++;   ?>
<?php echo makeForm('account', 'Číslo běžného účtu:', 'text', $switchNR, $account);  $switchNR++;   ?>
 
<h5>Kontaktní osoba projektu:</h5>

<?php echo makeForm('newContactName', 'Jméno a příjmení:', 'text', $switchNR, $newContactName);  $switchNR++;   ?>
<?php echo makeForm('newContactPhone', 'Telefon pevná linka:', 'text', $switchNR, $newContactPhone);  $switchNR++;   ?>
<?php echo makeForm('newContactMobile', 'Mobilní telefon:', 'text', $switchNR, $newContactMobile);  $switchNR++;   ?>
<?php echo makeForm('newContactEmail', 'E-mail:', 'email', $switchNR, $newContactEmail);  $switchNR++;   ?>
 
 <h5>Specifikace projektu <i><input type="checkbox" id="switch<?php echo $switchNR; ?>" class="correct"  value="Specifikace projektu" /><label class="correct" onclick="clearMail()" for="switch<?php echo $switchNR;  $switchNR++;  ?>"></label></i></h5>
 
<?php
$overkatquery = "SELECT * FROM overcategory Order by id ";
$overkatresult = mysql_query($overkatquery);
if (($overkatresult) && (mysql_numrows($overkatresult) > 0)) {
    while ($overow = @mysql_fetch_array($overkatresult)) {
 
      $checkquery = "Select category_id From projectCategory_online Where project_id =" . $newsid . "  And category_id = " . $overow['id'];
        $catres = mysql_query($catsq);
        $checkresult = mysql_query($checkquery);
        if (($checkresult) && (mysql_numrows($checkresult) > 0)) {
            $check = 1;
        } else {
            $check = 0;
        }
        echo mkChbox('over' . $overow['id'], $overow['id'], $_POST['cats'][$overow['id']], $check, $overow['name'], $_REQUEST['project']);
    }
 
}
?>

<h5>Požadavek na aktivaci </h5>

<?php echo makeForm('newRef', 'Reference:', 'textarea', $switchNR, $newRef);  $switchNR++;   ?>
<?php echo makeForm('newAktivace', 'Požadovaná aktivace:', 'textarea', $switchNR, $newAktivace);  $switchNR++;   ?>

<h5>Komunikační plán k DMS projektu</h5>


<?php echo makeForm('newCommunication', 'Popis komunikace:', 'textarea', $switchNR, $newCommunication);  $switchNR++;   ?>
<?php echo makeForm('newTV', 'Televize:', 'textarea', $switchNR, $newTV);  $switchNR++;   ?>
<?php echo makeForm('newPOS', 'Tiskové a propagační materiály:', 'textarea', $switchNR, $newPOS);  $switchNR++;   ?>
<?php echo makeForm('newNET', 'Internet, sociální sítě a on-line média:', 'textarea', $switchNR, $newNET);  $switchNR++;   ?>
<?php echo makeForm('newPrint', 'Tištěná média:', 'textarea', $switchNR, $newPrint);  $switchNR++;   ?>
<?php echo makeForm('newPropagace', 'Tiskové a propagační materiály:', 'textarea', $switchNR, $newPropagace);  $switchNR++;   ?>
<?php echo makeForm('newBoards', 'Média na cílených místech:', 'textarea', $switchNR, $newBoards);  $switchNR++;   ?>
<?php echo makeForm('newEvents', 'Speciální akce/ Eventy:', 'textarea', $switchNR, $newEvents);  $switchNR++;   ?>

 
<h5>Přiložené soubory</h5>
    <table class="files" style="width: 300px">
    <tr><td>

    <?php if ($file2) {?><a class='projectlink' href='/downloads/online/<?php echo $file2; ?>'>Logo projektu</a><?php }?></td> <td> <input type="checkbox" id="switch<?php echo $switchNR; ?>" class="correct"  value="Logo projektu" /><label class="correct" onclick="clearMail()" for="switch<?php echo $switchNR; ?>"></label>

    </td></tr>
    <?php $switchNR++; ?>
    <tr><td>
    <?php if ($file3) {?><a class='projectlink' href='/downloads/online/<?php echo $file3; ?>'>Osvědčení o konání veřejné sbírky</a><?php }?></td> <td> <input type="checkbox" id="switch<?php echo $switchNR; ?>" class="correct"  value="Osvědčení o konání veřejné sbírky" /><label class="correct" onclick="clearMail()" for="switch<?php echo $switchNR; ?>"></label>

</td></tr>
</table>
</fieldset>
</div>

<div  class="<?php echo $resp2; ?>">

<h5>Schválení</h5>

 <?php  if ($projectStatut == 1) {$supercolor = ' #d3f1c5 ';} elseif ($projectStatut == 3) {$supercolor = ' #4bb8ff ';}  elseif ($projectStatut == 2) {$supercolor = ' #fbbbbd ';} else { $supercolor = '';} ?>
 <div class="input-group mb-3"><div class="input-group-prepend"><span class="input-group-text" id="inputGroup-sizing-default">
 Stav schválení:</span></div>
    <input type="hidden" name="allreadyStatut" value="<?php echo $projectStatut; ?>" />
    <select style="background: <?php echo $supercolor; ?>" name="projectStatut" id="valid" title="Vyberte z možností" class="custom-select">
    <option value="0">Čeká</option>
    <option value='3' style='background: #4bb8ff' <?php if ($projectStatut == 3) {echo ' selected ';}?> >Předložit radě</option>
    <option value='1' style='background: #d3f1c5' <?php if ($projectStatut == 1) {echo ' selected ';}?> >Schváleno</option>
    <option value='2' style='background: #fbbbbd' <?php if ($projectStatut == 2) {echo ' selected ';}?> >Zamítnuto</option>
   </select>
     </div>

 <h5>Asociace s  DMS/ATS cloudem</h5>
  <label>Cloud Projekt:</label>

  <?php

$cloudquery = "SELECT * FROM `project` Where  DATE(`collectionTo`) > CURDATE()  ORDER BY name ASC";
$cloudresult = mysql_query($cloudquery);
if (($cloudresult) && (mysql_numrows($cloudresult) > 0)) {
    ?>
     <select name = "projectOfflineNr" data-placeholder="Vyber" class="chosen-select"   tabindex="-1"  style='width: 100%' data-placeholder="Vyber">
     <option style='width: 100%' value="-1">-- vyberte cloud projekt --</option>
     <?php
while ($comrow = mysql_fetch_array($cloudresult)) {
        ?>
     <option style='width: 100%' value="<?php echo $comrow['id']; ?>" <?php if ($projectOfflineNr == $comrow['id']) {echo ' selected ';}?> ><?php echo substr ($comrow['name'],0,70); ?> [<?php echo $comrow['collectionFrom']; ?> - <?php echo $comrow['collectionTo']; ?>][<?php echo $comrow['id']; ?>] </option>
     <?php
}
    ?>
     </select>
     <?php
}
?>





<input type='hidden' name='organization_name' value='<?php echo $name; ?>' />
<input type='hidden' name='organization_id' value='<?php echo $organization_id; ?>' />
<input type='hidden' name='mycompany' value='<?php echo $mycompany; ?>' />

<div class='sender'>       
   <input type="submit" class="btn btn-primary " value="Uložit změny" name="sendProject"> 
   <p id="correctionmail" onclick="asemble()"  >Sestavit žádost o doplnění</p>
   <p id="mailhere"></p>
   </div>

   <?php $switchNR++; ?>
<script>
  function asemble(){
    var mailstr = 'mailto:<?php echo $newContactEmail; ?>?subject=Doplňte, prosím, registraci projektu do systému DMS&body=Dobrý den, je potřeba doplnit/opravit následující údaje: ';   
    for (i = 1; i < <?php echo $switchNR; ?>; i++) {
       if(document.getElementById('switch'+i).checked === true) { mailstr = mailstr + ' ' + document.getElementById('switch'+i).value  + ', '; }
    }
    document.getElementById("mailhere").innerHTML = "<a href='" + mailstr + "'>ODESLAT ŽÁDOST</a> ";
  }
  function clearMail(){
    document.getElementById("mailhere").innerHTML = "";
    }
</script>
</div>
</form>


<hr/>

<script type="text/javascript">
    var config = {
      '.chosen-select'           : {},
      '.chosen-select-deselect'  : {allow_single_deselect:true},
      '.chosen-select-no-single' : {disable_search_threshold:10},
      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chosen-select-width'     : {width:"90%"}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }
    </script>



<form action="index.php?id=<?php echo $page_name; ?>new<?php if ($newsid > 0) {echo "&newsid=", $newsid;} ?>" method="post" name="noname" id="kontakt" class="row" style='width: 50%'>
<fieldset>
  <div class='QRcodes' >
  <div class="input-group mb-3"> <div class="input-group-prepend"><span class="input-group-text" id="QRinputGroup-sizing-default">DMS heslo pro QR:  </span> </div> <input type="text" required="required" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" value="<?php echo $newKeywords; ?>" name="QRnewKeywords" /></span> </div> 
  <div class="input-group mb-3"><div class="input-group-prepend"><span class="input-group-text" id="QRinputGroup-sizing-default">
   Hodnota DMS:</span></div>
      <select  name="QRvalue" id="QRvalue" title="Vyberte z možností" class="custom-select">
      <option value="30" >30,- Kč</option>
      <option value="60" >60,- Kč</option>
      <option value="90" >90,- Kč</option>
     </select>
   </div>
   <div class="input-group mb-3"><div class="input-group-prepend"><span class="input-group-text" id="QRinputGroup-sizing-default2">
   Typ DMS:</span></div>
      <select  name="QRtype" id="QRtype" title="Vyberte z možností" class="custom-select">
      <option value="onetime" >Jednorázová</option>
      <option value="year" >Roční</option>
     </select>
   </div>
   <div class="input-group mb-3"><div class="input-group-prepend"><span class="input-group-text" id="QRinputGroup-sizing-default3">
   Velikost obrázku:</span></div>
      <select  name="QRsize" id="QRQRsizetype" title="Vyberte z možností" class="custom-select">
      <option value="220" >Malý pro web (220px)</option>
      <option value="400" >Střední (400px)</option>
      <option value="540" >Velký (540px)</option>
     </select>
   </div>
   <p>Pečlivě si zkontrolujte heslo (můžete ho případně upravit) a vyberte cenu a typ DMS a velikost generovaného obrázku. Ten si pak můžete běžným způsobem uložit. (Klik pravým tlačítkem a uložit - Apple nevím).
   <div class='sender'>       
   <input type="submit" class="btn btn-primary " value="Generovat QR kód" name="sendQR"> 
   </div>
   <?php
    if($_POST['sendQR']) {
      if($_POST['QRtype'] == 'year') {  $trv = "TRV%20";  }
   ?>

  <img src='https://chart.googleapis.com/chart?cht=qr&chl=SMSTO%3A87777%3ADMS%20<?php echo $trv; ?><?php echo $_POST['QRnewKeywords']; ?>%20<?php echo $_POST['QRvalue']; ?>&chs=<?php echo $_POST['QRsize']; ?>x<?php echo $_POST['QRsize']; ?>&choe=UTF-8&chld=L|' title='DMS <?php echo $_POST['QRnewKeywords']; ?> <?php echo $_POST['QRvalue']; ?>'/>
   

  <?php
    }
    ?>
  </div>
  </fieldset>
  </form>