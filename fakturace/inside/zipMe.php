<h1>Download faktur</h1>

<?php 
   if(!$_POST[odkdy]) {
       $date = date("Y-m-d", (time() - 2592000)) ;
       $time = time() - 2592000 ;
       $showdate = date("d. m. Y", (time() - 2592000)) ;
   }
   else{
       $date =  $_POST[odkdy];
       $protime = explode( '-', $date ) ;
       $time =  mktime(0, 0, 0, $protime[1], $protime[2], $protime[0]); 
       $showdate = date("d. m. Y", $time ) ;
   }
    ?>

<div class="alert alert-warning" role="alert">
Tady si můžete stáhnout "zazipované" vygenerované faktury jednotlivých typů.
 <strong>Aktuálně generujeme faktury od <?php echo $showdate; ?></strong>
 <br/>To můžete změnit nastavením data níže. <BR/>Faktury se vygenerují po kliknutí na button "Načíst"
</div>

<style>
 .xx {
     display: inline-block;
     margin-right: 0.5em;
     color: #17a2b8;
     font-size: 20px;
 }
 a.zip{
    clear: left;
    display: block;
    border-bottom: 1px dotted #c0c0c0;
    padding-bottom: 0.5em;
    margin-bottom: 0.5em;

 }
 .non{
     display: block;
     clear: left;
     border-bottom: 1px dotted #c0c0c0;
     padding-bottom: 0.5em;
    margin-bottom: 0.5em;
 }
</style>

<form action="/fakturace/index.php?id=zipMe" method="post" name='date'> 
<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text" id="inputGroup-sizing-default">Od kdy</span>
    </div>


    <input type="date"   class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" value="<?php echo $date; ?>" name="odkdy"  >
    <input type="submit" class="btn  btn-primary " value="NAČÍST" name="send"> 
  </div>

  <div class="sender">       
  
 
  </div>
  </form>


<?php

 
if(isset($_POST['send'])) {
 /*
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL); 
 
*/
 $zip = new ZipArchive;
 $zip->open('../FA_ZIPS/FA_DMS-danovy-doklad-RP.zip', ZipArchive::CREATE);
 foreach (glob("../FA_DMS-danovy-doklad-RP/*") as $file) {    
    
     if(filemtime ($file) > $time) {
        $fa1++;
        $zip->addFile($file);
     }
 }
 $zip->close();

 $zip = new ZipArchive;
 $zip->open('../FA_ZIPS/FA_DMS-zaloha-rocni-poplatek.zip', ZipArchive::CREATE);
 foreach (glob("../FA_DMS-zaloha-rocni-poplatek/*") as $file) {
    if(filemtime ($file) > $time) {
        $fa2++;
        $zip->addFile($file);
     }
 }
 $zip->close();

 $zip = new ZipArchive;
 $zip->open('../FA_ZIPS/FA_DMS-zaloha-RP.zip', ZipArchive::CREATE);
 foreach (glob("../FA_DMS-zaloha-RP/*") as $file) {
    if(filemtime ($file) > $time) {
        $fa3++;
        $zip->addFile($file);
     }
 }
 $zip->close();
 

 $zip = new ZipArchive;
 $zip->open('../FA_ZIPS/FA_DMS-danovy-doklad-rocni-poplatek-dalsi-heslo.zip', ZipArchive::CREATE);
 foreach (glob("../FA_DMS-danovy-doklad-rocni-poplatek-dalsi-heslo/*") as $file) {
    if(filemtime ($file) > $time) {
        $fa4++;
        $zip->addFile($file);
     }
 }
 $zip->close();


 $zip = new ZipArchive;
 $zip->open('../FA_ZIPS/FA_DMS-danovy-doklad-rocni-poplatek.zip', ZipArchive::CREATE);
 foreach (glob("../FA_DMS-danovy-doklad-rocni-poplatek/*") as $file) {
    if(filemtime ($file) > $time) {
        $fa5++;
        $zip->addFile($file);
     }
 }
 $zip->close();
 
 if( $fa3 > 0 )  { echo "<a class='zip'  href='/FA_ZIPS/FA_DMS-zaloha-RP.zip' ><span class='xx far fa-file-archive'></span>".$fa3."x  faktura typu <b>FA_DMS-zaloha-RP</b> </a>"; } else { echo "<span  class='non'>Žádná faktura typu <b>FA_DMS-zaloha-RP</b> </span>"; }
   if( $fa1 > 0 )  { echo "<a class='zip' href='/FA_ZIPS/FA_DMS-danovy-doklad-RP.zip' ><span class='xx far fa-file-archive'></span>".$fa1."x  faktura typu <b>FA_DMS-danovy-doklad-RP</b> </a>"; } else { echo "<span  class='non'>Žádná faktura typu <b>FA_DMS-danovy-doklad-RP</b> </span>"; }
   if( $fa2 > 0 )  { echo "<a class='zip'  href='/FA_ZIPS/FA_DMS-zaloha-rocni-poplatek.zip' ><span class='xx far fa-file-archive'></span>".$fa2."x  faktura typu <b>FA_DMS-zaloha-rocni-poplatek</b> </a>"; } else { echo "<span  class='non'>Žádná faktura typu <b>FA_DMS-zaloha-rocni-poplatek</b> </span>"; }
   if( $fa5 > 0 )  { echo "<a class='zip'  href='/FA_ZIPS/FA_DMS-danovy-doklad-rocni-poplatek.zip' ><span class='xx far fa-file-archive'></span>".$fa5."x  faktura typu <b>FA_DMS-danovy-doklad-rocni-poplatek</b> </a>"; } else { echo "<span class='non'>Žádná faktura typu <b>FA_DMS-danovy-doklad-rocni-poplatek</b> </span>"; }
   if( $fa4 > 0 )  { echo "<a class='zip'  href='/FA_ZIPS/FA_DMS-danovy-doklad-rocni-poplatek-dalsi-heslo.zip' ><span class='xx far fa-file-archive'></span>".$fa4."x  faktura typu <b>FA_DMS-danovy-doklad-rocni-poplatek-dalsi-heslo</b> </a>"; } else { echo "<span  class='non'>Žádná faktura typu <b>FA_DMS-danovy-doklad-rocni-poplatek-dalsi-heslo</b> </span>"; }


}

   ?>
 
 