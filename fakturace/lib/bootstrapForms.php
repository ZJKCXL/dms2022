<?php
 
// bootInput - normalni input
// bootSelect - normalni select
// bootInputAppend - input s checkboxem na konci
// bootSelectAppendLink - select s linkem na konci
// bootCheckBox - normalni checkbox modrej

function bootInput($prepend,$required,$type,$value,$name,$js){
	$boot = "<div class='input-group mb-3'>";
	if(strlen($prepend)  > 0 ) { 
    	$boot .= "<div class='input-group-prepend'>";
		$boot .= "<span class='input-group-text' id='inputGroup-sizing-default'>";
		$boot .= $prepend;
		$boot .= "</span>";
		$boot .= "</div>";
		}
	if($type == 'file'){
			$boot .= "<label class='custom-file-label' for='".$name."'>Vybrat</label>";
		}		
	$boot .= "<input ";
	if(strlen($required)  > 0 ) { 
		$boot .= $required;
		}
	if(strlen($js)  > 0 ) { 
		$boot .= " ".$js." ";
		}		
	$boot .= " name = '".$name."'";
	$boot .= " id = '".$name."'";
	$boot .= " type = '".$type."'";
	$boot .= " value = '".$value."'";
	$boot .= "class='form-control' aria-label='Default' aria-describedby='inputGroup-sizing-default' />";
	$boot .= "</div>";
	return $boot;
}


function bootSelect($prepend,$required,$value,$selecty,$name,$js){

	$boot = "<div class='input-group mb-3'>";
	if(strlen($prepend)  > 0 ) { 
    	$boot .= "<div class='input-group-prepend'>";
		$boot .= "<span class='input-group-text' id='inputGroup-sizing-default'>";
		$boot .= $prepend;
		$boot .= "</span>";
		$boot .= "</div>";
		}

	$boot .= "<div class='form-control'>";
	$boot .= "<select ";
	$boot .= $required;
	$boot .= " name='".$name."' >";
	$boot .= "<option value=''>Není vybráno</option>";
    if ($selecty && @mysqli_num_rows($selecty)>0)
    {
        while ($srow =  mysqli_fetch_array($selecty))
        {
		 $boot .= "<option value=".$srow ["downId"]." >";
		 $boot .=  $srow['downNazev'] ;
		 $boot .= "</option>";
		  
		}
	}
	$boot .= "</select>";
	$boot .= "</div>";
	$boot .= "</div>";
	
	return $boot;
}

function bootSelectAppendLink($prepend,$required,$value,$selecty,$name,$js,$append,$appendLink){

	$boot = "<div class='input-group mb-3'>";
	if(strlen($prepend)  > 0 ) { 
    	$boot .= "<div class='input-group-prepend'>";
		$boot .= "<span class='input-group-text' id='inputGroup-sizing-default'>";
		$boot .= $prepend;
		$boot .= "</span>";
		$boot .= "</div>";
		}

	$boot .= "<div class='form-control'>";
	$boot .= "<select ";
	$boot .= $required;
	$boot .= " name='".$name."' >";
	$boot .= "<option value=''>Není vybráno</option>";
    if ($selecty && @mysqli_num_rows($selecty)>0)
    {
        while ($srow =  mysqli_fetch_array($selecty))
        {
		 $boot .= "<option value='".$srow ["downId"]."'"; 
		 if(mysqli_num_rows($selecty) == 1)  { $boot .= " selected "; }
		 if($value == $srow['downId']) { $boot .= " selected "; }
		 $boot .= ">";
		 $boot .=  $srow['downNazev'] ;		 
		 $boot .= "</option>";
		  
		}
	}
	$boot .= "</select>";
	$boot .= "</div>";
	$boot .= "<div class='input-group-append'>";
	$boot .= "<span class='input-group-text'>";
	if(strlen($appendLink)  > 0 ) {
		$boot .= "<a href='".$appendLink."' >";	 
	}
    $boot .= $append." &nbsp; ";
	if(strlen($appendLink)  > 0 ) {
		$boot .= "</a>";	 
	}
	$boot .= "</span></div>";
	$boot .= "</div>";
//	$boot .= "</div>";
 
      return $boot;
}

function bootInputAppend($prepend,$required,$type,$value,$name,$js,$append,$appendCheckboxName,$appendChecked){
	$boot = "<div class='input-group mb-3'>";
	if(strlen($prepend)  > 0 ) { 
    	$boot .= "<div class='input-group-prepend'>";
		$boot .= "<span class='input-group-text' id='inputGroup-sizing-default'>";
		$boot .= $prepend;
		$boot .= "</span>";
		$boot .= "</div>";
		}
	$boot .= "<input ";
	if(strlen($required)  > 0 ) { 
		$boot .= $required;
		}
	if(strlen($js)  > 0 ) { 
			$boot .= " ".$js." ";
	}		
	$boot .= " name = '".$name."'";
	$boot .= " id = '".$name."'";
	$boot .= " type = '".$type."'";
	$boot .= " value = '".$value."'";
	$boot .= "class='form-control' aria-label='Default' aria-describedby='inputGroup-sizing-default' />";

	$boot .= "<div class='input-group-append'>";
	$boot .= "<span class='input-group-text'>";

	$boot .= $append." &nbsp; ";
	
	if(strlen($appendCheckboxName)  > 0 ) {
		$boot .= "<input type='checkbox' ";
		$boot .= " name='".$appendCheckboxName."' ";   
		$boot .=  $appendChecked;
		$boot .= " > ";
	}
	$boot .= "</span></div>";
 
	$boot .= "</div>";
	return $boot;
}
 
function bootCheckBox($label,$value,$name,$newsid) {
	$boot .= '<div class="custom-control custom-checkbox my-1 mr-sm-2">';
	$boot .= '<input value="'.$value.'" name="'.$name.'" type="checkbox"';

   if($newsid){
       
      if ($value == 1){ $boot .= ' checked ';  }
   }
   else{
			$boot .= ' checked ';
   }
	$boot .= ' class="custom-control-input" id="'.$name.'" />';
	$boot .= '<label class="custom-control-label" for="'.$name.'" >';
	$boot .=  $label;  
	$boot .= "</label>"; 
	$boot .= "</div>";
	return $boot;
 
}








function showInTable($inName,$inValue,$maxporadi,$minporadi,$menucolor,$newsid,$page_name,$menu,$mother,$filter) {
 
   if($inName == 'public' ) {  $inName = 'pagesPublic'; }
   if($inName == 'trener_name' ) {  $inName = 'pagesNadpis'; }

  if($inName == 'pagesPublic') {
	if($inValue == 0){
	  $td  =  '<a href="index.php?id=';
	  $td .=  $page_name;
	  $td .= '&newsid='.$newsid;
	  $td .= '&high=1';
	  $td .=  $filter.'"';
	  $td .= 'class="far fa-star" title="Nepublikováno na webu. KLIK = Publikovat"></a>';	 
	  return $td;
	  }
	  else{
		$td  =  '<a href="index.php?id=';
		$td .=  $page_name;
		$td .= '&newsid='.$newsid;
		$td .= '&high=0';
		$td .=  $filter.'"';
		$td .= 'class="fas fa-star" title="Publikováno na webu. KLIK = Odpublikovat"></a>';	 
		return $td;		  
	  }
	  }

	  elseif($inName == 'formTime'){
			$new_datetime = date("d.m.Y", strtotime($inValue));
		 
		return $new_datetime;
	  }

	  elseif($inName == 'pagesOrder') {
		if($inValue > $minporadi) {
		$td1  =  '<a href="index.php?id=';
		$td1 .=  $page_name;
		$td1 .= '&newsid='.$newsid;
		$td1 .= '&move=up';
		$td1 .=  $filter.'"';
		$td1 .= 'class="far fa-arrow-alt-circle-up" title="Posunout v pořadí nahoru"></a>';	 
		}
		else{
			$td1  =  '<em class="nic"></em>';
		}

		if($inValue < $maxporadi) {
			$td2  =  '<a href="index.php?id=';
			$td2 .=  $page_name;
			$td2 .= '&newsid='.$newsid;
			$td2 .= '&move=down';
			$td2 .=  $filter.'"';
			$td2 .= 'class="far fa-arrow-alt-circle-down" title="Posunout v pořadí dolů"></a>';	 
			}
			else{
				$td2  =  '<em class="nic"></em>';
			}
		
        $td = $td1.$td2;		  
        return $td;
		}

		elseif($inName == 'pagesNadpis') {
			    $td  ='';
				$td  .=  '<a style="background: '.$menucolor.'" class="aintable" href="index.php?id='.$page_name.'new&amp;newsid='.$newsid;
				$td .=  $filter.'"';
				$td .= '"> ';
				if($mother > 0) {
					$td  .= '  <i class="far fa-arrow-alt-circle-right"></i>&nbsp; ';
				}
				$td .= substr($inValue,0,100);
				$td .= '</a>' ;	
			return $td;
			}
			elseif($inName == 'pagesTime') {
				$td  =   date("d.m.Y", $inValue) ;	
			return $td;
			}
			elseif($inName == 'aktuName') { 
				$td  =   $inValue ;	
			return $td;
			}		

		elseif($inName == 'pagesHomepage') {
			if($inValue > 0) {
				$td  =  '<i class="fas fa-home" data-toggle="tooltip" data-placement="top"  title="Stránka  je nastavena jako Homepage"></i>';	
			}
			return $td;
			}

		elseif($inName == 'menuName') {
				if(strlen($inValue) > 0) {
					$td  =  '<span class="around" style="background :'.$menucolor.'">';
					$td .= '<a class="far fa-list-alt"  title="Je součástí hlavního menu - KLIK: Přidat stránku do sekce '.$menuname.'"  href="index.php?id='.$page_name.'new&sendgrp='.$menu.'">';
					$td .= '</a></span>&nbsp;'.$inValue;	
				}
				return $td;
				}

		elseif($inName == 'pagesLocked') {
				if($inValue > 0) {
					$td  =  '<i class="fas fa-lock text-danger" title="Stránka podléhá přihlášení" ></i>';	
				}
				else{
					$td = '<i class="fas fa-unlock " title="Stránka veřejně dostupná" ></i>';
				}
				return $td;
				}			
			 
		elseif($inName == 'delete') {
		    $td  =  '<a onClick="return confirm(\'Skutečně chcete položku vymazat z databáze?\')" href="index.php?id=';
			$td .=  $page_name;
			$td .= '&delete='.$newsid;
			$td .=  $filter.'"';
			$td .= 'class="far fa-times-circle text-danger" title="Smazat"></a>';	 
			return $td;
	 	 }

		else {
   			return $inValue;
  		}
} 

/*

function bootInputJS($prepend,$required,$type,$id,$value,$name,$javascript){
	$boot = "<div class='input-group mb-3'>";
	if(strlen($prepend)  > 0 ) { 
    	$boot .= "<div class='input-group-prepend'>";
		$boot .= "<span class='input-group-text' id='inputGroup-sizing-default'>";
		$boot .= $prepend;
		$boot .= "</span>";
		$boot .= "</div>";
		}
	$boot .= "<input ";

	if(strlen($required)  > 0 ) { 
		$boot .= $required;
		}
	$boot .= " name = '".$name."'";
	$boot .= " id = '".$id."'";
	$boot .= " type = '".$type."'";
	$boot .= " value = '".$value."'";

	if(strlen($javascript)  > 0 ) { 
			$boot .=  $javascript;
			}
	$boot .= "class='form-control' aria-label='Default' aria-describedby='inputGroup-sizing-default' />";
	$boot .= "</div>";
}

function bootInputAppend($prepend,$required,$type,$id,$value,$name,$javascript,$append ){
	$boot = "<div class='input-group mb-3'>";
	if(strlen($prepend)  > 0 ) { 
    	$boot .= "<div class='input-group-prepend'>";
		$boot .= "<span class='input-group-text' id='inputGroup-sizing-default'>";
		$boot .= $prepend;
		$boot .= "</span>";
		$boot .= "</div>";
		}
	$boot .= "<input ";

	if(strlen($required)  > 0 ) { 
		$boot .= $required;
		}
	$boot .= " name = '".$name."'";
	$boot .= " id = '".$id."'";
	$boot .= " type = '".$type."'";
	$boot .= " value = '".$value."'";

	if(strlen($javascript)  > 0 ) { 
			$boot .=  $javascript;
			}

	$boot .= "class='form-control' aria-label='Default' aria-describedby='inputGroup-sizing-default' />";
   


	$boot .= "</div>";
}





*/

?>