<?php
 



function feistel_encrypt($in_text)
{
	$j=0;
	$result = "";
	for ($i=0;$i<strlen($in_text);$i++)
	{
		$in_byte = ord(substr($in_text,$i,1));
		$elem = pow(-1,$i%2);
		$data = $in_byte + $elem;
		$result .= chr($data);
	}
	$result = base64_encode($result);
	return $result;
	exit;
}

function feistel_decrypt($in_text)
{
	$j=0;
	$result = "";
	$in_text = base64_decode($in_text);
	for ($i=0;$i<strlen($in_text);$i++)
	{
		$in_byte = ord(substr($in_text,$i,1));
		$elem = pow(-1,$i%2);
		$data = $in_byte - $elem;
		$result .= chr($data);
	}
	return $result;
	exit;
}

function user_logged_in()
{        

	global $FAcookie;
	
	
    $in_cookie =  $_COOKIE[$FAcookie['name']];
	$result = false;
	if (strlen($in_cookie)>0)
	{
		$decoded_cookie = feistel_decrypt($in_cookie);
		$exploded_cookie = explode(";",$decoded_cookie);
		$result = ($exploded_cookie[0] == md5($_SERVER['HTTP_USER_AGENT']));

	}
	return $result;
}



function user_login($user_name, $user_pass)
{
 
	$result = false;
	global $conn;
	global $FAcookie;

 
   
	$link =  MySQLi_Connect( $conn["host"], $conn["user"], $conn["pass"] );    // napojení na server
	mysqli_set_charset (  $link , 'utf8' );
	$db=$conn["db"];
	mysqli_select_db($link,$db)  ;

	  $query = "Select tbluser_admin.* from tbluser_admin where ULogin = '".$user_name."' and UPass =PASSWORD('".$user_pass."') and UCanLogin = 1 and Valid = 1 and Deleted = 0";

	$res = $link->query($query);
	if ($res &&  mysqli_num_rows($res)>0)
	{
		$resarr = mysqli_fetch_array($res);
		$agent =$_SERVER['HTTP_USER_AGENT'];
		$agent = md5($agent);


		$cookie_data = $agent.";".$resarr["ID"].";".$resarr["UType"].";".$resarr["UFirm"];
		$cookie_data = feistel_encrypt($cookie_data);
		if (strlen($cookie_data)>0)
		{
			$result = setcookie($FAcookie["name"],$cookie_data,null,$FAcookie["path"],$FAcookie["domain"]);
		   
		}
	}
	return $result;
}

function user_logout()
{
	$result = false;
	global $FAcookie;
	$in_cookie = $_COOKIE[$FAcookie["name"]];
	if (strlen($in_cookie)>0)
	{
 	$result = @setcookie($FAcookie["name"],false,time() - 3600,$FAcookie["path"],$FAcookie["domain"]);
	}

	return $result;
}

function get_user_id()
{
	global $FAcookie;


  if (isset($_COOKIE[$FAcookie["name"]]) && $_COOKIE[$FAcookie["name"]] != "")
	{
	$in_cookie =  @$_COOKIE[$FAcookie['name']];

	if (isset($in_cookie) && $in_cookie != "")
	{
		$decoded_cookie = feistel_decrypt($in_cookie);
		$exploded_cookie = explode(";",$decoded_cookie);
		return $exploded_cookie[1];
	}
	else
	{
		return false;
	}
  }
  	else
	{
		return false;
	}
  
}

function get_user_name($in_user_id)
{
	global $conn;

	$result = false;

	return $result;
}

function get_user_level()
{
	global $FAcookie;

	$in_cookie = @$_COOKIE[$FAcookie["name"]];

	if (isset($in_cookie) && $in_cookie != "")
	{
		$decoded_cookie = feistel_decrypt($in_cookie);
		$exploded_cookie = explode(";",$decoded_cookie);
		return $exploded_cookie[2];
	}
	else
	{
		return false;
	}
}

function get_user_level_name()
{
	global $conn;

	$result = false;

	return $result;
}

function get_user_firm()
{
	global $FAcookie;

	$in_cookie = $_COOKIE[$FAcookie["name"]];

	if (isset($in_cookie) && $in_cookie != "")
	{
		$decoded_cookie = feistel_decrypt($in_cookie);
		$exploded_cookie = explode(";",$decoded_cookie);
		return $exploded_cookie[3];
	}
	else
	{
		return false;
	}
}

function get_user_name_by_id($in_id, $reversed = false, $separator = ", ")
{
	global $conn;

	$link =  MySQLi_Connect( $conn["host"], $conn["user"], $conn["pass"] );    // napojení na server
	mysqli_set_charset (  $link , 'utf8' );
	$db=$conn["db"];
	mysqli_select_db($link,$db)  ;
	$query = "Select USurname, UName from tbluser_admin where ID=".$in_id;
	$res = @$link->query($query);

	if ($res && @mysqli_num_rows($res)>0)
	{
		$resarr = @mysqli_fetch_array($res);
		if ($reversed)
		{
			return $resarr["USurname"].$separator.$resarr["UName"];
		}
		else
		{
			return $resarr["UName"]." ".$resarr["USurname"];
		}
	}
	else
	{
		return false;
	}

}


?>
