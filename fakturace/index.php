<?php
require_once("./lib/globals.php");
include "../sql/db.php";
 error_reporting(E_ERROR);
 ini_set('display_errors', 1);
 header("content-type: text/html; charset=utf-8");
 $id = $help = $newsid = $class = $type = $controlBoxEnd = $controlBoxStart = $menu_table = $send = $menu = $nas_sou = $supercount = false;
 
 @extract($_REQUEST);
 include_once("./lib/lib10.php"); 
 include_once("../lib/libFA.php"); 
 include_once("./lib/settings.php");
 include_once("./lib/priviledge_constants.php"); 
 include_once("./lib/user_functions.php"); 
 include_once("./lib/security_functions.php");
 include_once("./lib/bootstrapForms.php");
 include_once("./lib/pager.php");
 session_start();
 $_SESSION["file_info"] = array();
 if (!isset($_COOKIE["is_hidden"])) { $bodystyle = $logostyle = $sidebarstyle = ''; }else{ $bodystyle = 'class="full"'; $logostyle = 'class="fix"'; $sidebarstyle = 'class="active"'; }
 if (!isset($_COOKIE["is_3cols"])) { $formstyle  = ''; }else{ $formstyle = 'class="cols3"';   }
?>
<!DOCTYPE html>
<html lang="cs">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
<meta name="description" content="HandMade"/>
<meta name="keywords" lang="cs" content=""/>
<meta name="resource-type" content="document"/>
<meta name="copyright" content="HandMade"/>
<meta name="author" content="HandMade"/>
<meta name="robots" content="no,no-FOLLOW"/>
<meta http-equiv="Content-language" content="cs"/>
<meta http-equiv="Cache-control" content="no-cache"/>
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Expires" content="-1"/>
<title>Fakturace DMS | HandMade</title>
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo $tempdir; ?>/fakturace/favicons/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo $tempdir; ?>/fakturace/favicons/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo $tempdir; ?>/fakturace/favicons/favicon-16x16.png">
<link rel="manifest" href="<?php echo $tempdir; ?>/fakturace/favicons/site.webmanifest">
<link rel="mask-icon" href="<?php echo $tempdir; ?>/fakturace/favicons/safari-pinned-tab.svg" color="#005e68">
<meta name="msapplication-TileColor" content="#005e68">
<meta name="theme-color" content="#005e68">
<script type="text/javascript" src="https://code.jquery.com/jquery-latest.min.js"></script>


<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

<link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet"> 
<script type="text/javascript" src="./js/scripts.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<?php
if (user_logged_in())
{
	?>
	<link rel="stylesheet" href="./css/main.css" />
	<?php
}
elseif($_REQUEST['id'] == 'forget'){
	?>
	<link rel="stylesheet" href="./css/forget.css" />
	<?php	 
}
else
{
	?>
	<link rel="stylesheet" href="./css/login.css" />
	<?php
}
if(isset($id)){
if(strpos($id, 'newx')){    
?>
<!-- tinyMCE -->
<script type="text/javascript" src="./modules/jscripts/tiny_mce/tinymce.min.js"></script>
<script type="text/javascript" src="./modules/jscripts/tiny_mce/tiny_mce_config.js?ver=<?php echo time(); ?>"></script>
<!-- /tinyMCE -->
<?php 
}
} 
?>
<script>

(function($){

$("html").removeClass("v2"); 


$("#header").ready(function(){ $("#progress-bar").stop().animate({ width: "25%" },1500) });
$("#footer").ready(function(){ $("#progress-bar").stop().animate({ width: "75%" },1500) });


$(window).load(function(){

    $("#progress-bar").stop().animate({ width: "100%" },600,function(){
        $("#loading").fadeOut("fast",function(){ $(this).remove(); });
    });

});
})(jQuery);
</script>
</head>
<body <?php echo $bodystyle; ?>>
<div id='loading'><div id='progress-bar'></div><div id='loader'>Loading...</div></div>
<div id='logo' <?php echo $logostyle; ?>><a href="<?php echo $tempdir; ?>/fakturace/">HANDMADE</a></div>
<div id="login">
<?php
if (user_logged_in())
{
?>
<i class="fas fa-user-circle"> <span>  <?php  echo get_user_name_by_id(get_user_id(),true); ?></span></i>  <a href="scripts/logout.php">Odhlásit</a>
<?php 
}
?>
</div>
<nav id="sidebar" <?php echo $sidebarstyle; ?>> 
<?php 
  include "menu.php";
?>
</nav>
<section id='<?php echo $_REQUEST["id"]; ?>' class='mysection'>
<?php
if (user_logged_in())
{
	$id = $_REQUEST["id"];
}
elseif($_REQUEST['id'] == 'forget'){
	$id = $_REQUEST["id"];
}
else
{
 $id = "login";
}
$filename = "./inside/".$id  . ".php";
if (@file_exists($filename))
{
	//$log_action = false;
	include($filename);
	//if ($log_action !== false && $_REQUEST["newsid"] > 0)
	//{
	//	@log_action($id,$log_action,$_REQUEST["newsid"],print_r($_REQUEST,true));
	//}
}
else
{
	include("./inside/dashboard.php");
}
?>
</section>
<footer>
<button type="button" id="sidebarCollapse" class="btn btn-info"><i class="fas fa-align-left"></i><span> Menu ON/OFF</span></button>
<?php 
if($go3 == 1) {
	?>
 &nbsp;<button type="button" id="cols3" class="btn btn-info"><i class="far fa-file"></i><span>  3COLS</span></button>
<?php    
}
?>
<?php
if(count($_POST)) {
	$newREQ = '';
	foreach ( $_REQUEST as $param => $value ) {
	$newREQ .=  $param . ": " .  $value . "\n";
	}
	$hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);
	$result  = mysql_query("INSERT INTO  _spyLOGadmin (    `jason` ,  `time` ,  `timestamp`,  `userid`, `name`,  `infotxt` ) VALUES (  '".$newREQ."',  '".time()."', NOW( ), '".get_user_id()."',  '".$hostname."', '" .$info_text.$infotext. "' )");
//	$link->query($dataqueryLOG);
	}
?>
<a href='http://www.honeypot.cz' id='honeypot' target="_blank" ><span>Created by</span>HONEYPOT</a>
</footer>
<a href="#top" class="fas fa-angle-up cd-top" data-toggle='tooltip' data-placement='top' title="Zpátky nahoru"> </a>
</body>
</html>                                                 