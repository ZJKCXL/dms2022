<?php
/*
use SimpleExcel\SimpleExcel;
require_once('../lib/phpExcel/SimpleExcel/SimpleExcel.php'); // load the main class file (if you're not using autoloader)
$excel = new SimpleExcel('xml');                    // instantiate new object (will automatically construct the parser & writer type as XML)
$excel->parser->loadFile('example.xml');            // load an XML file from server to be parsed
$foo = $excel->parser->getField();                  // get complete array of the table
$bar = $excel->parser->getRow(3);                   // get specific array from the specified row (3rd row)
$baz = $excel->parser->getColumn(4);                // get specific array from the specified column (4th row)
$qux = $excel->parser->getCell(2,1);                // get specific data from the specified cell (2nd row in 1st column)
echo '<pre>';
print_r($foo);                                      // echo the array
echo '</pre>';
*/

/**
 * Warning: note that there must be no data sent to browser
 * (be it an echo, HTML element, or even a whitespace)
 * before the writer->saveFile() method get called
 */
include_once("../lib/globals.php");
include Globals::$GLOBAL_WEB_PATH . Globals::$GLOBAL_SQL_FILE;
use SimpleExcel\SimpleExcel;
require_once('../lib/phpExcel/SimpleExcel/SimpleExcel.php'); // load the main class file (if you're not using autoloader)
$excel = new SimpleExcel('csv');                    // instantiate new object (will automatically construct the parser & writer type as CSV)
 
$excel->writer->setData(
    array
    (
      array('Name', 'IC', 'E-mail', 'Kontakt', 'Telefon', 'Mobil', 'Statut.', 'Telefon'  , 'E-mail') 
    )    
  ); 
 
$query = "SELECT * FROM organization_online  Where ID IN (".$_GET['active'].") Order by name";
$result=mysql_query($query);
 
 if(($result)&&(mysql_numrows($result)>0)){
 while ($row = @mysql_fetch_array($result)){  
 
    $name = iconv( "UTF-8", "Windows-1250", $row['name'] );
    $statutar = iconv( "UTF-8", "Windows-1250", $row['statutar'] );
    $newContactName =  iconv( "UTF-8", "Windows-1250", $row['newContactName'] );
    $ad = array($name,  $row['companyId'],  $row['email'], $newContactName,  $row['newContactPhone'], $row['newContactMobile'], $statutar, $row['statutartelefon'], $row['statutaremail']   );
    $excel->writer->addRow( $ad );
    
 }
}
 $excel->writer->setDelimiter(";");                  // (optional) if delimiter not set, by default comma (",") will be used instead
 $excel->writer->saveFile('Aktivni');                // save the file with specified name (example.csv) 
                                                // and specified target (default to browser)
?> 