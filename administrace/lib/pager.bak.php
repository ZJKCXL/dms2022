aghoj
<?php

class Pager
{
	public $prev_title = "Předchozí";
	public $next_title = "Další";

	public $results_per_page = 10;
	public $adjacent = 7;
	public $length = 10;

	public $current = 1;
	public $offset = 0;
	public $pages = 0;

	private $start = 0;
	private $end = 0;
	private $range = array();

	public function getPages ($items, $current, $url)
	{
		$this->pages = ceil($items/$this->results_per_page);
		$this->current = $current;

		if ($this->pages == 1)
		{
			return "";
		}

		$this->current = ($this->current < 1) ? 1 : $this->current;

		$result = "";

		if ($this->pages <= $this->length) //pokud je mene stranek nez defaultni delka strankovace
		{
			for ($i = 1; $i <= $this->pages; $i++)
			{
				//vratit vsechny stranky
				$result .= ($i == $this->current) ? "<span class=\"pagerselected\">$i</span>" : "<a href=\"$url&pg=$i\" class=\"pager\">$i</a>";
			}
		}
		else
		{
			//pokud je aktualni stranka > 1 -> vypsat "Previous"
			$result .= ($this->current != 1) ? "<a href=\"$url&pg=".($current-1)."\" class=\"pagerrim\">$this->prev_title</a>" : "";

			$this->start = $this->current - floor($this->adjacent/2);
			$this->end = $this->current + floor($this->adjacent/2);

			//nastavit zacatek
			if ($this->start <= 0)
			{
				$this->end += abs($this->start)+1;
				$this->start = 1;
			}

			//nastavit konec
			if ($this->end > $this->pages)
			{
				$this->start -= $this->end - $this->pages;
				$this->end = $this->pages;
			}

			$this->range = range($this->start, $this->end);

			for ($i = 1; $i <= $this->pages; $i++)
			{
				//"..." na zacatku bloku
				$result .= ($this->range[0] > 2 && $i == $this->range[0]) ? "<span class=\"pager\">...</span>" : "";

				if ($i == 1 || $i == $this->pages || in_array($i, $this->range))
				{
					$result .= ($i == $this->current) ? "<span class=\"pagerselected\">$i</span>" : "<a href=\"$url&pg=$i\" class=\"pager\">$i</a>";
				}

				//"..." na konci bloku
				$result .= ($this->range[$this->adjacent-1] < $this->pages-1 && $i == $this->range[$this->adjacent-1]) ? "<span class=\"pager\">...</span>" : "";
			}

			//pokud je aktualni stranka < posledni -> vypsat "Next"
			$result .= ($this->current != $this->pages && $this->pages >= $this->length) ? "<a href=\"$url&pg=".($current+1)."\" class=\"pagerrim\">$this->next_title</a>" : "";
		}

		return $result;
	}
}


function pagerNewStart($kolik,$from,$rows)
{
	if($from<1){ $from = 1; }
	$from = $from-1;
	$limit = " LIMIT ".$from*$kolik.",".$kolik;
	return $limit;
}
 

?>
