<?php

define("ACTION_UPDATE",1);
define("ACTION_INSERT",2);
define("ACTION_DELETE",3);

function log_action($module,$action,$on,$data = false)
{
	$user_id = get_user_id();
	$user_level = get_user_level($user_id);

	if ($data !== false)
	{
		$data = gzcompress($data,9);
		$data = base64_encode($data);
	}

	global $conn;

	$link=@mysql_connect($conn["host"],$conn["user"],$conn["pass"]);
	if (!$link)
	{
		return false;
		exit;
	}
	$db=mysql_select_db($conn["db"], $link);
	if (!$db)
	{
		return false;
		exit;
	}


	$query = "INSERT INTO tblLog (LWho,LModule,LOn,LAction,LWhen";
	if (strlen($data)>0)
	{
		$query .= ",LData";
	}
	$query .= ") VALUES (";
	$query .= $user_id;
	$query .= ", '".trim(@mysql_escape_string($module))."'";
	$query .= ", ".intval($on);
	$query .= ", ".intval($action);
	$query .= ", NOW()";
	if (strlen($data)>0)
	{
		$query .= ", '".trim(@mysql_escape_string($data))."'";
	}
	$query .= ")";

	$res = @mysql_query($query,$link);
	return $res && @mysql_affected_rows($link);
}