<?php
	/* Note: This thumbnail creation script requires the GD PHP Extension.  
		If GD is not installed correctly PHP does not render this page correctly
		and SWFUpload will get "stuck" never calling uploadSuccess or uploadError
	 */

	// Get the session Id passed from SWFUpload. We have to do this to work-around the Flash Player Cookie Bug
	if (isset($_POST["PHPSESSID"])) {
		session_id($_POST["PHPSESSID"]);
	}

	session_start();
	ini_set("html_errors", "0");

	// Check the upload
	if (!isset($_FILES["Filedata"]) || !is_uploaded_file($_FILES["Filedata"]["tmp_name"]) || $_FILES["Filedata"]["error"] != 0) {
		echo "ERROR:invalid upload";
		exit(0);
	}

	// Get the image and create a thumbnail
	$img = imagecreatefromjpeg($_FILES["Filedata"]["tmp_name"]);
	if (!$img) {
		echo "ERROR:could not create image handle ". $_FILES["Filedata"]["tmp_name"];
		exit(0);
	}

	$image_width = $width = imageSX($img);
	$image_height = $height = imageSY($img);

	if (!$width || !$height) {
		echo "ERROR:Invalid width or height";
		exit(0);
	}

	// Build the box
  if(($image_width > 800)||($image_height >600)){
	$img_ratio = $width / $height;
  
	if ($width > $height) {
		$new_width = 800;
		$new_height = $new_width / $img_ratio;
	} else {
	  $new_height = 600;
		$new_width = $new_height * $img_ratio;
	}

	$new_img = ImageCreateTrueColor($new_width, $new_height);
	$white = imagecolorallocate($new_img, 239, 239, 239);
	if (!@imagefilledrectangle($new_img, 0, 0, $new_width, $new_height, $white)) {	// Fill the image black
		header("HTTP/1.1 500 Internal Server Error");
		echo "Could not fill new image";
		exit(0);
	}
	if (!@imagecopyresampled($new_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height)) {
		header("HTTP/1.0 500 Internal Server Error");
		echo "Could not resize image";
		exit(0);
	}
	if (!isset($_SESSION["file_info"])) {
		$_SESSION["file_info"] = array();
	}
  $file_id = md5($_FILES["Filedata"]["tmp_name"] + rand()*100000);
	// Use a output buffering to load the image into a variable
	ob_start();
	$tocopy = "../galpics/box/".$_FILES["Filedata"]['name'];	
	imagejpeg($new_img,$tocopy);
	imagejpeg($new_img);
	$imagevariable = ob_get_contents();
	ob_end_clean();
 }
  else{
	if (!isset($_SESSION["file_info"])) {
		$_SESSION["file_info"] = array();
	}
  $file_id = md5($_FILES["Filedata"]["tmp_name"] + rand()*100000);
	// Use a output buffering to load the image into a variable
	ob_start();
	$tocopy = "../galpics/box/".$_FILES["Filedata"]['name'];	
	imagejpeg($img,$tocopy);
	imagejpeg($img);
	$imagevariable = ob_get_contents();
	ob_end_clean();
  }
	
	
	// Build the thumbnail
  include './lib/settings.php';
  
	$target_width = $galery_width; 95;
	$target_height = $galery_height;70;
	$target_ratio = $target_width / $target_height;
  $source_ratio = $image_width / $image_height;
  
  if(	$source_ratio > $target_ratio ){
  $new_image_height = $target_height;
  $new_image_width = $image_width / ($image_height / $target_height);
  }
  else{
  $new_image_width = $target_width;
  $new_image_height =  $image_height / ($image_width / $target_width);
  }




			  	$new_image_width = intval($new_image_width);
					$new_image_height = intval($new_image_height);
					
					
	$new_img = ImageCreateTrueColor($target_width, $target_height);
	$white = imagecolorallocate($new_img, 239, 239, 239);
  imagefill($new_img,0,0, $white)	;	 	
	$help = @imagecreatetruecolor($new_image_width,$new_image_height);
	$white2 = imagecolorallocate($help, 239, 239, 239);	
  imagefill($help,0,0, $white2)	;		 
	imagecopyresampled($help,$img,0,0,0,0,$new_image_width,$new_image_height,$image_width,$image_height);	

	if (!@imagecopy($new_img,$help,-($new_image_width-$target_width)/2,-($new_image_height-$target_height)/2,0,0,$image_width,$image_height)){		
    header("HTTP/1.0 500 Internal Server Error");
		echo "Could not resize image";
		exit(0);
	}
	if (!isset($_SESSION["file_info"])) {
		$_SESSION["file_info"] = array();
	}

	// Use a output buffering to load the image into a variable
	ob_start();
	$tocopy = "../galpics/small/".$_FILES["Filedata"]['name'];	
	imagejpeg($new_img,$tocopy);
	imagejpeg($new_img);
	$imagevariable = ob_get_contents();
	ob_end_clean();
	
	
	
	$full_path = "../galpics/";
	move_uploaded_file($_FILES["Filedata"]['tmp_name'],$full_path.$_FILES["Filedata"]['name']); 	
	$_SESSION["file_info"][$file_id] = $imagevariable;

	// Use a output buffering to load the image into a variable
	ob_start();
	imagejpeg($new_img);
	$imagevariable = ob_get_contents();
	ob_end_clean();

	$file_id = md5($_FILES["Filedata"]["tmp_name"] + rand()*100000);
	
	$_SESSION["file_info"][$file_id] = $imagevariable;

	echo "FILEID:" . $file_id;	// Return the file id to the script
	
?>
