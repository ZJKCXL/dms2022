<style>

.google-visualization-table-tr-head:first-of-type, .google-visualization-table-th:first-of-type { color: #000;   }

.google-visualization-table-tr-head, .google-visualization-table-th { background: #fff; color: #000; min-width: 150px;  }
 #table_div2 .google-visualization-table-table tbody tr:last-of-type ,
  #table_div1 .google-visualization-table-table tbody tr:last-of-type ,
   #table_div0 .google-visualization-table-table tbody tr:last-of-type ,
   #table_div3 .google-visualization-table-table tbody tr:last-of-type 
  { background: #c0c0c0!important; color: #fff!important }

 



/*.google-visualization-table-table { min-width: 500px!important; padding: 0; margin: -5px }*/
#table_div { min-width: 550px!important; }
</style>
   <link rel="stylesheet" href="./js/superselect/prism.css">
   <link rel="stylesheet" href="./js/superselect/chosen.css">    

<?php

//schovany colors , colors: ['#863d02', '#ab4e03', '#ca5c04', '#e86904', '#fb780f', '#fb903b', '#fca45e', '#fdbb85', '#fdc89d', '#fee0c8', '#feeee0'  

if($_REQUEST['OfferFrom']){
  List($den, $mesic, $rok) = explode(".",$_REQUEST['OfferFrom']);
  $fromTime=@mktime(0,0,0,$mesic,$den,$rok);  
  $fromTimeLink = $_REQUEST['OfferFrom'];
}
else{ 
  List($den, $mesic, $rok) = explode(".",$_GET['fromTime']);
  $fromTime=@mktime(0,0,0,$mesic,$den,$rok);
 $fromTimeLink = $_GET['fromTime'];  
}
if($_REQUEST['OfferTo']){
   List($den, $mesic, $rok) = explode(".",$_REQUEST['OfferTo']);
   $toTime=@mktime(23,59,0,$mesic,$den,$rok);   
 $toTimeLink = $_REQUEST['OfferTo'];   
}
else{
   List($den, $mesic, $rok) = explode(".",$_GET['toTime']);
   $toTime=@mktime(23,59,0,$mesic,$den,$rok); 	
 $toTimeLink = $_GET['toTime'];   
}

$table_name = "tblquestion";
$page_name = "stats";
$info_text = "";

if($fromTime){
  $where3 = " And time >= ".$fromTime ;
}
if($toTime){
  $where4 .= " And time <= ".$toTime ;
}

//default vypis
 

?>
<style>
strong{
 display: block;
 border-bottom: 4px solid #efefef;
 clear: both;
 
}
td,th{
 line-height: 1em;
}
#chart_div0, #chart_div1, #chart_div2, #chart_div3, #chart_div4, #chart_div5, #chart_div6, #chart_div7 {
 floaT: left;
 margin-right: 2em;
} 
#table_div0, #table_div1, #table_div2, #table_div3, #table_div4, #table_div5, #table_div6, #table_div7 {
 floaT: right;
 margin-top: 2em;
 border: 4px solid #efefef;
} 
</style>
<h1>statistika DMS</h1>
<form action="index.php?id=<?php echo $page_name; ?>" method="post" name="noname"  >

<select id="month" name="month[]"  multiple  class="chosen-select" data-placeholder="Vyber měsíce" style="width: 400px" >
  <option value="1" <?php if ($_REQUEST['month'] && (in_array("1", $_REQUEST['month'])) ) { echo ' selected '; } ?> >Leden</option>
  <option value="2"  <?php if ($_REQUEST['month'] && (in_array("2", $_REQUEST['month'])) ) { echo ' selected '; } ?> >Únor</option>
  <option value="3"  <?php if ($_REQUEST['month'] && (in_array("3", $_REQUEST['month'])) ) { echo ' selected '; } ?> >Březen</option>
  <option value="4"  <?php if ($_REQUEST['month'] && (in_array("4", $_REQUEST['month'])) ) { echo ' selected '; } ?> >Duben</option>
  <option value="5" <?php if ($_REQUEST['month'] && (in_array("5", $_REQUEST['month'])) ) { echo ' selected '; } ?> >Květen</option>
  <option value="6" <?php if ($_REQUEST['month'] && (in_array("6", $_REQUEST['month'])) ) { echo ' selected '; } ?> >Červen</option>
  <option value="7" <?php if ($_REQUEST['month'] && (in_array("7", $_REQUEST['month'])) ) { echo ' selected '; } ?> >Červenec</option>
  <option value="8" <?php if ($_REQUEST['month'] && (in_array("8", $_REQUEST['month'])) ) { echo ' selected '; } ?> >Srpen</option>
  <option value="9" <?php if ($_REQUEST['month'] && (in_array("9", $_REQUEST['month'])) ) { echo ' selected '; } ?> >Září</option>
  <option value="10" <?php if ($_REQUEST['month'] && (in_array("10", $_REQUEST['month'])) ) { echo ' selected '; } ?> >Říjen</option>
  <option value="11" <?php if ($_REQUEST['month'] && (in_array("11", $_REQUEST['month'])) ) { echo ' selected '; } ?> >Listopad</option>
  <option value="12" <?php if ($_REQUEST['month'] && (in_array("12", $_REQUEST['month'])) ) { echo ' selected '; } ?> >Prosinec</option>                      
</select>

<?php

 //if(!$_REQUEST[year]) {   $_REQUEST['year'] = '2017';   }

?>

<select id="year" name="year[]"  multiple  class="chosen-select" data-placeholder="Vyber roky" style="width: 200px" >
  <option value="2017" <?php if ($_REQUEST['year'] && (in_array("2017", $_REQUEST['year'])) ) { echo ' selected '; } ?> >2017</option>
  <option value="2018" <?php if ($_REQUEST['year'] && (in_array("2018", $_REQUEST['year'])) ) { echo ' selected '; } ?> >2018</option>
  <option value="2019" <?php if ($_REQUEST['year'] && (in_array("2019", $_REQUEST['year'])) ) { echo ' selected '; } ?> >2019</option>                  
</select>

<div style="clear: both; padding-top: 10px;">
 
<select id="operator" name="operator" class="chosen-select" data-placeholder="Všichni operátoři" >
    <option value="-1">Všichni operátoři</option>
    <option value="vf_cz" <?php if ($_REQUEST['operator'] == 'vf_cz') {echo "selected";}?>>Vodafone</option>    
    <option value="tm_cz" <?php if ($_REQUEST['operator'] == 'tm_cz') {echo "selected";}?>>T-Mobile</option>    
    <option value="o2_cz" <?php if ($_REQUEST['operator'] == 'o2_cz') {echo "selected";}?>>O2</option>            
</select>
 
<?php
$query = "Select *  from tbldmsvalue Where Valid = 1 And Deleted = 0 ";
$res = @mysql_query($query);
if ($res && @mysql_num_rows($res)>0)
{
?>
<select id="value_id" name="value_id"  class="chosen-select" data-placeholder="Všechny hodnoty">
    <option value="-1">Všechny hodnoty</option>
    <?php
    while ($row = @mysql_fetch_array($res))
    {
    ?>
    <option value="<?php echo $row["ID"]?>" <?php if ($value_id > -1 && $value_id == $row["ID"]) {echo "selected";}?>><?php echo $row["DVName"]; ?></option>
    <?php
    }
    ?>
</select>
<?php
}
?>

 

<?php
$query = "Select *  from tbldmstype Where Valid = 1 And Deleted = 0 ";
$res = @mysql_query($query);
if ($res && @mysql_num_rows($res)>0)
{
?>
<select id="type_id" name="type_id"  class="chosen-select" data-placeholder="Všechny typy" >
    <option value="-1">Všechny typy</option>
    <?php
    while ($row = @mysql_fetch_array($res))
    {
    ?>
    <option value="<?php echo $row["ID"]?>" <?php if ($type_id > -1 && $type_id == $row["ID"]) {echo "selected";}?>><?php echo $row["DTName"]; ?></option>
    <?php
    }
    ?>
</select>
<?php
}
?>

<?php
$query = "Select *  from overcategory  ";
$res = @mysql_query($query);
if ($res && @mysql_num_rows($res)>0)
{
?>
<select id="cat_id" name="cat_id"   class="chosen-select" data-placeholder="Všechny kategorie" >
    <option value="-1">Všechny kategorie</option>
    <?php
    while ($row = @mysql_fetch_array($res))
    {
    ?>
    <option value="<?php echo $row["id"]?>" <?php if ($cat_id > -1 && $cat_id == $row["id"]) {echo "selected";}?>><?php echo $row["name"]; ?></option>
    <?php
    }
    ?>
</select>
<?php
}
?>

<?php
$query = "SELECT name FROM `reportMonthly` INNER JOIN keyword ON reportMonthly.keyword_id = keyword.ID Group by name  ";
$res = @mysql_query($query);
if ($res && @mysql_num_rows($res)>0)
{
?>
<select id="keyword_id" name="keyword_id"   class="chosen-select" data-placeholder="Všechna hesla">
    <option value="-1">Všechna hesla</option>
    <?php
    while ($row = @mysql_fetch_array($res))
    {
    ?>
    <option value="<?php echo $row["name"]?>" <?php if ( $keyword_id == $row["name"]) {echo "selected";}?>><?php echo $row["name"]; ?></option>
    <?php
    }
    ?>
</select>
<?php
}
?>

 

<?php
$query = "SELECT Kod,Nazev FROM `tblKraj`    ";
$res = @mysql_query($query);
if ($res && @mysql_num_rows($res)>0)
{
?>
<select id="kraj_id" name="kraj_id" class="chosen-select" data-placeholder="Všechna kraje" style="width: 250px">
    <option value="-1">Všechny kraje</option>
    <?php
    while ($row = @mysql_fetch_array($res))
    {
    ?>
    <option value="<?php echo $row["Kod"]?>" <?php if ( $kraj_id == $row["Kod"]) {echo "selected";}?> ><?php echo $row["Nazev"]; ?></option>
    <?php
    }
    ?>
</select>
<?php
}
?>

 
 <input type="submit" name="filtr" class="button" style='width: 170px!important; clear: left;' value="UKA"/>
 </div>
</form>

<?php
if(@$info_text!=""){
echo "<h3>".$info_text."</h3>";
}


$time=time();
$query = '';
//overcategory.name AS CATNAME, overcategory.id As CATID,
 $query5 = $query3 = $query2 = $query1 = $query0 = "SELECT  sum((increment * DVRealValue)) AS AMOUNT, tblKraj.Nazev, dmsType,keyword.name as 
 KNAME,month,year,operator,DTName,DVName,DVValue,DVRealValue,increment 
FROM `reportMonthly` 
INNER JOIN tbldmsvalue ON reportMonthly.dmsValue = tbldmsvalue.ID 
INNER JOIN tbldmstype ON reportMonthly.dmsType = tbldmstype.ID 
INNER JOIN keyword ON reportMonthly.keyword_id = keyword.ID 

LEFT JOIN organization ON reportMonthly.organization_id = organization.id  
LEFT JOIN (Select * From tblPSCOkresKraj Group by PSC) myzip ON  REPLACE(organization.zipcode, ' ', '') =  myzip.PSC 
LEFT JOIN tblKraj ON tblKraj.Kod =  myzip.KodKraj


 ";

$query4 = "SELECT sum((increment * DVRealValue)) AS AMOUNT, tblKraj.Nazev, dmsType,keyword.name as 
 KNAME,month,year,operator,DTName,DVName,DVValue,DVRealValue,increment , overcategory.name AS CATNAME
From  overcategory 
INNER JOIN projectCategory ON projectCategory.category_id = overcategory.id
INNER JOIN projectDS ON projectDS.project_id = projectCategory.project_id
INNER JOIN project ON project.id  = projectDS.project_id
INNER JOIN reportMonthly ON reportMonthly.project_id = project.id
INNER JOIN tbldmsvalue ON reportMonthly.dmsValue = tbldmsvalue.ID 
INNER JOIN tbldmstype ON reportMonthly.dmsType = tbldmstype.ID 
INNER JOIN keyword ON reportMonthly.keyword_id = keyword.ID 

LEFT JOIN organization ON reportMonthly.organization_id = organization.id  
LEFT JOIN (Select * From tblPSCOkresKraj Group by PSC) myzip ON  REPLACE(organization.zipcode, ' ', '') =  myzip.PSC 
LEFT JOIN tblKraj ON tblKraj.Kod =  myzip.KodKraj
";
 



$where = " Where (1 = 1)  ";
if(($_REQUEST['operator'])&&($_REQUEST['operator'] != -1))
{
    $where .=  " And   operator ='".$_REQUEST['operator']."' ";
}
if(($_REQUEST['value_id'])&&($_REQUEST['value_id'] != -1))
{
    $where .=  " And dmsValue ='".$_REQUEST['value_id']."' ";
}
if(($_REQUEST['type_id'])&&($_REQUEST['type_id'] != -1))
{
    $where .=  " And dmsType ='".$_REQUEST['type_id']."' ";
}
if(($_REQUEST['keyword_id'])&&($_REQUEST['keyword_id'] != -1))
{
    $where .=  " And keyword.name ='".$_REQUEST['keyword_id']."' ";
}
if(($_REQUEST['kraj_id'])&&($_REQUEST['kraj_id'] != -1))
{
    $where .=  " And tblKraj.Kod ='".$_REQUEST['kraj_id']."' ";
}
if(($_REQUEST['cat_id'])&&($_REQUEST['cat_id'] != -1))
{
$query5 = $query3 = $query2 = $query1 = $query0 = $query4;

    $where .=  " And  overcategory.id ='".$_REQUEST['cat_id']."' ";
}

  if($_REQUEST['month']){

    $where .= " And ( 1=0  ";

     foreach ($_REQUEST['month'] as $selectedOption)
       $where .=  ' OR  month = '.$selectedOption ;

    $where .=  ' ) ';
  }
  else{
       $where .= " And ( 1=0 ) ";
  }
  
  if($_REQUEST['year']){

    $where .= " And ( 1=0  ";

     foreach ($_REQUEST['year'] as $selectedOption)
       $where .=  ' OR  year = '.$selectedOption ;

    $where .=  ' ) ';
  }  
    else{
       $where .= " And ( 1=0 ) ";
  }

 

    $query0 .= $where." Group by operator Order by AMOUNT DESC";
    $query1 .= $where." GROUP BY dmsValue Order by AMOUNT DESC";
    $query2 .= $where." Group by dmsType Order by AMOUNT DESC";
    echo $query3 .= $where." Group by keyword.name Order by AMOUNT DESC   ";
    $query4 .= $where." Group by  overcategory.id Order by AMOUNT DESC  ";
    $query5 .= $where." Group by  tblKraj.Nazev Order by AMOUNT DESC  ";    


 

$result=mysql_query($query0);
if ($result && @mysql_num_rows($result)>0)
{
?>
<p>&nbsp;</p>
<strong>Podíl operátorů</strong>
    <script type="text/javascript" src="http://www.google.com/jsapi"></script>
    <script type="text/javascript"> 
      // Load the Visualization API and the piechart package.
      google.load('visualization', '1', {'packages':['piechart']});
      // Set a callback to run when the Google Visualization API is loaded.
      google.setOnLoadCallback(drawChart);
      // Callback that creates and populates a data table, 
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {
      // Create our data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Operátor');
        data.addColumn('number', 'Suma');
        data.addRows([
 <?php
    while ($row = @mysql_fetch_array($result)){
          if($row['operator'] == 'vf_cz') { $operatorname = "Vodafone"; }
          elseif($row['operator'] == 'tm_cz') { $operatorname = "T-Mobile"; }
          elseif($row['operator'] == 'o2_cz') { $operatorname = "O2"; }
          elseif($row['operator'] == 'NaN') { $operatorname = "Neznámý"; }
          else{  $operatorname = $row['operator']; }
   ?>
          ['<?php echo $operatorname; ?>', <?php echo $row['AMOUNT']; ?>],
<?php
    }
 ?>
           ['',0]
        ]);
        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('chart_div0'));
        chart.draw(data, {width: 500, height: 250, is3D: false, title: ''  } );
      }
    </script>
<div id="chart_div0"></div>

<?php
}
?>

      

<?php    
$result=mysql_query($query0);
if ($result && @mysql_num_rows($result)>0)
{
?>
    
    <script type='text/javascript'>
      google.load('visualization', '1', {packages:['table']});
      google.setOnLoadCallback(drawTable);
      function drawTable() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Operátor');
        data.addColumn('number', 'Suma');
        data.addRows(<?php echo mysql_num_rows($result) +1;?>);
 <?php
 $i = 0;
 $total = 0;
    while ($row = @mysql_fetch_array($result)){
         if($row['operator'] == 'vf_cz') { $operatorname = "Vodafone"; }
          elseif($row['operator'] == 'tm_cz') { $operatorname = "T-Mobile"; }
          elseif($row['operator'] == 'o2_cz') { $operatorname = "O2"; }
          elseif($row['operator'] == 'NaN') { $operatorname = "Neznámý"; }          
          else{  $operatorname = $row['operator']; }
   ?>
        data.setCell(<?php echo $i; ?>, 0, '<?php echo $operatorname; ?>');
        data.setCell(<?php echo $i; ?>, 1, <?php echo $row['AMOUNT']; ?>);   
        
<?php 
        $total = $total+ $row['AMOUNT'];
$i++;
    }
 ?>
        data.setCell(<?php echo $i; ?>, 0, 'Total');
        data.setCell(<?php echo $i; ?>, 1, <?php echo $total; ?>);       
       var table = new google.visualization.Table(document.getElementById('table_div0'));
       table.draw(data, {showRowNumber: false});
      }
    </script>
    <div id='table_div0'></div>
<?php
 }
?>








<?php
$result1=mysql_query($query1);
if ($result1 && @mysql_num_rows($result1)>0)
{
?>
<strong>Hodnoty DMS</strong>
    <script type="text/javascript" src="http://www.google.com/jsapi"></script>
    <script type="text/javascript"> 
      // Load the Visualization API and the piechart package.
      google.load('visualization', '1', {'packages':['piechart']});
      // Set a callback to run when the Google Visualization API is loaded.
      google.setOnLoadCallback(drawChart);
      // Callback that creates and populates a data table, 
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {
      // Create our data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'DMS Typ');
        data.addColumn('number', 'Suma');
        data.addRows([
 <?php
    while ($row1 = @mysql_fetch_array($result1)){
   ?>
          ['<?php echo $row1[DVName]; ?>', <?php echo $row1[AMOUNT]; ?>],
<?php
 
    }
 ?>
           ['',0]
        ]);
        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('chart_div1'));
chart.draw(data, {width: 500, height: 250, is3D: false, title: ''  } );

      }
    </script>
<div id="chart_div1"></div>
<?php
}
?>


<?php
$result1=mysql_query($query1);
if ($result1 && @mysql_num_rows($result1)>0)
{
?>
    
    <script type='text/javascript'>
      google.load('visualization', '1', {packages:['table']});
      google.setOnLoadCallback(drawTable);
      function drawTable() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Hodnota DMS');
        data.addColumn('number', 'Suma Kč');
        data.addRows(<?php echo mysql_num_rows($result1) + 1;?>);
 <?php
 $i = 0;
  $total = 0;
    while ($row1 = @mysql_fetch_array($result1)){
   ?>
        data.setCell(<?php echo $i; ?>, 0, '<?php echo $row1[DVName]; ?>');
        data.setCell(<?php echo $i; ?>, 1, <?php echo $row1['AMOUNT']; ?>);   
        
<?php
        $total = $total+ $row1['AMOUNT'];
$i++;
    }
 ?>
        data.setCell(<?php echo $i; ?>, 0, 'Total');
        data.setCell(<?php echo $i; ?>, 1, <?php echo $total; ?>);    

       var table = new google.visualization.Table(document.getElementById('table_div1'));
       table.draw(data, {showRowNumber: false});
      }
    </script>
    <div id='table_div1'></div>
<?php
 }
?>














<?php
$result2=mysql_query($query2);
if ($result2 && @mysql_num_rows($result2)>0)
{
?>
<strong>Typy DMS</strong>
    <script type="text/javascript" src="http://www.google.com/jsapi"></script>
    <script type="text/javascript"> 
      // Load the Visualization API and the piechart package.
      google.load('visualization', '2', {'packages':['piechart']});
      // Set a callback to run when the Google Visualization API is loaded.
      google.setOnLoadCallback(drawChart);
      // Callback that creates and populates a data table, 
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {
      // Create our data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Typ DMS');
        data.addColumn('number', 'Suma v Kč');
        data.addRows([
 <?php
    while ($row2 = @mysql_fetch_array($result2)){
   ?>
          ['<?php echo $row2[DTName]; ?>', <?php echo $row2[AMOUNT]; ?>],
<?php
    }
 ?>
           ['',0]
        ]);
        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('chart_div2'));
chart.draw(data, {width: 500, height: 250, is3D: false, title: ''   } );

      }
    </script>
<div id="chart_div2"></div>
<?php
}
?>
 
<?php
$result2=mysql_query($query2);
if ($result2 && @mysql_num_rows($result2)>0)
{
?>
    
    <script type='text/javascript'>
      google.load('visualization', '1', {packages:['table']});
      google.setOnLoadCallback(drawTable);
      function drawTable() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Typ DMS');
        data.addColumn('number', 'Suma v Kč');
        data.addRows(<?php echo mysql_num_rows($result2) + 1;?>);
 <?php
 $i = 0;
 $total = 0;
    while ($row2 = @mysql_fetch_array($result2)){
   ?>
        data.setCell(<?php echo $i; ?>, 0, '<?php echo $row2[DTName]; ?>');
        data.setCell(<?php echo $i; ?>, 1, <?php echo $row2['AMOUNT']; ?>);   
        
<?php
 $total = $total+ $row2['AMOUNT'];
$i++;
    }
 ?>
        data.setCell(<?php echo $i; ?>, 0, 'Total');
        data.setCell(<?php echo $i; ?>, 1, <?php echo $total; ?>);   

       var table = new google.visualization.Table(document.getElementById('table_div2'));
       table.draw(data, {showRowNumber: false});
      }
    </script>
    <div id='table_div2'></div>
<?php
 }
?>










<?php
$result3=mysql_query($query3);
if ($result3 && @mysql_num_rows($result3)>0)
{
?>
<strong>Projekty</strong>
    <script type="text/javascript" src="http://www.google.com/jsapi"></script>
    <script type="text/javascript"> 
      // Load the Visualization API and the piechart package.
      google.load('visualization', '3', {'packages':['piechart']});
      // Set a callback to run when the Google Visualization API is loaded.
      google.setOnLoadCallback(drawChart);
      // Callback that creates and populates a data table, 
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {
      // Create our data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Projekt');
        data.addColumn('number', 'Suma v Kč');
        data.addRows([
 <?php
    while ($row3 = @mysql_fetch_array($result3)){
   ?>
          ['<?php echo $row3[KNAME]; ?>', <?php echo $row3[AMOUNT]; ?>],
<?php
    }
 ?>
           ['',0]
        ]);
        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('chart_div3'));
chart.draw(data, {width: 500, height: 250, is3D: false, title: ''    } );

      }
    </script>
<div id="chart_div3"></div>
<?php
}
?>
 
<?php
$result3=mysql_query($query3);
if ($result3 && @mysql_num_rows($result3)>0)
{
?>
    
    <script type='text/javascript'>
      google.load('visualization', '3', {packages:['table']});
      google.setOnLoadCallback(drawTable);
      function drawTable() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Projekt');
        data.addColumn('number', 'Suma v Kč');
        data.addRows(<?php echo mysql_num_rows($result3) + 1;?>);
 <?php
  $total = 0;
 $i = 0;
    while ($row3 = @mysql_fetch_array($result3)){
   ?>
        data.setCell(<?php echo $i; ?>, 0, '<?php echo $row3[KNAME]; ?>');
        data.setCell(<?php echo $i; ?>, 1, <?php echo $row3['AMOUNT']; ?>);   
        
<?php
$total = $total + $row3['AMOUNT'];
$i++;
    }
 ?>
         data.setCell(<?php echo $i; ?>, 0, 'Celkem pro TOP 10');
        data.setCell(<?php echo $i; ?>, 1, <?php echo $total; ?>);   
       var table = new google.visualization.Table(document.getElementById('table_div3'));
       table.draw(data, {showRowNumber: false});
      }
    </script>
    <div id='table_div3'></div>
<?php
 }
?>


 
<?php
$result4=mysql_query($query4);
if ($result4 && @mysql_num_rows($result4)>0)
{
?>
<strong>Kategorie</strong>
    <script type="text/javascript" src="http://www.google.com/jsapi"></script>
    <script type="text/javascript"> 
      // Load the Visualization API and the piechart package.
      google.load('visualization', '4', {'packages':['piechart']});
      // Set a callback to run when the Google Visualization API is loaded.
      google.setOnLoadCallback(drawChart);
      // Callback that creates and populates a data table, 
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {
      // Create our data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Kategorie');
        data.addColumn('number', 'Suma v Kč');
        data.addRows([
 <?php
    while ($row4 = @mysql_fetch_array($result4)){
   ?>
          ['<?php echo substr($row4[CATNAME],0,10); ?>...', <?php echo $row4[AMOUNT]; ?>],
<?php
    }
 ?>
           ['',0]
        ]);
        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('chart_div4'));
chart.draw(data, {width: 500, height: 250, is3D: false, title: '' } );

      }
    </script>
<div id="chart_div4"></div>
<?php
}
?>
 
<?php
$result4=mysql_query($query4);
if ($result4 && @mysql_num_rows($result4)>0)
{
?>
    
    <script type='text/javascript'>
      google.load('visualization', '4', {packages:['table']});
      google.setOnLoadCallback(drawTable);
      function drawTable() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Kategorie');
        data.addColumn('number', 'Suma v Kč');
        data.addRows(<?php echo mysql_num_rows($result4);?>);
 <?php
 $i = 0;
    while ($row4 = @mysql_fetch_array($result4)){
   ?>
        data.setCell(<?php echo $i; ?>, 0, '<?php echo $row4[CATNAME]; ?>');
        data.setCell(<?php echo $i; ?>, 1, <?php echo $row4['AMOUNT']; ?>);   
        
<?php
$i++;
    }
 ?>
       var table = new google.visualization.Table(document.getElementById('table_div4'));
       table.draw(data, {showRowNumber: false});
      }
    </script>
    <div id='table_div4'></div>
<?php
 }
?>


  







<?php
$result5=mysql_query($query5);
if ($result5 && @mysql_num_rows($result5)>0)
{
?>
<strong>Projekty</strong>
    <script type="text/javascript" src="http://www.google.com/jsapi"></script>
    <script type="text/javascript"> 
      // Load the Visualization API and the piechart package.
      google.load('visualization', '5', {'packages':['piechart']});
      // Set a callback to run when the Google Visualization API is loaded.
      google.setOnLoadCallback(drawChart);
      // Callback that creates and populates a data table, 
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {
      // Create our data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Kraj');
        data.addColumn('number', 'Suma v Kč');
        data.addRows([
 <?php
    while ($row5 = @mysql_fetch_array($result5)){
   ?>
          ['<?php echo $row5[Nazev]; ?>', <?php echo $row5[AMOUNT]; ?>],
<?php
    }
 ?>
           ['',0]
        ]);
        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('chart_div5'));
chart.draw(data, {width: 500, height: 250, is3D: false, title: ''    } );

      }
    </script>
<div id="chart_div5"></div>
<?php
}
?>
 
<?php
$result5=mysql_query($query5);
if ($result5 && @mysql_num_rows($result5)>0)
{
?>
    
    <script type='text/javascript'>
      google.load('visualization', '5', {packages:['table']});
      google.setOnLoadCallback(drawTable);
      function drawTable() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Kraj');
        data.addColumn('number', 'Suma v Kč');
        data.addRows(<?php echo mysql_num_rows($result5) + 1;?>);
 <?php
  $total = 0;
 $i = 0;
    while ($row5 = @mysql_fetch_array($result5)){
   ?>
        data.setCell(<?php echo $i; ?>, 0, '<?php echo $row5[Nazev]; ?>');
        data.setCell(<?php echo $i; ?>, 1, <?php echo $row5['AMOUNT']; ?>);   
        
<?php
$total = $total + $row5['AMOUNT'];
$i++;
    }
 ?>
         data.setCell(<?php echo $i; ?>, 0, 'Celkem');
        data.setCell(<?php echo $i; ?>, 1, <?php echo $total; ?>);   
       var table = new google.visualization.Table(document.getElementById('table_div5'));
       table.draw(data, {showRowNumber: false});
      }
    </script>
    <div id='table_div5'></div>
<?php
 }
?>







 

<strong>&nbsp;</strong>

<br/>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js" type="text/javascript"></script>
  <script src="./js/superselect/chosen.jquery.js" type="text/javascript"></script>
  <script src="./js/superselect/prism.js" type="text/javascript" charset="utf-8"></script>
  <script type="text/javascript">
    var config = {
      '.chosen-select'           : {},
      '.chosen-select-deselect'  : {allow_single_deselect:true},
      '.chosen-select-no-single' : {disable_search_threshold:10},
      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chosen-select-width'     : {width:"90%"}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }
  </script>
