// JavaScript Document
	tinyMCE.init({
		// General options
		mode : "textareas",
		theme : "advanced",
		skin : "o2k7",
		plugins : "style,table,advlink,insertdatetime,contextmenu,paste,noneditable,visualchars,nonbreaking,xhtmlxtras,inlinepopups",
		theme_advanced_buttons1 : "bold,italic,justifyleft,|,bullist,numlist,|,link,unlink,|,paste,pastetext,pasteword,selectall|,outdent,indent,blockquote,|,styleselect,formatselect,|,undo,redo,|,cleanup,removeformat,code",
		theme_advanced_buttons2 : "tablecontrols,|,sub,sup,forecolor",
		//theme_advanced_buttons2 : "",
		theme_advanced_buttons3 : "",
		theme_advanced_buttons4 : "",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,
		theme_advanced_blockformats : "p,h2,h3,h4",
		content_css : "http://www.knezaplaceni.cz/administrace/inside/css/word.css",
		translate_mode : true,
		language : "cs",
	paste_create_paragraphs : false,
	paste_create_linebreaks : false,		
		 apply_source_formatting : true,
    //convert_fonts_to_spans : true,
    entity_encoding : "named",
    entities : "160,nbsp",
    paste_auto_cleanup_on_paste : false,
    paste_strip_class_attributes : "all",
    setup : function(ed) {
    ed.onPaste.add(function(ed, e, o) {
		alert("Pro vložení textu použijte jednu z ikon 'vložit' v horní liště");
		//return tinymce.dom.Event.cancel(e);
    } );}
	});
