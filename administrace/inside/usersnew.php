
<script type="text/javascript" language="javascript" src="./js/dom.js"></script>
<script type="text/javascript" language="javascript" src="./js/check_form.js"></script>
<script type="text/javascript" language="javascript">kickstart_Tiny();</script>
<?php 
$table_name = "tbluser";
$page_name = "users";
$priviledge_arr = false;

if (isset($_REQUEST["newsid"]) && is_numeric($_REQUEST["newsid"]) && $_REQUEST["newsid"]>0)
{
	$user_id = $_REQUEST["newsid"];
}
else
{
	$user_id = false;
	$row["UCanLogin"] = 1;
}

if ($user_id)
{
	$query = "Select * from ".$table_name." where ID = ".$user_id." and Deleted = 0";
	$res = @mysql_query($query);

	if ($res && @mysql_num_rows($res)>0)
	{
		$row = @mysql_fetch_array($res);
	}

	$query = "Select PUPriviledge from tblpriviledgeuser where PUUser = ".$user_id." and Deleted = 0";
	$res = @mysql_query($query);

	if ($res && @mysql_num_rows($res)>0)
	{
		$priviledge_arr = array();
		while ($priv_row = @mysql_fetch_array($res))
		{
			$priviledge_arr[] = $priv_row["PUPriviledge"];
		}
	}

}

?>
<form action="index.php?id=<?php  echo $page_name; ?><?php if($user_id>0){echo "&newsid=",$user_id;}?>" method="post" name="noname"  ENCTYPE="multipart/form-data">
<?php 
if ($user_id)
{
?>
    <input type="hidden" name="user_id" value="<?php  echo $user_id; ?>" />
<?php 
}
?>


<div style="text-align:left; width: 640px;" align="center">

<p>&nbsp;</p>
<fieldset><h5>Informace o u&#382;ivateli</h5>
<?php if (@$row["UFoto"]){?>
<div id="photo"  style="width: 60px; float: right; margin-right: 50px; padding: 10px">
<img src="../images/usersthumbs/<?php  echo $row["UFoto"]; ?>" />
</div>
<?php } ?>
<label>Jméno</label>
<input title="Jméno - Max 50 znaků, toto pole je povinné" class="required" style="float:left;width: 200px" type="text" value="<?php  echo $row["UName"]; ?>" name="user_name" /><br/>
<label>Příjmení</label>
<input title="Příjmení - Max50 znaků, toto pole je povinné" class="required" style="float:left;width: 200px" type="text" value="<?php  echo $row["USurname"]; ?>" name="user_surname" /><br/>
<label>E-mail</label>
<input title="E-mail" class="required" style="float:left;width: 200px" type="text" value="<?php  echo $row["UEmail"]; ?>" name="UEmail" /><br/>
<label>Login</label>
<input title="Login - Max 64 znaků, toto pole je povinné" class="required" style="float:left;width: 200px" type="text" value="<?php  echo $row["ULogin"]; ?>" name="user_login" /><br/>
<label>Heslo</label>
<input title="Heslo - Max 64 znaků, toto pole je povinné" class="<?php  if (!$user_id) { echo "required";} ?>" style="float:left;width: 200px" type="password" value="" name="user_pass" /><br/>
<label>Heslo (opakovat)</label>
<input title="Heslo (opakovat) - Max 64 znaků, toto pole je povinné" class="<?php  if (!$user_id) { echo "required";} ?>" style="float:left;width: 200px" type="password" value="" name="user_pass_repeat" /><br/>
<label>Foto</label>
<input  type="file" name="userFoto" accept="image/*,text/plain"  style="float:left;width: 200px"  />
<br /><br /><br />
<label>Společnost</label>
<?php 
  $query = "Select * from tblloga where logoImage != -1 ORDER BY logoName ";
$res2 = @mysql_query($query);

if ($res2 && @mysql_num_rows($res2)>0)
{
?>
<select name="UFirm" id="user_level"  style="float:left;width: 200px" >
<option value="">--nic--</option>
<?php 
while ($row2 = @mysql_fetch_array($res2))
{
?>
        <option value="<?php  echo $row2['ID']; ?>"<?php  if ($row2["ID"] == intval($row["UFirm"])) { echo " selected"; } ?>><?php  echo $row2['logoName']; ?></option>
<?php 
}
?>
</select>
<?php 
}
?>
<br />
<label>Profil</label>
<textarea name="UProfil" style="width:565px; height:150px">
<?php  echo @$row['UProfil']; ?>
</textarea><br />
<label>Úroveň přístupu</label>
<?php 
  $query = "Select tblusertype.* from tblusertype where Valid = 1 ORDER BY ID";
$res2 = @mysql_query($query);

if ($res2 && @mysql_num_rows($res2)>0)
{
?>
<select name="user_level" id="user_level"  style="float:left;width: 200px" >
<?php 
while ($row2 = @mysql_fetch_array($res2))
{
?>
        <option value="<?php  echo $row2['ID']; ?>"<?php  if ($row2["ID"] == intval($row["UType"])) { echo " selected"; } ?>><?php  echo $row2['Description']; ?></option>
<?php 
}
?>
</select>
<?php 
}
?>

<br/>
<label>Smí se přihlásit</label>
<input type="checkbox" name="can_login" <?php  if ($row["UCanLogin"] == 1) { echo "checked";} ?>  style="float:left;" />
</fieldset>
<fieldset><h5>Doplňková oprávnění</h5>
<?php 

$priviledge_query = "Select ID, Description from tblpriviledge where Deleted = 0 order by Description";

$priviledge_res = @mysql_query($priviledge_query);
if ($priviledge_res && @mysql_num_rows($priviledge_res)>0)
{
	while ($priviledge_resarr = @mysql_fetch_array($priviledge_res))
	{
	?>
	<input type="checkbox" name="privs[]" value="<?php  echo $priviledge_resarr["ID"]; ?>" <?php  if (is_array($priviledge_arr) && in_array($priviledge_resarr["ID"],$priviledge_arr)) { echo "checked"; } ?>/>&nbsp;<?php  echo $priviledge_resarr["Description"]; ?><br />
	<?php 
	}
}
?>
</fieldset>

<p>&nbsp;</p>
<input type="submit" value="odeslat" name="send">
</div>
</form>
