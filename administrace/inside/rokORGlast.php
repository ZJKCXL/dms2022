<script type="text/javascript" src="https://code.jquery.com/jquery-latest.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>

<style>
label {
 
    width: auto!important;
 
}
</style>

 <?php
   if($_GET['rok'] > 0) { $rok = $_GET['rok']; } else {  $rok = date(Y)  ;  }
   if($_GET['order'])   { $order = $_GET['order']; } else {   $order = 'organization.name';  }
 ?>
 <table  id="tableOUT"   class="table stripe row-border m-b-0 toggle-circle footable-loaded footable tablet breakpoint"> 
 <thead>
 <tr> 
 <th>Organizace</th>
 <th>Suma rok</th>
 </tr>
 </thead>
 <tbody> <?php
   echo  "<h2>Přehled DMS za rok ".$rok." po organizacích</h2>";
     $keywordsquery =  "Select sum(DVRealValue*increment) AS MONEY, organization_id AS OID, organization.name AS ONAME From reportMonthly, organization, tbldmsvalue Where organization_id = organization.id And organization_id = reportMonthly.organization_id And tbldmsvalue.ID = reportMonthly.dmsValue And year = '".$rok."' Group by organization_id Order by MONEY DESC";
   $key_res = @mysql_query($keywordsquery);
   if ( $key_res && @mysql_num_rows($key_res)>0)    {        
   while  ($key_row = @mysql_fetch_array($key_res))   {    

    
   if($key_row['MONEY'] > 999999) {  $b = '<b>'; $bend = '</b>';  } else { $b = $bend = ''; }

   echo "<tr><td>".$b. $key_row['ONAME'].$bend."</td><td style='text-align:right'>". $b.$key_row['MONEY'].$bend."</td></tr>";
  }
  }
 ?>
 </tbody>

</table>
<script type="text/javascript">

$(document).ready( function () {
        $('#tableOUT').DataTable( {
        paging: false ,
        "order": [[ 1,  "desc" ]] 
 
        } );



      } );

 

var tableToExcel = (function() {
    var uri = 'data:application/vnd.ms-excel;base64,'
    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
    , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
    , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
  return function(table, name, filename) {
    if (!table.nodeType) table = document.getElementById(table)
    var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
    //window.location.href = uri + base64(format(template, ctx))
    var link = document.createElement("a");
                    link.download = filename + ".xls";
                    link.href = uri + base64(format(template, ctx));
                    link.click();
  }
}) 
</script>

<img src='/file_type_images/3xlsx.png' style="cursor:pointer; width: 60px; float: right"   onclick="tableToExcel('tableOUT', 'Přehled DMS za rok <?php echo $rok; ?>', 'Rok_<?php echo $rok; ?>_DMS')" title="Export 2 Excel" id='btnExport' />