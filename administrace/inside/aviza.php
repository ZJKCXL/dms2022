<style>
 .mouseover:hover {
   background: #e7e7e7;
 }
 #EmAll { min-width: 1350px; }   
 #generator a , #generator div{
   display: block;
   padding: 5px 15px 5px 10px;
   background: #e7e7e7;
   color: #000;
   float: left;
   margin-right: 15px;
   line-height: 35px;
   max-height: 35px;
   cursor: pointer;
 }    
 #generator div span {
   display: inline;
 
    width: auto;
        padding: 0;
    margin: 0;
    height: 35px;
 }
 #generator a:hover{
   background: #00ACEE;
   color: #fff;
 }
 .totalvata {
   margin-bottom: 0.5em;
   font-size: 150%;
   float: right;
   margin-top: -2em;
   
 }
 a span {
   width: auto;
   max-height: 35px;
 }
 .fa-check-square-o, .fa-exclamation-triangle {
   font-size: 30px;
   margin: 5px 10px 0 0;
   color: green;
 }
 .fa-exclamation-triangle {
  color: red;
  font-size: 25px;
 }
 .fa-spinner {
   color: orange;
 } 
 #inavisa1Fa1,  #inavisa1Fa2, #inavisa1Fa3,
 #inavisa3Fa1,  #inavisa3Fa2, #inavisa3Fa3, 
 #inavisa2Fa1,  #inavisa2Fa2, #inavisa2Fa3,
 #inavisa3Fa1,  #inavisa3Fa2, #inavisa3Fa3,
 #inavisa4Fa1,  #inavisa4Fa2, #inavisa4Fa3  
  {display: none;}
 .done {
   display: inline-block ;
     padding: 5px 15px 5px 10px;
 }
 #generator #avisa2,  #generator #avisa3,  #generator #avisa4 {
    display: none;
  }
  .noclick  {
        cursor: text!important;
        text-decoration: none;
  }
  #generator .noclick:hover  {
        background: #e7e7e7!important;
       color: #000!important;
       cursor: text!important;
  }
  #generator #avisa1fake, #generator #avisa2fake,#generator  #avisa3fake, #generator #avisa4fake {
        display: none;
        cursor: text!important;
  }
  #generator  #avisa2.showme, #generator  #avisa3.showme, #generator  #avisa3 .showme,  #generator  #avisa4.showme,  #generator  #avisa4 .showme  {
    display: block ;
  }
  #generator i#wrong { font-style: normal; visibility: hidden; }
  .prehled em.right { text-align: right; padding-right: 1em; }
</style>


<script>

/* Prvni cudlik */
function avisaFromAts(m,y){
   
    if(document.getElementById('avisa1')){
          document.getElementById('inavisa1Fa1').style.display = 'block';
          document.getElementById('inavisa1Fa2').style.display = 'none';
          document.getElementById('inavisa1Fa3').style.display = 'none';
      var anticache = Math.floor(Math.random()*1000);
      var url='../2017/jobs/avisaFromAts.php?month='+m+'&year='+y+'&anticache='+anticache;
         //  alert(url);
         var semka = document.getElementById('inavisa1');      
         if (window.ActiveXObject)
        {
          httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
        }
        else
        {
          httpRequest = new XMLHttpRequest();
        }
        httpRequest.open("GET", url, true);
        httpRequest.onreadystatechange= function () {processRequest1(semka); } ;
        httpRequest.send(null);
        
    } 
    return false;
}

 function processRequest1(semka) {
  if (httpRequest.readyState == 4)
  {
 

   //alert(httpRequest.responseText);

    if((httpRequest.status == 200)&&(!httpRequest.responseText.includes("GLOBALERROR")  ))
    {   
      //semka.innerHTML = httpRequest.responseText;
          document.getElementById('inavisa1FFa2').style.display = 'block';
          document.getElementById('inavisa1FFa1').style.display = 'none';
          document.getElementById('inavisa1FFa3').style.display = 'none'; 
          document.getElementById('avisa2').style.display = 'block';  
          document.getElementById('avisa1').style.display = 'none';  
          document.getElementById('avisa1fake').style.display = 'block';  

    }
    else
    {
          document.getElementById('inavisa1FFa2').style.display = 'none';
          document.getElementById('inavisa1FFa1').style.display = 'none';
          document.getElementById('inavisa1FFa3').style.display = 'block';    
          document.getElementById('avisa1').style.display = 'none';  
          document.getElementById('avisa1fake').style.display = 'block';             
    }  
  }
}

/* Druhej cudlik */
function avisaGenerate(m,y){
    if(document.getElementById('avisa2')){
          document.getElementById('inavisa2Fa1').style.display = 'block';
          document.getElementById('inavisa2Fa2').style.display = 'none';
          document.getElementById('inavisa2Fa3').style.display = 'none';
      var anticache = Math.floor(Math.random()*1000);
      var url='../2017/jobs/avisaGenerate.php?month='+m+'&year='+y+'&anticache='+anticache;
        // alert(url);
         var semka2 = document.getElementById('inavisa2');      
         if (window.ActiveXObject)
        {
          httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
        }
        else
        {
          httpRequest = new XMLHttpRequest();
        }
        httpRequest.open("GET", url, true);
        httpRequest.onreadystatechange= function () {processRequest2(semka2); } ;
        httpRequest.send(null);
        return false;
    } 
}

 function processRequest2(semka2) {
  if (httpRequest.readyState == 4)
  {
    //alert( httpRequest.responseText);
    if((httpRequest.status == 200)&&(httpRequest.responseText.includes("success")  ))
    {   
      //semka.innerHTML = httpRequest.responseText;
          document.getElementById('inavisa2FFa2').style.display = 'block';
          document.getElementById('inavisa2FFa1').style.display = 'none';
          document.getElementById('inavisa2FFa3').style.display = 'none';        
          document.getElementById('avisa3').style.display = 'block'; 
          document.getElementById('avisa4').style.display = 'block';           
          document.getElementById('avisa2').style.display = 'none';  
          document.getElementById('avisa2fake').style.display = 'block';             
    }
    else
    {
          document.getElementById('inavisa2FFa2').style.display = 'none';
          document.getElementById('inavisa2FFa1').style.display = 'none';
          document.getElementById('inavisa2FFa3').style.display = 'block';    
          document.getElementById('avisa2').style.display = 'none';  
          document.getElementById('avisa2fake').style.display = 'block';              
    }  
  }
}
 

/* 4. cudlik */
function avisaSendMail(m,y){
    if(document.getElementById('avisa4')){
          document.getElementById('inavisa4Fa1').style.display = 'block';
          document.getElementById('inavisa4Fa2').style.display = 'none';
          document.getElementById('inavisa4Fa3').style.display = 'none';
      var anticache = Math.floor(Math.random()*1000);
      var url='../2017/jobs/avisaSednMail.php?month='+m+'&year='+y+'&anticache='+anticache;
       // alert(url);
         var semka4 = document.getElementById('inavisa4');      
         if (window.ActiveXObject)
        {
          httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
        }
        else
        {
          httpRequest = new XMLHttpRequest();
        }
        httpRequest.open("GET", url, true);
        httpRequest.onreadystatechange= function () {processRequest4(semka4); } ;
        httpRequest.send(null);
        return false;
    } 
}

function processRequest4(semka4) {
  if (httpRequest.readyState == 4)
  {
    //alert( httpRequest.responseText);
    if((httpRequest.status == 200)&&(!httpRequest.responseText.includes("ERR")  ))
    {   
      //semka.innerHTML = httpRequest.responseText;
          document.getElementById('inavisa4FFa2').style.display = 'block';
          document.getElementById('inavisa4FFa1').style.display = 'none';
          document.getElementById('inavisa4FFa3').style.display = 'none';        
          document.getElementById('avisa4').style.display = 'none';           
          document.getElementById('avisa4fake').style.display = 'block';             
    }
    else
    {
          document.getElementById('inavisa4FFa2').style.display = 'none';
          document.getElementById('inavisa4FFa1').style.display = 'none';
          document.getElementById('inavisa4FFa3').style.display = 'block';        
          document.getElementById('avisa4').style.display = 'none';           
          document.getElementById('avisa4fake').style.display = 'block';    
          document.getElementById('wrong').style.visibility = 'visible';             

    }  
  }
}

</script>




<?php

 

$lastmonth = intval(date(m,time())) - 1;
if(!$month) { $month = $lastmonth; }
$actuyear = intval(date(Y,time()))  ;
if(!$year) { $year = $actuyear; }
$table_name = "organization_online";
$page_name = "aviza";
$this_adress = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
$filtry=explode('-',$filter);
?>
<h1>Avíza</h1>

<form action="index.php?id=<?php echo $page_name; ?>" method="post" name="noname">

<select name='month'>
<option value='1' <?php if($month == 1) { echo " selected "; } ?> >Leden</option>
<option value='2' <?php if($month == 2) { echo " selected "; } ?> >Únor</option>
<option value='3' <?php if($month == 3) { echo " selected "; } ?> >Březen</option>
<option value='4' <?php if($month == 4) { echo " selected "; } ?> >Duben</option>
<option value='5' <?php if($month == 5) { echo " selected "; } ?> >Květen</option>
<option value='6' <?php if($month == 6) { echo " selected "; } ?> >Červen</option>
<option value='7' <?php if($month == 7) { echo " selected "; } ?> >Červenec</option>
<option value='8' <?php if($month == 8) { echo " selected "; } ?> >Srpen</option>
<option value='9' <?php if($month == 9) { echo " selected "; } ?> >Září</option>
<option value='10' <?php if($month == 10) { echo " selected "; } ?> >Říjen</option>
<option value='11' <?php if($month == 11) { echo " selected "; } ?> >Listopad</option>
<option value='12' <?php if($month == 12) { echo " selected "; } ?> >Prosinec</option>
</select>

<select name='year'>
<option value='2017' <?php if($year == 2017) { echo " selected "; } ?>>2017</option>
<option value='2018' <?php if($year == 2018) { echo " selected "; } ?>>2018</option>
<option value='2019' <?php if($year == 2019) { echo " selected "; } ?>>2019</option>
<option value='2020' <?php if($year == 2020) { echo " selected "; } ?>>2020</option>
<option value='2021' <?php if($year == 2021) { echo " selected "; } ?>>2021</option>
<option value='2022' <?php if($year == 2022) { echo " selected "; } ?>>2022</option>
</select>

 <input type="text" title="Fulltext  " name="fulltext" id="fulltext" value="<? echo trim($_REQUEST["fulltext"]); ?>" />
 <input type="submit" name="filtr" class="button1" value="Filtrovat"/>
</form>
<br/>
<?php


if($_POST['sendapro'] == 'ok') {
   if($_POST['yes'] == 'on' ) { $apro = 1; } else { $apro = 0; }

$aproquery = "Select avizoAproved,id From tbl_aviza_approved Where avizoMonth = ".$_POST['month']." And avizoYear = ".$_POST['year'];
$aproresult=mysql_query($aproquery);
if(($aproresult)&&(mysql_numrows($aproresult)>0)){
   $upquery = "Update tbl_aviza_approved SEt avizoAproved = ".$apro." Where avizoMonth = ".$_POST['month']." And avizoYear = ".$_POST['year'];
   mysql_query($upquery);
}
else{
   $upquery = "INSERT INTO `tbl_aviza_approved` (`id`, `avizoMonth`, `avizoYear`, `avizoAproved`) VALUES ('', '".$_POST['month']."', '".$_POST['year']."', '".$apro."')";
   mysql_query($upquery); 
}


 


  

}



$where = "";

if(  ($fulltext)||($year) ||($month)){
 $filter = "yes";
}
if($month > -1) {
 $where .= " And dmsNotification.month =   ".$month;  
 $filter .= "&month=".$month;
}
else{
 $where .= " And dmsNotification.month =   ".$lastmonth;  
 $filter .= "&month=".$lastmonth;
}
if($year > 2016) {
 $where .= "  And dmsNotification.year =   ".$year;  
 $filter .= "&year=".$year;
}
else{
 $where .= " And dmsNotification.year =   ".$actuyear;  
 $filter .= "&year=".$actuyear;
}

if($fulltext) {
 $fulltext = trim($fulltext);
 $where .= " AND (keyword.name LIKE '%".$fulltext."%' OR project.name LIKE '%".$fulltext."%' OR organization.name LIKE '%".$fulltext."%'  ) " ;
 $filter .= "&fulltext=".$fulltext;
}
if($by) {
 $where .= " And (konfTime = ".$by.") " ;
 $filter .= "&by=".$by;
}
if($order){
 $filter .= "&order=".$order; 
}
/*
echo $year;
echo $where;
echo "<hr/>";
*/
if(($filtry[0])&&($filtry[0] <> 0)){ $where = " WHERE reakce = ".$filtry[0]; }

if($order == 2){ $orderby = " project.name "; }
elseif($order == 1){ $orderby = $table_name.".valid  "; }
elseif($order == 3){ $orderby =  " organization.name "; }
elseif($order == 4) { $orderby = " dmsNotification.notification_file "; }
elseif($order == 5) { $orderby = " amount DESC "; }
else { $orderby = " project.name " ; }

$query = "SELECT projectDS.ds_id AS DSID, project.ID as MYID, raw_data, amount, collection.validFrom AS NEW, dmsNotification.notification_file AS MYFILE, project.name AS prjname, project.id AS prid, collection.name AS KW, organization.companyId AS ICO, organization.name AS orgname, GROUP_CONCAT( keyword.name
ORDER BY keyword.name SEPARATOR ',' ) AS keywords FROM collection
LEFT JOIN projectDS ON projectDS.project_id = collection.project_id
INNER JOIN dmsNotification ON dmsNotification.project_id = collection.project_id
INNER JOIN project ON project.id = collection.project_id
INNER JOIN organization ON organization.id = project.organization_id
INNER JOIN collectionKeyword ON collection.id = collectionKeyword.collection_id
INNER JOIN keyword ON keyword.id = collectionKeyword.keyword_id 
WHERE   CURDATE( ) >= collection.validFrom
 ". $where." 
  GROUP BY collection.project_id ORDER BY ".$orderby;

//
 // echo $query;

$result=mysql_query($query);
echo mysql_error();

$aproquery = "Select avizoAproved From tbl_aviza_approved Where avizoMonth = ".$month." And avizoYear = ".$year;
$aproresult=mysql_query($aproquery);
if(($aproresult)&&(mysql_numrows($aproresult)>0)){
  if(mysql_result($aproresult,0,"avizoAproved") > 0){
      $aproved = " checked ";
  }
  else {
      $aproved = "  ";
  }
}

  $vataquery = "SELECT sum(amount) as VATA FROM  dmsNotification  WHERE 1 = 1  ". $where ;
  
  $vataresult=mysql_query($vataquery);
  
   if(($vataresult)&&(mysql_numrows($vataresult)>0)){
  if(mysql_result($vataresult,0,"VATA") > 0){


  echo "<p class='totalvata'>Celkem : ".$vata = number_format(mysql_result($vataresult,0,"VATA"), 2, ',', ' ');
  ?>
  
  <form style = "float: right" method="post" action="#" name = "aproved"><input <?php echo $aproved; ?> type="checkbox" name="yes" /><input type="submit" name="sendapro"  value="ok">
  <input type="hidden" name="month" value="<?php echo $month; ?>" />
  <input type="hidden" name="year" value="<?php echo $year; ?>" />  
  </form>
  <?php
  echo "</p>";
  }
}



$tot = mysql_numrows($result);
if(($result)&&(mysql_numrows($result)>0)){
?>
<form method="post" action="index.php?id=aviza&from=<?php echo $from; ?>&filter=<?php echo $filter; ?>" id="EmAll">
 

<?php
 $fltr = "&amp;from=".$from; 
 if($service) { $fltr .= "&amp;service=".$service; } 
 if($perio_id) { $fltr .= "&amp;perio_id=".$perio_id; } 
 if($tema_id) { $fltr .= "&amp;tema_id=".$tema_id; } 
 if($fulltext) { $fltr .= "&amp;fulltext=".$fulltext; } 
 if($year) { $fltr .= "&amp;year=".$year; }  
 if($month) { $fltr .= "&amp;month=".$month; }      
?>

<?php echo long2shortEMMlink("Organizace",60,'mouse header', 30, 'em', "aviza&order=3".$fltr , "Klik = seřadit podle Jména ABC"); ?>
<?php echo long2shortEMMlink("Projekt",60,'mouse header', 30, 'em', "aviza&order=2".$fltr , "Klik = seřadit podle Jména ABC"); ?>
<?php echo long2shortEMM("IČO",15,'header', 6, 'em'   , "Klik = seřadit podle sumy"); ?>
<?php echo long2shortEMMlink("Suma",15,'mouse header', 7, 'em' , "aviza&order=5".$fltr, "Suma"); ?>
<?php echo long2shortEMM("Účet",30,'header', 12, 'em', "Účet"); ?>
<?php echo long2shortEMMlink("Avizo",60,'mouse header', 30, 'em' , "aviza&order=4".$fltr , "Klik = seřadit podle jména souboru"); ?>
<?php
if(!$from){ $from = 0; }
$to = pagerStart(25,@$from,$result);
for ($i=@$from;$i<$to;$i++)
     {
$orgname= mysql_result($result,$i,"orgname");
$prjname= mysql_result($result,$i,"prjname"); 
$ICO= mysql_result($result,$i,"ICO"); 
$suma=   number_format(mysql_result($result,$i,"amount"), 0, ',', ' ');
$myfile= mysql_result($result,$i,"MYFILE"); 
$ucet =  json_decode( mysql_result($result,$i,"raw_data") , true);
$ucet =  $ucet['account'];
$styler = 'right';
$tstID =  mysql_result($result,$i,"MYID");

?>
<div class="prehled" style="background: <?php echo $color; ?>">
<div style="float:left; width: 99%" >
<p style="float: left; width: 15px; "></p>
 
<?php echo long2shortEMM($orgname,60, $style, 30, 'em',   $jmeno); ?>
<?php echo long2shortEMM($prjname,60,$style,30, 'em', $email);  ?> 
<?php echo long2shortEMM ($ICO , 15,$style, 6, 'em', $ico); ?> 
<?php echo long2shortEMM ($suma , 15,$styler, 6, 'em', $suma); ?> 
<?php echo long2shortEMM ($ucet , 30,$style, 12, 'em', $ucet); ?> 
<?php echo long2shortEMMlinkOUT($myfile,35, 'mouse mouseover', 25, 'em',"http://www.darcovskasms.cz/avizo/".$myfile ,  $jmeno); ?>  
<?php echo long2shortEMMlinkOUT('EXCL'  ,5, 'mouse mouseover',  5, 'em',"http://www.darcovskasms.cz/excel/NEWdetailDMS2.php?month=".$month."&year=". $year."&finalID=".$tstID   ,  'EXCL'); ?>  

 
 
</div>
</div>


<?php




//konec cyklu komentaru
}




 
pagerEnd(25, @$result, @$id, @$from, @$filter);
 

}
else{

echo "<br/>Pro zvolený rok a měsíc zatím žádná data. Proveďte následující <strong>4 kroky</strong>: <br/>";

}
?>
<p style = "clear: both; border-top: 50px solid white; display: block" ><strong>Počkejte vždy do ukončení procesu</strong> (dokud oranžové kolečko nepřestane rotovat)<br/>Nevypínejte prohlížeč, nezavírejte okno. <strong>Vážně! Fakt. Opravdu!</strong><br/>
Pokud se objeví červený trojúhelník  po ukončení prvního kroku, ověřte, jestli jsou opravdu v ATS připravená data, v ostatních případech kontaktujte <a href="mailto:zjk@honeypot.cz">správce aplikace</a>.</p>
<div id='generator'>

<?php   

    /* kontrola */
    $checkquery = "Select count(*) AS KOLIK From reportMonthly   Where year = ".$year." And month = ".$month;
    $res = mysql_query($checkquery);
    if ($res && @mysql_num_rows($res) > 0) {
    while ($row = @mysql_fetch_array($res)) {
       $kolik = $row['KOLIK'];
    }
    }
    if($kolik > 0) {  
       $finishavisa1 = 1;
       $done2 = 'class=showme';
       ?>
      <div id="avisa1" class="noclick" >[1] Data z ATS OK<span id="inavisa1">
      <i id="inavisa1Fa1" class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
      <i id="inavisa1Fa2 done" class="fa fa-check-square-o"></i>
      <i id="inavisa1Fa3" class="fa fa-exclamation-triangle"></i>     </span>   </div>
      <?php       
    }
    else{
       $finishavisa1 = 0;
       ?>
      <div id="avisa1"  onClick="avisaFromAts(<?php echo $month.','.$year; ?>)"   >
      [1] Načíst data z ATS 
      <span id="inavisa1">
      <i id="inavisa1Fa1" class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
      <i id="inavisa1Fa2" class="fa fa-check-square-o"></i>
      <i id="inavisa1Fa3" class="fa fa-exclamation-triangle"></i>   
      </span>
      </div>
       <?php
    }
   ?>
      <div id="avisa1fake" class="noclick" >[1] Data z ATS OK<span id="inavisa1">
      <i id="inavisa1FFa1" class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
      <i id="inavisa1FFa2" class="fa fa-check-square-o"></i>
      <i id="inavisa1FFa3" class="fa fa-exclamation-triangle"></i>     </span>   </div>

<?php
 // echo '/data/www/darcovskasms.cz/avizo/*'.$year.'-'.$month.'.pdf';
// echo '/data/www/darcovskasms.cz/avizo/*'.$year.'-'.$month.'.pdf';
   $list = glob('/data/www/darcovskasms.cz/avizo/*'.$year.'-'.$month.'.pdf');
 // echo count($list);
   if(count($list) > 0) { 
   $finishavisa2 = 1;
   $done3 = 'class=showme';
   $click2 = "class='showme noclick'";
?>

   <div   id="avisa2" <?php echo $click2; ?>  >  [2] Avíza <?php echo count($list); ?>x OK
    <span id="inavisa2">
    <i id="inavisa2Fa1" class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
    <i id="inavisa2Fa2 done" class="fa fa-check-square-o"></i>
    <i id="inavisa2Fa3" class="fa fa-exclamation-triangle"></i>   
    </span>
</div>
      <?php       
    }
    else{
       $finishavisa2 = 0;
       ?>

<div id="avisa2" <?php echo $done2; ?> onClick="avisaGenerate(<?php echo $month.','.$year; ?>)" >
  [2] Vygenerovat avíza
  <span id="inavisa2">
    <i id="inavisa2Fa1" class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
    <i id="inavisa2Fa2" class="fa fa-check-square-o"></i>
    <i id="inavisa2Fa3" class="fa fa-exclamation-triangle"></i>   
  </span>
</div>
       <?php
    }
   ?>

      <div id="avisa2fake" class="noclick" >[2] Avíza <?php echo count($list); ?>x OK<span id="inavisa1">
      <i id="inavisa2FFa1" class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
      <i id="inavisa2FFa2" class="fa fa-check-square-o"></i>
      <i id="inavisa2FFa3" class="fa fa-exclamation-triangle"></i>     </span>   </div>



<a href="https://www.darcovskasms.cz/2017/jobs/generateBankDataTestZJK.php?year=<?php echo $year; ?>&month=<?php echo $month; ?>"   id="avisa3"  <?php echo $done3; ?>   >
  [3] Stáhnout soubor do banky
  <span id="inavisa3">
    <i id="inavisa3Fa1" class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
    <i id="inavisa3Fa2" class="showme  fa fa-check-square-o"></i>
    <i id="inavisa3Fa3" class="fa fa-exclamation-triangle"></i>   
  </span>
</a>




<?php   

    /* kontrola */
    $checkquery = "Select mailCount,smallDate,year,month   From tbl_app_sended_notifications   Where year = ".$year." And month = ".$month;
    $res = mysql_query($checkquery);
    if ($res && @mysql_num_rows($res) > 0) {
    while ($row = @mysql_fetch_array($res)) {
       $kolik2 = $row['mailCount'];
       $kdy = $row['smallDate'];
    }
    }
    if(($kolik2)||(($year<=2017)&&($month<8))) {  
       $finishavisa4 = 1;
      $done4 =  'class=showme';
       ?>
<div id="avisa4"  class="showme noclick" >
  [4] E-mail <?php echo $kolik2; ?>x OK (<?php echo $kdy; ?>)
  <span id="inavisa4">
    <i id="inavisa4Fa1" class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
    <i id="inavisa4FFa2" class="showme fa fa-check-square-o"></i>
    <i id="inavisa4Fa3" class="fa fa-exclamation-triangle"></i>   
  </span>
</div>
    <?php }
    else{
    ?>
<div   id="avisa4" <?php echo $done3; ?>  onClick="avisaSendMail(<?php echo $month.','.$year; ?>)">
  [4] Rozeslat e-mail o avízech
  <span id="inavisa4">
    <i id="inavisa4Fa1" class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
    <i id="inavisa4Fa2" class="fa fa-check-square-o"></i>
    <i id="inavisa4Fa3" class="fa fa-exclamation-triangle"></i>   
  </span>
</div>
     <?php
    }
    ?>
      <div id="avisa4fake" class="noclick" >[1] Rozesláno <i id='wrong'>s chybami</i></span><span id="inavisa4">
      <i id="inavisa4FFa1" class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
      <i id="inavisa4FFa2" class="fa fa-check-square-o"></i>
      <i id="inavisa4FFa3" class="fa fa-exclamation-triangle"></i>     </span>   </div>






 

</div>
<?php

 
echo "<br/><div style='float:right; background: #e7e7e7; padding: 0 3px; line-height: 2em;'>Celkem: ".$tot."</div>";
?>
<span id="copytext" style="height:150;width:162;  display: none"><?php echo $emailstring; ?></span> 
<textarea id="holdtext" style="display:none;"></TEXTAREA>

 
<div style="float: right">
<!--<a href="mailto:dmsasistent@donorsforum.cz?subject=DMS&bcc=<?php echo $emailstring; ?>" title="Generovat Outlook Msg"><img style="border: none; margin-right: 10px;"src="./images/outlook.jpg"></a>
-->
</div>





<br/>
<h1>
<a href='./databaseAVIZA.php?where=<?php echo $where; ?>'>Excel</a> 
</h1>
 
