<h1>Správa uživatelů</h1>

<style>
	.graph{
		width: 500px;
		height: 350px; 
		margin-left: -40px;
		margin-top: -60px;
	}

	.graphFull {
		float: left;
		width: auto;
		margin: 1% 0;
		width: 350px;
		height: 250px; 
		background: #F4F8FA;
		border-color: #D4E5EC;
		position: relative;
		float: left;
		margin: 0% 1% 1% 0;

		border-style: solid;
		border-width: 1px;
		-webkit-border-radius: 3px;
		-moz-border-radius: 3px;
		border-radius: 3px;
		overflow: hidden;
		float: left;
	}
	.data {
		height: 10px;
		width: 100%;
	}
	.data a{
		display: block;
		padding: 3px 5px;
		margin-right: 3px;
		margin-bottom: 5px;
		border-style: solid;
		border-width: 1px;
		-webkit-border-radius: 3px;
		-moz-border-radius: 3px;
		border-radius: 3px; 
		background: #F4F8FA;
		border-color: #D4E5EC;
		float:left;
		font-family: 'Dosis', sans-serif;
		color: #5296C1;
		font-size: 10px;
	}
	.data a:hover, .data a.active{

		background: #E9F2F5;
	}

	.graphFull h3 {
		padding: 0.5em;
		font-size: 1.2em;
		font-weight: normal;
		font-family: 'Dosis', sans-serif;
		font-size: 100%;
		color: #5296C1;
		margin: 0;
		background-color: #E9F2F5;
		border: none;
	}
</style>

<?php
// var_dump($_POST);
$info_text = "";
//photo
$full_path = $globalgal . "/images/users/";
$fullpathico = $globalgal . "/images/usersthumbs/";
$time = time();
$file_name = date("d_m_y", $time) . "TZ" . $time;
$file_name2 = date("d_m_y", $time) . "TZ" . $time;
$table_name = "pool_tbluser";
$page_name = "poolusers";
$deletext = "Uživatel byl smazán.";
$updatext = "Uživatel byl opraven.";
$addtext = "Uživatel byla přidán.";
$jeho = "Uživatele";

function deletePicture ($newsid, $column, $table_name) {
	include "../sql/db.php";
	$full_path = "../images/users/";
	$fullpathico = "../images/usersthumbs/";
	$query = "SELECT " . $column . " FROM " . $table_name . " WHERE ID=" . $newsid;
// echo $query;
	$result = @mysql($db, $query);
	if (($result) && (mysql_numrows($result) > 0))
	{
		while ($row = mysql_fetch_array($result))
		{
			if ((strlen($row[$column]) > 0) && ($row[$column] != NULL))
			{
				@unlink($full_path . $row[$column]);
				@unlink($fullpathico . $row[$column]);
			}
		}
	}
}

if (isset($_REQUEST["delete"]) && is_numeric($_REQUEST["delete"]) && $_REQUEST["delete"] > 0)
{
	$delete = $_REQUEST["delete"];

	$query = "Update " . $table_name . " set Deleted = 1 WHERE ID = " . $_REQUEST["delete"];
	deletePicture($delete, 'UFoto', $table_name);
	$del_res = @mysql_query($query);
	if ($del_res)
	{
		if (mysql_affected_rows($link) > 0)
		{
			$info_text .= "<br/>Uživatel byl smazán.";
		}
		else
		{
			$info_text .= "<br/>Uživatel nebyl smazán.";
		}
	}
	else
	{
		$info_text .= "<br\>Chyba během mazání uživatele.";
	}
}
if (is_uploaded_file(@$_FILES["userFoto"]['tmp_name']))
{

	$path_parts = @pathinfo($_FILES["userFoto"]['name'], PATHINFO_EXTENSION);

	if ($path_parts == "jpeg" || $path_parts == "jpg" || $path_parts == "JPEG" || $path_parts == "JPG")
	{

		$image_filename = strtotime("now") . "." . $path_parts;

		if (!move_uploaded_file($_FILES["userFoto"]['tmp_name'], $full_path . $image_filename))
		{
			$info_text .= "<br/>Nepovedlo se uložit fotku";
		}
		else
		{
			$info_text .= "";
		}
		$new_image_width = 50;
		$new_image_height = 50;

		$image_dims = getimagesize($full_path . $image_filename);
		$image_width = $image_dims[0];
		$image_height = $image_dims[1];

		if ($image_width < 3000 && $image_width > 50 && $image_height < 3000 && $image_height > 50)
		{


			$ratio = $image_width / $image_height; //pomer puvodni fotky
			$new_ratio = $new_image_width / $new_image_height;  //pomer pozadovany
			if ($ratio > $new_ratio)
			{
				$new_image_height2 = $new_image_height;
				$change_ratio = $image_height / $new_image_height2;  // pomer zmeny
				$new_image_width2 = $image_width / $change_ratio;
				$new_image_width2 = intval($new_image_width2);
				$new_image_height2 = intval($new_image_height2);
				$img = imagecreatefromjpeg($full_path . $image_filename);
				// color thumb
				$thumb = @imagecreatetruecolor($new_image_width, $new_image_height);
				$white = @imagecolorallocate($thumb, 255, 255, 255);
				$help = @imagecreatetruecolor($new_image_width2, $new_image_height2);
				@imagefill($thumb, 0, 0, $white);
				imagecopyresampled($help, $img, 0, 0, 0, 0, $new_image_width2, $new_image_height2, $image_width, $image_height);
				imagecopy($thumb, $help, 0, 0, 0, 0, $image_width, $image_height);
				imagejpeg($thumb, $fullpathico . $image_filename);
			}
			else
			{
				$new_image_width2 = $new_image_width;
				$change_ratio = $image_width / $new_image_width2;  // pomer zmeny
				$new_image_height2 = $image_height / $change_ratio;
				$new_image_width2 = intval($new_image_width2);
				$new_image_height2 = intval($new_image_height2);
				$img = imagecreatefromjpeg($full_path . $image_filename);
				// color thumb
				$thumb = @imagecreatetruecolor($new_image_width, $new_image_height);
				$white = @imagecolorallocate($thumb, 255, 255, 255);
				$help = @imagecreatetruecolor($new_image_width2, $new_image_height2);
				@imagefill($thumb, 0, 0, $white);
				imagecopyresampled($help, $img, 0, 0, 0, 0, $new_image_width2, $new_image_height2, $image_width, $image_height);
				imagecopy($thumb, $help, 0, 0, 0, 0, $image_width, $image_height);
				imagejpeg($thumb, $fullpathico . $image_filename);
			}
		}
	}
	else
	{
		$info_text .= "<br/>Špatný formát fotky";
	}
}
else
{
	//$info_text .= "<br/>Bez fotky";
}


//konec mazani

if (isset($_REQUEST["send"]) && isset($_REQUEST["newsid"]) && is_numeric($_REQUEST["newsid"]) && $_REQUEST["newsid"] > 0)
{
	if (strlen(trim($_POST["user_login"])))
	{

		$query = "Update " . $table_name . " set ";

		$col_query = " ULogin";
		$val_query = "  '" . trim(strip_tags($_POST["user_login"])) . "'";
		$query .= $col_query . " = " . $val_query;

		$col_query = ", UName";
		$val_query = "  '" . trim(strip_tags($_POST["user_name"])) . "'";
		$query .= $col_query . " = " . $val_query;

		$col_query = ", USurname";
		$val_query = "  '" . trim($_POST["user_surname"]) . "'";
		$query .= $col_query . " = " . $val_query;

		$col_query = ", UHis";
		$val_query = "  '" . trim($_POST["UHis"]) . "'";
		$query .= $col_query . " = " . $val_query;

		$col_query = ", UType";
		$val_query = "  " . intval($_POST["user_level"]) . "";
		$query .= $col_query . " = " . $val_query;

		if (isset($image_filename) && strlen($image_filename) > 0)
		{
			$col_query = ", UFoto";
			$val_query = " '" . $image_filename . "'";
			$query .= $col_query . " = " . $val_query;
			deletePicture($newsid, 'UFoto', $table_name);
		}

		if (strlen($_POST["user_pass"]))
		{
			if ($_POST["user_pass"] == $_POST["user_pass_repeat"])
			{
				$col_query = ", UPass";
				$val_query = "  PASSWORD('" . trim($_POST["user_pass"]) . "')";
				$query .= $col_query . " = " . $val_query;
			}
		}


		if (isset($_POST["can_login"]))
		{
			$col_query = ", UCanLogin";
			$val_query = "  1";
			$query .= $col_query . " = " . $val_query;
		}
		else
		{
			$col_query = ", UCanLogin";
			$val_query = "  0";
			$query .= $col_query . " = " . $val_query;
		}

		$query .= " where ID = " . $_REQUEST["newsid"];

		$res = @mysql_query($query);

		if ($res && @mysql_affected_rows($link) > 0)
		{
			$info_text .= "<br/>Uživatel úspěně uložen.";

			$priv_query = "Update tblpriviledgeuser set Deleted = 1 where PUUser = " . $_REQUEST["newsid"];
			$priv_res = @mysql_query($priv_query);

			if (isset($_REQUEST["privs"]) && is_array($_REQUEST["privs"]))
			{
				foreach ($_REQUEST["privs"] as $priv)
				{
					$ins_query = "Insert into tblpriviledgeuser (PUUser, PUPriviledge) values (" . $_REQUEST["newsid"] . "," . $priv . ")";
					$ins_res = @mysql_query($ins_query);
				}
			}
		}
		else
		{
			$info_text .= "<br/>Uživatel úspěně uložen.";

			$priv_query = "Update tblpriviledgeuser set Deleted = 1 where PUUser = " . $_REQUEST["newsid"];
			$priv_res = @mysql_query($priv_query);

			if (isset($_REQUEST["privs"]) && is_array($_REQUEST["privs"]))
			{
				foreach ($_REQUEST["privs"] as $priv)
				{
					$ins_query = "Insert into tblpriviledgeuser (PUUser, PUPriviledge) values (" . $_REQUEST["newsid"] . "," . $priv . ")";
					$ins_res = @mysql_query($ins_query);
				}
			}
		}
	}
	else
	{
		$info_text .= "<br/>Chyba během ukládání uživatele. Nebyly zadány všechny povinné parametry";
	}
}
//posila novou
else
if (@$_REQUEST["send"])
{

	if (strlen(trim(@$_POST["user_login"])))
	{

		$query = "Insert into " . $table_name;

		$col_query = " ULogin";
		$val_query = "  '" . trim(strip_tags($_POST["user_login"])) . "'";

		$col_query .=", UName";
		$val_query .=" , '" . trim(strip_tags($_POST["user_name"])) . "'";

		$col_query .=", USurname";
		$val_query .=" , '" . trim($_POST["user_surname"]) . "'";

		$col_query .=", UHis";
		$val_query .=" , '" . trim($_POST["UHis"]) . "'";



		if (isset($image_filename) && strlen($image_filename) > 0)
		{
			$col_query .= ", UFoto";
			$val_query .= ", '" . $image_filename . "'";
		}

		if (strlen($_POST["user_pass"]))
		{
			if ($_POST["user_pass"] == $_POST["user_pass_repeat"])
			{
				$col_query .=", UPass";
				$val_query .=",  PASSWORD('" . trim($_POST["user_pass"]) . "')";
			}
		}


		if (isset($_POST["can_login"]))
		{
			$col_query .=", UCanLogin";
			$val_query .=",  1";
		}
		else
		{
			$col_query .=", UCanLogin";
			$val_query .=" , 0";
		}

		$query .= " (" . $col_query . ") values (" . $val_query . ")";

		$res = mysql_query($query);

		if ($res && @mysql_affected_rows($link) > 0)
		{
			$info_text .= "<br/>Uživatel úspěšně uložen.";
		}
		else
		{
			$info_text .= "<br/>Uživatel nebyl uložen.";
		}
	}
	else
	{
		$info_text .= "<br/>Chyba během ukládání uživatele. Nebyly zadány všechny povinné parametry!";
	}
}


// ende
if (@$info_text != "")
{
	echo "<h3>" . $info_text . "</h3>";
}

$time = time();
$ano11 = $ne11 = $fuk11 = $nic11 = $ano10 = $ne10 = $fuk10 = $nic10 = $ano1 = $ne1 = $fuk1 = $nic1 = $ano8 = $ne8 = $fuk8 = $nic8 = $ano7 = $ne7 = $fuk7 = $nic7 = $ano6 = $ne6 = $fuk6 = $nic6 = $ano5 = $ne5 = $fuk5 = $nic5 = $ano4 = $ne4 = $fuk4 = $nic4 = $ano3 = $ne3 = $fuk3 = $nic3 = $ano9 = $ne9 = $fuk9 = $nic9 = $ano2 = $ne2 = $fuk2 = $nic2 = 0;
$result = @mysql_query("SELECT * FROM $table_name where Deleted = 0 ORDER BY UHis,USurname,UName,ULogin");

if (@mysql_num_rows($result) > 0)
{
	while ($resarr = @mysql_fetch_array($result))
	{
		$newsid = $resarr["ID"];
		$text = $resarr["USurname"] . ", " . $resarr["UName"] . " - " . $resarr["ULogin"];
		$time = $resarr["konfTime"];
		$nadace = $resarr["UFirm"];
		$katx = $resarr["UHis"];
		if ($katx == 3)
		{
			$kategorie = " N";
		}
		if ($katx == 1)
		{
			$kategorie = " NF";
		}
		if ($katx == 2)
		{
			$kategorie = " FNaF";
		}

		$vote1 = $resarr["vote1"];
		$vote2 = $resarr["vote2"];
		$vote3 = $resarr["vote3"];
		$vote4 = $resarr["vote4"];
		$vote5 = $resarr["vote5"];
		$vote6 = $resarr["vote6"];
		$vote7 = $resarr["vote7"];
		$vote8 = $resarr["vote8"];
		$vote9 = $resarr["vote9"];
		$vote10 = $resarr["vote10"];
		$vote11 = $resarr["vote11"];




		$second = $resarr["konfSecond"];

		if ($resarr["UHis"] == 3)
		{
			if ($vote1 == 1)
			{
				$ano1++;
				$first = "<span style='background:#AEEFAA; width: 30px;display:block; float: left; '>ANO</span>";
			}
			if ($vote1 == 2)
			{
				$ne1++;
				$first = "<span style='background:#FFDAD8; width: 30px;display:block; float: left; '>NE </span>";
			}
			if ($vote1 == 3)
			{
				$fuk1++;
				$first = "<span style='background:#e7e7e7; width: 30px; display:block; float: left; ;'>ZDR</span>";
			}
			if ($vote1 == 0)
			{
				$nic1++;
				$first = "<span style='background:#e7e7e7; width: 30px; display:block; float: left; '>---</span>";
			}

			if ($vote2 == 1)
			{
				$ano2++;
				$dva = "<span style='background:#AEEFAA; width: 30px;display:block; float: left; '>ANO</span>";
			}
			if ($vote2 == 2)
			{
				$ne2++;
				$dva = "<span style='background:#FFDAD8; width: 30px;display:block; float: left; '>NE </span>";
			}
			if ($vote2 == 3)
			{
				$fuk2++;
				$dva = "<span style='background:#e7e7e7; width: 30px; display:block; float: left; ;'>ZDR</span>";
			}
			if ($vote2 == 0)
			{
				$nic2++;
				$dva = "<span style='background:#e7e7e7; width: 30px; display:block; float: left; '>---</span>";
			}

			if ($vote3 == 1)
			{
				$ano3++;
				$tri = "<span style='background:#AEEFAA; width: 30px;display:block; float: left; '>ANO</span>";
			}
			if ($vote3 == 2)
			{
				$ne3++;
				$tri = "<span style='background:#FFDAD8; width: 30px;display:block; float: left; '>NE </span>";
			}
			if ($vote3 == 3)
			{
				$fuk3++;
				$tri = "<span style='background:#e7e7e7; width: 30px; display:block; float: left; ;'>ZDR</span>";
			}
			if ($vote3 == 0)
			{
				$nic3++;
				$tri = "<span style='background:#e7e7e7; width: 30px; display:block; float: left; '>---</span>";
			}

			if ($vote4 == 1)
			{
				$ano4++;
				$ctyri = "<span style='background:#AEEFAA; width: 30px;display:block; float: left; '>ANO</span>";
			}
			if ($vote4 == 2)
			{
				$ne4++;
				$ctyri = "<span style='background:#FFDAD8; width: 30px;display:block; float: left; '>NE </span>";
			}
			if ($vote4 == 3)
			{
				$fuk4++;
				$ctyri = "<span style='background:#e7e7e7; width: 30px; display:block; float: left; ;'>ZDR</span>";
			}
			if ($vote4 == 0)
			{
				$nic4++;
				$ctyri = "<span style='background:#e7e7e7; width: 30px; display:block; float: left; '>---</span>";
			}

			if ($vote11 == 1)
			{
				$ano11++;
				$devet = "<span style='background:#AEEFAA; width: 30px;display:block; float: left; '>ANO</span>";
			}
			if ($vote11 == 2)
			{
				$ne11++;
				$devet = "<span style='background:#FFDAD8; width: 30px;display:block; float: left; '>NE </span>";
			}
			if ($vote11 == 3)
			{
				$fuk11++;
				$devet = "<span style='background:#e7e7e7; width: 30px; display:block; float: left; ;'>ZDR</span>";
			}
			if ($vote11 == 0)
			{
				$nic11++;
				$devet = "<span style='background:#e7e7e7; width: 30px; display:block; float: left; '>---</span>";
			}
		}
		else
		{
			$first = $dva = $tri = $ctyri = "<span style='background:#efefef; width: 30px; display:block; float: left; color: #c0c0c0 '>&nbsp;</span>";
		}

		if ($resarr["UHis"] == 2)
		{
			if ($vote5 == 1)
			{
				$ano5++;
				$pet = "<span style='background:#AEEFAA; width: 30px;display:block; float: left; '>ANO</span>";
			}
			if ($vote5 == 2)
			{
				$ne5++;
				$pet = "<span style='background:#FFDAD8; width: 30px;display:block; float: left; '>NE </span>";
			}
			if ($vote5 == 3)
			{
				$fuk5++;
				$pet = "<span style='background:#e7e7e7; width: 30px; display:block; float: left; ;'>ZDR</span>";
			}
			if ($vote5 == 0)
			{
				$nic5++;
				$pet = "<span style='background:#e7e7e7; width: 30px; display:block; float: left; '>---</span>";
			}

			if ($vote6 == 1)
			{
				$ano6++;
				$sest = "<span style='background:#AEEFAA; width: 30px;display:block; float: left; '>ANO</span>";
			}
			if ($vote6 == 2)
			{
				$ne6++;
				$sest = "<span style='background:#FFDAD8; width: 30px;display:block; float: left; '>NE </span>";
			}
			if ($vote6 == 3)
			{
				$fuk6++;
				$sest = "<span style='background:#e7e7e7; width: 30px; display:block; float: left; ;'>ZDR</span>";
			}
			if ($vote6 == 0)
			{
				$nic6++;
				$sest = "<span style='background:#e7e7e7; width: 30px; display:block; float: left; '>---</span>";
			}

			if ($vote10 == 1)
			{
				$ano10++;
				$devet = "<span style='background:#AEEFAA; width: 30px;display:block; float: left; '>ANO</span>";
			}
			if ($vote10 == 2)
			{
				$ne10++;
				$devet = "<span style='background:#FFDAD8; width: 30px;display:block; float: left; '>NE </span>";
			}
			if ($vote10 == 3)
			{
				$fuk10++;
				$devet = "<span style='background:#e7e7e7; width: 30px; display:block; float: left; ;'>ZDR</span>";
			}
			if ($vote10 == 0)
			{
				$nic10++;
				$devet = "<span style='background:#e7e7e7; width: 30px; display:block; float: left; '>---</span>";
			}
		}
		else
		{
			$pet = $sest = "<span style='background:#efefef; width: 30px; display:block; float: left; color: #c0c0c0 '>&nbsp;</span>";
		}


		if ($resarr["UHis"] == 1)
		{
			if ($vote7 == 1)
			{
				$ano7++;
				$sedm = "<span style='background:#AEEFAA; width: 30px;display:block; float: left; '>ANO</span>";
			}
			if ($vote7 == 2)
			{
				$ne7++;
				$sedm = "<span style='background:#FFDAD8; width: 30px;display:block; float: left; '>NE </span>";
			}
			if ($vote7 == 3)
			{
				$fuk7++;
				$sedm = "<span style='background:#e7e7e7; width: 30px; display:block; float: left; ;'>ZDR</span>";
			}
			if ($vote7 == 0)
			{
				$nic7++;
				$sedm = "<span style='background:#e7e7e7; width: 30px; display:block; float: left; '>---</span>";
			}

			if ($vote9 == 1)
			{
				$ano9++;
				$devet = "<span style='background:#AEEFAA; width: 30px;display:block; float: left; '>ANO</span>";
			}
			if ($vote9 == 2)
			{
				$ne9++;
				$devet = "<span style='background:#FFDAD8; width: 30px;display:block; float: left; '>NE </span>";
			}
			if ($vote9 == 3)
			{
				$fuk9++;
				$devet = "<span style='background:#e7e7e7; width: 30px; display:block; float: left; ;'>ZDR</span>";
			}
			if ($vote9 == 0)
			{
				$nic9++;
				$devet = "<span style='background:#e7e7e7; width: 30px; display:block; float: left; '>---</span>";
			}
		}
		else
		{
			$sedm = "<span style='background:#efefef; width: 30px; display:block; float: left; color: #c0c0c0 '>&nbsp;</span>";
		}








		// tady je clearing> UPDATE `pool_tbluser` SET `konfTime` = '0000-00-00 00:00:00' , konfIP = '', vote1 = 0, vote2 = 0, vote3 = 0, vote4 = 0, vote5 = 0, vote6 = 0, vote7 = 0, vote8 = 0, vote9 = 0

		$ip = "<span style=' width: 70px;'>" . $resarr["konfIP"] . "</span>";
		?>
		<div class="prehled" style=' width: 1320px; background: #fff'>
			<div style="float:left;">
				<em class="w4">
					<a href="index.php?id=<?php echo $page_name; ?>new&amp;newsid=<?php echo $newsid; ?>">
						<img src="./pictures/edit.gif" alt="Opravit" title="Opravit" border="0"></a>
					<a onClick="return confirm('Skutečně chcete <?php echo $jeho; ?> vymazat z databáze?')" href="index.php?id=<?php echo $page_name; ?>&amp;delete=<?php echo $newsid; ?>">
						<img src="./pictures/delete.gif" alt="Smazat" title="Smazat" border="0"></a>
				</em>
				<p style='display:block; float: left; width: 390px;'>
		<?php //echo $devet;  ?>
		<?php echo $first; ?> 
		<?php echo $dva; ?>
		<?php echo $tri; ?>
		<?php echo $ctyri; ?>
		<?php echo $pet; ?>
		<?php echo $sest; ?>
		<?php echo $sedm; ?>


		<?php echo $time; ?> |</p>
				<a href="index.php?id=<?php echo $page_name; ?>new&amp;newsid=<?php echo $newsid; ?>"><strong><?php echo strip_tags($text), " "; ?></strong> | <?php echo $nadace; ?> | <?php echo $kategorie; ?></a>
			</div>
			<img style="float:right" src="./pictures/<?php if ($resarr["UCanLogin"] == 1)
		{
			echo "true.gif\" alt=\"Uživatel se smí přihlásit\" title=\"Uživatel se smí přihlásit\"";
		}
		else
		{
			echo "false.gif\" alt=\"Uživatel se nesmí přihlásit\" title=\"Uživatel se nesmí přihlásití\"";
		} ?>  border="0">
		<?php echo $ip; ?>
		</div>                                     
		<?php
	}
}
?>


<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
google.load("visualization", "1", {packages: ["corechart"]});
google.setOnLoadCallback(drawChart9);
function drawChart9() {
	var data = google.visualization.arrayToDataTable([
		['Stanovy', 'Hlasování'],
		['(<?php echo $ano9; ?>) PRO', <?php echo $ano9; ?>], ['(<?php echo $fuk9; ?>) Zdržel se', <?php echo $fuk9; ?>], ['(<?php echo $nic9; ?>) Nehlasoval', <?php echo $nic9; ?>], ['(<?php echo $ne9; ?>) PROTI', <?php echo $ne9; ?>], ]);
	var options = {
		//is3D: true,
		fontSize: 9,
		backgroundColor: 'transparent',
		slices: {
			25: {color: "#c0c0c0"}, //neutral
			1: {color: "#F0CC86"}, //plus
			2: {color: "#c0c0c0"}, //minus
			0: {color: "#469A44"}, //chvala
			3: {color: "#d6152d"}, //kritika
		},
	};
	// var chart = new google.visualization.PieChart(document.getElementById('piechart'));
	var chart = new google.visualization.PieChart(document.getElementById('piechart_9d'));
	chart.draw(data, options);
}


google.load("visualization", "1", {packages: ["corechart"]});
google.setOnLoadCallback(drawChart10);
function drawChart10() {
	var data = google.visualization.arrayToDataTable([
		['Stanovy', 'Hlasování'],
		['(<?php echo $ano10; ?>) PRO', <?php echo $ano10; ?>], ['(<?php echo $fuk10; ?>) Zdržel se', <?php echo $fuk10; ?>], ['(<?php echo $nic10; ?>) Nehlasoval', <?php echo $nic10; ?>], ['(<?php echo $ne10; ?>) PROTI', <?php echo $ne10; ?>], ]);
	var options = {
		//is3D: true,
		fontSize: 9,
		backgroundColor: 'transparent',
		slices: {
			25: {color: "#c0c0c0"}, //neutral
			1: {color: "#F0CC86"}, //plus
			2: {color: "#c0c0c0"}, //minus
			0: {color: "#469A44"}, //chvala
			3: {color: "#d6152d"}, //kritika
		},
	};
	// var chart = new google.visualization.PieChart(document.getElementById('piechart'));
	var chart = new google.visualization.PieChart(document.getElementById('piechart_10d'));
	chart.draw(data, options);
}

google.load("visualization", "1", {packages: ["corechart"]});
google.setOnLoadCallback(drawChart11);
function drawChart11() {
	var data = google.visualization.arrayToDataTable([
		['Stanovy', 'Hlasování'],
		['(<?php echo $ano11; ?>) PRO', <?php echo $ano11; ?>], ['(<?php echo $fuk11; ?>) Zdržel se', <?php echo $fuk11; ?>], ['(<?php echo $nic11; ?>) Nehlasoval', <?php echo $nic11; ?>], ['(<?php echo $ne11; ?>) PROTI', <?php echo $ne11; ?>], ]);
	var options = {
		//is3D: true,
		fontSize: 9,
		backgroundColor: 'transparent',
		slices: {
			25: {color: "#c0c0c0"}, //neutral
			1: {color: "#F0CC86"}, //plus
			2: {color: "#c0c0c0"}, //minus
			0: {color: "#469A44"}, //chvala
			3: {color: "#d6152d"}, //kritika
		},
	};
	// var chart = new google.visualization.PieChart(document.getElementById('piechart'));
	var chart = new google.visualization.PieChart(document.getElementById('piechart_11d'));
	chart.draw(data, options);
}



google.setOnLoadCallback(drawChart1);
function drawChart1() {
	var data = google.visualization.arrayToDataTable([
		['Hlasování', 'Počet'],
		['(<?php echo $ano1; ?>) PRO', <?php echo $ano1; ?>], ['(<?php echo $fuk1; ?>) Zdržel se', <?php echo $fuk1; ?>], ['(<?php echo $nic1; ?>) Nehlasoval', <?php echo $nic1; ?>], ['(<?php echo $ne1; ?>) PROTI', <?php echo $ne1; ?>], ]);
	var options = {
		//is3D: true,
		fontSize: 9,
		backgroundColor: 'transparent',
		slices: {
			25: {color: "#c0c0c0"}, //neutral
			1: {color: "#F0CC86"}, //plus
			2: {color: "#c0c0c0"}, //minus
			0: {color: "#469A44"}, //chvala
			3: {color: "#d6152d"}, //kritika
		},
	};
	// var chart = new google.visualization.PieChart(document.getElementById('piechart'));
	var chart = new google.visualization.PieChart(document.getElementById('piechart_1d'));
	chart.draw(data, options);
}



google.setOnLoadCallback(drawChart2);
function drawChart2() {
	var data = google.visualization.arrayToDataTable([
		['Hlasování', 'Počet'],
		['(<?php echo $ano2; ?>) PRO', <?php echo $ano2; ?>], ['(<?php echo $fuk2; ?>) Zdržel se', <?php echo $fuk2; ?>], ['(<?php echo $nic2; ?>) Nehlasoval', <?php echo $nic2; ?>], ['(<?php echo $ne2; ?>) PROTI', <?php echo $ne2; ?>], ]);
	var options = {
		//is3D: true,
		fontSize: 9,
		backgroundColor: 'transparent',
		slices: {
			25: {color: "#c0c0c0"}, //neutral
			1: {color: "#F0CC86"}, //plus
			2: {color: "#c0c0c0"}, //minus
			0: {color: "#469A44"}, //chvala
			3: {color: "#d6152d"}, //kritika
		},
	};
	// var chart = new google.visualization.PieChart(document.getElementById('piechart'));
	var chart = new google.visualization.PieChart(document.getElementById('piechart_2d'));
	chart.draw(data, options);
}

google.setOnLoadCallback(drawChart3);
function drawChart3() {
	var data = google.visualization.arrayToDataTable([
		['Hlasování', 'Počet'],
		['(<?php echo $ano3; ?>) PRO', <?php echo $ano3; ?>], ['(<?php echo $fuk3; ?>) Zdržel se', <?php echo $fuk3; ?>], ['(<?php echo $nic3; ?>) Nehlasoval', <?php echo $nic3; ?>], ['(<?php echo $ne3; ?>) PROTI', <?php echo $ne3; ?>], ]);
	var options = {
		//is3D: true,
		fontSize: 9,
		backgroundColor: 'transparent',
		slices: {
			25: {color: "#c0c0c0"}, //neutral
			1: {color: "#F0CC86"}, //plus
			2: {color: "#c0c0c0"}, //minus
			0: {color: "#469A44"}, //chvala
			3: {color: "#d6152d"}, //kritika
		},
	};
	// var chart = new google.visualization.PieChart(document.getElementById('piechart'));
	var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
	chart.draw(data, options);
}


google.setOnLoadCallback(drawChart4);
function drawChart4() {
	var data = google.visualization.arrayToDataTable([
		['Hlasování', 'Počet'],
		['(<?php echo $ano4; ?>) PRO', <?php echo $ano4; ?>], ['(<?php echo $fuk4; ?>) Zdržel se', <?php echo $fuk4; ?>], ['(<?php echo $nic4; ?>) Nehlasoval', <?php echo $nic4; ?>], ['(<?php echo $ne4; ?>) PROTI', <?php echo $ne4; ?>], ]);
	var options = {
		//is3D: true,
		fontSize: 9,
		backgroundColor: 'transparent',
		slices: {
			25: {color: "#c0c0c0"}, //neutral
			1: {color: "#F0CC86"}, //plus
			2: {color: "#c0c0c0"}, //minus
			0: {color: "#469A44"}, //chvala
			3: {color: "#d6152d"}, //kritika
		},
	};
	// var chart = new google.visualization.PieChart(document.getElementById('piechart'));
	var chart = new google.visualization.PieChart(document.getElementById('piechart_4d'));
	chart.draw(data, options);
}


google.setOnLoadCallback(drawChart5);
function drawChart5() {
	var data = google.visualization.arrayToDataTable([
		['Hlasování', 'Počet'],
		['(<?php echo $ano5; ?>) PRO', <?php echo $ano5; ?>], ['(<?php echo $fuk5; ?>) Zdržel se', <?php echo $fuk5; ?>], ['(<?php echo $nic5; ?>) Nehlasoval', <?php echo $nic5; ?>], ['(<?php echo $ne5; ?>) PROTI', <?php echo $ne5; ?>], ]);
	var options = {
		//is3D: true,
		fontSize: 9,
		backgroundColor: 'transparent',
		slices: {
			25: {color: "#c0c0c0"}, //neutral
			1: {color: "#F0CC86"}, //plus
			2: {color: "#c0c0c0"}, //minus
			0: {color: "#469A44"}, //chvala
			3: {color: "#d6152d"}, //kritika
		},
	};
	// var chart = new google.visualization.PieChart(document.getElementById('piechart'));
	var chart = new google.visualization.PieChart(document.getElementById('piechart_5d'));
	chart.draw(data, options);
}


google.setOnLoadCallback(drawChart6);
function drawChart6() {
	var data = google.visualization.arrayToDataTable([
		['Hlasování', 'Počet'],
		['(<?php echo $ano6; ?>) PRO', <?php echo $ano6; ?>], ['(<?php echo $fuk6; ?>) Zdržel se', <?php echo $fuk6; ?>], ['(<?php echo $nic6; ?>) Nehlasoval', <?php echo $nic6; ?>], ['(<?php echo $ne6; ?>) PROTI', <?php echo $ne6; ?>], ]);
	var options = {
		//is3D: true,
		fontSize: 9,
		backgroundColor: 'transparent',
		slices: {
			25: {color: "#c0c0c0"}, //neutral
			1: {color: "#F0CC86"}, //plus
			2: {color: "#c0c0c0"}, //minus
			0: {color: "#469A44"}, //chvala
			3: {color: "#d6152d"}, //kritika
		},
	};
	// var chart = new google.visualization.PieChart(document.getElementById('piechart'));
	var chart = new google.visualization.PieChart(document.getElementById('piechart_6d'));
	chart.draw(data, options);
}


google.setOnLoadCallback(drawChart7);
function drawChart7() {
	var data = google.visualization.arrayToDataTable([
		['Hlasování', 'Počet'],
		['(<?php echo $ano7; ?>) PRO', <?php echo $ano7; ?>], ['(<?php echo $fuk7; ?>) Zdržel se', <?php echo $fuk7; ?>], ['(<?php echo $nic7; ?>) Nehlasoval', <?php echo $nic7; ?>], ['(<?php echo $ne7; ?>) PROTI', <?php echo $ne7; ?>], ]);
	var options = {
		//is3D: true,
		fontSize: 9,
		backgroundColor: 'transparent',
		slices: {
			25: {color: "#c0c0c0"}, //neutral
			1: {color: "#F0CC86"}, //plus
			2: {color: "#c0c0c0"}, //minus
			0: {color: "#469A44"}, //chvala
			3: {color: "#d6152d"}, //kritika
		},
	};
	// var chart = new google.visualization.PieChart(document.getElementById('piechart'));
	var chart = new google.visualization.PieChart(document.getElementById('piechart_7d'));
	chart.draw(data, options);
}

</script>
<div class='mainclr'></div>
<p>&nbsp;</p>
<!--

<div class="graphFull"   >
<h3  >Stanovy - Nadační fondy</h3>        
<div class="graph  " id="xpiechart_9d" ></div>
</div> 




<div class="graphFull"   >
<h3  >Stanovy - Firemní nadace a fondy</h3>        
<div class="graph  " id="piechart_10d" ></div>
</div> 

 

<div class="graphFull"   >
<h3  >Stanovy - Nadace</h3>        
<div class="graph  " id="piechart_11d" ></div>
</div>      
-->
<div class="graphFull"   >
	<h3  >Fodorová Irena</h3>        
	<div class="graph  " id="piechart_1d" ></div>
</div>   

<div class="graphFull"   >
	<h3  >Nedvědická Blanka</h3>        
	<div class="graph  " id="piechart_2d" ></div>
</div> 


<div class="graphFull"   >
	<h3  >Purš Ivo</h3>        
	<div class="graph  " id="piechart_3d" ></div>
</div> 



<div class="graphFull"   >
	<h3  >Štambachová Jitka</h3>        
	<div class="graph  " id="piechart_4d" ></div>
</div>           


<div class="graphFull"   >
	<h3  >Břeská Lenka</h3>        
	<div class="graph  " id="piechart_5d" ></div>
</div>                         


<div class="graphFull"   >
	<h3  >Tornikidis Zuzana</h3>        
	<div class="graph  " id="piechart_6d" ></div>
</div>

<div class="graphFull"   >
	<h3 > Dvořák Jakub </h3>        
	<div class="graph  " id="piechart_7d" ></div>
</div>   

<h1><a href="index.php?id=<?php echo $page_name; ?>new">Přidat další</a></h1>
