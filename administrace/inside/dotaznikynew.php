<style>
        .bignebig{
         display: block;
         float: left;
         margin-top: 4px;
         width: 200px;
        }
        .bigneg{
         width: 200px;
         margin-top: 4px;
 
        } 
        td {  vertical-align: top; }  
        tr{  border-bottom: 1px dotted #D9E8FB; }     
        fieldset table {  width: 830px;}
</style>
<?php
$table_name = "organization_online_self";
$page_name = "dotazniky";

if (isset($_REQUEST["newsid"]) && $_REQUEST["newsid"]>0)
{
    $newsid = $_REQUEST["newsid"];
}
else
{
    $newsid = false;
}


if (@$newsid)
{
    $query = "SELECT * FROM ".$table_name." WHERE ID =".$newsid;
   //echo $query;
    $res = mysql_query($query);
    if ($res && @mysql_num_rows($res)>0)
    {
        $row = mysql_fetch_array($res);
    }
}



?>




   
    <?php
      $checkquery = "Select * From organization_online_self Where ID =".$newsid;
  $checkresult=mysql_query($checkquery);
  if(($checkresult)&&(mysql_numrows($checkresult)>0)){
  while ($row = @mysql_fetch_array($checkresult)){
       $field1 = $row['field1']; 
     $field2 = $row['field2'];
     $field3 = $row['field3'];
     $field4 = $row['field4'];
     $field5 = $row['field5'];
     $field6 = $row['field6'];
     $field7 = $row['field7'];
     $field8 = $row['field8'];
     $field9 = $row['field9'];
     $field10 = $row['field10'];
     $field11 = $row['field11'];
     $field12 = $row['field12'];  
     $description1 = $row['description1']; 
     $description2 = $row['description2'];
     $description3 = $row['description3'];
     $description4 = $row['description4'];   
     $myID =   $row['ID'];
     $edited = $row['edited'] ;
     $companyID = $row['companyID'];  
     
     $comquery = "Select name From organization_online Where ID = ".$companyID;
     $comresult=mysql_query($comquery);
       if(($comresult)&&(mysql_numrows($comresult)>0)){
       while ($comrow = @mysql_fetch_array($comresult)){
          $mycompany = $comrow['name'];
       }
       }
     
        
      }
  }
   function mkRadio($fld,$nr,$post,$fromDB,$txt) {
  
    // $return =  '<div class="xseachinput"><div class="xseachinputpadding"><div class="titleceeper radiokeeper">';

     $return .= '<tr><td><input disabled class="radiosmall" id="'.$fld.$nr.'" type="radio"   name="'.$fld.'" value="'.$nr.'" ';
     if(($post == $nr)||($fromDB == $nr)) { $return .= ' checked '; }
     $return .= ' /></td><td>  ';
          $return .=  $txt.' </td></tr>';
     return $return;
  }   
  
  
  
  
    ?>
<h1>Sebehodnotící  dotazník - <?php echo $mycompany; ?> (Poslední změna: <?php echo $edited; ?> )</h1>
<p>&nbsp;</p>
<form ENCTYPE="multipart/form-data" action="index.php?id=<?php echo $page_name; ?><?php if($newsid>0){echo "&newsid=",$newsid;}?>" method="post" name="noname" id="kontakt">
<div style="text-align:left; width: 540px;" align="center">    
<fieldset><h5>A. Controlling</h5>
<p><b>Strategické řízení organizace - Lidé a organizace </b> </p>    
<table>
<?php   
  echo mkRadio('field1',1,$_POST['field1'],$field1,'Organizace necítí potřebu dlouhodobě plánovat, strategické plány se nesestavují.');
  echo mkRadio('field1',2,$_POST['field1'],$field1,'Odpovědnost za dlouhodobé plánování (zpracování strategie) není stanovena, cíle určuje vedení. Správní rada nenastavuje dlouhodobé cíle a nezabývá se jejich vyhodnocením.');
  echo mkRadio('field1',3,$_POST['field1'],$field1,'Vedení organizace si uvědomuje svou odpovědnost za sestavení dlouhodobých plánů, chybí mu však zkušenosti se zpracováním strategií');
  echo mkRadio('field1',4,$_POST['field1'],$field1,'Je nastavena odpovědnost za zpracování dlouhodobých věcných i finančních plánů v popisu práce určených pracovníků. Při tvorbě strategie se využívají neformální školení. Strategie je schvalována správní radou. ');    
  echo mkRadio('field1',5,$_POST['field1'],$field1,'Je nastavena týmová odpovědnost za zpracování strategie, strategie je schvalována správní radou. Při tvorbě strategie se využívají interní formální školení, nevyužívají se externí znalosti. ');            
?>
</table>

<p><b>Operativní plánování (Roční rozpočtování) - Pravidla a postupy </b> </p>    
<table>
<?php   
  echo mkRadio('field2',1,$_POST['field2'],$field2,'Organizace sestavení finančního rozpočtu nevyžaduje a stanovením postupů se nezabývá. ');    
  echo mkRadio('field2',2,$_POST['field2'],$field2,'Neexistují nastavené postupy pro rozpočtování, činnost je vykonávána pouze s ohledem na případné sledování daňové povinnosti. Přístup ke zpracování finančních rozpočtů je nahodilý. ');
  echo mkRadio('field2',3,$_POST['field2'],$field2,'Sestavování finančního rozpočtu probíhá bez formalizovaného postupu opakovaným způsobem, není zaručena úplnos u nových aktivit, není analyzováno, které položky musí rozpočet u nových aktivit obsahovat, aby byl úplný.');
  echo mkRadio('field2',4,$_POST['field2'],$field2,'Postupy zpracování finančních rozpočtů jsou definovány a formalizovány ve vnitřních předpisech.');
  echo mkRadio('field2',5,$_POST['field2'],$field2,'Je stanovena zodpovědnost za procesy tvorby finančního rozpočtu a v nich stanovené povinnosti; tato odpovědnost se vyžaduje. Postupy se prověřují a dle potřeby aktualizují, proces je tak zdravý a úplný.');           
?>
</table>

<p><b>Manažerské výkaznictví - Pravidla a postupy </b> </p>    
<table>
<?php   
  echo mkRadio('field3',1,$_POST['field3'],$field3,'Manažerské výkaznictví organizace nevyužívá. ');    
  echo mkRadio('field3',2,$_POST['field3'],$field3,'Tvorba reportů je prováděna nahodile, neexistují kromě finančních výkazů jiné standardní reporty, které by měli jasně definovanou strukturu a popsánu náplň jednotlivých položek výkazu. V reportech se vyskytují chyby, na které se náhodně přichází.');
  echo mkRadio('field3',3,$_POST['field3'],$field3,'Proces tvorby a distribuce reportů je směřován vždy k ucelenému období. Popisy reportů neexistují stejně jako definice zdrojových dat pro jednotlivá pole reportů. V reportech se vyskytují chyby sporadicky, odhalovány jsou náhodně.');
  echo mkRadio('field3',4,$_POST['field3'],$field3,'Jsou definována reportovací období a struktura všech předkládaných reportů. Existuje směrnice popisující manažerské výkaznictví. Chyby v reportech jsou nezávisle odhalovány převážně při kontrolách před předložením managementu.');
  echo mkRadio('field3',5,$_POST['field3'],$field3,'Jsou stanoveni jednotliví vlastníci reportů a nad reporty je vybudován kontrolní mechanismus zajišťující jejich konzistentnost a správnost. Systém reportingu je popsán v interní směrnici a vlastník procesu je motivován k jeho zlepšování. Management dostává vždy ověřená data s minimální pravděpodobností výskytu chyby.');   ?>
</table> 

<p><b>Poznámka</b> </p>    
<table>
<tr><td><textarea style='width: 820px; height: 100px; border: none' name="description1" id="area1"   ><?php if($_POST['description1']) { echo $_POST['description1']; }else { echo $description1; } ?></textarea></td></tr>
</table>                                            
</fieldset>
 

 <fieldset><h5>B. Řízení zdrojů</h5>
<p><b>Řízení Cash-Flow - Pravidla a postupy </b> </p>    
<table>
<?php   
  echo mkRadio('field4',1,$_POST['field4'],$field4,'Organizace se řízením cash-flow nezabývá. ');    
  echo mkRadio('field4',2,$_POST['field4'],$field4,'Proces řízení CF není popsán, neexistují postupy pro indikaci a řešení výpadů CF. ');
  echo mkRadio('field4',3,$_POST['field4'],$field4,'Proces není nastaven, nicméně organizace již v minulosti výpadky CF řešila a je navyklý obvyklý postup v tomto případě. Výpadky nejsou s dostatečnou dobou dopředu indikovány, proces tuto funkcionalitu nezajišťuje. ');
  echo mkRadio('field4',4,$_POST['field4'],$field4,'Proces řízení CF je již v základních rysech nastaven, existuje formální plán CF a směrnice popisující hlavní indikátory procesu. ');
  echo mkRadio('field4',5,$_POST['field4'],$field4,'Proces řízení CF je popsán v interní směrnici včetně nastavení indikátorů pro včasná varování. S plánem CF se aktivně pracuje a pravidelně se vyhodnocuje. ');            
?>
</table>

<p><b>Fundraising (Obstarávání zdrojů)  Lidé a organizace </b> </p>    
<table>
<?php   
  echo mkRadio('field5',1,$_POST['field5'],$field5,'Organizace má zajištěn omezený zdroj finančních prostředků, obstarávání dalších zdrojů pro svou činnost se nezabývá.');    
  echo mkRadio('field5',2,$_POST['field5'],$field5,'Není nastavena odpovědnost za získávání zdrojů. ');
  echo mkRadio('field5',3,$_POST['field5'],$field5,'Odpovědnost za získávání zdrojů není formalizována, aktivně se k ní hlásí vedení společnosti. ');
  echo mkRadio('field5',4,$_POST['field5'],$field5,'Probíhá školení a trénink zaměstnanců organizace v získávání zdrojů. Je motivační systém navázán na získávání zdrojů. ');         
?>
</table>

<p><b>Bezpečnost finančních operací - Rizika </b> </p>    
<table>
<?php   
  echo mkRadio('field6',1,$_POST['field6'],$field6,'Organizace se bezpečností finančních operací nezabývá. ');    
  echo mkRadio('field6',2,$_POST['field6'],$field6,'Organizace si není vědoma rizik spojených s finančními operacemi. ');
  echo mkRadio('field6',3,$_POST['field6'],$field6,'Organizace si uvědomuje rizika spojená s finančními operacemi a pro jejich zabezpečení využívá metod zdarma poskytnutých třetími stranami (podpisy, přístupová hesla atd..)');
  echo mkRadio('field6',4,$_POST['field6'],$field6,'Řízení rizik v rámci bezpečnosti je formalizováno. Závěry z řízení rizik se objevují v bezpečnostní směrnici, v případě výskytu rizik existují opatření a nápravné mechanismy. ');
  echo mkRadio('field6',6,$_POST['field6'],$field6,'Rizika jsou identifikována a kvantifikována, řízení rizik je formalizováno, existuje plán na řízení a vyhodnocování bezpečnostních rizik. ');   
  ?>
</table> 

<p><b>Poznámka</b> </p>    
<table>
<tr><td><textarea style='width: 820px; height: 100px; border: none' name="description2" id="area1"   ><?php if($_POST['description2']) { echo $_POST['description2']; }else { echo $description2; } ?></textarea></td></tr>
</table>                                            
</fieldset>


 <fieldset><h5>C. Finanční účetnictví a daně</h5>
<p><b>Vedení účetnictví  Pravidla a postupy</b> </p>    
<table>
<?php   
  echo mkRadio('field7',1,$_POST['field7'],$field7,'Organizace nevede podvojné účetnictví.');    
  echo mkRadio('field7',2,$_POST['field7'],$field7,'Neexistují nastavené postupy pro vedení účetnictví a sledování požadavků v oblasti daní. Činnost je vykonávána operativně a nahodile. Interně v organizaci chybí přehled o podkladech, které potřebuje účetní pro svou práci. ');
  echo mkRadio('field7',3,$_POST['field7'],$field7,'Začíná probíhat opakovaný intuitivní proces účtování a operativní zpracování daňových přiznání, bez záruky jejich plného souladu s legislativou. ');
  echo mkRadio('field7',4,$_POST['field7'],$field7,'Postupy vedení účetnictví a zpracování daňových výkazů jsou definovány a formalizovány ve vnitřních předpisech. ');
  echo mkRadio('field7',5,$_POST['field7'],$field7,'Je stanovena zodpovědnost za procesy účtování a v nich stanovené povinnosti z pohledu daňové optimalizace; tato odpovědnost se vyžaduje. Postupy účtování se prověřují a dle potřeby aktualizují, proces je tak zdravý a úplný. ');             
?>
</table>

<p><b>Vazby mezi účetnictvím a controllingem  Pravidla a postupy </b> </p>    
<table>
<?php   
  echo mkRadio('field8',1,$_POST['field8'],$field8,'Controlling se neprovádí. ');    
  echo mkRadio('field8',2,$_POST['field8'],$field8,'Neexistují nastavené postupy pro přenos údajů z účetnictví do controllingu. Přístup ke zpracování analýz je nahodilý. ');
  echo mkRadio('field8',3,$_POST['field8'],$field8,'Začíná probíhat opakovaný intuitivní proces zpracování a analýzy dat z účetnictví controllingem.  ');
  echo mkRadio('field8',4,$_POST['field8'],$field8,'Postupy pro controlling dat z účetnictví jsou definovány a formalizovány ve vnitřních předpisech. ');
  echo mkRadio('field8',5,$_POST['field8'],$field8,'Je stanovena zodpovědnost za controlling dat z účetnictví; tato odpovědnost se vyžaduje. Postupy controllingu se prověřují a dle potřeby aktualizují, proces je tak zdravý a úplný. ');      
?>
</table>

<p><b>Oběh účetních dokladů - Pravidla a postupy </b> </p>    
<table>
<?php   
  echo mkRadio('field9',1,$_POST['field9'],$field9,'Organizace nemá upraven oběh účetních dokladů. ');    
  echo mkRadio('field9',2,$_POST['field9'],$field9,'Neexistují nastavené postupy pro oběh účetních dokladů, přístup k likvidaci účetních dokladů je operativní.');
  echo mkRadio('field9',3,$_POST['field9'],$field9,'Začíná probíhat opakovaný intuitivní proces oběhu účetních dokladů, nezaručující dodržování ŘKS pro oblast vedení účetnictví. ');
  echo mkRadio('field9',4,$_POST['field9'],$field9,'Postupy ŘKS pro vedení účetnictví jsou definovány a formalizovány ve vnitřních předpisech.  ');
  echo mkRadio('field9',5,$_POST['field9'],$field9,'Je stanovena zodpovědnost za ŘKS v rámci procesů účtování; tato odpovědnost se vyžaduje. ŘKM v postupech oběhu účetních dokladů se prověřují a dle potřeby aktualizují, proces je tak zdravý a úplný.  ');    
  ?>
</table> 

<p><b>Finanční výkaznictví (informace pro externí subjekty) - Pravidla a postupy  </b> </p>    
<table>
<?php   
  echo mkRadio('field10',1,$_POST['field10'],$field10,'Organizace nezpracovává finanční výkazy. ');    
  echo mkRadio('field10',2,$_POST['field10'],$field10,'Neexistují nastavené postupy pro finančního výkaznictví, činnost je vykonávána pouze s ohledem na povinnosti při odevzdávání daňových přiznání. Přístup ke zpracování výkazů je ad hoc. ');
  echo mkRadio('field10',3,$_POST['field10'],$field10,'Začíná probíhat intuitivní proces operativního zpracování finančních výkazů, nezaručující však jejich správnost.  ');
  echo mkRadio('field10',4,$_POST['field10'],$field10,'Postupy zpracování finančních výkazů jsou definovány a formalizovány ve vnitřních předpisech.  ');
  echo mkRadio('field10',5,$_POST['field10'],$field10,'Je stanovena zodpovědnost za procesy finančního reportingu a v nich stanovené povinnosti; tato odpovědnost se vyžaduje. Postupy se prověřují a dle potřeby aktualizují, proces je tak zdravý a úplný. ');       
  ?>
</table> 

<p><b>Poznámka</b> </p>    
<table>
<tr><td><textarea style='width: 820px; height: 100px; border: none' name="description3" id="area1"   ><?php if($_POST['description3']) { echo $_POST['description3']; }else { echo $description3; } ?></textarea></td></tr>
</table>                                            
</fieldset>


 <fieldset><h5> D. Komunikace</h5>
<p><b>Internet a webové stránky (upload odkazu na webové stránky organizace, projektu (kampaně, FCB apod.)</b> </p>    
<table>
<?php   
  echo mkRadio('field11',1,$_POST['field11'],$field11,'Organizace nemá své webové stránky ');    
  echo mkRadio('field11',2,$_POST['field11'],$field11,'Webové stránky organizace jsou součástí webových stránek na existujícím portále jiné organizace/subjektu. ');
  echo mkRadio('field11',3,$_POST['field11'],$field11,'Organizace má své webové stránky, které aktualizuje nejméně 1x za 2 měsíce. ');
  echo mkRadio('field11',4,$_POST['field11'],$field11,'Organizace má své webové stránky, speciální sekci pro realizované projekty/sbírky, využívá sociální sítě a pravidelně komunikuje s veřejností jejich prostřednictvím. ');
  echo mkRadio('field11',5,$_POST['field11'],$field11,'Organizace má vypracovanou kompletní internetovou prezentaci, realizuje kampaně na internetu, využívá sociální sítě apod.');              
?>
</table>

<p><b>Transparentnost a komunikace (upload výročních zpráv za poslední 2 roky)  </b> </p>    
<table>
<?php   
  echo mkRadio('field12',1,$_POST['field12'],$field12,'Organizace nevydává a nezveřejňuje výroční zprávu ani jinou roční zprávu o činnosti. ');    
  echo mkRadio('field12',2,$_POST['field12'],$field12,'Organizace zveřejňuje obdobu výroční (roční) zprávy a na vyžádání jí poskytuje ');
  echo mkRadio('field12',3,$_POST['field12'],$field12,'Organizace pravidelně zveřejňuje kompletní výroční zprávu v tištěné podobě a elektronické podobě a vyvěšuje jí na svých webových stránkách.');
  echo mkRadio('field12',4,$_POST['field12'],$field12,'Organizace pravidelně zveřejňuje výroční zprávu, jejíchž součástí je i výrok auditora, zveřejňuje jí na svých webových stránkách, rozesílá jí partnerům a má i jiné formy komunikace s dárci a veřejností (bulletin, mailing list apod.). '); 
?>
</table>

 

<p><b>Poznámka</b> </p>    
<table>
<tr><td><textarea style='width: 820px; height: 100px; border: none' name="description4" id="area1"   ><?php if($_POST['description4']) { echo $_POST['description4']; }else { echo $description4; } ?></textarea></td></tr>
</table>                                            
</fieldset>


  

    <fieldset>

<p>&nbsp;</p>
 
</div>
</form>
