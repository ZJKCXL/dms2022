<?

header('Content-type: text/css');

include_once("../framework/globals.php");
include_once(Globals::$PATH_BASE_DIR."framework/db.php");

$query = "SELECT tblAction.AApplColor AS color";
$query .= " FROM tblAction";
$query .= " GROUP BY (color)";
$query .= " UNION DISTINCT";
$query .= " SELECT tblClient.CApplColor AS color";
$query .= " FROM tblClient";
$query .= " GROUP BY (color)";
$query .= " UNION DISTINCT";
$query .= " SELECT tblTemplate.TApplColor AS color";
$query .= " FROM tblTemplate";
$query .= " GROUP BY (color)";
$query .= " UNION DISTINCT";
$query .= " SELECT tblColorScheme.CSColor AS color";
$query .= " FROM tblColorScheme";
$query .= " GROUP BY (color)";

$db_handler = new Data();
$db_handler->doQuery($query);
if ($db_handler->getNumRows() > 0)
{
	while ($row = $db_handler->getFetchedDataLine())
	{
		echo ".bg_".$row["color"]."\n";
		echo "{\n";
		echo "	background: #".$row["color"]."\n";
		echo "}\n";
		echo ".color_".$row["color"]."\n";
		echo "{\n";
		echo "	color: #".$row["color"].";\n";
		echo "}\n";
		echo ".border_color_".$row["color"]."\n";
		echo "{\n";
		echo "	border-color: #".$row["color"].";\n";
		echo "}\n";
	}
}

?>