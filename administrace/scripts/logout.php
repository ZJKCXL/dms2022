<?php

include "../../sql/db.php";
include("../lib/user_functions.php");

if (user_logged_in())
{
	user_logout();
}	

 header("Location: ../index.php");
?>
