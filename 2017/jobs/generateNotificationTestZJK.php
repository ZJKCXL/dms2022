<?php
include_once("../config/conf.protected.php");
include_once("../config/conf.php");
include_once("../sql/db.php");

set_time_limit(0);
setlocale(LC_MONETARY, 'cs_CZ.UTF-8');

\Kernel\Kernel::parseState();

define("APP_NAME", "Fórum dárců, spolek");
define("FILE_DIR", "/data/www/darcovskasms.cz/avizo/");

$date = date('Y-m-d');

if (isset($_REQUEST["year"])) {
    $year = trim($_REQUEST["year"]);
} else {
    $year = date("Y");
}

if (isset($_REQUEST["month"])) {
    $month = trim($_REQUEST["month"]);
} else {
    $month = date("n");
}

 echo $year;
 echo "<br/>";
 echo $month;

$dt = \Application\reportMonthly::load($year, $month);
$sm = \Application\reportMonthly::generateSums();

$data = \Application\reportMonthly::getSums();

if (\Kernel\Func::resultValidArr($data)) {
    echo "Got [".count($data)."] sumed projects from server<br /><br />";
    foreach ($data as $project_id => $project_sums) {
        echo "Processing project [".$project_id."] with data [".json_encode($project_sums)."]<br />";
        $res = \Application\dmsNotification::storeNotification($project_id, $project_sums["price"], $year, $month, false, $date);
        if ($res) {
            echo "... success<br />";
        } else {
            echo "... error<br />";
        }
        flush();
    }
}

?>
