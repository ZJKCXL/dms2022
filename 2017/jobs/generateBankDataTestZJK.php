<?php
include_once("../config/conf.protected.php");
include_once("../config/conf.php");
include_once("../sql/db.php");

set_time_limit(0);
setlocale(LC_MONETARY, 'cs_CZ.UTF-8');

\Kernel\Kernel::parseState();

define("APP_NAME", "Fórum dárců, z.s.");
define("FILE_DIR", "/data/www/darcovskasms.cz/avizo/");

$date = date('Y-m-d');

if (isset($_REQUEST["year"])) {
    $year = trim($_REQUEST["year"]);
} else {
    $year = date("Y");
}

if (isset($_REQUEST["month"])) {
    $month = trim($_REQUEST["month"]);
} else {
    $month = date("n");
}

\Application\dmsBankData::generateData($year, $month);
\Export\ABO::serveFile("aviza-".$year."-".$month."-1");


?>
