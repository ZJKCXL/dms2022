<title>8. Vytvořit Kolekce</title>
<?php
include_once("../config/conf.protected.php");
include_once("../config/conf.php");
include_once("../sql/db.php");

\Kernel\Kernel::parseState();

$collections = \Application\dmsCollection::getCollectionsNoATS();

if (\Kernel\Func::resultValidArr($collections)) {
    echo "Got [".count($collections)."] collections from server<br /><br />";
    foreach ($collections as $collection) {
        $collection_data = \Application\dmsCollection::getDetail($collection["collection_id"]);
        $keyword_data = \Application\dmsKeyword::getDetail($collection["keyword_id"]);

        echo "Processing collection [".$collection_data["name"]."] with ID [".$collection_data["id"]."]<br />";
        $valid_from = ($collection_data["validFrom"]);
        $valid_to = ($collection_data["validTo"]);
        echo " ... valid from [".$valid_from."] to [".$valid_to."]<br />";
        echo " ... has keyword<br />";
        echo " ... ... name [".$keyword_data["name"]."] with ID [".$keyword_data["id"]."]<br />";

        $ats_response = \Application\dmsCollection::publishATS($collection_data["id"]);
        if (\Kernel\Func::resultValidArr($ats_response)) {
            echo " ... SUCCESS with ID [".$ats_response["id"]."]";
        } else {
            echo " ... ERROR";
        }

        echo "<br />";
    }
} else {
    echo "No collections from server<br /><br />";
}
?>
