<title>9. updatovat Kolekce</title>
<?php
include_once("../config/conf.protected.php");
include_once("../config/conf.php");
include_once("../sql/db.php");

\Kernel\Kernel::parseState();

$collections = \ATS\CollectionList::getList();
if (\Kernel\Func::resultValidArr($collections)) {
    echo "Got [".count($collections)."] collections from server<br /><br />";
    foreach ($collections as $collection) {
        echo "Processing collection [".$collection["name"]."] with ID [".$collection["id"]."]<br />";
        $valid_from = substr($collection["valid_from"],0,10);
        $valid_to = substr($collection["valid_to"],0,10);
        echo " ... valid from [".$valid_from."] to [".$valid_to."]<br />";
        $query = "SELECT keyword.id, collectionKeyword.collection_id, collectionKeyword.ats_id, keyword.name";
        $query .= " FROM collection, collectionKeyword, keyword";
        $query .= " WHERE 1 =1 ";
        $query .= " AND collectionKeyword.collection_id = collection.id";
        $query .= " AND keyword.id = collectionKeyword.keyword_id";
        $query .= " AND keyword.name = '".$collection["name"]."'";
        $query .= " AND collection.validFrom = '".$valid_from."'";
        //$query .= " AND collection.validTo = '".$valid_to."'";

        echo "<hr/>".$query;

        $res = @mysql_query($query);

        if ($res && @mysql_num_rows($res) > 0) {
            while ($row = @mysql_fetch_array($res)) {
                echo " ... found keyword ID [".$row["id"]."] in DB<br />";
                echo " ... ATS ID is [".$row["ats_id"]."]<br />";
                if (!isset($row["ats_id"]) || is_null($row["ats_id"]) || $row["ats_id"] != $collection["id"]) {
                    echo " ...... ATS ID not set or does not match, updating<br />";
                    $upd_query = "UPDATE collectionKeyword ";
                    $upd_query .= " SET ats_id = ".$collection["id"];
                    $upd_query .= " WHERE keyword_id = ".$row["id"];

                    $upd_res = @mysql_query($upd_query);
                    if ($res && @mysql_affected_rows() >= 0) {
                        echo " ...... success<br />";
                    } else {
                        echo " ...... ERROR with [".@mysql_error()."]<br />";
                    }
                }
            }
        } else {
            echo " ... ERROR no collection ID found in DB<br />";
        }

        echo "<br />";
    }
}
?>
<script>
  window.open ("http://www.darcovskasms.cz/2017/jobs/ZJKClearProjectAsoc.php", "_newtab" );    
</script>