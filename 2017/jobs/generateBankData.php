<?php
include_once("../config/conf.protected.php");
include_once("../config/conf.php");
include_once("../sql/db.php");

set_time_limit(0);
setlocale(LC_MONETARY, 'cs_CZ.UTF-8');

\Kernel\Kernel::parseState();

define("APP_NAME", "Fórum dárců, z.s.");
define("FILE_DIR", "/data/www/darcovskasms.cz/avizo/");

\Application\dmsBankData::generateData(2017, 7);
\Export\ABO::serveFile("aviza-2017-7-1");


?>
