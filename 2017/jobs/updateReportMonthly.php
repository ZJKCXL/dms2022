<?php
include_once("../config/conf.protected.php");
include_once("../config/conf.php");
include_once("../sql/db.php");

\Kernel\Kernel::parseState();

if (isset($_REQUEST["year"])) {
    $year = trim($_REQUEST["year"]);
} else {
    $year = date("Y");
}

if (isset($_REQUEST["month"])) {
    $month = trim($_REQUEST["month"]);
} else {
    $month = date("m");
}

$date = $year."-".$month."-01";

$value_ids = array("30" => 1, "60" => 2, "90" => 3);
$type_ids = array("onetime" => 1, "permanent" => 2, "year" => 3);

$key_query = "SELECT collection.id as coll_id, keyword.name, collectionKeyword.ats_id, collectionKeyword.keyword_id, collection.validFrom, collection.validTo";
$key_query .= " FROM collection, collectionKeyword, keyword";
$key_query .= " WHERE 1=1";
$key_query .= " AND collectionKeyword.collection_id = collection.id";
$key_query .= " AND keyword.id = collectionKeyword.keyword_id";
$key_query .= " AND (collection.validFrom <= '".$date."' OR MONTH(collection.validFrom) = ".$month.")";
$key_query .= " AND collection.validTo >= '".$date."'";
$key_query .= " AND collectionKeyword.ats_id IS NOT NULL";

/*
echo $key_query;         */

 

$key_res = @mysql_query($key_query);

if ($key_res && @mysql_num_rows($key_res) > 0) {
    echo "Got [".@mysql_num_rows($key_res)."] active kewyord with ATS ID from server\n\n";
    while ($keyword = @mysql_fetch_array($key_res)) {
        echo "Processing kewyord [".$keyword["name"]."] with keyword ID [".$keyword["keyword_id"]."], ATS ID [".$keyword["ats_id"]."]\n";
        $monthly_report = \ATS\MonthlyReport::getReport($keyword["ats_id"], $year, $month);
        if (\Kernel\Func::resultValidArr($monthly_report)) {
            echo " ... monthly report for [".$month."/".$year."] retrieved as [".json_encode($monthly_report)."]\n";
            foreach ($monthly_report as $data_row) {
                var_dump($data_row);
                $project_id = \Application\dmsCollection::getProject($keyword["coll_id"]);
                $organization_id = \Application\dmsProject::getOrganization($project_id);
                if (isset($value_ids[$data_row["price"]]) && isset($type_ids[$data_row["sms_report_type"]])) {
                    $val = $value_ids[$data_row["price"]];
                    $type = $type_ids[$data_row["sms_report_type"]];
                    $count = $data_row["cnt"];
                    echo " ...... updating count for type [".$type."]\n";
                    echo " ...... updating count for val [".$val."] with [".$count."]\n";

                    $dl_query = "INSERT INTO reportMonthly (keyword_id, organization_id, project_id, collection_id, operator, year, month, increment, dmsValue, dmsType)";
                    $dl_query .= " VALUES (";
                    $dl_query .= "".$keyword["keyword_id"];
                    $dl_query .= ", ".$organization_id;
                    $dl_query .= ", ".$project_id;
                    $dl_query .= ", ".$keyword["coll_id"];
                    $dl_query .= ", '".$data_row["operator"]."'";
                    $dl_query .= ", ".$year;
                    $dl_query .= ", ".$month;
                    $dl_query .= ", ".$count;
                    $dl_query .= ", ".$val;
                    $dl_query .= ", ".$type;
                    $dl_query .= ")";
                    $dl_query .= " ON DUPLICATE KEY UPDATE increment = ".$count;

                    $dl_res = @mysql_query($dl_query);
                    if ($dl_res && @mysql_affected_rows() >= 0) {
                        echo " ...... success\n";
                    } else {
                        echo " ...... ERROR with text [".@mysql_error()."]\n";
                    }

                }
            }
        } else {
            echo " ... ERROR no month report retrieved\n";
            echo " ... ... result was ".json_encode($monthly_report)."\n";
        }
        echo "\n";
    }
}
?>
