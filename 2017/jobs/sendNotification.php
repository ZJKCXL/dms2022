<?php
include_once("../config/conf.protected.php");
include_once("../config/conf.php");
include_once("../sql/db.php");

set_time_limit(0);
setlocale(LC_MONETARY, 'cs_CZ.UTF-8');

\Kernel\Kernel::parseState();

define("APP_NAME", "Fórum dárců, z.s.");
define("FILE_DIR", "/data/www/darcovskasms.cz/avizo/");

$notif = \Application\dmsNotification::getNotifications(2017, 3);
if (\Kernel\Func::resultValidArr($notif)) {
    \Application\dmsNotification::loadTemplate();
    foreach ($notif as $notif_data) {
        $raw_data = json_decode($notif_data["raw_data"], true);
        $conts = array();
        foreach ($raw_data["contacts"] as $contact) {
            $conts[] = $contact["email"];
        }
        echo "Sending project [".$raw_data["collection"][0]["name"]."] to [".implode(", ", $conts)."]";
        $res = \Application\dmsNotification::generateEmail($notif_data, $year, $month);
        if ($res) {
            echo " ... OK<br />";
        } else {
            echo " ... ERR<br />";
        }
        flush();
    }
}


?>
