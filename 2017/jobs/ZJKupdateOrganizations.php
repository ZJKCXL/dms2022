<title>7. Updatovat organizace Behe</title>
<?php
include_once("../config/conf.protected.php");
include_once("../config/conf.php");
include_once("../sql/db.php");

\Kernel\Kernel::parseState();

$foundations = \ATS\FoundationList::getList();
if (\Kernel\Func::resultValidArr($foundations)) {
    echo "Got [".count($foundations)."] collections from server<br /><br />";
    foreach ($foundations as $foundation) {
        echo "Processing foundation [".$foundation["name"]."] with ID [".$foundation["id"]."]<br />";
        $query = "SELECT organization.id, organization.ats_id";
        $query .= " FROM organization";
        $query .= " WHERE 1 =1 ";
        $query .= " AND organization.id = ".$foundation["name"];

        $res = @mysql_query($query);

        if ($res && @mysql_num_rows($res) > 0) {
            while ($row = @mysql_fetch_array($res)) {
                echo " ... found keyword ID [".$row["id"]."] in DB<br />";
                echo " ... ATS ID is [".$row["ats_id"]."]<br />";
                if (!isset($row["ats_id"]) || is_null($row["ats_id"]) || $row["ats_id"] != $foundation["id"]) {
                    echo " ...... ATS ID not set or does not match, updating<br />";
                    $upd_query = "UPDATE organization ";
                    $upd_query .= " SET ats_id = ".$foundation["id"];
                    $upd_query .= " WHERE id = ".$row["id"];

                    $upd_res = @mysql_query($upd_query);
                    if ($res && @mysql_affected_rows() >= 0) {
                        echo " ...... success<br />";
                    } else {
                        echo " ...... ERROR with [".@mysql_error()."]<br />";
                    }
                }
            }
        } else {
            echo " ... ERROR no foundation ID found in DB<br />";
        }

        echo "<br />";
    }
}
?>
<script>
  window.open ("https://www.darcovskasms.cz/2017/jobs/ZJKcreateCollections.php", "_newtab" );    
</script>