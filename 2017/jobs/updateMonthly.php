<?php
include_once("../config/conf.protected.php");
include_once("../config/conf.php");
include_once("../sql/db.php");

\Kernel\Kernel::parseState();

if (isset($_REQUEST["year"])) {
    $year = trim($_REQUEST["year"]);
} else {
    $year = date("Y");
}

if (isset($_REQUEST["month"])) {
    $month = trim($_REQUEST["month"]);
} else {
    $month = date("m");
}

$date = $year."-".$month."-01";

$value_ids = array("30" => 1, "60" => 2, "90" => 3);
$type_ids = array("onetime" => 1, "permanent" => 2, "year" => 3);

$key_query = "SELECT collection.id as coll_id, keyword.name, collectionKeyword.ats_id, collectionKeyword.keyword_id, collection.validFrom, collection.validTo";
$key_query .= " FROM collection, collectionKeyword, keyword";
$key_query .= " WHERE 1=1";
$key_query .= " AND collectionKeyword.collection_id = collection.id";
$key_query .= " AND keyword.id = collectionKeyword.keyword_id";
$key_query .= " AND collection.validFrom <= '".$date."'";
$key_query .= " AND collection.validTo >= '".$date."'";
$key_query .= " AND collectionKeyword.ats_id IS NOT NULL";

echo $key_query;

$key_res = @mysql_query($key_query);

if ($key_res && @mysql_num_rows($key_res) > 0) {
    echo "Got [".@mysql_num_rows($key_res)."] active kewyord with ATS ID from server\n\n";
    while ($keyword = @mysql_fetch_array($key_res)) {
        echo "Processing kewyord [".$keyword["name"]."] with keyword ID [".$keyword["keyword_id"]."], ATS ID [".$keyword["ats_id"]."]\n";
        /*
        $daily_report = \ATS\DailyReport::getReport($keyword["ats_id"], $date);
        if (\Kernel\Func::resultValidArr($daily_report)) {
            echo " ... daily report for [".$date."] retrieved as [".json_encode($daily_report)."]\n";
            foreach ($daily_report as $val => $count_row) {
                foreach ($count_row as $type_val => $count) {
                    if (isset($value_ids[$val])) {
                        echo " ...... updating count for type [".$type_val."]\n";
                        echo " ...... updating count for val [".$val."] with [".$count."]\n";

                        $dl_query = "INSERT INTO reportDaily (reportDailyFile_period, keyword_id, increment, dmsValue, dmsType, atsValidFrom, atsValidTo)";
                        $dl_query .= " VALUES (";
                        $dl_query .= " '".$date."'";
                        $dl_query .= ", ".$keyword["keyword_id"];
                        $dl_query .= ", ".$count;
                        $dl_query .= ", ".$value_ids[$val];
                        $dl_query .= ", ".$type_ids[$type_val];
                        $dl_query .= ", '".$keyword["validFrom"]."'";
                        $dl_query .= ", '".$keyword["validTo"]."'";
                        $dl_query .= ")";
                        $dl_query .= " ON DUPLICATE KEY UPDATE increment = ".$count;

                        $dl_res = @mysql_query($dl_query);
                        if ($dl_res && @mysql_affected_rows() >= 0) {
                            echo " ...... success\n";
                        } else {
                            echo " ...... ERROR with text [".@mysql_error()."]\n";
                        }

                    }
                }
            }
        } else {
            echo " ... ERROR no daily report retrieved\n";
        }
        */
        echo "\n";
    }
}
?>
