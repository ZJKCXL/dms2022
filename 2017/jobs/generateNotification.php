<?php
include_once("../config/conf.protected.php");
include_once("../config/conf.php");
include_once("../sql/db.php");

set_time_limit(0);
setlocale(LC_MONETARY, 'cs_CZ.UTF-8');

\Kernel\Kernel::parseState();

define("APP_NAME", "Fórum dárců, spolek");
define("FILE_DIR", "/data/www/darcovskasms.cz/avizo/");

$date = "2017-08-30";

$year = 2017;
$month = 9;

$dt = \Application\reportMonthly::load($year, $month);
$sm = \Application\reportMonthly::generateSums();

$data = \Application\reportMonthly::getSums();

if (\Kernel\Func::resultValidArr($data)) {
    echo "Got [".count($data)."] sumed projects from server<br /><br />";
    foreach ($data as $project_id => $project_sums) {
        echo "Processing project [".$project_id."] with data [".json_encode($project_sums)."]<br />";
        $res = \Application\dmsNotification::storeNotification($project_id, $project_sums["price"], $year, $month, false, $date);
        if ($res) {
            echo "... success<br />";
        } else {
            echo "... error<br />";
        }
        flush();
    }
}

?>
