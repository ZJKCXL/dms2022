<?php
include_once("../config/conf.protected.php");
include_once("../config/conf.php");
include_once("../sql/db.php");
/*
set_time_limit(0);
setlocale(LC_MONETARY, 'cs_CZ.UTF-8');
\Kernel\Kernel::parseState();
define("APP_NAME", "Fórum dárců, spolek");
define("FILE_DIR", "/data/www/darcovskasms.cz/board/");
$in_org = 429;
$res = \Application\dmsOrg4Board::storeBoard($in_org);
*/

$mpdf = new \Mpdf\Mpdf();

$mpdf->Bookmark('Start of the document');

$query = "SELECT *, project_online.name AS PRNAME, project_online.description AS PRDES, organization_online.name AS ORGNAME, organization_online.description AS ORGDESC FROM  project_online,organization_online  WHERE   project_online.organization_id = organization_online.id and    project_online.id  = ".$_REQUEST['in_org'];
$res = mysql_query($query);
if ($res && mysql_num_rows($res)>0) {
while ($row = @mysql_fetch_array($res)){
      $org_name = $row['ORGNAME'];
      $org_description = $row['ORGDESC'];
      $project_description = $row['PRDES'];
      $project_name = $row['PRNAME'];
      $new_keywords = $row['newKeywords'];

      $newFinalMoney = $row['newFinalMoney'];
      $newWaitCount = $row['newWaitCount'];
      $newOtherMoney = $row['newOtherMoney'];
      $newUcel = $row['newUcel'];
      $newSbirkaOd = $row['newSbirkaOd'];
      $newSbirkaDo = $row['newSbirkaDo'];
      $webPage = $row['webPage'];
      $newContactName = $row['newContactName'];
      $newContactPhone = $row['newContactPhone'];
      $newContactMobile = $row['newContactMobile'];
      $newContactEmail = $row['newContactEmail'];

      $newCommunication = $row['newCommunication'];
   
}
}

$myHtml = 
'<style>
    table {border: 4px solid #e7e7e7; border-collapse: collapse; width: 100%; font-family: sans-serif; margin-top: 10px}
    td, th {  border: 1px solid #c0c0c0; padding: 10px 20px;  text-align: left; font-weight: normal; vertical-align: top; font-size: 12px}
    th { background: #449cc4; color: #fff; width: 30%; }
    img { height: 100px;     }
    </style>
    <img src="/board/logo.png" />
    <table>
    <tr><th>Název organizace</th><td colspan=2>'.$org_name.'</td></tr>
    <tr><th>Poslání organizace</th><td colspan=2>'.nl2br ($org_description).'</td></tr>
    <tr><th>Název projektu</th><td colspan=2><h3>'.$project_name.'</h3></td></tr>
    <tr><th>Návrh DMS hesla</th><td colspan=2>'.$new_keywords.'</td></tr>
    <tr><th>Popis projektu</th><td colspan=2>'.nl2br ($project_description).'</td></tr>
    <tr><th>Financování projektu</th><td colspan=2>Cílová částka projektu: '.$newFinalMoney.' Kč<br/> 
    Cílová částka z DMS za rok: '.$newWaitCount.' Kč<br/>
    Další zdroje financování projektu: '.nl2br ($newOtherMoney ).' </td></tr>
    <tr><th>Využití prostředků z DMS</th><td colspan=2>'.nl2br ($newUcel).'</td></tr>
    <tr><th>Veřejná sbírka</th><td>OD: '.$newSbirkaOd.'</td><td>DO: '.$newSbirkaDo.'</td></tr>
    <tr><th>Webová adresa</th><td colspan=2>'.nl2br ($webPage).'</td></tr>
    <tr><th>Kontakt</th><td colspan=2>'.$newContactName.',  '.$newContactPhone.', '.$newContactMobile.', '.$newContactEmail.' </td></tr>
    <tr><th>Komunikační plán k DMS</th><td colspan=2>'.nl2br ($newCommunication).'</td></tr>
    <tr><th>Vyjádření FD</th><td colspan=2> </td></tr>
 
    
    

</table>';

$mpdf->WriteHTML($myHtml);

$mpdf->Output();


?>
