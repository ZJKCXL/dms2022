<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
        <title>DMS Avizo</title>
        <meta name='description' content='DMS Avizo' />
        <meta name='keywords' content='DMS Avizo' />
    </head>
    <body>
        <style>
            body { background-color: #ffffff; font-family:  sans-serif; font-size: 12px;  }
            * {  box-sizing: border-box; }
            #main { padding: 1%;  }
            address { font-style: normal; }
            #number { float: right; font-weight: bold; font-size: 150% }
            h2, h3 {margin: 0; padding: 0; font-size: 15px}
            h4 { margin: 0; padding: 0; float: left; clear: left; min-width: 9em;  }
            h4+span {float: left;}
            .line { background: #fff; width: 100%; min-height: 190px;  position:relative;  border: 5px solid #449cc4; padding: 20px 20px 20px 35px; margin : 25px 3px 35px 3px; }
            .intable div {  min-height: 150px;  position:relative;}
            .line h2, .line h3, table .intable h2, table .intable h3  { color: #449cc4;   font-weight: normal;   }

            #client, #payment {  width: 99% }
            #invoiced, #total {   width: 99%   }
            #total p { font-size: 25px;  }
            #descr { clear: both; margin-bottom: 15px }
            table { width: 100%; border: none; }
            table tr.project { background-color: #e5e5e5; }
            table tr.keyword { background-color: #f5f5f5; }
            table td { padding: 10px 15px; }
            table tr td:nth-of-type(2) { text-align: right;  }
            h1 {  clear: left; color:   #0881B8; padding: 10px 15px; font-weight: normal; border-bottom: 5px solid  #e5e5e5;  border-top: 5px solid  #e5e5e5;}
            img { height: 120px; float: left; margin-right: 25px; margin-bottom: 25px;}
            #me address { padding-top: 55px; }
            #me span { font-size: 12px; }
            .final { border-top: 5px solid  #e5e5e5; text-align: center; clear: both; }
            td { vertical-align: top }
            table, table.main td { border: 0px solid red; padding: 0;}
            table.main  .intable  td { border: 5px solid #449cc4;  padding: 20px; position: relative;  }
            table.main .intable  {  position: relative;  }
        </style>
        <main id="main">
            <table class='main'>
                <tr>
                    <td width='130px'>
                        <div id="me">
                        <img src="/avizo/logo.png"  />
                    </td>
                    <td   style='text-align: left; padding-top: 35px'>

                        <address>
                            nám. Winstona Churchilla 1800/2<br/>130 00 Praha 3<br/> 
                            IČ: 66004501 <br/>
                            Tel.: 240 201 193 <br/>
                            Číslo účtu: 238181841/0300, <br/>
                            ČSOB IBAN: CZ27 0300 0000 0002 3818 1841
                        </address>
                        <span>Fórum dárců, z.s., spisová značka L 8294 vedená u Městského soudu v Praze</span>
                        </div>
                    </td>
                    <td width='250px'>
                        <div id='number'>
                            <span>e.č. - <%NUM_FORMATED%></span>
                        </div>
                    </td>
                </tr>
            </table>




            <h1>AVÍZO K NAŠÍ PLATBĚ </h1>

            <table class='main'>
                <tr>
                    <td width='50%' style='padding-right: 10px;'>
                        <table class='intable'><tr><td>
                                    <div id="client" >
                                        <h2 style=' color: #449cc4;   font-weight: normal; ' >Odběratel</h2>
                                        <address><strong><%ORGANIZATION_NAME%></strong> <br/>
                                            <%ORGANIZATION_STREET%> <br/>
                                            <%ORGANIZATION_ZIP%> <%ORGANIZATION_CITY%><br/>
                                            IČ: <%ORGANIZATION_COMPANYID%><br/><br/>

                                            Odpovědná osoba: <%ORGANIZATION_PERSON%>
                                        </address>
                                    </div>
                                </td></tr></table>
                    </td>
                    <td width='50%' style='padding-left: 10px;'>
                        <table class='intable'><tr><td>
                                    <div id="invoiced" >
                                        <h3 style=' color: #449cc4;   font-weight: normal; ' >Datum / Vystavil(a)</h3>
                                        <h4>Datum vystavení</h4>
                                        <span><%GENERATION_DATE%></span>
                                        <h4>Datum úhrady</h4>
                                        <span>dle smlouvy</span>
                                        <h4>Vystavil(a)</h4>
                                        <span>Hana Daňková</span>
                                    </div>
                                </td></tr></table>
                    </td>
                </tr>
            </table>
            <div id="descr" class="line">
                <h3>Popis</h3>
                <p>
                    <%NOTIFICATION_TEXT%>
                </p>
            </div>
            <table class='main'>
                <tr>
                    <td width='50%' style='padding-right: 10px;'>
                        <table class='intable'><tr><td>
                                    <div id="payment"  >
                                        <h3 style=' color: #449cc4;   font-weight: normal; ' >Způsob platby </h3>
                                        <p>Bankovním převodem z účtu Fóra dárců <br/>
                                            Číslo účtu: 238181841 <br/>
                                            Kód banky: 0300 <br/>
                                            IBAN: CZ27 0300 0000 0002 3818 1841 <br/>
                                            Na účet: <%ACCOUNT%> <br/>
                                            Variabilní symbol: <%VAR_SYMBOL%>
                                        </p>
                                    </div>
                                </td></tr></table>
                    </td>
                    <td width='50%' style='padding-left: 10px; text-align: center'>
                        <table class='intable'><tr><td>
                                    <div id="total">
                                        <h3 style=' color: #449cc4;   font-weight: normal; ' >Celkem k přijetí</h3>
                                        <br />
                                        <p style='font-size: 200%; font-weight: bold;'><%AMOUNT%></p>
                                    </div>
                                </td></tr></table>
                    </td>
                </tr>
            </table>
            <p>&nbsp;</p>
            <div class="final">
                <p>Těšíme se na další spolupráci</p>
            </div>
        </main>
    </body>
</html>