<?php
  namespace String;

  class Levenshtein {
      const DISTANCE_THRESHOLD = 0.4;

      public static function getDistance($in_search_word, $in_dictionary_word) {
          $dist = levenshtein($in_dictionary_word, $in_search_word);

          $result = $dist / @strlen($in_dictionary_word);

          return $result;
      }

  }
?>
