<?php
  namespace String;

  class Metaphone {
      const DISTANCE_THRESHOLD = 0.75;

      public static function getMetaphoneRatio($in_search_word, $in_dictionary_word) {
          $sound_s = metaphone($in_search_word);
          $sound_d = metaphone($in_dictionary_word);

          $result = \String\Similarity::getSimiliarity($sound_s, $sound_d);

          return $result;
      }

  }
?>
