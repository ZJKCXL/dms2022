<?php

namespace String;

class AnchorBuilder
{

    static public function buildAnchor($in_href, $in_text, $in_title = null, $in_target = null, $in_attr_arr = null)
    {
        if (strlen($in_href) > 0 && strlen($in_text) > 0) {
            $anchor = "<a href=\"" . BASE_URL . $in_href . "\" title=\"" . $in_title . "\"";
            if (!is_null($in_target) && $in_target !== false) {
                $anchor .= " target=\"" . $in_target . "\"";
            }
            if (is_array($in_attr_arr) && count($in_attr_arr) > 0) {
                foreach ($in_attr_arr as $key => $value) {
                    $anchor .= " " . $key . "=\"" . $value . "\"";
                }
            }
            $anchor .= ">" . $in_text . "</a>";

            return $anchor;
        }

        return false;
    }

}
