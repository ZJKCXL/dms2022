<?
namespace Kernel;

/* Need complete reahul, is terribly wrong sir! */

class Priviledge {

    const PRIVILEDGE_ADMIN = '0000-14834465820.70855700-53952-38f5d29d3af0de26c6e3bbc05b47a172';
    const PRIVILEDGE_ORDERS = '0000-14834465870.70677700-98614-f6dfe3fdf6f8091ffff752fef9aa9577';
    const PRIVILEDGE_CATALOGS = '0000-14834465920.01863900-26915-4ec59187d14ad09b3d93af899d573d5b';
    const PRIVILEDGE_SUPPLIERS = '0000-14834465960.95421700-44571-e6edeee2bf9e5cd143805a90c6c03e3e';

    const PRIVILEDGE_SUPPLIER_EDIT = '0000-14849103740.70097200-18238-d3cfb2168ed4693bce94b200697f41a1';

    public static function hasPriviledge($in_user, $in_priviledge) {
        $result = false;

        $user_type = \Application\User::getUserType($in_user);
        if ($user_type !== false) {
            $result = self::groupHasPriviledge($user_type, $in_priviledge);
        }
        if (!$result) {
            $result = self::userHasPriviledge($in_user, $in_priviledge);
        }

        return $result;
    }

    private static function userHasPriviledge($in_user, $in_priviledge) {
        $result = false;

        return $result;
    }

    private static function groupHasPriviledge($in_user_group, $in_priviledge) {
        $result = false;

        switch ($in_priviledge) {
            case self::PRIVILEDGE_ADMIN:
                $result = ($in_user_group == \Application\UserType::TYPE_ADMIN);
                break;
            case self::PRIVILEDGE_CATALOGS:
                $result = true;
                break;
            case self::PRIVILEDGE_ORDERS:
                $result = true;
                break;
            case self::PRIVILEDGE_SUPPLIERS:
                $result = true;
                break;
            case self::PRIVILEDGE_SUPPLIER_EDIT:
                $result = ($in_user_group == \Application\UserType::TYPE_ADMIN);
                break;
            default:
                $result = true;
                break;
        }

        return $result;
    }

}
?>
