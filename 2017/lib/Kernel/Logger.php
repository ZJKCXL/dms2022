<?
namespace Kernel;

define("LOG_ACCESS","access");
define("LOG_SYSTEM","system");
define("LOG_ERROR","error");
define("LOG_TARGET_DEBUG","debug");
define("LOG_TARGET_MAIL","mail");
define("LOG_TARGET_CRON","cron");
define("LOG_TARGET_PROPULSION","propulsion");

class Logger
{
    private static $errcode;

    const LOG_ACCESS = "access";
    const LOG_API = "api";
    const LOG_DEBUG = "debug";
    const LOG_SYSTEM = "system";
    const LOG_ERROR = "error";
    const LOG_TARGET_DEBUG = "debug";
    const LOG_TARGET_MAIL = "mail";
    const LOG_TARGET_CRON = "cron";
    const LOG_TARGET_PROPULSION = "propulsion";
    const LOG_API_ERROR = "api_error";
    const LOG_TARGET_QUERY = "query";

    public static function lastError () {
        return self::$errcode;
    }

    public static function logToFile ($in_message, $in_log_type = LOG_SYSTEM, $in_data = false, $full_backtrace = false) {
        if (strlen($in_message) <= 0)
        {
            self::$errcode = LOGGER_EMPTY_MESSAGE;
            return false;
        }

        switch ($in_log_type) {
            case "system":
                $log_file = "system.log";
                break;
            case "debug":
                $log_file = "debug.log";
                break;
            case "mail":
                $log_file = "mail.log";
                break;
            case "error":
                $log_file = "error.log";
                break;
            case "admin":
                $log_file = "admin.log";
                break;
            case "cron":
                $log_file = "cron.log";
                break;
            case "access":
                $log_file = "access.log";
                break;
            case "user":
                $log_file = "user.log";
                break;
            case "import":
                $log_file = "import.log";
                break;
            case "propulsion":
                $log_file = "propulsion.log";
                break;
            case "api":
                $log_file = "api.log";
                break;
            case "api_error":
                $log_file = "api_error.log";
                break;
            case "query":
                $log_file = "query.log";
                break;
            default:
                self::$errcode = LOGGER_UNKNOWN_LOG_TYPE;
                break;
        }

        if ($log_file) {
            $pre_message = date("Y-m-d H:i:s")." : [".$_SERVER['PHP_SELF']."]";
            $backtrace = debug_backtrace();
            if ($backtrace != false && is_array($backtrace) && @count($backtrace)>1) {
                $last_incl = $backtrace[0];
                $last_incl_name = str_replace(BASE_DIR, "", $last_incl["file"]);
                if (strlen($last_incl_name) > 0) {
                    $pre_message .= "[".$last_incl_name."]";
                }
                $back_row = $backtrace[1];
                if (!is_null($back_row["class"])) {
                    $pre_message .= "[";
                    $pre_message .= $back_row["class"];
                    if (!is_null($back_row["function"])) {
                        $pre_message .= "::".$back_row["function"];
                    }
                    $pre_message .= "]";
                }
            }
            $pre_message .= " ";
            if ($in_data !== false) {
                $in_message .= " [[".json_encode($in_data)."]]";
            }
            if ($full_backtrace !== false){
                if ($backtrace != false && is_array($backtrace) && @count($backtrace)>1) {
                    $in_message .= " ||".json_encode($backtrace)."||";
                }
            }
            if ($file = @fopen(LOGS_DIR.$log_file, "a+")) {
                @fputs($file, $pre_message.$in_message."\n");
                @fflush($file);
                @fclose($file);
                return true;
            } else {
                self::$errcode = LOGGER_CANNOT_OPEN_FILE;
                return false;
            }
        }

        return false;
    }

    public static function showLog($in_log_type = LOG_SYSTEM, $reversed = false) {

        $result = false;

        switch ($in_log_type) {
            case "system":
                $log_file = "system.log";
                break;
            case "debug":
                $log_file = "debug.log";
                break;
            case "mail":
                $log_file = "mail.log";
                break;
            case "error":
                $log_file = "error.log";
                break;
            case "admin":
                $log_file = "admin.log";
                break;
            case "cron":
                $log_file = "cron.log";
                break;
            case "access":
                $log_file = "access.log";
                break;
            case "user":
                $log_file = "user.log";
                break;
            case "import":
                $log_file = "import.log";
                break;
            case "propulsion":
                $log_file = "propulsion.log";
                break;
            case "api":
                $log_file = "api.log";
                break;
            case "api_error":
                $log_file = "api_error.log";
                break;
            default:
                self::$errcode = LOGGER_UNKNOWN_LOG_TYPE;
                break;
        }
        if ($log_file) {
            $lines = @file(LOGS_DIR.$log_file);
            if ($lines !== false) {
                if ($reversed) {
                    $result = array_reverse($lines);
                } else {
                    $result = $lines;
                }
            }
        }

        return $result;

    }
}

?>