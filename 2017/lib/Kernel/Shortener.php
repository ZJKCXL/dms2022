<?php

namespace Kernel;

class Shortener {

    public static function shortenData($in_data) {
           $result = false;

           $result = \DB\DataStorage::storeData($in_data);
           if ($result != false) {
               $result = \String\StringUtils::id_to_uid($result);
           }

           return $result;
    }

    public static function getData($in_uid) {
            $result = false;

            $id = \String\StringUtils::uid_to_id($in_uid);
            if (intval($id) > 0) {
                $result = \DB\DataStorage::retrieveData($id);
            }

            return $result;
    }

}

?>
