<?php

namespace Kernel;

require_once(BASE_DIR."/config/routes.php");

class Route
{
    private static $route = array();
    private static $route_data = false;
    private static $routes = false;
    private static $route_params = false;

    public static function parseRoute()
    {
        $result = false;

        $params = Parameters::getParams();

        if ($params !== false && is_array($params) && @count($params) > 0)
        {
            if (strlen($params["r"]) > 0)
            {
                $route_string = trim($params["r"],"/");
                if (strlen($route_string) > 0)
                {
                    $routes = explode("/",$route_string);
                    if ($routes !== false && is_array($routes) && @count($routes)>0)
                    {
                        self::$route = $routes;
                        $result = true;
                    }
                }
            }
        }

        return $result;
    }

    public static function getRoute()
    {
        return self::$route;
    }

    private static function loadRoutes()
    {
        global $routes_cfg;

        if (isset($routes_cfg) && is_array($routes_cfg) && @count($routes_cfg)>0)
        {
            self::$routes = $routes_cfg;
        }

        return true;
    }

    public static function isValid()
    {
        $result = false;

        if (self::$routes === false)
        {
            self::loadRoutes();
        }

        $route_parts = self::$route;
        while (is_array($route_parts) && @count($route_parts)>0)
        {
            if (isset(self::$routes[implode("-",$route_parts)]))
            {
                $result = true;
                break;
            }
            else
            {
                array_pop($route_parts);
            }
        }

        return $result;
    }

    private static function parseRouteData()
    {
        if (self::$routes === false) {
            self::loadRoutes();
        }

        if (self::isValid()) {
            self::$route_data = false;

            $route_parts = self::$route;
            while (is_array($route_parts) && @count($route_parts)>0) {
                if (isset(self::$routes[implode("-",$route_parts)])) {
                    self::$route_data = self::$routes[implode("-",$route_parts)];
                    self::$route_data["route"] = implode("-",$route_parts);
                    break;
                } else {
                    $param = array_pop($route_parts);
                    self::$route_params[] = $param;
                }
            }

            switch (self::$route_data["content_type"]) {
                case "page":
                case "page-export":
                case "pdf-export":
                    self::$route_data["content_dir"] = "ui/workspace";
                    break;
                case "overlay":
                    self::$route_data["content_dir"] = "ui/overlay";
                    break;
                case "report":
                case "report-export":
                case "report-pdf-export":
                    self::$route_data["content_dir"] = "report";
                    break;
                case "script":
                case "script-export":
                    self::$route_data["content_dir"] = "scripts";
                    break;
                case "endpoint":
                    self::$route_data["content_dir"] = "endpoints";
                    break;
                case "api":
                    self::$route_data["content_dir"] = "api";
                    self::$route_data["content_params"]["no_password_expiration"] = 1;
                    self::$route_data["content_params"]["require_auth"] = 1;
                    break;
                case "export":
                    self::$route_data["content_dir"] = "export";
                    self::$route_data["content_params"]["no_password_expiration"] = 1;
                    self::$route_data["content_params"]["require_auth"] = 1;
                    break;
            }
            if (!isset(self::$route_data["need_menu"]))
            {
                self::$route_data["need_menu"] = true;
            }
            if (!isset(self::$route_data["is_secure"]))
            {
                self::$route_data["is_secure"] = false;
            }
            if (self::$route_params != false && is_array(self::$route_params) && count(self::$route_params) > 0)
            {
                self::$route_data["route_params"] = self::$route_params;
            }
        }

        return true;
    }

    public static function getRouteData()
    {
        if (self::$route_data === false)
        {
            self::$route_data == self::parseRouteData();
        }
        return self::$route_data;
    }

    public static function getParams()
    {
        return \Kernel\Parameters::getParam("route_params");
    }
}
