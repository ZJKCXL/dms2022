<?
namespace Kernel;

class User {

    const USER_SESSION_VAL = "auth_user";
    const USER_DATA_SESSION_VAL = "auth_user_data";

    public static function setUser($in_gid) {
        $result = false;

        if (\String\GID::validateGID($in_gid)) {
            $result = \Kernel\Session::storeData(self::USER_SESSION_VAL,$in_gid);
        }
        return $result;
    }

    public static function setUserData($in_data) {
        $result = false;

        if ($in_data != false) {
            $result = \Kernel\Session::storeData(self::USER_DATA_SESSION_VAL,$in_data);
        }

        return $result;
    }

    public static function getUserData() {
        $result = false;

        $result = \Kernel\Session::getData(self::USER_DATA_SESSION_VAL);

        return $result;
    }

    public static function clearUser() {
        $result = false;

        $result = \Kernel\Session::clearData(self::USER_SESSION_VAL);

        return $result;
    }

    public static function clearUserData() {
        $result = false;

        $result = \Kernel\Session::clearData(self::USER_DATA_SESSION_VAL);

        return $result;
    }

    public static function getID() {
        $result = false;

        $gid = \Kernel\Session::getData(self::USER_SESSION_VAL);
        if (\String\GID::validateGID($gid)) {
            $user_data = \Kernel\Session::getData(self::USER_DATA_SESSION_VAL);
            if ($user_data !== false) {
                $result = $user_data["id"];
            }
        }

        return $result;
    }

    public static function getGID() {
        $result = false;

        $gid = \Kernel\Session::getData(self::USER_SESSION_VAL);
        if (\String\GID::validateGID($gid)) {
            $user_data = \Kernel\Session::getData(self::USER_DATA_SESSION_VAL);
            if ($user_data !== false) {
                $result = $user_data["gid"];
            }
        }

        return $result;
    }

    public static function hasValidUser() {
        return (self::getID() > 0);
    }

    public static function getUserType() {
        $result = false;

        $user_data = self::getUserData();
        if ($user_data !== false && is_array($user_data) && @count($user_data) > 0) {
            $result = $user_data["type"];
        }

        return $result;
    }

    public static function getUserCompany() {
        $result = false;

        $user_data = self::getUserData();
        if ($user_data !== false && is_array($user_data) && @count($user_data) > 0) {
            $result = $user_data["company"];
        }

        return $result;
    }

    public static function getUserCountry() {
        $result = false;

        $user_data = self::getUserData();
        if ($user_data !== false && is_array($user_data) && @count($user_data) > 0) {
            $result = $user_data["country"];
        }

        return $result;
    }
}

?>