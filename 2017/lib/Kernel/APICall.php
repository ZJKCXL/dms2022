<?php

namespace Kernel;

class APICall {

    function call($method, $url, $data = false, $set_session = true, $auth_user = false, $auth_pass = false) {
        $curl = curl_init();

        switch ($method)
        {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);

                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }


        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        if ($set_session) {
            curl_setopt($curl, CURLOPT_COOKIE, "PHPSESSID=".session_id());
        }
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        if ($auth_user !== false && $auth_pass !== false) {
            curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($curl, CURLOPT_USERPWD, $auth_user.":".$auth_pass);
        }

        session_write_close();

        $result = curl_exec($curl);

        curl_close($curl);

        if ($result !== false && strlen($result) > 0) {
            $result = json_decode($result, true);
        }

        return $result;
    }
}
?>
