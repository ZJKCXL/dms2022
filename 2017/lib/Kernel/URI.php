<?php
  namespace Kernel;

  class URI {
      public static function getFromApplURI($in_appl_uri) {
          $result = false;

          $data = \Kernel\Shortener::getData($in_appl_uri);
          if (\Kernel\Func::resultValidArr($data)) {
              $result = $data["route"];
          }

          return $result;
      }
  }
?>
