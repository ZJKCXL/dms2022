<?php

namespace Kernel;

class Translation
{

    /**
     * Returns the current language in use
     *
     * @return string
     */
    public static function getLang()
    {
        $result = LANG_DEFAULT;
        if (isset($_COOKIE[COOKIE_LANGUAGE]) && preg_match('/[a-z]{2}/', $_COOKIE[COOKIE_LANGUAGE]))
        {
            $result = $_COOKIE[COOKIE_LANGUAGE];
        }

        return $result;
    }

    public static function init($lang = "cs") {
        @include_once(RESOURCES_DIR."translations/".$lang."/lang.php");
    }

    /**
     * Translate a message using the translation files
     *
     * @param string $message_id
     * @param string $locale (e.g. 'en_US')
     * @return string (message_id on error or non-existant translation)
     */
    public static function get($message_id, $lang = "cs") {

    }

}
