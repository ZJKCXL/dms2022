<?php

//TODO:
//od behe content_js, content_css -> dostanu a pridam do template
//- udelat si "prepare" funkci na pripravu techhle promennych -> prvne je prelozim do elementu, a pak se k nim chovam stejne jako pri nahrade v sablonach

namespace Kernel;

require 'config/templates.php';

class Template
{

    protected static function translate($content)
    {
        if (!autoload_exists('\Kernel\Translation')) {
            return $content;
        }

        if (preg_match_all('/<\\#(.*)\\#>/', $content, $matches)) {
            if (@count($matches) > 0 && @count($matches[1]) > 0) {
                foreach ($matches[1] as $match) {
                    $content = preg_replace('/<\\#'.$match.'\\#>/', \Kernel\Translation::get($match), $content);
                }
            }
        }

        return $content;
    }

    public static function get($template_name, $vars = array(), &$content = "")
    {
        global $templates_cfg;

        if (key_exists($template_name, $templates_cfg) && @file_exists(BASE_DIR . $templates_cfg[$template_name]["file"])) {
            if ($content == "") {
                $content = file_get_contents(BASE_DIR . $templates_cfg[$template_name]["file"]);
            } else {
                $content = str_replace("<%$template_name%>", file_get_contents(BASE_DIR . $templates_cfg[$template_name]["file"]), $content);
            }

            if (is_array($templates_cfg[$template_name]["requires"])) {
                foreach ($templates_cfg[$template_name]["requires"] as $require) {
                    self::get($require, $vars, $content);
                }
            }
        }

        if (key_exists('helpers', $templates_cfg[$template_name]) && is_array($templates_cfg[$template_name]['helpers'])) {
            foreach ($templates_cfg[$template_name]['helpers'] as $content_placeholder => $helper) {
                if (@file_exists($helper)) {
                    include_once BASE_DIR . $helper;

                    $content = str_replace('<$' . $content_placeholder . '$>', $helper_content, $content);
                }
            }
        } else {
            $content = self::prepareData($content, $vars);
        }

        if (is_array($vars)) {
            foreach ($vars as $var_name => $var_value) {
                $content = str_replace('<$' . $var_name . '$>', $var_value, $content);
            }
        }

        $content = self::translate($content);

        return $content;
    }

    public static function prepareData($content, $vars)
    {
        if (preg_match_all('/(<\\$[A-Z_]+\\$>)/', $content, $matches)) {
            $matches = array_unique($matches[0]);
            foreach ($matches as $match) {
                $constant_name = $match;
                $constant_name = str_replace("<$", "", $constant_name);
                $constant_name = str_replace("$>", "", $constant_name);

                $constant_value = defined($constant_name) ? constant($constant_name) : "";

                $content = str_replace($match, $constant_value, $content);
            }
        }

        /* preg_match_all('/(<#[\w\-_]+#>)/', $content, $matches);
          $matches = array_unique($matches[0]);
          foreach ($matches as $match) {
          $constant_name = $match;
          $constant_name = str_replace("<#", "", $constant_name);
          $constant_name = str_replace("#>", "", $constant_name);

          $content = str_replace($match, Translation::get($constant_name), $content);
          } */

        if (is_array($vars)) {
            if (key_exists("content_js", $vars) && is_array($vars["content_js"]) && @count($vars["content_js"]) > 0) {
                $replace = "";

                foreach ($vars["content_js"] as $file) {
                    $component = new UIComponent("script");
                    $component->setAttribute("type", "text/javascript");
                    $component->setAttribute("src", $file);
                    $replace .= $component->getHTML() . "\n";
                }

                $content = str_replace('<$content_js$>', $replace, $content);
            }

            if (key_exists("content_css", $vars) && is_array($vars["content_css"]) && @count($vars["content_css"]) > 0) {
                $replace = "";

                foreach ($vars["content_css"] as $file) {
                    $component = new UIComponent("link");
                    $component->setAttribute("rel", "stylesheet");
                    $component->setAttribute("type", "text/css");
                    $component->setAttribute("href", $file);
                    $component->setAttribute("media", "all");
                    $replace .= $component->getHTML() . "\n";
                }

                $content = str_replace('<$content_css$>', $replace, $content);
            }
        }

        return $content;
    }

    public static function templateCleanup($content)
    {
        $content = preg_replace("/<\\$[A-Za-z_]+\\$>/", "", $content);
        $content = preg_replace("/<%[A-Za-z_]+%>/", "", $content);
        $content = preg_replace("/<#(.*)#>/", "", $content);

        return $content;
    }

}
