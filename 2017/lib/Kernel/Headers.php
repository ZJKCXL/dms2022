<?php
namespace Kernel;

class Headers {
    const HEADER_MORO_HMAC = "X-Moro-HMAC";
    const HEADER_REQUEST_ID = "X-Request-ID";
    const HEADER_MESSAGE_ID = "X-Message-ID";
    const HEADER_DATE = "X-Request-Date";
    const HEADER_STATUS_FORBIDDEN = "HTTP/1.0 403 Forbidden";

    public static function generateMessageID() {

        $result = sprintf(
            "<%s.%s@%s>",
            base_convert(microtime(), 10, 36),
            base_convert(bin2hex(openssl_random_pseudo_bytes(8)), 16, 36),
            $_SERVER['SERVER_NAME']
        );

        return $result;
    }

    public static function generateHeader($header_name, $header_val) {
        header($header_name.": ".$header_val);
    }

    public static function generateStatusHeader($header_type) {
        header($header_type);
    }

    public static function requestHasHeader($in_header) {
        $result = false;

        $headers = \Kernel\Request::getHeaders();
        if (\Kernel\Func::resultValidArr($headers)) {
            $result = array_key_exists(strtolower($in_header), $headers);
        }

        return $result;
    }

    public static function requestGetHeader($in_header) {
        $result = false;
        $headers = \Kernel\Request::getHeaders();
        if (\Kernel\Func::resultValidArr($headers)) {
            $header = $headers[strtolower($in_header)];
            if (strlen($header) > 0) {
                $result = $header;
            }
        }

        return $result;
    }
}
?>
