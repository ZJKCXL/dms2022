<?php

namespace Kernel;

class MessageQueue {
    private static $queue;
    private static $SESSION_TAG = "msqs";

    public static function clearQueue() {
        self::$queue = array();
        \Kernel\Session::storeData(self::$SESSION_TAG,self::$queue);
    }

    public static function enqueue($in_message) {
        self::loadQueue();
        self::$queue[] = $in_message;
        self::flushQueue();
    }

    public static function flushQueue() {
        \Kernel\Session::storeData(self::$SESSION_TAG,self::$queue);
    }

    public static function loadQueue() {
        self::$queue = \Kernel\Session::getData(self::$SESSION_TAG);
    }

    public static function hasData() {
        self::loadQueue();
        return count(self::$queue) > 0;
    }

    public static function getQueue() {
        self::loadQueue();
        return self::$queue;
    }

}

?>
