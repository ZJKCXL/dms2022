<?php
namespace Kernel;

class Message{
    const MESSAGE_OK = 1;
    const MESSAGE_ERROR = 2;
    const MESSAGE_INFO = 3;
    const MESSAGE_QUESTION = 4;

    public static function create($in_message_text, $in_type = self::MESSAGE_INFO) {
        $result = array();

        $result["id"] = \String\GID::create();
        $result["text"] = $in_message_text;
        switch ($in_type) {
            case self::MESSAGE_OK: $result["status"] = "ok"; break;
            case self::MESSAGE_ERROR: $result["status"] = "error"; break;
            case self::MESSAGE_INFO: $result["status"] = "info"; break;
            case self::MESSAGE_QUESTION: $result["status"] = "question"; break;
            default: $result["status"] = "info";
        }

        return $result;
    }
}
?>
