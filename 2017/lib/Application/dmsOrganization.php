<?php
namespace Application;

class dmsOrganization {
    private static $cache = array();

    public static function getATSid($in_organization_id) {
        $result = false;

        $data = self::getDetail($in_organization_id);
        if (\Kernel\Func::resultValidArr($data) > 0) {
            $result = $data["ats_id"];
        }

        return $result;
    }

    public static function getDetail($in_organization_id) {
        $result = false;

        global $link;

        $result = false;

        if (isset(self::$cache[$in_organization_id])) {
            $result = self::$cache[$in_organization_id];
        } else {
            $query = "SELECT organization.*";
            $query .= " FROM organization";
            $query .= " WHERE 1=1";
            $query .= " AND organization.id = ".$in_organization_id;

            $res = @mysql_query($query, $link);
            if ($res && @mysql_num_rows($res) > 0) {
                while ($row = @mysql_fetch_array($res)) {
                      $result = $row;
                }
                self::$cache[$in_organization_id] = $result;
            }
        }

        return $result;

    }
}
?>
