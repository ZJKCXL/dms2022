<?php
namespace Application;

class dmsPerson {
    private static $cache = array();

    public static function getDetail($in_person_id) {
        $result = false;

        global $link;

        $result = false;

        if (isset(self::$cache[$in_person_id])) {
            $result = self::$cache[$in_person_id];
        } else {
            $query = "SELECT person.*";
            $query .= " FROM person";
            $query .= " WHERE 1=1";
            $query .= " AND person.id = ".$in_person_id;

            $res = @mysql_query($query, $link);
            if ($res && @mysql_num_rows($res) > 0) {
                while ($row = @mysql_fetch_array($res)) {
                      $result = $row;
                }
                self::$cache[$in_person_id] = $result;
            }
        }

        return $result;

    }
}
?>
