<?php
namespace Application;

class reportMonthly {
    const DMS_TAX = 1;

    private static $data = array();
    private static $sums = array();

    public static function load($year = false, $month = false, $project = false) {
        global $link;

        $result = false;

        if ($year === false) {
            $year = date("Y");
        }

        if ($month === false) {
            $month = date("m");
        }

        $query = "SELECT reportMonthly.*, tbldmsvalue.DVValue";
        $query .= " FROM reportMonthly, tbldmsvalue";
        $query .= " WHERE 1=1";
        $query .= " AND tbldmsvalue.ID = reportMonthly.dmsValue";
        if ($year !== false) {
            $query .= " AND reportMonthly.year = ".$year;
        }
        if ($month !== false) {
            $query .= " AND reportMonthly.month = ".$month;
        }
        if ($project !== false) {
            $query .= " AND reportMonthly.project_id = ".$project;
        }


        $res = @mysql_query($query, $link);
        if ($res && @mysql_num_rows($res) > 0) {
            while ($row = @mysql_fetch_array($res)) {
                self::$data[$row["project_id"]][$row["DVValue"]] += $row["increment"];
            }
        }

        $result = @count(self::$data) > 0;

        return $result;
    }

    public static function generateSums() {
        $result = false;

        if (@count(self::$data) > 0) {
            foreach (self::$data as $organization_id => $organization_data) {
                foreach ($organization_data as $value => $count) {
                    self::$sums[$organization_id]["sum"] += $value*$count;
                    self::$sums[$organization_id]["count"] += $count;
                }
            }
            if (@count(self::$sums) > 0) {
                foreach (self::$sums as &$sum_data) {
                    $sum_data["price"] = $sum_data["sum"] - ($sum_data["count"] * self::DMS_TAX);
                }
            }
        }
    }

    public static function getData() {
        $result = self::$data;

        return $result;
    }

    public static function getSums() {
        $result = self::$sums;

        return $result;
    }

}
?>
