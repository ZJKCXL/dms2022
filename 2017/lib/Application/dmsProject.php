<?php
namespace Application;

class dmsProject {

    private static $cache = array();
    private static $data_cache = array();

    public static function getData($in_project_id, $on_date = false) {
        global $link;

        $result = false;

        if (isset(self::$data_cache[$in_project_id])) {
            $result = self::$data_cache[$in_project_id];
        } else {
            $query = "SELECT project.*, organization.name as organization_name, organization.street, organization.zipcode, organization.city, organization.companyId";
            $query .= " FROM project, organization";
            $query .= " WHERE 1=1";
            $query .= " AND organization.id = project.organization_id";
            $query .= " AND project.id = ".$in_project_id;


            $res = @mysql_query($query, $link);
            if ($res && @mysql_num_rows($res) > 0) {
                while ($row = @mysql_fetch_array($res)) {
                    $result = $row;
                }
            }

            $contacts = self::getContacts($in_project_id);
            if (\Kernel\Func::resultValidArr($contacts)) {
                $result["contacts"] = $contacts;
            }

            $collection = \Application\dmsCollection::getProjectCollection($in_project_id, $on_date);
            if (\Kernel\Func::resultValidArr($collection)) {
                $result["collection"] = $collection;
            }

            $keywords = self::getKeywords($in_project_id);
            if (\Kernel\Func::resultValidArr($keywords)) {
                $result["keywords"] = $keywords;
            }

            self::$data_cache[$in_project_id] = $result;
        }

        return $result;
    }

    public static function getOrganization($in_project_id) {
        $result = false;

        $data = self::getDetail($in_project_id);
        if (\Kernel\Func::resultValidArr($data) > 0) {
            $result = $data["organization_id"];
        }

        return $result;
    }

    public static function getDetail($in_project_id) {
        $result = false;

        global $link;

        $result = false;

        if (isset(self::$cache[$in_project_id])) {
            $result = self::$cache[$in_project_id];
        } else {
            $query = "SELECT project.*";
            $query .= " FROM project";
            $query .= " WHERE 1=1";
            $query .= " AND project.id = ".$in_project_id;

            $res = @mysql_query($query, $link);
            if ($res && @mysql_num_rows($res) > 0) {
                while ($row = @mysql_fetch_array($res)) {
                    $result = $row;
                }
                $contacts = self::getContacts($in_project_id);
                if (\Kernel\Func::resultValidArr($contacts)) {
                    $result["contacts"] = $contacts;
                }
                self::$cache[$in_project_id] = $result;
            }
        }

        return $result;

    }

    public static function getContacts($in_project_id) {
        $result = false;

        global $link;

        $query = "SELECT projectPerson.person_id";
        $query .= " FROM projectPerson";
        $query .= " WHERE 1=1";
        $query .= " AND projectPerson.project_id = ".$in_project_id;

        $res = @mysql_query($query, $link);
        if ($res && @mysql_num_rows($res) > 0) {
            while ($row = @mysql_fetch_array($res)) {
                $result[] = $row;
            }

        }

        if (\Kernel\Func::resultValidArr($result)) {
            foreach ($result as &$person_row) {
                $person = \Application\dmsPerson::getDetail($person_row["person_id"]);
                if (\Kernel\Func::resultValidArr($person)) {
                    $person_row = array_merge($person_row, $person);
                }
            }
        }


        return $result;
    }

    public static function getKeywords($in_project_id) {
        $result = false;

        global $link;

        $query = "SELECT collectionKeyword.keyword_id";
        $query .= " FROM collectionKeyword, collection";
        $query .= " WHERE 1=1";
        $query .= " AND collectionKeyword.collection_id = collection.id";
        $query .= " AND collection.project_id = ".intval($in_project_id);

        $res = @mysql_query($query, $link);
        if ($res && @mysql_num_rows($res) > 0) {
            while ($row = @mysql_fetch_array($res)) {
                $result[] = $row;
            }
        }

        if (\Kernel\Func::resultValidArr($result)) {
            foreach ($result as &$keyword_row) {
                $keyword = \Application\dmsKeyword::getDetail($keyword_row["keyword_id"]);
                if (\Kernel\Func::resultValidArr($keyword)) {
                    $keyword_row = array_merge($keyword_row, $keyword);
                }
            }
        }

        return $result;
    }

}
?>
