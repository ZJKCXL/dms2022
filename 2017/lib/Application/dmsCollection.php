<?php
namespace Application;

class dmsCollection {

    private static $cache = array();

    public static function publishATS($in_collection_id) {
        $result = false;

        $data = self::getDetail($in_collection_id);
        echo " ... ... self data is ".json_encode($data)."<br />";
        if (\Kernel\Func::resultValidArr($data)) {
            if ($data["project_id"] !== false && !is_null($data["project_id"])) {
                $org = \Application\dmsProject::getOrganization(intval($data["project_id"]));
                echo " ... ... org data is ".json_encode($org)."<br />";
                if (intval($org) > 0) {
                    $org_ats = \Application\dmsOrganization::getATSid(intval($org));
                    echo " ... ... org ats data is ".json_encode($org_ats)."<br />";
                    if (intval($org_ats) > 0) {
                        $keywords = self::getKeywords($in_collection_id);
                        echo " ... ... keywords data is ".json_encode($keywords)."<br />";
                        if (\Kernel\Func::resultValidArr($keywords)) {
                            foreach ($keywords as $keyword) {
                                $keyword_data = \Application\dmsKeyword::getDetail($keyword["keyword_id"]);
                                echo " ... ... keyword data detail is ".json_encode($keyword_data)."<br />";
                                if (\Kernel\Func::resultValidArr($keyword_data)) {
                                    if (($data["validFrom"] !== false && !is_null($data["validFrom"]))
                                    && ($data["validTo"] !== false && !is_null($data["validTo"]))
                                    && ($data["ats_id"] === false || is_null($data["ats_id"]))) {
                                        $result = \ATS\CollectionCreate::createCollection(intval($org_ats), $keyword_data["name"], $data["validFrom"], $data["validTo"]);
                                        echo " ... ... ATS result data is ".json_encode($result)."<br />";
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return $result;
    }

    public static function getATSid($in_collection_id) {
        $result = false;

        $data = self::getDetail($in_collection_id);
        if (\Kernel\Func::resultValidArr($data) > 0) {
            $result = $data["ats_id"];
        }

        return $result;
    }

    public static function getProject($in_collection_id) {
        $result = false;

        $data = self::getDetail($in_collection_id);
        if (\Kernel\Func::resultValidArr($data) > 0) {
            $result = $data["project_id"];
        }

        return $result;
    }

    public static function getDetail($in_collection_id) {
        $result = false;

        global $link;

        $result = false;

        if (isset(self::$cache[$in_collection_id])) {
            $result = self::$cache[$in_collection_id];
        } else {
            $query = "SELECT collection.*";
            $query .= " FROM collection";
            $query .= " WHERE 1=1";
            $query .= " AND collection.id = ".$in_collection_id;

            $res = @mysql_query($query, $link);
            if ($res && @mysql_num_rows($res) > 0) {
                while ($row = @mysql_fetch_array($res)) {
                    $result = $row;
                }
                self::$cache[$in_collection_id] = $result;
            }
        }

        return $result;

    }

    public static function getProjectCollection($in_project_id, $on_date = false) {
        $result = false;

        global $link;

        if ($on_date == false) {
            $on_date = date("Y-m-d");
        }

        $month = \Tools\Date::getMonth($on_date);
        $year = \Tools\Date::getYear($on_date);

        $query = "SELECT collection.*";
        $query .= " FROM collection";
        $query .= " WHERE 1=1";
        $query .= " AND project_id = ".$in_project_id;
        $query .= " AND validFrom <= '".\Tools\Month::getLastDay($month, $year)."'";
        $query .= " AND validTo >= '".\Tools\Month::getFirstDay($month, $year)."'";

        $res = @mysql_query($query, $link);
        if ($res && @mysql_num_rows($res) > 0) {
            while ($row = @mysql_fetch_array($res)) {
                $result[] = $row;
            }
        }

        return $result;
    }

    public static function getKeywords($in_collection_id) {
        $result = false;

        global $link;

        $query = "SELECT collectionKeyword.*";
        $query .= " FROM collectionKeyword";
        $query .= " WHERE 1=1";
        $query .= " AND collection_id = ".$in_collection_id;

        $res = @mysql_query($query, $link);
        if ($res && @mysql_num_rows($res) > 0) {
            while ($row = @mysql_fetch_array($res)) {
                $result[] = $row;
            }
        }

        return $result;
    }

    public static function getKeywordsByATSid($in_ats_id) {
        $result = false;

        global $link;

        $query = "SELECT collectionKeyword.*";
        $query .= " FROM collectionKeyword";
        $query .= " WHERE 1=1";
        $query .= " AND ats_id = ".$in_ats_id;

        $res = @mysql_query($query, $link);
        if ($res && @mysql_num_rows($res) > 0) {
            while ($row = @mysql_fetch_array($res)) {
                $result[] = $row;
            }
        }

        return $result;
    }

    public static function getCollectionsNoATS() {
        $result = false;

        global $link;

        $query = "SELECT collectionKeyword.*";
        $query .= " FROM collectionKeyword";
        $query .= " WHERE 1=1";
        $query .= " AND ats_id IS NULL";
        $query .= " AND YEAR(edited) >= 2017";

        $res = @mysql_query($query, $link);
        if ($res && @mysql_num_rows($res) > 0) {
            while ($row = @mysql_fetch_array($res)) {
                $result[] = $row;
            }
        }


        return $result;
    }
}
?>
