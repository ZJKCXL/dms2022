<?php
namespace Application;

class dmsKeyword {
    private static $cache = array();

    public static function getDetail($in_keyword_id) {
        $result = false;

        global $link;

        $result = false;

        if (isset(self::$cache[$in_keyword_id])) {
            $result = self::$cache[$in_keyword_id];
        } else {
            $query = "SELECT keyword.*";
            $query .= " FROM keyword";
            $query .= " WHERE 1=1";
            $query .= " AND keyword.id = ".$in_keyword_id;

            $res = @mysql_query($query, $link);
            if ($res && @mysql_num_rows($res) > 0) {
                while ($row = @mysql_fetch_array($res)) {
                    $result = $row;
                }
                self::$cache[$in_keyword_id] = $result;
            }
        }

        return $result;

    }

    public static function getColection($in_keyword_id) {
        $result = false;

        global $link;

        $query = "SELECT collectionKeyword.collection_id";
        $query .= " FROM collectionKeyword";
        $query .= " WHERE 1=1";
        $query .= " AND collectionKeyword.id = ".$in_keyword_id;

        $res = @mysql_query($query, $link);
        if ($res && @mysql_num_rows($res) > 0) {
            while ($row = @mysql_fetch_array($res)) {
                $result = $row;
            }
        }
        return $result;

    }
}
?>
