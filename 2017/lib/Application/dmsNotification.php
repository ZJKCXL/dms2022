<?php
namespace Application;

class dmsNotification {

    const NOTIFICATION_TEXT_DATA = "Na základě vzájemné smlouvy vás tímto informujeme, že bude provedena úhrada peněžní částky vybrané za došlé DMS za měsíc <%MONTH_NAME%> <%YEAR%> v rámci projektu ";
    //const NOTIFICATION_TEXT_DATA = "Na základě vzájemné smlouvy vás tímto informujeme, že bude provedena úhrada peněžní částky vybrané za došlé DMS za měsíc březen 2017 – doplatek za období 21.3. – 31.3. 2017 v rámci projektu ";
    const NOTIFICATION_TEMPLATE = "/avizo/template.php";
    const EMAIL_TEMPLATE = "/avizo/email.php";

    const ORGANIZATION_NAME = "<%ORGANIZATION_NAME%>";
    const ORGANIZATION_STREET = "<%ORGANIZATION_STREET%>";
    const ORGANIZATION_ZIP = "<%ORGANIZATION_ZIP%>";
    const ORGANIZATION_CITY = "<%ORGANIZATION_CITY%>";
    const ORGANIZATION_COMPANYID = "<%ORGANIZATION_COMPANYID%>";
    const ORGANIZATION_PERSON = "<%ORGANIZATION_PERSON%>";

    const AMOUNT = "<%AMOUNT%>";
    const ACCOUNT = "<%ACCOUNT%>";
    const VAR_SYMBOL = "<%VAR_SYMBOL%>";

    const GENERATION_DATE = "<%GENERATION_DATE%>";
    const NUM_FORMATED = "<%NUM_FORMATED%>";

    const NOTIFICATION_TEXT = "<%NOTIFICATION_TEXT%>";
    const EMAIL_COLLECTION = "<%COLLECITON%>";
    const EMAIL_DATE = "<%DATE%>";
    const EMAIL_SUBJECT = " [dms] Avizo kolekce <%COLLECITON%> za obdobi <%DATE%>";
    const EMAIL_SENDER = "dmsasistent@donorsforum.cz";


    //21.-31.3.2017
    //const NOTIFICATION_ID_START = 1;
    const NOTIFICATION_ID_START = 255;

    private static $template_data = "";
    private static $email_template_data = "";

    public static function loadTemplate() {
        global $globalgal;

        $result = false;

        $tmp_data = file_get_contents($globalgal.self::NOTIFICATION_TEMPLATE);
        if (strlen($tmp_data) > 0) {
            self::$template_data = $tmp_data;
        }

        $tmp_data = file_get_contents($globalgal.self::EMAIL_TEMPLATE);
        if (strlen($tmp_data) > 0) {
            self::$email_template_data = $tmp_data;
        }

        $result = @strlen(self::$template_data) > 0;

        return $result;
    }

    public static function generateNotification($in_project, $in_amount, $in_notification_id, $in_year, $in_month, $force_issue_date = false) {
        $result = false;

        $not_data = self::$template_data;
        $project_data = \Application\dmsProject::getData($in_project);
        if (strlen($not_data) > 0 && \Kernel\Func::resultValidArr($project_data)) {
            $not_data = str_replace(self::ORGANIZATION_NAME, $project_data["organization_name"], $not_data);
            $not_data = str_replace(self::ORGANIZATION_CITY, $project_data["city"], $not_data);
            $not_data = str_replace(self::ORGANIZATION_COMPANYID, $project_data["companyId"], $not_data);
            $not_data = str_replace(self::ORGANIZATION_STREET, $project_data["street"], $not_data);
            $not_data = str_replace(self::ORGANIZATION_ZIP, $project_data["zipcode"], $not_data);

            $not_data = str_replace(self::AMOUNT, money_format("%n", $in_amount), $not_data);
            $not_data = str_replace(self::ACCOUNT, $project_data["account"], $not_data);

            $not_text = self::NOTIFICATION_TEXT_DATA;
            $not_text = str_replace("<%MONTH_NAME%>", \Tools\Month::getLongName($in_month), $not_text);
            $not_text = str_replace("<%YEAR%>", $in_year, $not_text);

            $not_data = str_replace(self::NOTIFICATION_TEXT, $not_text." \"".$project_data["name"]."\", <strong>DMS ".$project_data["collection"][0]["name"]."</strong>", $not_data);

            if (\Kernel\Func::resultValidArr($project_data["contacts"])) {
                foreach ($project_data["contacts"] as $contact_row) {
                    $contacts[] = $contact_row["firstname"]." ".$contact_row["surname"];
                }
                if (\Kernel\Func::resultValidArr($contacts)) {
                    $not_data = str_replace(self::ORGANIZATION_PERSON, implode(", ", $contacts), $not_data);
                }
            }

            if ($force_issue_date !== false) {
                $dt = strtotime($force_issue_date);
                $not_data = str_replace(self::GENERATION_DATE, date("j.n.Y", $dt), $not_data);
            } else {
                $not_data = str_replace(self::GENERATION_DATE, date("j.n.Y"), $not_data);
            }
            $not_data = str_replace(self::NUM_FORMATED, $in_notification_id."/".str_pad($in_month, 2, "0", STR_PAD_LEFT)."/".$in_year, $not_data);
            $not_data = str_replace(self::VAR_SYMBOL, $in_notification_id.str_pad($in_month, 2, "0", STR_PAD_LEFT).$in_year, $not_data);

            $result = $not_data;
        }

        return $result;
    }

    public static function getNextNotificationID($year, $month) {
        global $link;

        $result = false;

        $query = "SELECT  dmsNotification.notification_id ";
        $query .= " FROM dmsNotification";
        $query .= " WHERE 1=1";
        $query .= " AND dmsNotification.year = ".$year;
        $query .= " AND dmsNotification.month = ".$month;
        $query .= " Order by notification_id   ";
         
 

        $res = @mysql_query($query, $link);
        if ($res && @mysql_num_rows($res) > 0) {
            while ($row = @mysql_fetch_array($res)) {
             $result = intval($row["notification_id"])+1;
            }
        } else {          
           $result = self::NOTIFICATION_ID_START;
        }

        return $result;
    }

    public static function storeNotification($in_project, $in_amount, $in_year, $in_month, $force_download = false, $force_issue_date = false) {
        $result = false;

        global $link;

        if (strlen(self::$template_data) <= 0) {
            self::loadTemplate();
        }

        $proj_data = \Application\dmsProject::getData($in_project, $in_year."-".$in_month."-01");
        if (\Kernel\Func::resultValidArr($proj_data)) {
            $not_id = self::getNextNotificationID($in_year, $in_month);
            if (intval($not_id) > 0) {
                $not_text = self::generateNotification($in_project, $in_amount, $not_id, $in_year, $in_month, $force_issue_date);

                $pdf_name = $proj_data["collection"][0]["name"]."-".$not_id."-".$in_year."-".$in_month.".pdf";
                \Export\PDF::init();
                \Export\PDF::exportData($not_text);
                \Export\PDF::doExport($pdf_name, $force_download);

                if (strlen($not_text) > 0) {
                    $sql .= "INSERT INTO dmsNotification (notification_id, project_id, year, month, amount, generated_when, raw_data, notification_text, notification_file)";
                    $sql .= " VALUES (";
                    $sql .= $not_id;
                    $sql .= ", ".$in_project;
                    $sql .= ", ".$in_year;
                    $sql .= ", ".$in_month;
                    $sql .= ", ".$in_amount;
                    $sql .= ", NOW()";
                    $sql .= ",'".@mysql_real_escape_string(json_encode($proj_data), $link)."'";
                    $sql .= ",'".@mysql_real_escape_string($not_text, $link)."'";
                    $sql .= ",'".@mysql_real_escape_string($pdf_name, $link)."'";
                    $sql .= ")";

                    $res = @mysql_query($sql, $link);
                    $result = ($res && @mysql_affected_rows($link) > 0);
                    
                }
            }
        }

        return $result;
    }

    public static function getNotifications($year = false, $month = false, $project = false) {
        global $link;

        $result = false;

        if ($year === false) {
            $year = date("Y");
        }

        if ($month === false) {
            $month = date("m");
        }

        $query = "SELECT dmsNotification.*";
        $query .= " FROM dmsNotification";
        $query .= " WHERE 1=1";
        if ($year !== false) {
            $query .= " AND dmsNotification.year = ".$year;
        }
        if ($month !== false) {
            $query .= " AND dmsNotification.month = ".$month;
        }
        if ($project !== false) {
            $query .= " AND dmsNotification.project_id = ".$project;
        }
        $query .= " ORDER by  notification_file ";
        $res = @mysql_query($query, $link);
        if ($res && @mysql_num_rows($res) > 0) {
            while ($row = @mysql_fetch_array($res)) {
                $result[] = $row;
            }
        }

        return $result;
    }

    public static function generateEmail($in_notification, $year, $month) {
        $result = false;

        $body = self::$email_template_data;
        /* nahrazeni body */

        $raw_data = json_decode($in_notification["raw_data"], true);

        $recepients = array();
        if (\Kernel\Func::resultValidArr($raw_data["contacts"])) {
            foreach ($raw_data["contacts"] as $contact) {
                if (strlen($contact["email"]) && \String\Validate::string($contact["email"], "email", false)) {
                    $recepients[] = $contact["email"];
                }
            }
        }

        //$recepients = array("vladimir@honeypot.cz", "zjk@honeypot.cz");


        $body = str_replace(self::EMAIL_COLLECTION, $raw_data["collection"][0]["name"], $body);
        $body = str_replace(self::EMAIL_DATE, $month." - ".$year, $body);

        $subject = self::EMAIL_SUBJECT;
        $subject = str_replace(self::EMAIL_COLLECTION, $raw_data["collection"][0]["name"], $subject);
        $subject = str_replace(self::EMAIL_DATE, $month." - ".$year, $subject);

        if (\Kernel\Func::resultValidArr($recepients)) {
            $result = \Tools\Mail::doSend($recepients, $subject, $body, false, self::EMAIL_SENDER, self::EMAIL_SENDER);
        }

        return $result;
    }
}
?>
