<?php
namespace Application;

class dmsOrg4Board {

    const BOARD_TEXT_DATA = "RADA DMS - <%YEAR%> ";
    const BOARD_TEMPLATE = "/board/template.php";

    const ORGANIZATION_NAME = "<%ORGANIZATION_NAME%>";
    const ORGANIZATION_STREET = "<%ORGANIZATION_STREET%>";
    const ORGANIZATION_ZIP = "<%ORGANIZATION_ZIP%>";
    const ORGANIZATION_CITY = "<%ORGANIZATION_CITY%>";
    const ORGANIZATION_COMPANYID = "<%ORGANIZATION_COMPANYID%>";
    const ORGANIZATION_PERSON = "<%ORGANIZATION_PERSON%>";

    const AMOUNT = "<%AMOUNT%>";
    const ACCOUNT = "<%ACCOUNT%>";
    const VAR_SYMBOL = "<%VAR_SYMBOL%>";

    const GENERATION_DATE = "<%GENERATION_DATE%>";
    const NUM_FORMATED = "<%NUM_FORMATED%>";

    const BOARD_TEXT = "<%BOARD_TEXT%>";

    private static $template_data = "";
 
    public static function loadTemplate() {
        global $globalgal;

        $result = false;

        $tmp_data = file_get_contents($globalgal.self::BOARD_TEMPLATE);
        if (strlen($tmp_data) > 0) {
            self::$template_data = $tmp_data;
        }
        $result = @strlen(self::$template_data) > 0;
        return $result;
    }

    public static function generateBoard($in_org) {
        $result = false;

        $not_data = self::$template_data;
        $project_data = \Application\dmsProject::getData($in_project);
        if (strlen($not_data) > 0 && \Kernel\Func::resultValidArr($project_data)) {
            $not_data = str_replace(self::ORGANIZATION_NAME, $project_data["organization_name"], $not_data);
            $not_data = str_replace(self::ORGANIZATION_CITY, $project_data["city"], $not_data);
            $not_data = str_replace(self::ORGANIZATION_COMPANYID, $project_data["companyId"], $not_data);
            $not_data = str_replace(self::ORGANIZATION_STREET, $project_data["street"], $not_data);
            $not_data = str_replace(self::ORGANIZATION_ZIP, $project_data["zipcode"], $not_data);

            $not_data = str_replace(self::AMOUNT, money_format("%n", $in_amount), $not_data);
            $not_data = str_replace(self::ACCOUNT, $project_data["account"], $not_data);

            $not_text = self::NOTIFICATION_TEXT_DATA;
            $not_text = str_replace("<%MONTH_NAME%>", \Tools\Month::getLongName($in_month), $not_text);
            $not_text = str_replace("<%YEAR%>", $in_year, $not_text);

            $not_data = str_replace(self::NOTIFICATION_TEXT, $not_text." \"".$project_data["name"]."\", <strong>DMS ".$project_data["collection"][0]["name"]."</strong>", $not_data);

            if (\Kernel\Func::resultValidArr($project_data["contacts"])) {
                foreach ($project_data["contacts"] as $contact_row) {
                    $contacts[] = $contact_row["firstname"]." ".$contact_row["surname"];
                }
                if (\Kernel\Func::resultValidArr($contacts)) {
                    $not_data = str_replace(self::ORGANIZATION_PERSON, implode(", ", $contacts), $not_data);
                }
            }

            if ($force_issue_date !== false) {
                $dt = strtotime($force_issue_date);
                $not_data = str_replace(self::GENERATION_DATE, date("j.n.Y", $dt), $not_data);
            } else {
                $not_data = str_replace(self::GENERATION_DATE, date("j.n.Y"), $not_data);
            }
            $not_data = str_replace(self::NUM_FORMATED, $in_notification_id."/".str_pad($in_month, 2, "0", STR_PAD_LEFT)."/".$in_year, $not_data);
            $not_data = str_replace(self::VAR_SYMBOL, $in_notification_id.str_pad($in_month, 2, "0", STR_PAD_LEFT).$in_year, $not_data);

          //  $result = $not_data;
        }

       //  return $result;
    }

 

    public static function storeBoard($in_org) {
        $result = false;

        global $link;

        if (strlen(self::$template_data) <= 0) {
            self::loadTemplate();
        }

        
         
    
         
                $not_text = self::generateBoard($in_org);

                $pdf_name = $proj_data["collection"][0]["name"]."-".$not_id."-".$in_year."-".$in_month.".pdf";
                \Export\PDF::init();
                \Export\PDF::exportData($not_text);
                \Export\PDF::doExport($pdf_name, $force_download);

 
           
      

        return $result;
    }

 

 
}
?>
