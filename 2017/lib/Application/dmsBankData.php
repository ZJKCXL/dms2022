<?php
namespace Application;

class dmsBankData {

    const DMS_TRAILER = "DMS";
    const FORUM_REMARK = "FORUM DARCU";
    const REMARK_SEPARATOR = "|";
    const FD_ACCOUNT = "238181841/0300";

    private static $data = false;

    public static function generateData($in_year, $in_month) {
        $result = false;

        $notif = \Application\dmsNotification::getNotifications($in_year, $in_month);
        if (\Kernel\Func::resultValidArr($notif)) {
            \Export\ABO::init();
            \Export\ABO::outputUhl1("Fórum dárcù");
            \Export\ABO::outputAccountingHeader();
            $entries = array();
            foreach ($notif as $notif_data) {
                $data = json_decode($notif_data["raw_data"], true);
                if (\Kernel\Func::resultValidArr($data)) {
                    $entry = array();
                    $entry["amount"] = $notif_data["amount"];
                    $entry["variableSymbol"] = $notif_data["notification_id"].str_pad($notif_data["month"], 2, "0", STR_PAD_LEFT).$notif_data["year"];
                    $entry["constantSymbol"] = "0308";
                    $entry["creditAccount"] = $data["account"];
                    $entry["debetAccount"] = self::FD_ACCOUNT;
                    $entry["remarks"] = self::DMS_TRAILER." ".$data["collection"][0]["name"].self::REMARK_SEPARATOR.self::FORUM_REMARK.self::REMARK_SEPARATOR.\Tools\Month::getLongName($notif_data["month"]).self::REMARK_SEPARATOR.$data["organization_name"];
                    $entry["purgeDate"] = \Tools\DateTimeExt::convertToDate($notif_data["generated_when"]);

                    $entries[] = $entry;
                }
            }
            if (\Kernel\Func::resultValidArr($entries)) {
                \Export\ABO::outputAccountingBody($entries);
            }
            \Export\ABO::outputAccountingFooter();
        }

        $res = \Export\ABO::getBuffer();
        if ($res !== false && strlen($res) > 0) {
            $result = true;
            self::$data = $res;
        }

        return $result;
    }

}
?>
