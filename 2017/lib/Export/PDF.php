<?php
namespace Export;

class PDF {

    const PDF_OUTPUT_METHOD = "F";
    const PDF_DEFAULT_FORMAT = "A4";

    public static $pdf_obj = false;
    private static $out_file_name = false;
    private static $out_method = self::PDF_OUTPUT_METHOD;
    private static $out_page_format = self::PDF_DEFAULT_FORMAT;

    public static function init($page_format = false, $params = array()) {
        if ($page_format !== false) {
            self::$out_page_format = $page_format;
        }

        self::$pdf_obj = new \Mpdf\Mpdf(array(self::$out_method, self::$out_page_format));

        if (self::$pdf_obj !== null) {
            self::$pdf_obj->_setPageSize(self::$out_page_format,$orientation);
            self::$pdf_obj->SetAuthor(APP_NAME);
            self::$pdf_obj->charset_in = "utf-8";
            if (isset($params["footer"])) {
                self::$pdf_obj->SetFooter($params["footer"]);
            }
        }
    }

    public static function exportData($content) {
        if (self::$pdf_obj !== null) {
            self::$pdf_obj->WriteHTML($content);
        }
    }

    public static function doExport($filename, $force_download = false) {
        if (self::$pdf_obj !== null) {
            if ($force_download) {
                echo self::$pdf_obj->Output(FILE_DIR.$filename, "D");
            } else {
                echo self::$pdf_obj->Output(FILE_DIR.$filename, self::$out_method);
            }
        }
    }
}
?>
