<?php
namespace Export;

class ABO {
    /**
    * End of line
    * @var string
    */
    public static $eoln = "\r\n";

    /**
    * Suffix
    * @var string
    */
    public static $suffix = 'kpc';

    /**
    * Encoding of input data (output must be in ASCII encoding).
    * @var string
    */
    protected static $_inputEncoding = 'UTF-8';

    /**
    * Output file buffer.
    * @var string
    */
    protected static $buffer = '';

    /**
    * Group has common debet account.
    * @var boolean
    */
    protected static $_commmonDebetAccount = false;

    public static function init() {
        self::$buffer = "";
        setlocale(LC_ALL, 'cs_CZ.UTF-8');
    }

    protected static function _convertDate($date)
    {
        return substr($date, 8, 2) . substr($date, 5, 2) . substr($date, 2, 2);
    }

    protected static function _convertString($string, $maxLength)
    {
        $result = iconv(self::$_inputEncoding, 'ASCII//TRANSLIT', $string);
        $result = strtoupper($result);
        $result = str_replace('@', '', $result);

        if (strlen($string) > $maxLength) {
            $result = substr($result, 0, $maxLength);
        } else {
            $result = sprintf("%-{$maxLength}s", $result);
        }

        return $result;
    }

    protected static function _checkAndConvertAccount($account) {
        if (! preg_match("/^(\d{1,6}-)?(\d{2,10})\/\d{3,4}$/", $account, $match)) {
            echo "Account '{$account}' is not in the format PPPPPP-CCCCCCCCCC/KKKK";
        }

        $anteNumber = ($match[1]) ? rtrim($match[1], '-') : "";
        $number = $match[2];

        if (!self::_isModulo11($number)) {
            echo "Account number '{$number}' is not modulo 11.";
        }

        if (!self::_isModulo11($anteNumber)) {
            echo "Account antenumber '{$anteNumber}' is not modulo 11.";
        }

        return ($anteNumber) ? "{$anteNumber}{$number}" : $number;
    }

    protected static function _isModulo11($number) {
        static $weights = array(1, 2, 4, 8 , 5, 10, 9, 7, 3, 6);

        $sum = 0;
        $reverse = strrev($number);
        $length = strlen($reverse);
        for ($n=0; $n<$length; $n++) {
            $sum+= (int) substr($reverse, $n, 1) * $weights[$n];
        }

        return ($sum % 11) == 0;
    }

    public static function outputUhl1($clientName) {
        self::$buffer .=  'UHL1';
        self::$buffer .=  self::_convertDate(date('Y-m-d'));
        self::$buffer .=  self::_convertString($clientName, 20);
        self::$buffer .=  '1234567890';
        self::$buffer .=  '001';
        self::$buffer .=  '999';
        self::$buffer .=  '111111';
        self::$buffer .=  '222222';
        self::$buffer .=  self::$eoln;
    }

    public static function outputAccountingHeader() {
        self::$buffer .=  '1';
        self::$buffer .=  ' ';
        self::$buffer .=  '1501';
        self::$buffer .=  ' ';
        self::$buffer .=  '111111';
        self::$buffer .=  ' ';
        self::$buffer .=  '2400';
        self::$buffer .=  self::$eoln;
    }

    public static function outputAccountingFooter() {
        self::$buffer .=  '5';
        self::$buffer .=  ' ';
        self::$buffer .=  '+';
        self::$buffer .=  self::$eoln;
    }

    /**
    * It parses the given array of debet entries and creates appropriate groups.
    *
    * In the first step it groups items with the same purgeDate.
    * In the second step it calculate total amount of each group and check if the debet
    * account in the group is the same.
    * In the third step it outputs groups.
    * @param array $entries Each item is in the format of entry {@see outputDebetEntry}
    *              with the additional (required) field 'purgeDate'.
    */
    public static function outputAccountingBody($entries) {
        /// first step
        $groups = array();
        foreach ($entries as $n => $entry) {
            if (! array_key_exists($entry['purgeDate'], $groups)) {
                $groups[$entry['purgeDate']] = array();
            }
            array_push($groups[$entry['purgeDate']], $n);
        }

        /// second stage
        foreach ($groups as $purgeDate => $group) {
            $totalAmount = 0.0;
            $commonDebetAccount = true;
            foreach ($group as $entryNum) {
                $totalAmount += $entries[$entryNum]['amount'];
                if ($commonDebetAccount === true) {
                    $commonDebetAccount = $entries[$entryNum]['debetAccount'];
                } elseif (is_string($commonDebetAccount)) {
                    if ($commonDebetAccount != $entries[$entryNum]['debetAccount']) {
                        $commonDebetAccount = false;
                    }
                }
            }

            $groups[$purgeDate] = array(
                'totalAmount' => $totalAmount,
                'commonDebetAccount' => $commonDebetAccount,
                'entries' => $groups[$purgeDate]
            );
        }

        /// third stage
        foreach ($groups as $purgeDate => $group) {
            self::outputGroupHeader(
                $group['totalAmount'], $purgeDate, $group['commonDebetAccount']);

            foreach ($group['entries'] as $entryNum) {
                self::outputDebetEntry($entries[$entryNum]);
            }

            self::outputGroupFooter();
        }
    }

    /**
    * Group header is characterized by the common purge date and possibly
    * common debet account.
    *
    * @param float $amount Total sum of all debet entries in this group (2 digits precison).
    * @param date $purgeDate Date in the format 'YYYY-MM-DD'.
    * @param string $accountDebet Common debet account (optional).
    */
    public function outputGroupHeader($amount, $purgeDate, $accountDebet='') {
        self::$buffer .=  '2';
        self::$buffer .=  ' ';
        if ($accountDebet) {
            self::$_commmonDebetAccount = true;
            self::$buffer .=  self::_checkAndConvertAccount($accountDebet);
        } else {
            self::$_commmonDebetAccount = false;
        }
        self::$buffer .=  ' ';
        self::$buffer .=  sprintf("%d", $amount*100);
        self::$buffer .=  ' ';
        self::$buffer .=  self::_convertDate($purgeDate);
        self::$buffer .=  self::$eoln;
    }

    public function outputGroupFooter() {
        self::$buffer .=  '3';
        self::$buffer .=  ' ';
        self::$buffer .=  '+';
        self::$buffer .=  self::$eoln;
    }

    /**
    * Output one debet entry.
    *
    * @param array $entry Associative array with the items debetAccount, creditAccount,
    *                     amount (2 digits precison), variableSymbol, constantSymbol,
    *                     specificSymbol (optional) and remarks (array, optional).
    */
    public function outputDebetEntry($entry)
    {
        if (! self::$_commmonDebetAccount) {
            self::$buffer .=  self::_checkAndConvertAccount($entry['debetAccount']);
            self::$buffer .=  ' ';
        }

        try {
            self::$buffer .=  self::_checkAndConvertAccount($entry['creditAccount']);
        } catch (InvalidArgumentException $e) {
            die("Wrong format of account for " . print_r($entry, true) . $e->getMessage());
        }
        self::$buffer .=  ' ';

        self::$buffer .=  sprintf("%d", $entry['amount']*100);
        self::$buffer .=  ' ';

        if (! preg_match("/^\d{1,10}$/", $entry['variableSymbol'])) {
            throw new InvalidArgumentException(
                "Variable symbol have to consist of 1-10 digits");
        }
        self::$buffer .=  $entry['variableSymbol'];
        self::$buffer .=  ' ';

        if (! preg_match("/^\d{1,4}$/", $entry['constantSymbol'])) {
            throw new InvalidArgumentException(
                "Constant symbol have to consist of 1-4 digits");
        }
        list ($accountNumber, $bankCode) = explode("/", $entry['creditAccount']);
        self::$buffer .=  sprintf("%04d", $bankCode)
        . sprintf("%04d", $entry['constantSymbol']);
        self::$buffer .=  ' ';

        if (! isset($entry['specificSymbol'])) {
            $entry['specificSymbol'] = '';
        }
        if (! preg_match("/^\d{0,10}$/", $entry['specificSymbol'])) {
            throw new InvalidArgumentException(
                "Specific symbol have to consist of 0-10 digits");
        }
        self::$buffer .=  $entry['specificSymbol'];
        self::$buffer .=  ' ';

        if (isset($entry['remarks'])) {
            $remarks = array();
            if (is_string($entry['remarks'])) {
                $remarks = explode('|', $entry['remarks']);
            } elseif (is_array($entry['remarks'])) {
                $remarks = array_values($entry['remarks']);
            }
            $length = (count($remarks) > 4) ? 4 : count($remarks);

            $result = array();
            for ($n=0; $n<$length; $n++) {
                $string = trim($remarks[$n]);
                if (! empty($string)) {
                    $result[] = self::_convertString($remarks[$n], 35);
                }
            }

            if (! empty($result)) {
                self::$buffer .=  'AV:' . implode('|', $result);
            }
        }
        self::$buffer .=  self::$eoln;
    }

    public static function getBuffer() {
        return self::$buffer;
    }

    public static function serveFile($file_name) {
        $result = false;

        header("Cache-Control: ");// leave blank to avoid IE errors
        header("Pragma: ");// leave blank to avoid IE errors
        header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment; filename=\"".$file_name.".".self::$suffix."\"");

        echo self::$buffer;
    }
}
?>
