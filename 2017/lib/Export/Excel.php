<?

namespace Export;

class Excel {

    const EXCEL_SUFFIX = ".xls";
    const EXCEL_VERSION = "Excel5";
    const EXCEL_OUTPUT = "php://output";
    const SLEEP_INTERVAL = 1;

    public static $excel_obj = false;
    private static $out_file_name = false;


    public static function init($out_file_name, $in_desc = false) {
        self::$excel_obj = new \PHPExcel();
        self::$out_file_name = now()."-".\String\StringUtils::normalize($out_file_name,"-").self::EXCEL_SUFFIX;
    }

    public static function exportData($in_data,$in_desc = false, $start_row = false) {
        self::$excel_obj->getProperties()->setCreator(APP_NAME);
        self::$excel_obj->getProperties()->setLastModifiedBy(APP_NAME);

        self::$excel_obj->setActiveSheetIndex(0);

        if ($in_desc != false) {
            self::$excel_obj->getProperties()->setTitle($in_desc);
            self::$excel_obj->getProperties()->setSubject($in_desc);
            self::$excel_obj->getProperties()->setDescription($in_desc);
            self::$excel_obj->getActiveSheet()->setTitle('Data');
        }

        if (\Kernel\Func::resultValidArr($in_data)) {
            if ($start_row !== false && intval($start_row)>0) {
                $row = $start_row;
            } else {
                $row = 1;
            }

            foreach ($in_data as $row_idx => $data_row) {
                if (\Kernel\Func::resultValidArr($data_row))  {
                    $col = 0;
                    foreach ($data_row as $data_idx => $data_column) {
                        if (!is_array($data_column)) {
                            self::$excel_obj->getActiveSheet()->setCellValueByColumnAndRow($col,$row,$data_column);
                        } else {
                            switch ($data_idx) {
                                case "wrap":
                                    foreach ($data_column as $style_name => $style_val) {
                                        self::$excel_obj->getActiveSheet()->getStyle($style_val.$row)->getAlignment()->setWrapText(true);
                                    }
                                    break;
                                case "merge":
                                    foreach ($data_column as $style_name => $style_val) {
                                        $range = $style_val;
                                        $range = explode(":",$range);
                                        if (count($range) == 2){
                                            $range[0] .= $row;
                                            $range[1] .= $row;
                                            $range = implode(":",$range);
                                            self::$excel_obj->getActiveSheet()->mergeCells($range);
                                        }
                                    }
                                    break;
                                case "bold":
                                    foreach ($data_column as $style_name => $style_val) {
                                        $range = $style_val;
                                        $range = explode(":",$range);
                                        if (count($range) == 2) {
                                            $range[0] .= $row;
                                            $range[1] .= $row;
                                            $range = implode(":",$range);
                                            self::$excel_obj->getActiveSheet()->getStyle($range)->getFont()->setBold(true);
                                        }
                                    }
                                    break;
                                case "center":
                                    foreach ($data_column as $style_name => $style_val) {
                                        $range = $style_val;
                                        $range = explode(":",$range);
                                        if (count($range) == 2) {
                                            $range[0] .= $row;
                                            $range[1] .= $row;
                                            $range = implode(":",$range);
                                            self::$excel_obj->getActiveSheet()->getStyle($range)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                                        }
                                    }
                                    break;
                                case "number":
                                    foreach ($data_column as $style_name => $style_val) {
                                        $addr = $style_val.$row;
                                        self::$excel_obj->getActiveSheet()->getCell($addr)->setDataType(\PHPExcel_Cell_DataType::TYPE_NUMERIC);
                                    }
                                    break;
                                case "money":
                                    foreach ($data_column as $style_name => $style_val) {
                                        $addr = $style_val.$row;
                                        self::$excel_obj->getActiveSheet()->getStyle($style_val.$row)->getNumberFormat()->setFormatCode("### ### ###.00 Kč;0.00 Kč;0.00 Kč");
                                    }
                                    break;
                                case "hour":
                                    foreach ($data_column as $style_name => $style_val) {
                                        $addr = $style_val.$row;
                                        self::$excel_obj->getActiveSheet()->getStyle($style_val.$row)->getNumberFormat()->setFormatCode("mm:ss");
                                    }
                                    break;
                            }
                        }
                        $col++;
                    }
                }

                if (\Kernel\Func::resultValidArr($data_row) && $row_idx == "head") {
                    $cell_span = "A".$row.":".\String\StringUtils::num2alpha(count($data_row)).$row;
                    self::$excel_obj->getActiveSheet()->getStyle($cell_span)->getFont()->setBold(true);
                }

                $row++;
            }

            $last_cell = \String\StringUtils::num2alpha(count($data_row)-1).($row - 1);

            self::$excel_obj->getActiveSheet()->getStyle("A1:".$last_cell)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_TOP);
            for ($i = 65;$i < 65 + $col;$i++) {
                self::$excel_obj->getActiveSheet()->getColumnDimension(chr($i))->setAutoSize(true);
            }

            for ($j=1;$j<$row;$j++) {
                self::$excel_obj->getActiveSheet()->getRowDimension($j)->setRowHeight(-1);
            }
        }

        return true;
    }

    private static function exportHeaders() {
        header("Cache-Control: ");// leave blank to avoid IE errors
        header("Pragma: ");// leave blank to avoid IE errors
        header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment; filename=\"".self::$out_file_name."\"");
    }

    public static function doExport() {
        self::exportHeaders();
        sleep(1);

        $writer = \PHPExcel_IOFactory::createWriter(self::$excel_obj, self::EXCEL_VERSION);
        $writer->save(self::EXCEL_OUTPUT);
        self::$excel_obj->disconnectWorksheets();
        self::$excel_obj = false;

    }

    /*
    public static function export_array_excel_multi(&$excel_obj,$in_data,$in_desc = false, $start_row = false)
    {
    self::$excel_obj->getProperties()->setCreator("Eclipse");
    self::$excel_obj->getProperties()->setLastModifiedBy("Eclipse");

    self::$excel_obj->setActiveSheetIndex(0);

    if ($in_desc != false)
    {
    self::$excel_obj->getProperties()->setTitle($in_desc);
    self::$excel_obj->getProperties()->setSubject($in_desc);
    self::$excel_obj->getProperties()->setDescription($in_desc);
    self::$excel_obj->getActiveSheet()->setTitle('Data');
    }

    if (is_array($in_data))
    {
    if ($start_row !== false && intval($start_row)>0)
    {
    $row = $start_row;
    }
    else
    {
    $row = 1;
    }

    foreach ($in_data as $data_set)
    {
    export_array_excel($excel_obj,$data_set,$in_desc,$row);
    $row += count($data_set);
    }
    }
    } */
}

?>