<?php
namespace ATS;


class DailyReport {
    const ENDPOINT = 'api_collection_msgs_out_count';

    /**
    * funkce vraci DMS daily report
    *
    * @param int $in_collecton_id ID sbirky
    * @param date $in_date datum zacatku reportu ve formatu Y-m-d
    * @param date $to_date|false datum konce reportu ve formatu Y-m-d
    * @param boolean $only_delivered|true pouze DMS s dorucenkami
    *
    * @return array|false
    */

    public static function getReport($in_collecton_id, $in_date, $to_date = false, $only_delivered = true) {
        $result = false;
        $params = array();

        $params["item_id"] = $in_collecton_id;
        $params["dt_from"] = $in_date."T00:00:00";
        if ($to_date === false) {
            $params["dt_to"] = $in_date."T23:59:59";
        } else {
            $params["dt_to"] = $to_date."T23:59:59";
        }
        if ($only_delivered) {
            $params["only_delivered"] = "true";
        } else {
            $params["only_delivered"] = "false";
        }

        if (\Kernel\Func::resultValidArr($params)) {
            $res = \ATS\Request::doQuery(self::ENDPOINT, $params);
            if (\Kernel\Func::resultValidArr($res)) {
                if ($res["status"] == \ATS\Request::REPONSE_OK) {
                    $result = $res["data"];
                }
            }
        }

        return $result;
    }
}
?>
