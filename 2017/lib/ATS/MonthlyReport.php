<?php
namespace ATS;


class MonthlyReport {
    const ENDPOINT = 'api_collection_month_report_get';

    /**
    * funkce vraci DMS mesicni report
    *
    * @param int $in_collecton_id ID sbirky
    * @param int $in_year rok
    * @param int $in_year mesic
    *
    * @return array|false
    */

    public static function getReport($in_collecton_id, $in_year, $in_month) {
        $result = false;
        $params = array();

        $params["item_id"] = $in_collecton_id;
        $params["year"] = $in_year;
        $params["month"] = $in_month;

        if (\Kernel\Func::resultValidArr($params)) {
            $res = \ATS\Request::doQuery(self::ENDPOINT, $params);
            if (\Kernel\Func::resultValidArr($res)) {
                if ($res["status"] == \ATS\Request::REPONSE_OK) {
                    $result = $res["data"];
                }
            }
        }

        return $result;
    }
}
?>
