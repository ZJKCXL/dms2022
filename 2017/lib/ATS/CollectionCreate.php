<?php
namespace ATS;


class CollectionCreate {
    const ENDPOINT = 'api_collection_create';

    /**
    * funkce vraci DMS daily report
    *
    * @param int $in_collecton_id ID sbirky
    * @param date $in_date datum zacatku reportu ve formatu Y-m-d
    * @param date $to_date|false datum konce reportu ve formatu Y-m-d
    * @param boolean $only_delivered|true pouze DMS s dorucenkami
    *
    * @return array|false
    */

    public static function createCollection($foundation_id, $keyword_base, $in_date, $to_date) {
        $result = false;
        $params = array();

        $params["foundation_id"] = $foundation_id;
        $params["keyword_base"] = $keyword_base;
        $params["valid_from"] = $in_date."T00:00:00";
        $params["valid_to"] = $to_date."T23:59:59";
        $params["can_permanent"] = "true";
        $params["can_onetime"] = "true";
        $params["can_year"] = "true";
        $params["can_assent"] = "false";

        if (\Kernel\Func::resultValidArr($params)) {
            $res = \ATS\Request::doQuery(self::ENDPOINT, $params);
            if (\Kernel\Func::resultValidArr($res)) {
                if ($res["status"] == \ATS\Request::REPONSE_OK) {
                    $result = $res["data"];
                }
            }
        }

        return $result;
    }
}
?>
