<?php
namespace ATS;


class FoundationCreate {
    const ENDPOINT = 'api_foundation_create';

    /**
    * funkce vraci DMS daily report
    *
    * @param int $in_collecton_id ID sbirky
    * @param date $in_date datum zacatku reportu ve formatu Y-m-d
    * @param date $to_date|false datum konce reportu ve formatu Y-m-d
    * @param boolean $only_delivered|true pouze DMS s dorucenkami
    *
    * @return array|false
    */

    public static function createFoundation($foundation_name) {
        $result = false;
        $params = array();

        $params["name"] = $foundation_name;

        if (\Kernel\Func::resultValidArr($params)) {
            $res = \ATS\Request::doQuery(self::ENDPOINT, $params);
            if (\Kernel\Func::resultValidArr($res)) {
                if ($res["status"] == \ATS\Request::REPONSE_OK) {
                    $result = $res["data"];
                }
            }
        }

        return $result;
    }
}
?>
