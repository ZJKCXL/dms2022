<?php

namespace ATS;

class Request {
    const REQUEST_METHOD_GET = "GET";
    const REQUEST_METHOD_POST = "POST";
    const REQUEST_METHOD_PUT = "PUT";
    const REQUEST_METHOD_DELETE = "DELETE";

    const RESPONSE_STATUS = "status";
    const REPONSE_OK = 200;
    const REPONSE_MSG = "status_message";

    private static function execute($target, $data = false, $method = false) {
        $result = false;
        $now = time();

        $url = \ATS\Connector::API_URL.\ATS\Connector::API_REST_ENDPOINT.$target;

        $ch = @curl_init();

        switch ($method) {
            case "POST":
                @curl_setopt($ch, CURLOPT_POST, 1);
                if ($data)
                    @curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                @curl_setopt($ch, CURLOPT_PUT, 1);
                break;
            default:
                if ($data) {
                    $url = sprintf("%s?%s", $url, http_build_query($data));
                }
        }

        @curl_setopt($ch,CURLOPT_URL,$url);

        $request_id = \Kernel\Headers::generateMessageID();

        @curl_setopt($ch,CURLOPT_HTTPHEADER,array(
            'Accept: application/json',
            'Content-Type: application/json',
            \Kernel\Headers::HEADER_DATE.": ".\Tools\DateTimeExt::convertIMFDate($now),
            \Kernel\Headers::HEADER_REQUEST_ID.": ".$request_id
        ));
        @curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        @curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
        @curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
        @curl_setopt($ch,CURLOPT_FOLLOWLOCATION,true);
        @curl_setopt($ch,CURLOPT_VERBOSE,true);
        @curl_setopt($ch,CURLINFO_HEADER_OUT,true);

        $response = @curl_exec($ch);
        $info = @curl_getinfo($ch);
        @curl_close($ch);

/*        echo "<br />CURL info: ".json_encode($info)."<br /><br />";
        echo "<br />CURL response: ".json_encode($response)."<br /><br />";*/

        if (strlen($response) > 0) {
            $response = json_decode($response, true);
            if (\Kernel\Func::resultValidArr($response)) {
                if ($response[self::RESPONSE_STATUS] == self::REPONSE_OK) {
                    $result["data"] = $response[$target];
                    if (is_null($result["data"])) {
                        $result["data"] = $response[str_replace("api_","",$target)];
                    }
                }
                $result["status"] = $response[self::RESPONSE_STATUS];
                $result["msg"] = $response[self::REPONSE_MSG];
            }
        }

        return $result;
    }

    public static function doQuery($target, $data = false) {
        $result = false;

        $res = self::execute($target, $data, self::REQUEST_METHOD_GET);

        if ($res != false) {
            $result = $res;
        }

        return $res;
    }

    public static function doPost($target, $data = false) {
        $result = false;

        $res = self::execute($target, $data, self::REQUEST_METHOD_POST);

        if ($res != false) {
            $result = $res;
        }

        return $res;
    }

    public static function doDelete($target, $data = false) {
        $result = false;

        $res = self::execute($target, $data, self::REQUEST_METHOD_DELETE);

        if ($res != false) {
            $result = $res;
        }

        return $res;
    }
}
?>
