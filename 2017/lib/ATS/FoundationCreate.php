<?php
namespace ATS;


class FoundationCreate {
    const ENDPOINT = 'api_foundation_create';

    /**
    * funkce vraci id zalozene organizace
    *
    * @param string $foundation_name Jmeno organizace
    *
    * @return array|false
    */

    public static function createFoundation($foundation_name) {
        $result = false;
        $params = array();

        $params["name"] = $foundation_name;

        if (\Kernel\Func::resultValidArr($params)) {
            $res = \ATS\Request::doQuery(self::ENDPOINT, $params);
            if (\Kernel\Func::resultValidArr($res)) {
                if ($res["status"] == \ATS\Request::REPONSE_OK) {
                    $result = $res["data"];
                }
            }
        }

        return $result;
    }
}
?>
