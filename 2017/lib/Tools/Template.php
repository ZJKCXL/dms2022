<?php
namespace Tools;

class Template {

    const TEMPLATE_EMAIL_CONFIRM = "email_confirm.template.php";
    const TEMPLATE_EMAIL_HUB = "email_hub.template.php";

    public static function getTemplate($in_template, $in_substitions = false) {
        $result = false;

        $file_name = TEMPLATES_DIR.$in_template;
        if (@file_exists($file_name)) {
            $template = @file_get_contents($file_name);
            if (strlen($template) > 0) {
                if ($in_substitions !== false && is_array($in_substitions) && @count($in_substitions)) {
                    foreach ($in_substitions as $key => $value) {
                        $template = str_replace("<%".mb_strtoupper($key)."%>",$value,$template);
                    }
                }
                $result = $template;
            }
        }


        return $result;
    }

}
?>
