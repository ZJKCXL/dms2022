<?
namespace Tools;

/*
Staticky objekt DateTime je namespace pro vsechny funkce pracujici s casem a datem.
Date pouze s datem a Time pouze s casem;
Behemoth, 2006-31-03

Metoda diffDateTime ma switch na letni cas (default = false). Cela finta je pridani "GMT"
za datum (pr. "2007-03-02 GMT"), aby strtotime nepocitaly s chybejici/prebyvajici
hodinou.
kaspi, 2006-06-04
*/

class Date
{
    static public function Now($format = "Y-m-d") {
        return date($format,time());
    }

    static public function getYear ($date) {
        return date("Y", strtotime($date));
    }

    static public function getMonth ($date) {
        return date("m", strtotime($date));
    }

    static public function getDay ($date) {
        return date("d", strtotime($date));
    }

    static public function getDayOfWeek ($date) {
        return date("w", strtotime($date));
    }

    public static function convertToUS ($date, $inputFromat) {
        $result = false;

        $formatParts = array();
        if (preg_match_all("/\w/", $inputFromat, $matches))
        {
            foreach ($matches[0] as $key => $component)
            {
                switch ($component)
                {
                    case "d":
                    case "j":
                        $formatParts["d"] = $key;
                        break;
                    case "m":
                    case "n":
                        $formatParts["m"] = $key;
                        break;
                    case "Y":
                    case "y":
                        $formatParts["Y"] = $key;
                        break;
                }
            }
        }

        if (preg_match_all("/\d+/", $date, $matches))
        {
            if (@count($matches[0]) == @count($formatParts))
            {
                $result = date("Y-m-d", mktime(0, 0, 0, $matches[0][$formatParts["m"]], $matches[0][$formatParts["d"]], $matches[0][$formatParts["Y"]]));
            }
        }

        return $result;
    }

    public static function getWorkingDays($startDate, $endDate, $holidays) {
        // do strtotime calculations just once
        $endDate = strtotime($endDate);
        $startDate = strtotime($startDate);

        //The total number of days between the two dates. We compute the no. of seconds and divide it to 60*60*24
        //We add one to inlude both dates in the interval.
        $days = ($endDate - $startDate) / 86400 + 1;

        $no_full_weeks = floor($days / 7);
        $no_remaining_days = fmod($days, 7);

        //It will return 1 if it's Monday,.. ,7 for Sunday
        $the_first_day_of_week = date("N", $startDate);
        $the_last_day_of_week = date("N", $endDate);

        //---->The two can be equal in leap years when february has 29 days, the equal sign is added here
        //In the first case the whole interval is within a week, in the second case the interval falls in two weeks.
        if ($the_first_day_of_week <= $the_last_day_of_week)
        {
            if ($the_first_day_of_week <= 6 && 6 <= $the_last_day_of_week)
                $no_remaining_days--;
            if ($the_first_day_of_week <= 7 && 7 <= $the_last_day_of_week)
                $no_remaining_days--;
        }
        else
        {
            // (edit by Tokes to fix an edge case where the start day was a Sunday
            // and the end day was NOT a Saturday)
            // the day of the week for start is later than the day of the week for end
            if ($the_first_day_of_week == 7)
            {
                // if the start date is a Sunday, then we definitely subtract 1 day
                $no_remaining_days--;

                if ($the_last_day_of_week == 6)
                {
                    // if the end date is a Saturday, then we subtract another day
                    $no_remaining_days--;
                }
            }
            else
            {
                // the start date was a Saturday (or earlier), and the end date was (Mon..Fri)
                // so we skip an entire weekend and subtract 2 days
                $no_remaining_days -= 2;
            }
        }

        //The no. of business days is: (number of weeks between the two dates) * (5 working days) + the remainder
        //---->february in none leap years gave a remainder of 0 but still calculated weekends between first and last day, this is one way to fix it
        $workingDays = $no_full_weeks * 5;
        if ($no_remaining_days > 0)
        {
            $workingDays += $no_remaining_days;
        }

        //We subtract the holidays
        foreach ($holidays as $holiday)
        {
            $time_stamp = strtotime($holiday);
            //If the holiday doesn't fall in weekend
            if ($startDate <= $time_stamp && $time_stamp <= $endDate && date("N", $time_stamp) != 6 && date("N", $time_stamp) != 7)
                $workingDays--;
        }

        return $workingDays;
    }

    static public function convertUSToCZ($in_us_date) {
        $result = false;

        if (strlen($in_us_date) == 10)
        {
            $stmp = strtotime($in_us_date);
            $result = date("j.n.Y",$stmp);
        }

        return $result;
    }

    static public function compare($left_date, $right_date) {
        $result = false;

        $left_stmp = strtotime($left_date." 05:00:00");
        $right_stmp = strtotime($right_date." 05:00:00");

        if ($left_stmp == $right_stmp) {
            $result = 0;
        } else {
            $result = ($left_stmp > $right_stmp ? -1 : 1);
        }

        return $result;
    }

    static public function compareMonthDay($left_date, $right_date) {
        $result = false;

        $left_date = "1970-".self::getMonth($left_date)."-".self::getDay($left_date);
        $right_date = "1970-".self::getMonth($right_date)."-".self::getDay($right_date);

        $left_stmp = strtotime($left_date." 05:00:00");
        $right_stmp = strtotime($right_date." 05:00:00");

        if ($left_stmp == $right_stmp) {
            $result = 0;
        } else {
            $result = ($left_stmp > $right_stmp ? -1 : 1);
        }

        return $result;
    }

    static public function diffDays($left_date, $right_date) {
        $result = false;

        $left = date_create($left_date);
        $right = date_create($right_date);

        $result = date_diff($left, $right);
        $result = $result->format("%a");
        if (self::compare($left_date, $right_date) < 0) {
            $result *= -1;
        } else {
            $result *= 1;
        }

        return $result;
    }

    static public function diffWeeks($left_date, $right_date) {
        $result = false;

        $left = date_create($left_date);
        $right = date_create($right_date);

        $result = date_diff($left, $right);
        $result = $result->format("%a");
        $result = floor($result / 7);
        if (self::compare($left_date, $right_date) < 0) {
            $result *= -1;
        } else {
            $result *= 1;
        }

        return $result;
    }

    static public function addYear($in_date, $in_years = 1) {
        $result = false;

        $dt = date_create($in_date);
        if ($dt !== false) {
            $dt->add(new \DateInterval('P'.$in_years."Y"));
            $result = $dt->format("Y-m-d");
        }

        return $result;
    }

    static public function addMonth($in_date, $in_months = 1) {
        $result = false;

        $dt = date_create($in_date);
        if ($dt !== false) {
            $dt->add(new \DateInterval('P'.$in_months."M"));
            $result = $dt->format("Y-m-d");
        }

        return $result;
    }

    static public function addDay($in_date, $in_days = 1) {
        $result = false;

        $dt = date_create($in_date);
        if ($dt !== false) {
            $dt->add(new \DateInterval('P'.$in_days."D"));
            $result = $dt->format("Y-m-d");
        }

        return $result;
    }

    static public function subDay($in_date, $in_days = 1) {
        $result = false;

        $dt = date_create($in_date);
        if ($dt !== false) {
            $dt->sub(new \DateInterval('P'.$in_days."D"));
            $result = $dt->format("Y-m-d");
        }

        return $result;
    }

    static public function subYear($in_date, $in_years = 1) {
        $result = false;

        $dt = date_create($in_date);
        if ($dt !== false) {
            $dt->sub(new \DateInterval('P'.$in_years."Y"));
            $result = $dt->format("Y-m-d");
        }

        return $result;
    }

    static public function subMonth($in_date, $in_months = 1) {
        $result = false;

        $dt = date_create($in_date);
        if ($dt !== false) {
            $dt->sub(new \DateInterval('P'.$in_months."M"));
            $result = $dt->format("Y-m-d");
        }

        return $result;
    }

    static public function addWeek($in_date, $in_weeks = 1) {
        $result = false;

        $dt = date_create($in_date);
        if ($dt !== false) {
            $dt->add(new \DateInterval('P'.($in_weeks * 7)."D"));
            $result = $dt->format("Y-m-d");
        }

        return $result;
    }

    static public function subWeek($in_date, $in_weeks = 1) {
        $result = false;

        $dt = date_create($in_date);
        if ($dt !== false) {
            $dt->sub(new \DateInterval('P'.($in_weeks * 7)."D"));
            $result = $dt->format("Y-m-d");
        }

        return $result;
    }

    public static function getDayName($in_date, $long = false, $lowercase = false) {
        $result = false;

        $day = date("N",strtotime($in_date." 05:00:00"));
        $result = \Tools\Week::getDayName($day, $long, $lowercase);

        return $result;
    }

}


?>