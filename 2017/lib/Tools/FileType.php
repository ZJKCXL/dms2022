<?php

namespace Tools;

class FileType {

    public static function get_forbidden_extensions()
    {
        $result = false;

        $tbl = new DataList("tblFileExtension","FEText");
        $result = $tbl->get_values();
        unset($tbl);

        return $result;
    }

    public static function extension_forbidden($in_ext)
    {
        $result = false;

        if (strlen($in_ext) > 0)
        {
            $in_ext = mb_strtolower($in_ext);
        }

        $ext = self::get_forbidden_extensions();
        if (is_array($ext) && @count($ext)>0)
        {
            $ext = array_column($ext,"data");
            $result = in_array($in_ext, $ext);
        }
        unset($ext);

        return $result;
    }
}
?>
