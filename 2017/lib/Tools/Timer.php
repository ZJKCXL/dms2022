<?php

namespace Tools;

class Timer {
    private static $start;
    private static $end;

    public static function start() {
        self::$start = microtime(true);
        self::$end = -1;
    }

    public static function stop() {
        self::$end = microtime(true);
    }

    public static function getElapsed() {
        $result = false;

        if (self::$start > -1 && self::$end > -1 && self::$start <= self::$end ) {
            $result = self::$end - self::$start;
        }

        return $result;
    }

    public static function clear() {
        self::$start = -1;
        self::$end = -1;
    }
}

Timer::clear();
?>
