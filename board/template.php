<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <title>DMS Avizo</title>
    <meta name='description' content='DMS Avizo' />
    <meta name='keywords' content='DMS Avizo' />
</head>
<body>
<style>
  body { background-color: #ffffff; font-family:  sans-serif; font-size: 15px; }
  * {  box-sizing: border-box; }
  #main { padding: 1%;  }
  h1 {  clear: left; color:   #0881B8; padding: 10px 15px; font-weight: normal; border-bottom: 5px solid  #e5e5e5;  border-top: 5px solid  #e5e5e5;}
  img { height: 190px; float: left; margin-right: 25px; margin-bottom: 25px;}
  #me address { padding-top: 55px; }
  table { border-collapse: collapse; border: 4px solid #e7e7e7; width: 100% }
  th, td { border-collapse: collapse; border: 1px solid #c0c0c0; padding: 15px 20px; text-align: left; }
  
  /*
  address { font-style: normal; }
  #number { float: right; }
  h2, h3 {margin: 0; padding: 0; font-size: 20px}
  h4 { margin: 0; padding: 0; float: left; clear: left; min-width: 9em;  }
  h4+span {float: left;}
  .line { min-height: 190px; position:relative;  border: 5px solid #449cc4; padding: 20px 20px 20px 35px; margin : 25px 0 35px 0;   }
  .line h2, .line h3 { color: #449cc4; position: absolute; top: -15px; margin-left: -15px; font-weight: normal; background: #fff; padding: 0 15px 0 15px; }
  #client, #payment { float: left; width: 49%; margin-right: 2% }
  #invoiced, #total { float: right; width: 49% }
  #total p { font-size: 25px;  }
  #descr { clear: both; margin-bottom: 15px }
  table { width: 100%; border: none; }
  table tr.project { background-color: #e5e5e5; } 
  table tr.keyword { background-color: #f5f5f5; } 
  table td { padding: 10px 15px; }
  table tr td:nth-of-type(2) { text-align: right;  }

  
  #me span { font-size: 12px; }
  .final { border-top: 5px solid  #e5e5e5; text-align: center; clear: both; }
  */
</style>
<main id="main">
  <div id='number'>
            <span> </span>  
  </div>

  <div id="me">
            <img src="./logo.png" />
            <address>
                nám. Winstona Churchilla 1800/2<br/>130 00 Praha 3<br/> 
                IČ: 66004501 <br/>
                Tel.: 240 201 193 <br/>
                Číslo účtu: 238181841/0300, <br/>
                ČSOB IBAN: CZ27 0300 0000 0002 3818 1841
            </address>
            <span>Fórum dárců, spolek, spisová značka L 8294 vedená u Městského soudu v Praze</span>
   </div>

   <h1>RADA DMS</h1>

   <table>
   <tr><th>Název organizace</th><td><%ORGANIZATION_NAME%></td></tr>
   </table>
 
</main>
</body>
</html>