<div lang="de" class="goog-trans-section"> 
<div class="goog-trans-control"></div>                                        
<br /><b>Lage:</b><br /><br />In lebhafter Atmosph�re, ca. 50 m zum Strand. Zum Ortszentrum ca. 600 m.<br /><br /><b>Ausstattung: </b><br /><br />310 Zimmer, Lifts. Rezeption, Restaurant, Bar, Gesch�fte, S��wasserpool. Liegen am Pool inklusive, Auflagen und Schirme gegen Geb�hr. <br /><br /><b>Landeskategorie: </b><br /><br />4 Sterne.<br /><br /><b>Unterbringung:</b><br /><br />Doppelzimmer (A1-3A) mit Minibar, Klimaanlage, Mietsafe, Sat.-TV, Telefon, Bad oder Dusche, WC, F�hn und Balkon. Appartements Typ A1 (A2-4D): Zus�tzliches Schlafzimmer, sonst wie DZ.<br /><br /><b>Essen & Trinken: </b><br /><br />Halbpension. Alle Mahlzeiten in Buffetform.<br /> 
</div> 

<script type="text/javascript">
    function googleSectionalElementInit() {
        new google.translate.SectionalElement({
                sectionalNodeClassName: 'goog-trans-section',
                controlNodeClassName: 'goog-trans-control',
                background: '#f4fa58'   ,
                pageLanguage: 'de'  
            }, 'google_sectional_element');
    }
</script>
<script type="text/javascript" src="http://translate.google.com/translate_a/element.js?cb=googleSectionalElementInit&ug=section&hl=cs"></script>