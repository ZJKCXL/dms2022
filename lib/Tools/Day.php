<?php
namespace Tools;

class Day {
    public static function daysToMonth($in_day_count) {
        $result = 1;

        if ($in_day_count > 30) {
            $result = (int)floor($in_day_count / 30);
        }

        return $result;
    }
}
?>
