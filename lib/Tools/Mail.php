<?

namespace Tools;

class Mail {

    public static function doSend ($arr_recipients, $subject, $body, $arr_attachements = '', $from = MAIL_SENDER, $return_path = MAIL_RETURN_PATH, $encoding = '8bit', $charset = 'UTF-8') {
        $result = false;

        $mail_handle = new \Mail\htmlMimeMail();
        $mail_handle->setSMTPParams(SMTP_HOST, SMTP_PORT, null, SMTP_AUTH, SMTP_USER, SMTP_PASS);
        $mail_handle->setReturnPath($return_path);
        $mail_handle->setFrom($from);
        $mail_handle->setTextEncoding($encoding);
        $mail_handle->setTextCharset($charset);
        $mail_handle->setHtmlCharset($charset);
        $mail_handle->setHeadCharset($charset);
        $mail_handle->setSubject($subject);

        $text_body = \Html2Text\Html2Text::convert($body);

        $mail_handle->setHtml($body,$text_body,BASE_DIR.'images/');

        if (is_array($arr_attachements)) {
            foreach ($arr_attachements as $key=>$value) {
                $mail_handle->addAttachment($mail_handle->getFile($value),$key);
            }
        }

        if (!is_array($arr_recipients)) {
            $arr_recipients=array($arr_recipients);
        }

        if ($arr_recipients != '' || count($arr_recipients)>0) {
            if ($mail_handle->send($arr_recipients, 'smtp')) {
                \Kernel\Logger::logToFile("Email to [".implode(",",$arr_recipients)."] with subject [".$subject."] - SUCCESS","mail");
                $result = true;
            } else {
                \Kernel\Logger::logToFile("Email to [".implode(", ",$arr_recipients)."] with subject [".$subject."] - FAIL","mail");
                \Kernel\Logger::logToFile("Sending e-mail to [".implode(",",$arr_recipients)."] has failed","error");
                $result = false;
            }
        }

        return $result;
    }

}

?>
