<?php
namespace Tools;

class Week {
    const DAY_SUNDAY = 0;
    const DAY_MONDAY = 1;
    const DAY_TUESDAY = 2;
    const DAY_WEDNESDAY = 3;
    const DAY_THURSDAY = 4;
    const DAY_FRIDAY = 5;
    const DAY_SATURDAY = 6;

    const WEEK_START_STANDARD = 1;
    const WEEK_START_FLOWER = 6;

    const DAY_NAMES = array(1 => "Po", 2 => "Út", 3 => "St", 4 => "Čt", 5 => "Pá", 6 => "So", 7 => "Ne");
    const DAY_NAMES_LONG = array(1 => "Pondělí", 2 => "Úterý", 3 => "Středa", 4 => "Čtvrtek", 5 => "Pátek", 6 => "Sobota", 7 => "Neděle");

    const WEEK_TYPE_STANDARD = 1;
    const WEEK_TYPE_FLOWER = 2;

    public static function getWeekStart($in_date) {
        $result = false;

        $stmp = strtotime($in_date." 05:00:00");
        $day = date("N", $stmp);

        $result = \Tools\Date::subDay($in_date, ($day - 1));

        return $result;
    }

    public static function getWeekEnd($in_date) {
        $result = false;

        $stmp = strtotime($in_date." 05:00:00");
        $day = date("N", $stmp);

        $result = \Tools\Date::addDay($in_date, (7- $day));

        return $result;
    }

    public static function getDayName($day_num, $long = false, $lowercase = false) {
        $result = false;

        if ($day_num >= 1 && $day_num <= 7) {
            if ($long) {
                $result = self::DAY_NAMES_LONG[$day_num];
            } else {
                $result = self::DAY_NAMES[$day_num];
            }
        }

        if ($lowercase !== false && $result !== false) {
            $result = mb_strtolower($result);
        }


        return $result;
    }

    public static function getWeekNumber($week_type_flower = self::WEEK_TYPE_STANDARD, $on_date = false) {
        $result = false;

        if ($on_date == false) {
            $on_date = \Tools\Date::Now();
        }

        $stmp = strtotime($on_date." 05:00:00");
        $week_num = intval(date("W", $stmp));

        if ($week_type_flower == self::WEEK_TYPE_FLOWER) {
            $day_num = date("N", $stmp);
            if ($day_num >= self::WEEK_START_FLOWER) {
                $week_num++;
            }
        }

        $result = $week_num;

        return $result;
    }
}
?>
