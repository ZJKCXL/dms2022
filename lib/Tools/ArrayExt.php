<?

namespace Tools;

define("ORDER_BY_VALUE",1);
define("ORDER_BY_VALUE_AS_NUMERIC",2);
define("ORDER_BY_VALUE_AS_DATE",3);

define("ORDER_DIRECTION_ASC",1);
define("ORDER_DIRECTION_DESC",2);

class ArrayExt {

    const ORDER_BY_VALUE = 1;
    const ORDER_BY_VALUE_AS_NUMERIC = 2;
    const ORDER_BY_VALUE_AS_DATE = 3;

    const ORDER_DIRECTION_ASC = 1;
    const ORDER_DIRECTION_DESC = 2;

    const DIFF_FLAG_FIELD = "__diff";

    const DIFF_LEFT_ONLY = "__left_only";
    const DIFF_RIGHT_ONLY = "__right_only";
    const DIFF_DIFFERENT = "__different";

    private static $reduced_key_set;

    public static function sort_array ($array, $index, $order = self::ORDER_BY_VALUE, $order_direction = self::ORDER_DIRECTION_ASC) {
        if (is_array($array) && count($array)>0)
        {
            if ($order == ORDER_BY_VALUE_AS_DATE)
            {
                foreach (array_keys($array) as $key) $temp[$key] = strtotime($array[$key][$index]);
            }
            else
            {
                foreach (array_keys($array) as $key) $temp[$key] = $array[$key][$index];
            }

            $sort_flags = SORT_LOCALE_STRING;
            if ($order == ORDER_BY_VALUE_AS_NUMERIC)
            {
                $sort_flags = SORT_NUMERIC;
            }

            if ($order_direction == ORDER_DIRECTION_DESC)
            {
                arsort($temp, $sort_flags);
            }
            else
            {
                asort($temp, $sort_flags);
            }

            foreach (array_keys($temp) as $key) (is_numeric($key))? $sorted[] = $array[$key] : $sorted[$key] = $array[$key];

            return $sorted;
        }

        return $array;
    }

    public static function sort_array_multiple_indexes (&$array, $index, $order = ORDER_BY_VALUE,  $order_direction = ORDER_DIRECTION_ASC, $index2 = false, $order2 = ORDER_BY_VALUE, $order_direction2 = ORDER_DIRECTION_ASC)
    {
        if (is_array($array) && count($array)>0) {
            $sort_flags = SORT_LOCALE_STRING;
            if ($order == ORDER_BY_VALUE_AS_NUMERIC) {
                $sort_flags = SORT_NUMERIC;
            }

            $sort_flags2 = SORT_LOCALE_STRING;
            if ($order2 == ORDER_BY_VALUE_AS_NUMERIC) {
                $sort_flags2 = SORT_NUMERIC;
            }

            switch ($order_direction) {
                case ORDER_DIRECTION_ASC:
                    $sort_order = SORT_ASC;
                    break;
                case ORDER_DIRECTION_DESC:
                $sort_order = SORT_DESC;
                break;
            }

            switch ($order_direction2) {
                case ORDER_DIRECTION_ASC:
                    $sort_order2 = SORT_ASC;
                    break;
                case ORDER_DIRECTION_DESC:
                    $sort_order2 = SORT_DESC;
                    break;
            }

            foreach ($array as $key => $row) {
                $index_arr[$key]  = $row[$index];
                $index_arr2[$key] = $row[$index2];
            }
            array_multisort($index_arr, $sort_order, $sort_flags, $index_arr2, $sort_order2, $sort_flags2, $array);
        }

        return $array;
    }

    public static function remove_nulls($in_array)
    {
        $result = false;

        if (is_array($in_array) && count($in_array)>0)
        {
            foreach ($in_array as $key => $val)
            {
                if (!is_null($val))
                {
                    $result[$key] = $val;
                }
            }
        }

        return $result;

    }

    public static function convert_sort_dir($in_sort_direciton)
    {
        $result = false;

        switch ($in_sort_direciton)
        {
            case 1: $result = ORDER_DIRECTION_ASC;
                break;
            case -1: $result = ORDER_DIRECTION_DESC;
                break;
        }

        return $result;
    }

    public static function apply_filter($in_array, $in_filter)
    {
        $result = false;

        if (is_array($in_array) && count($in_array)>0)
        {
            foreach ($in_array as $key => $val)
            {
                if (is_array($in_filter) && @count($in_array))
                {
                    $valid = true;
                    foreach ($in_filter as $filt_key => $filt_val)
                    {
                        $valid = \String\StringUtils::contains($filt_val,$val[$filt_key]);
                        if ($valid && ((string)intval($filt_val) == $filt_val) && (intval($filt_val)*16 == intval($val[$filt_key])) && $filt_val !== "0")
                        {
                            $valid = false;
                        }
                        if (!$valid)
                        {
                            break;
                        }
                    }
                    if ($valid)
                    {
                        $result[$key] = $val;
                    }
                }
                else
                {
                    $result[$key] = $val;
                }
            }
        }

        return $result;
    }

    public static function unique($in_array) {
        $result = false;

        $tmp_arr = array();
        if ($in_array != false && is_array($in_array) && @count($in_array) > 0) {
            foreach ($in_array as $idx => $val) {
                $tmp_arr[$idx] = json_encode($val);
            }
            if (@count($tmp_arr) > 0) {
                $tmp_arr = array_unique($tmp_arr);
            }
            if (@count($tmp_arr) > 0) {
                $result = array();
                foreach ($tmp_arr as $idx => $val) {
                    $result[$idx] = json_decode($val, true);
                }
            }
        }


        return $result;
    }

    public static function key_search($in_array, $in_value) {
        $result = false;

        if ($in_array != false && is_array($in_array) && @count($in_array) > 0) {
            $found = array_keys($in_array, $in_value);
            if (!$found) {
                $in_array = array_map(array('\String\StringUtils', 'parse_column_name'), $in_array);

                $found = array_keys($in_array, $in_value);
            }

            if (is_array($found)) {
                $result = $found;
            }
        }

        return $result;
    }

    public static function search_multi($in_array, $in_value, $in_idx) {
        $result = false;

        if ($in_array != false && is_array($in_array) && @count($in_array) > 0) {
            $result = array();
            foreach ($in_array as $idx => &$val) {
                if ($val[$in_idx] == $in_value) {
                    $result[] = $idx;
                }
            }
        }

        return $result;
    }

    public static function intersect($left_array, $right_array, $left_array_idx, $right_array_idx, $left_array_idx_in_result, $right_array_idx_in_result) {
        $result = false;

        if (is_array($left_array) && is_array($right_array)) {
            $result = array();
            foreach ($left_array as $left_row) {
                $val = $left_row[$left_array_idx];
                $right_idxs = self::search_multi($right_array, $val, $right_array_idx);
                if ($right_idxs !== false && is_array($right_idxs) && @count($right_idxs)) {
                    foreach ($right_idxs as $right_val_idx) {
                        $row = array();
                        foreach ($left_array_idx_in_result as $left_result) {
                            $row[$left_result] = $left_row[$left_result];
                        }
                        foreach ($right_array_idx_in_result as $right_result) {
                            $row[$right_result] = $right_array[$right_val_idx][$right_result];
                        }
                        $result[] = $row;
                    }
                }
            }
        }


        return $result;
    }

    public static function diffAssoc($in_left_array, $in_right_array, $ommit_keys = false) {
        $result = false;

        foreach ($in_left_array as $key => &$val) {
            if (!$ommit_keys || array_search($key, $ommit_keys) === false) {
                if (!isset($in_right_array[$key]) && !is_null($in_right_array[$key])) {
                    $result[$key]["value"] = $val;
                    $result[$key][self::DIFF_FLAG_FIELD] = self::DIFF_LEFT_ONLY;
                } else {
                    if (is_null($val)) {
                        $val = "NULL";
                    }
                    if (is_null($in_right_array[$key])) {
                        $in_right_array[$key] = "NULL";
                    }
                    if (json_encode($val) != json_encode($in_right_array[$key])) {
                        $result[$key]["value"] = $val;
                        $result[$key][self::DIFF_FLAG_FIELD] = self::DIFF_DIFFERENT;
                    }
                }
            }
        }

        foreach ($in_right_array as $key => $val) {
            if (!$ommit_keys || array_search($key, $ommit_keys) === false) {
                if (!isset($in_left_array[$key]) && !is_null($in_left_array[$key])) {
                    $result[$key]["value"] = $val;
                    $result[$key][self::DIFF_FLAG_FIELD] = self::DIFF_RIGHT_ONLY;
                }
            }
        }

        return $result;
    }

    public static function isAssoc($in_array) {
        $result = false;

        $keys = array_keys($in_array);

        if (\Kernel\Func::resultValidArr($keys)) {
            foreach ($keys as $key) {
                if ((string)intval($key) !== (string)$key) {
                    $result = true;
                    break;
                }
            }
        }

        return $result;
    }

    public static function diffNumeric($in_left_array, $in_right_array) {
        $result = false;

        $i = 0;

        foreach ($in_left_array as $key => $val) {
            $has = array_search($val, $in_right_array);
            if ($has === false) {
                $result[$i]["value"] = $val;
                $result[$i][self::DIFF_FLAG_FIELD] = self::DIFF_LEFT_ONLY;
                $i++;
            }
        }

        foreach ($in_right_array as $key => $val) {
            $has = array_search($val, $in_left_array);
            if ($has === false) {
                $result[$i]["value"] = $val;
                $result[$i][self::DIFF_FLAG_FIELD] = self::DIFF_RIGHT_ONLY;
                $i++;
            }
        }

        return $result;
    }

    public static function key_remains($key) {
        return (array_search($key, self::$reduced_key_set) !== false);
    }

    public static function key_gone($key) {
        return (array_search($key, self::$reduced_key_set) === false);
    }

    public static function keys_reduce($in_array, $reduced_key_set) {
        $result = false;

        self::$reduced_key_set = $reduced_key_set;
        $result = array_filter($in_array, "self::key_remains", ARRAY_FILTER_USE_KEY);

        return $result;
    }

    public static function keys_reduce_multi($in_array, $reduced_key_set) {
        $result = false;
        self::$reduced_key_set = $reduced_key_set;

        foreach ($in_array as $internal_array) {
            $result[] = array_filter($internal_array, "self::key_remains", ARRAY_FILTER_USE_KEY);
        }

        return $result;
    }

    public static function keys_remove($in_array, $reduced_key_set) {
        $result = false;

        self::$reduced_key_set = $reduced_key_set;
        $result = array_filter($in_array, "self::key_gone", ARRAY_FILTER_USE_KEY);

        return $result;
    }

    public static function keys_remove_multi($in_array, $reduced_key_set) {
        $result = false;
        self::$reduced_key_set = $reduced_key_set;

        foreach ($in_array as $internal_array) {
            $result[] = array_filter($internal_array, "self::key_gone", ARRAY_FILTER_USE_KEY);
        }

        return $result;
    }

    public static function multi_to_one($in_array) {
        $result = false;

        foreach ($in_array as $internal_array) {
            $internal_keys = array_keys($internal_array);
            if (\Kernel\Func::resultValidArr($internal_keys)) {
                $result[] = $internal_array[$internal_keys[0]];
            }
        }

        return $result;
    }
}

?>