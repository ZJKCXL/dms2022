<?
namespace String;

class StringUtils
{
    const SHORTENER_BASIS = 111111;

    public static function validate_string($type, $string) {
        $result = false;
        $use_ereg = true;

        switch ($type) {
            case "extended":
                //$str="0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZáčďéěíňóřšťúůťýžÁČĎÉĚÍŇÓŘŠŤÚŮŤÝŽüöäëÜÖÄËß\n\r\t\040\041\042\043\044\045\046\047\050\051\052\053\054\055\056\057\072\073\074\075\076\077\100\134\137\140\173\175´";
                //$eregstr="^[" . $str . "]{1,}$";
                // hack kvuli azbukam a jinym polakum
                $eregstr = "^.*$";
                break;
            case "safeChars":
                $str="0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_";
                $eregstr="^[" . $str . "]{1,}$";
                break;
            case "numbers":
                $str="0123456789";
                $eregstr="^[" . $str . "]{1,}$";
                break;
            case "float":
                $eregstr = "^([0123456789]{1,})(()|(([\054])([0123456789]{1,})))$";
                break;
            case "alphabetLowercase":
                $str="abcdefghijklmnopqrstuvwxyzáčďéěíňóřšťúůťýžüöäë ";
                $eregstr="^[" . $str . "]{1,}$";
                break;
            case "alphabetUppercase":
                $str="ABCDEFGHIJKLMNOPQRSTUVWXYZÁČĎÉĚÍŇÓŘŠŤÚŮŤÝŽÜÖÄËß ";
                $eregstr="^[" . $str . "]{1,}$";
                break;
            case "alphabet":
                $str="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZáčďéěíňóřšťúůťýžÁČĎÉĚÍŇÓŘŠŤÚŮŤÝŽüöäëÜÖÄËß ";
                $eregstr="^[" . $str . "]{1,}$";
                break;
            case "alphanumeric":
                $str="0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZáčďéěíňóřšťúůťýžÁČĎÉĚÍŇÓŘŠŤÚŮŤÝŽüöäëÜÖÄËß?,. ";
                $eregstr="^[" . $str . "]{1,}$";
                break;
            case "date":
                $eregstr="^(((0[1-9]|[12][0-9]|3[01])([.])(0[13578]|10|12)([.])([1-2][0,9][0-9][0-9]))|(([0][1-9]|[12][0-9]|30)([.])(0[469]|11)([.])([1-2][0,9][0-9][0-9]))|((0[1-9]|1[0-9]|2[0-8])([.])(02)([.])([1-2][0,9][0-9][0-9]))|((29)([.])(02)([.])([02468][048]00))|((29)([.])(02)([.])([13579][26]00))|((29)([.])(02)([.])([0-9][0-9][0][48]))|((29)([.])(02)([.])([0-9][0-9][2468][048]))|((29)([.])(02)([.])([0-9][0-9][13579][26])))$";
                break;
            case "us_date":
                $eregstr="^(([0-9]{4})([-])((0[1-9])|(1[012]))([-])((0[1-9])|([12][0-9])|(3[01])))$";
                break;
            case "hours":
                $eregstr="^((([0][0-9])|([1][0-9])|([2][0-3]))([:])([0-5][0-9]))$";//([:])([0-5][0-9]))";
                break;
            case "url":
                $eregstr="^([htt]+(p|s))|[ftp]+[:]\/\/[a-zA-Z0-9]+([-_\.]?[a-zA-Z0-9])*\.[a-zA-Z]{2,4}(\/{1}[-_~&=\?\.a-z0-9]*)*$";
                break;
            case "email":
                $eregstr="^[A-Za-z0-9]+[A-Za-z0-9\._-]*[A-Za-z0-9]+@[A-Za-z0-9]+[A-Za-z0-9\._-]*[A-Za-z0-9]+\.[A-Za-z]{2,4}$";
                break;
            case "phone":
                //			$eregstr="^[1-9]{1}[0-9]{8}$";
                //$eregstr="^((([+][1-9][0-9]{2}([ ]{0,1}))|([0]{2}([ ]{0,1})[1-9][0-9]{2}([ ]{0,1})))|())([1-9]{1}[0-9]{2}([ ]{0,1})[0-9]{3}([ ]{0,1})[0-9]{3})$";
                //$eregstr = "^([+]{0,1}[0-9]{9,13})$";
                $eregstr = "^[+]{0,1}[0-9 ]+$";
                break;
            case "time":
                $eregstr="^[0-9]{1,2}[:][0-9]{1,2}$";
                break;
            case "month":
                $eregstr = "^(0?[1-9]|1[012])$";
                break;
            case "year":
                $eregstr = "^(19[0-9][0-9]|2([0-9][0-9][0-9]))$";
                break;
            case "gid":
                $use_ereg = false;
                $result = \String\GID::validateGID($string);
                break;
        }

        if ($use_ereg) {
            if (preg_match("/".$eregstr."/",$string)) {
                $result = true;;
            }
        }

        return $result;
    }

    public static function dechechize($in_string)
    {
        $result = $in_string;
        $result = str_replace("ě","e",$result);
        $result = str_replace("š","s",$result);
        $result = str_replace("č","c",$result);
        $result = str_replace("ř","r",$result);
        $result = str_replace("ž","z",$result);
        $result = str_replace("ý","y",$result);
        $result = str_replace("á","a",$result);
        $result = str_replace("í","i",$result);
        $result = str_replace("é","e",$result);
        $result = str_replace("ů","u",$result);
        $result = str_replace("ú","u",$result);
        $result = str_replace("ď","d",$result);
        $result = str_replace("ť","t",$result);
        $result = str_replace("ň","n",$result);
        $result = str_replace("ó","o",$result);
        $result = str_replace("ö","o",$result);

        return $result;
    }

    public static function autolink($in_text)
    {
        $ret = ereg_replace("[[:alpha:]]+://[^<>[:space:]]+[[:alnum:]/]","<a href=\"\\0\" target=\"_blank\">\\0</a>", $in_text);
        return $ret;
    }

    public static function strip_trailing_slashes($in_str)
    {
        $result = str_replace("/","",$in_str);
        return $result;
    }

    public static function strip_spaces($in_str)
    {
        $result = str_replace(" ","",$in_str);
        return $result;
    }

    public static function br2nl($str, $replace = "\n", $multiIstance = FALSE)
    {
        $base = '<[bB][rR][\s]*[/]*[\s]*>';
        $pattern = '|'.$base.'|';
        if ($multiIstance === TRUE)
        {
            $pattern = '|([\s]*'.$base.'[\s]*)+|';
        }

        return preg_replace($pattern, $replace, $str);
    }

    public static function parse_block($in_data, $in_regex)
    {
        $result = false;
        $parts = array();

        $count = @preg_match($in_regex, $in_data, $parts);

        if ($count > 0)
        {
            $result = $parts[1];
            $result = trim($result);
        }

        if (strlen($result) <= 0)
        {
            $result = false;
        }

        return $result;
    }

    public static function inflate_data($in_data)
    {
        $result = @gzuncompress($in_data);
        return $result;
    }

    public static function deflate_data($in_data)
    {
        $result = @gzcompress($in_data,3);
        return $result;
    }

    public static function format_file_size($bytes, $precision = 2)
    {
        $units = array('B', 'KB', 'MB', 'GB', 'TB');

        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);

        $bytes /= (1 << (10 * $pow));

        return round($bytes, $precision) . ' ' . $units[$pow];
    }

    public static function num2alpha($n)
    {
        for($r = ""; $n >= 0; $n = intval($n / 26) - 1)
            $r = chr($n%26 + 0x41) . $r;
        return $r;
    }

    public static function normalize($in_str, $normalize_char = "-") {
        $in_str = mb_strtolower($in_str,"utf-8");
        $in_str = self::dechechize($in_str);
        $in_str = str_replace(",",$normalize_char,$in_str);
        $in_str = str_replace(".",$normalize_char,$in_str);
        $in_str = str_replace(" ",$normalize_char,$in_str);

        return $in_str;
    }

    public static function contains($in_needle, $in_haystack)
    {
        $n = mb_strtoupper($in_needle,"utf-8");
        $h = mb_strtoupper($in_haystack,"utf-8");
        return (mb_strpos($h, $n) !== false);
    }

    public static function id_to_uid($in_id)
    {
        return base_convert($in_id * self::SHORTENER_BASIS, 10, 36);
    }

    public static function uid_to_id($in_uid)
    {
        return (int) (base_convert($in_uid, 36, 10) / self::SHORTENER_BASIS);
    }

    public static function build_password()
    {
        $chars="0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_!@#$^&*()";

        for ($x=0;$x<8;$x++)
        {
            $new_pass .= substr($chars,rand(1,strlen($chars)),1);
        }

        return $new_pass;
    }

    public static function strip_word_html($text, $allowed_tags = '')
    {
        mb_regex_encoding('UTF-8');
        //replace MS special characters first
        $search = array('/‘/u', '/’/u', '/“/u', '/”/u', '/—/u');
        $replace = array('\'', '\'', '"', '"', '-');
        $text = preg_replace($search, $replace, $text);
        //make sure _all_ html entities are converted to the plain ascii equivalents - it appears
        //in some MS headers, some html entities are encoded and some aren't
        $text = html_entity_decode($text, ENT_QUOTES, 'UTF-8');
        //try to strip out any C style comments first, since these, embedded in html comments, seem to
        //prevent strip_tags from removing html comments (MS Word introduced combination)
        if(mb_stripos($text, '/*') !== FALSE){
            $text = mb_eregi_replace('#/*.*?*/#s', '', $text, 'm');
        }
        //introduce a space into any arithmetic expressions that could be caught by strip_tags so that they won't be
        //'<1' becomes '< 1'(note: somewhat application specific)
        $text = preg_replace(array('/<([0-9]+)/'), array('< $1'), $text);
        $text = strip_tags($text, $allowed_tags);
        //eliminate extraneous whitespace from start and end of line, or anywhere there are two or more spaces, convert it to one
        //$text = preg_replace(array('/^ss+/', '/ss+$/', '/ss+/u'), array('', '', ' '), $text);
        $text = preg_replace(array('/^ss+/', '/^ss+$/', '/^ss+/u'), array('', '', ' '), $text);
        //strip out inline css and simplify style tags
        $search = array('#]*>(.*?)#isu', '#]*>(.*?)#isu', '#]*>(.*?)#isu');
        $replace = array('$2', '$2', '$1');
        $text = preg_replace($search, $replace, $text);
        //on some of the ?newer MS Word exports, where you get conditionals of the form 'if gte mso 9', etc., it appears
        //that whatever is in one of the html comments prevents strip_tags from eradicating the html comment that contains
        //some MS Style Definitions - this last bit gets rid of any leftover comments */
        $num_matches = preg_match_all("/<!--/u", $text, $matches);
        if($num_matches){
            $text = preg_replace('//isu', '', $text);
        }
        return $text;
    }

    public static function parse_column_name($in_column_definition) {
        $result = false;

        $result = preg_split('/[\s\.]/', $in_column_definition);
        $result = $result[@count($result)-1];

        return $result;
    }

    public static function parse_column_column($in_column_definition) {
        $result = false;

        $result = $in_column_definition;

        if (strpos($result," ") !== false) {
            $result = substr($result, 0, strpos($result," "));
        }

        return $result;
    }

    public static function json_prettify($in_json_string) {
        $result = $in_json_string;

        $result = str_replace("{","",$result);
        $result = str_replace("}","",$result);
        $result = str_replace("\":",": ",$result);
        $result = str_replace("\"","",$result);
        $result = str_replace(",",", ",$result);

        return $result;
    }

    public static function remove_spaces($in_string) {
        $result = $in_string;

        $result = str_replace(" ","",$result);

        return $result;
    }

}
?>