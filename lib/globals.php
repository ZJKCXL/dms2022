<?php
class Globals
{
  public static $META_DESC_LENGTH_IDEAL = 160; 
  public static $META_KEYWORDS_LENGTH_IDEAL = 160; 
  public static $META_TITLE_LENGTH_IDEAL = 60; 
  public static $META_DESC_LENGTH = 200; 
  public static $META_KEYWORDS_LENGTH = 200; 
  public static $META_TITLE_LENGTH = 70;    
  public static $WORKING_CHANGE = 0;    
  public static $GLOBAL_MY_MAIL = "forum@darcu.cz"; 
  public static $GLOBAL_MAIL_ADMIN = "dmsasistent@donorsforum.cz";
  public static $GLOBAL_MAIL_ACCOUNT = "marcela.hroncova@seznam.cz";
  public static $GLOBAL_MAIL_TEST = "forum@darcu.cz";
  public static $GLOBAL_WEB_PATH = "/data/www/darcovskasms.cz";
  public static $GLOBAL_WEB = "http://www.darcovskasms.cz/";
  public static $GLOBAL_MEIL_GOTRAP = 0;
  public static $GLOBAL_SQL_FILE = '/sql/db.php'; 
  public static $ACTUAL_INVOICE_NR =  422000; 
  public static $ACTUAL_PROFORMA_NR = 322000; 
  public static $GLOBAL_MONEY_TXT1 = "<p class'warmoney'>
  <strong>Vaše registrace bude ukončena po uhrazení registračního poplatku dle <a href='/cenik-poplatku-za-sluzbu-darcovska-sms.html'>platného ceníku</a>.</p><p>&nbsp;</strong></p>";
 }
// echo Globals::$ACTUAL_INVOICE_NR;
?>