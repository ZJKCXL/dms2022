<?php
namespace String;

class Tokenizer {

    const MIN_WORD_LEN = 3;

    public static function getTokens($in_string, $include_order = false, $omit_repetitions = true) {
        $result = false;

        if (strlen($in_string) > 0) {
            $tokens = explode(" ", $in_string);
            if (\Kernel\Func::resultValidArr($tokens)) {
                foreach ($tokens as $token) {
                    $token = \String\StringUtils::normalize($token);
                    if (strlen($token) >= self::MIN_WORD_LEN) {
                        $result[] = $token;
                    }
                }
            }
        }

        if (\Kernel\Func::resultValidArr($result) && $omit_repetitions) {
            $result = array_unique($result);
        }

        return $result;
    }
}
?>
