<?php
namespace Kernel;

class ScriptResponse {

    const RESULT_OK = "ok";
    const RESULT_ERR = "err";

    const REQUEST_ID_HEADER = "X-Request-ID";

    private $msg_id;
    private $msg_status;
    private $msg_result_code;
    private $msg_result_text;
    private $msg_result_target;
    private $msg_data;
    private $msg_debug;

    public function __construct() {
        $this->msg_id = uniqid("", true);
    }

    public function setResult($in_status, $in_result_code = false, $in_result_text = false, $in_result_target = false) {
        $this->msg_status = $in_status;
        if ($in_result_code !== false) {
            $this->msg_result_code = $in_result_code;
        }
        if ($in_result_text !== false) {
            $this->msg_result_text = $in_result_text;
        }
        if ($in_result_target !== false) {
            $this->msg_result_target = $in_result_target;
        }
    }

    public function setPayload($in_data) {
        $this->msg_data = $in_data;
    }

    public function setDebugInfo($in_data) {
        $this->msg_debug = $in_data;
    }

    public function generate($json_encode = false) {
        $result = false;

        header(self::REQUEST_ID_HEADER, $this->msg_id);

        switch ($this->msg_status) {
            case self::RESULT_OK:
                $toast_type = \Kernel\Message::MESSAGE_OK;
                break;
            case self::RESULT_ERR:
                $toast_type = \Kernel\Message::MESSAGE_ERROR;
                break;
            default:
                $toast_type = \Kernel\Message::MESSAGE_ERROR;
                break;
        }

        if (isset($this->msg_result_code) && intval($this->msg_result_code) < 0) {
            \Kernel\Logger::logToFile("error ".$msg->msg_result_code." met with text [".$this->msg_result_text."]",\Kernel\Logger::LOG_ERROR);
        }

        if (isset($this->msg_result_text) && strlen($this->msg_result_text)) {
            \Kernel\MessageQueue::enqueue(\Kernel\Message::create($this->msg_result_text, $toast_type));
        }

        if (isset($this->msg_result_target) && strlen(isset($this->msg_result_target)) >= 0) {
            if (strpos($this->msg_result_target, "://") === false) {
                $target = BASE_URL.$this->msg_result_target;
            } else {
                $target = $this->msg_result_target;
            }
            if (DEBUG == 1) {
                \Kernel\Logger::logToFile("script response redirecting to ".$target,\Kernel\Logger::LOG_DEBUG,$this);
            }
            header("Location: ".$target);
        }

        if (isset($this->msg_data)) {
            echo "\n".$this->msg_data;
        }
        if (isset($this->msg_debug)) {
            //$result["debug"] = $this->msg_debug;
        }

        return true;
    }
}
?>
