<?php

namespace Kernel;

class Kernel
{
    private static $route = false;
    private static $params = false;
    private static $request = false;

    private static function parseParameters()
    {
        $result = \Kernel\Parameters::parseParams();

        if ($result !== false)
        {
            self::$params = \Kernel\Parameters::getParams();
        }

        return $result;
    }

    private static function parseRoute() {
        $result = \Kernel\Route::parseRoute();

        if ($result !== false)
        {
            self::$route = \Kernel\Route::getRoute();
        }

        return $result;
    }

    private static function storeState() {
        $state = array();

        $state["request"] = self::$request;
        $state["route"] = self::$route;
        $state["params"] = self::$params;

        $result = \Kernel\Session::storeState($state);
    }

    private static function parseRequest() {
        $result = \Kernel\Request::parseRequest();

        if ($result !== false) {
            self::$request = \Kernel\Request::getRequest();
        }

        return $result;
    }

    public static function parseState() {
        self::parseRequest();
        self::parseParameters();
        self::parseRoute();

        if (self::$route !== false) {

        }

        self::storeState();

        return true;
    }

    public static function getRoute() {
        return self::$route;
    }

    public static function getParams() {
        return self::$params;
    }

    public static function getRequest() {
        return self::$request;
    }

}
