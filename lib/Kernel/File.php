<?
/**
* Tohle bude fungovat diky funkci array_column() jenom v PHP 5.5 a vyssi!
* Behe 2014-02-08
*/

namespace Kernel;

define("FILE_DETAIL_TEXT","detail");
define("FILE_SERVE_TEXT","serve");
define("FILE_DELETE_TEXT","delete");

define("FILE_MAXIMUM_SIZE",10485760);

define("FILE_ERROR_GENERIC",-600);
define("FILE_ERROR_INVALID_FILE_TYPE",-601);
define("FILE_ERROR_TOO_BIG",-602);

class File {

    public static function doProcessFilesData() {
        $result = false;
        $files = array();

        if (isset($_FILES) && is_array($_FILES) && @count($_FILES) > 0) {
            foreach($_FILES as $domain => $key_row) {
                foreach($key_row as $key => $key_data) {;
                    if (is_array($key_data)) {
                        foreach ($key_data as $idx => $val) {
                            $files[$domain][$idx]["domain"] = $domain;
                            $files[$domain][$idx][$key] = $val;
                        }
                    } else {
                        $files[$domain][0]["domain"] = $domain;
                        $files[$domain][0][$key] = $key_data;
                    }
                }
            }

            if (@count($files) > 0) {
                foreach ($files as $domain => $domain_data) {
                    foreach ($domain_data as $domain_row) {
                        if (strlen($domain_row["name"]) > 0) {
                            $result[] = $domain_row;
                        }
                    }
                }
            }
        }

        return $result;
    }

    public static function doStore($in_tmp_name, $in_file_name)
    {
        $result = false;

        if (@is_uploaded_file($in_tmp_name)) {
            if (@move_uploaded_file($in_tmp_name,FILE_DIR.$in_file_name)) {
                return true;
            }
        }

        return $result;
    }

    public static function getInfo($in_tmp_name) {
        $result = false;

        if (isset($_FILES) && is_array($_FILES)) {
            $files = self::process_files_data();
            if ($files !== false && is_array($files) && @count($files) > 0) {
                foreach ($files as $file_data) {
                    if ($file_data["tmp_name"] == $in_tmp_name) {
                        $result = $file_data;
                        break;
                    }
                }
            }
        }

        return $result;
    }

    public static function getExtension($in_file_name)
    {
        $result = false;

        $ext = pathinfo($in_file_name, PATHINFO_EXTENSION);
        if ($ext !== false && !is_null($ext) && strlen($ext)>0)
        {
            $result = $ext;
        }

        return $result;
    }

    /*    public static function is_valid_type($in_file_name)
    {
    $result = false;

    $ext = self::get_extension($in_file_name);
    if ($ext != false)
    {
    $result = !FileType::extension_forbidden($ext);
    }

    return $result;
    }      */

    public static function isValidSize($in_file_size)
    {
        $result = false;

        $result = $in_file_size < FILE_MAXIMUM_SIZE;

        return $result;
    }

    public static function doServeFile($in_file_id)
    {
        $result = false;

        $file_data = \DB\File::get_data($in_file_id);
        if (\Kernel\Func::resultValidArr($file_data)) {

            header('Content-Description: File Transfer');
            header('Content-Transfer-Encoding: binary');
            header("Cache-Control: ");// leave blank to avoid IE errors
            header("Pragma: ");// leave blank to avoid IE errors
            header("Content-type: application/octet-stream");
            header("Content-Disposition: attachment; filename=\"".$file_data["name_display"]."\"");
            header("Content-length:".(string)($file_data["size"]));
            sleep(2);

            @readfile(FILE_DIR.$file_data["name_fs"]);
        }

        return $result;
    }

    public static function doSizeFormatHuman($in_size, $unit = "") {
        if( (!$unit && $in_size >= 1<<30) || $unit == "GB")
            return number_format($in_size/(1<<30),2)." GB";
        if( (!$unit && $in_size >= 1<<20) || $unit == "MB")
            return number_format($in_size/(1<<20),2)." MB";
        if( (!$unit && $in_size >= 1<<10) || $unit == "KB")
            return number_format($in_size/(1<<10),2)." KB";
        return number_format($in_size)." bytů";
    }
}

?>