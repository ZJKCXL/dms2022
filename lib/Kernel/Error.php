<?
namespace Kernel;

define("NO_ERR",0);

define("CONN_CANNOT_CONNECT_RDBMS",-1);
define("CONN_CANNOT_SET_CHARSET",-2);
define("CONN_CANNOT_SELECT_DB",-3);

define("DATA_EMPTY_QUERY",-10);
define("DATA_QUERY_FAILED",-11);
define("DATA_NO_DATA",-12);
define("DATA_NO_FURTHER_DATA",-13);

define("QUERY_IS_SELECT",-21);
define("QUERY_IS_INSERT",-22);
define("QUERY_IS_UPDATE",-23);
define("QUERY_IS_DELETE",-24);
define("QUERY_IS_UKNOWN",-25);

define("OBJECT_NO_GID", -100);
define("OBJECT_CANNOT_LOAD", -101);
define("OBJECT_CANNOT_CREATE", -102);

define("OBJECTLIST_OUT_OF_BOUNDS",-110);
define("OBJECTLIST_CANNOT_EXTEND",-111);
define("OBJECTLIST_CANNOT_DELETE",-112);
define("OBJECTLIST_GID_NOT_FOUND",-113);

define("LOGGER_UNKNOWN_LOG_TYPE", -200);
define("LOGGER_EMPTY_MESSAGE", -201);
define("LOGGER_CANNOT_OPEN_FILE", -202);

define("VALIDATE_STRING_UNKNOWN_TYPE", -300);
define("VALIDATE_STRING_EMPTY", -301);
define("VALIDATE_STRING_INVALID", -302);

define("GID_INVALID_PARAMS", -400);
define("GID_NO_GID", -401);
define("GID_INVALID_GID", -402);

class Error {
    private $errcode;

    private static $do_not_log_errcode = array(-12, -13, -301);

    const SESSION_STORE_KEY = "error";

    public function __construct($inErrCode, $ext_info=null)
    {
        if (isset($inErrCode) && is_numeric($inErrCode))
        {
            $this->errcode = $inErrCode;
            if ($inErrCode != NO_ERR)
            {
                self::logIt($inErrCode,$ext_info);
            }
        }
        else
        {
            return false;
        }
    }

    protected static function logIt($inErrCode, $ext_info=null) {
        $msg_string = "Error ".$inErrCode." met";
        if (isset($ext_info) && strlen($ext_info)>0)
        {
            $msg_string.=", extended info : [".$ext_info."]";
        }

        if (array_search($inErrCode, self::$do_not_log_errcode) === false)
        {
            Logger::logToFile($msg_string, "error");
        }
    }

    public function errCode() {
        if (isset($this->errcode) && is_numeric($this->errcode))
        {
            return $this->errcode;
        }
        else
        {
            return false;
        }
    }

    public static function redirectErrorPage($in_err_code, $incident_id, $debug_info = false) {
        $result = "<script language=\"JavaScript\">location.replace('".BASE_URL."error/".$in_err_code."/&incident=".$incident_id."')</script>";

        return $result;
    }

    public static function raiseError($in_err_code, $debug_info = false, $redirect_error_page = true) {
        $result = true;

        $params = \Kernel\Parameters::getParams(false);
        $incident_id = uniqid("ERR",true);
        $stack_trace = debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT);

        self::storeError($in_err_code, $incident_id, array("params" => $params, "stack_trace" => $stack_trace));

        \Kernel\Logger::logToFile("[".$incident_id."] error ".$in_err_code." has been raised",\Kernel\Logger::LOG_ERROR, $params, true);

        if ($redirect_error_page) {
            echo self::redirectErrorPage($in_err_code, $incident_id, $debug_info);
        }

        return $result;

    }

    public static function storeError($in_err_code, $incident_id, $data = false, $debug_info = false) {
        $result = true;

        $incident = array();
        $incident[$incident_id]["incident_id"] = $incident_id;
        $incident[$incident_id]["time"] = microtime(true);
        $incident[$incident_id]["code"] = $in_err_code;
        if ($data !== false) {
            $incident[$incident_id]["data"] = $data;
        }
        if ($debug_info !== false) {
            $incident[$incident_id]["debug_info"] = $debug_info;
        }

        $incidents = \Kernel\Session::getData(self::SESSION_STORE_KEY);
        if (\Kernel\Func::resultValidArr($incidents)) {
            $incident = array_merge($incidents, $incident);
        }

        $result = \Kernel\Session::storeData(self::SESSION_STORE_KEY, $incident);

        return $result;
    }

    public static function getLastError($unhandled_only = true) {
        $result = false;

        $incidents = \Kernel\Session::getData(self::SESSION_STORE_KEY);
        if (\Kernel\Func::resultValidArr($incidents)) {
            $result = end($incidents);
            $key = key($incidents);
            $incidents[$key]["handled"] = true;
            \Kernel\Session::storeData(self::SESSION_STORE_KEY, $incidents);
            if ($unhandled_only) {
                if ($result["handled"] == true) {
                    $result = false;
                }
            }

        }

        return $result;
    }
}

?>