<?php

namespace Kernel;

class Request {

    private static $referer = false;
    private static $request_uri = false;
    private static $method = false;
    private static $secure = false;
    private static $user_agent = false;
    private static $ip_addr = false;
    private static $body = false;
    private static $headers = false;

    private static function parseReferer($strip_params = true) {
        $result = false;

        $ref = str_replace(BASE_URL,"",$_SERVER["HTTP_REFERER"]);
        if (strlen($ref) > 0)
        {
            if ($strip_params)
            {
                if (strpos($ref,"&") !== false)
                {
                    $ref = substr($ref,0,strpos($ref,"&"));
                }
                if (strpos($ref,"?") !== false)
                {
                    $ref = substr($ref,0,strpos($ref,"?"));
                }
            }
            $ref = trim($ref,"/");
            $result = $ref;
        }

        return $result;
    }

    private static function parseRequestURI($strip_params = false) {
        $result = false;

        $ref = str_replace(APP_URL,"",$_SERVER["REQUEST_URI"]);
        if (strlen($ref) > 0)
        {
            if ($strip_params)
            {
                if (strpos($ref,"&") !== false)
                {
                    $ref = substr($ref,0,strpos($ref,"&"));
                }
                if (strpos($ref,"?") !== false)
                {
                    $ref = substr($ref,0,strpos($ref,"?"));
                }
            }
            $ref = trim($ref,"/");
            $ref = str_replace("//","/", $ref);
            $result = $ref;
        }

        return $result;
    }

    private static function parseMethod() {
        return strtolower($_SERVER['REQUEST_METHOD']);
    }

    private static function parseUserAgent() {
        return $_SERVER['HTTP_USER_AGENT'];
    }

    private static function parseIPAddr() {
        return $_SERVER['REMOTE_ADDR'];
    }

    private static function parseSecure() {
        $result = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off');

        return $result;
    }

    private static function parseBody() {
        $result = false;

        $body = file_get_contents('php://input');
        if ($body !== false && strlen($body) > 0) {
            $result = $body;
        }

        return $result;
    }

    private static function parseHeaders() {
        $result = false;

        if (function_exists("apache_request_headers")) {
            $headers = apache_request_headers();
        } else {
            $headers = false;
        }
        if (\Kernel\Func::resultValidArr($headers)) {
            $result = array();
            foreach ($headers as $header => $value) {
                $result[strtolower($header)] = $value;
            }
        }

        return $result;
    }

    public static function parseRequest() {
        self::$referer = self::parseReferer();
        self::$request_uri = self::parseRequestURI();
        self::$method = self::parseMethod();
        self::$secure = self::parseSecure();
        self::$user_agent = self::parseUserAgent();
        self::$ip_addr = self::parseIPAddr();
        self::$body = self::parseBody();
        self::$headers = self::parseHeaders();
    }

    public static function getReferer() {
        return self::$referer;
    }

    public static function getRequestURI() {
        return self::$request_uri;
    }

    public static function getMethod() {
        return self::$method;
    }

    public static function getSecure() {
        return self::$secure;
    }

    public static function getUserAgent() {
        return self::$user_agent;
    }

    public static function getIPAddr() {
        return self::$ip_addr;
    }

    public static function getBody() {
        return self::$body;
    }

    public static function getHeaders() {
        return self::$headers;
    }

    public static function getRequest() {
        $result = array();

        $result["referer"] = self::$referer;
        $result["request_uri"] = self::$request_uri;
        $result["method"] = self::$method;
        $result["secure"] = self::$secure;
        $result["user_agent"] = self::$user_agent;
        $result["ip_addr"] = self::$ip_addr;
        $result["body"] = self::$body;
        $result["headers"] = self::$headers;

        return $result;
    }

}