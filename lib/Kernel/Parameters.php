<?

namespace Kernel;

define("PARAMETER_CHECK_NON_ZERO",1);
define("PARAMETER_CHECK_ABOVE_ZERO",2);
define("PARAMETER_CHECK_BELOW_ZERO",3);
define("PARAMETER_CHECK_ZERO_PLUS",4);
define("PARAMETER_CHECK_ZERO_MINUS",5);
define("PARAMETER_CHECK_TRUE",6);
define("PARAMETER_CHECK_FALSE",7);

define("ERROR_PARAMETER_INVALID",-500);

class Parameters {

    const PARAMETER_CHECK_NON_ZERO = 1;
    const PARAMETER_CHECK_ABOVE_ZERO = 2;
    const PARAMETER_CHECK_BELOW_ZERO = 3;
    const PARAMETER_CHECK_ZERO_PLUS = 4;
    const PARAMETER_CHECK_ZERO_MINUS = 5;
    const PARAMETER_CHECK_TRUE = 6;
    const PARAMETER_CHECK_FALSE = 7;
    const PARAMETER_CHECK_ZERO = 8;

    const PARAMETER_SANITIZE_STRING = 1;
    const PARAMETER_SANITIZE_INTEGER = 2;
    const PARAMETER_SANITIZE_FLOAT = 3;

    private static $check_params = array();
    private static $params = array();
    private static $omit_params = array("_ga","__utma","__utmz","PHPSESSID");
    private static $omit_anon_params = array("user","eclipse_lang");
    private static $omit_appl_params = array("user","eclipse_lang","r","route","params","route_params");

    public static function parseParams()
    {
        $result = false;

        if (isset($_REQUEST) && is_array($_REQUEST)) {
            foreach ($_REQUEST as $key => $val) {
                if (!in_array($key,self::$omit_params)) {
                    self::$params[mb_strtolower($key)] = $val;
                }
            }

            $result = true;
        }

        return $result;
    }

    public static function getParams($json_encoded = false)
    {
        $result = self::$params;
        if ($json_encoded) {
            $result = json_encode($result);
        }
        return $result;
    }

    public static function getParamsAnon($json_encoded = false)
    {
        $tmp_params = self::$params;

        if (@count(self::$omit_anon_params) > 0 && @count($tmp_params > 0)) {
            foreach (self::$omit_anon_params as $param_name) {
                unset($tmp_params[$param_name]);
            }
        }

        $result = $tmp_params;
        if ($json_encoded) {
            $result = json_encode($result);
        }
        return $result;
    }

    public static function getParamsAppl($json_encoded = false, $force_params = false, $omit_empty = false)
    {
        if ($force_params !== false && is_array($force_params)) {
            $tmp_params = $force_params;
        } else {
            $tmp_params = self::$params;
        }

        if (@count(self::$omit_appl_params) > 0 && @count($tmp_params > 0)) {
            foreach (self::$omit_appl_params as $param_name) {
                unset($tmp_params[$param_name]);
            }
        }

        if ($omit_empty) {
            if (@count($tmp_params) > 0) {
                foreach ($tmp_params as $param_name => $param_val) {
                    if (strlen($param_val) <= 0 || intval($param_val) == -1) {
                        //
                    } else {
                        $result[$param_name] = $param_val;
                    }
                }
            }
        } else {
            $result = $tmp_params;
        }

        if ($json_encoded) {
            $result = json_encode($result);
        }
        return $result;
    }

    public static function getParam($in_param_name, $sanitize = false, $sanitize_type = self::PARAMETER_SANITIZE_STRING)
    {
        $result = false;

        $in_param_name = mb_strtolower($in_param_name);

        if (isset(self::$params[$in_param_name])) {
            if ($sanitize) {
                switch ($sanitize_type) {
                    default:
                    case self::PARAMETER_SANITIZE_STRING: $result = trim(strip_tags(self::$params[$in_param_name])); break;
                    case self::PARAMETER_SANITIZE_INTEGER: $result = intval(self::$params[$in_param_name]); break;
                    case self::PARAMETER_SANITIZE_FLOAT: $result = floatval(str_replace(",",".",self::$params[$in_param_name])); break;
                }

            } else {
                $result = self::$params[$in_param_name];
            }
        }

        return $result;
    }

    public static function setParam($in_param_name, $in_param_val)
    {

        $in_param_name = mb_strtolower($in_param_name);

        self::$params[$in_param_name] = $in_param_val;
        $_REQUEST[$in_param_name] = $in_param_val;

        return true;
    }

    public static function setParams($in_params, $force_overwite = false)
    {
        if (isset($in_params) && is_array($in_params) && @count($in_params) > 0) {
            foreach ($in_params as $param_name => $param_val) {
                self::$params[$param_name] = $param_val;
                if ($force_overwite) {
                    $_REQUEST[$param_name] = $param_val;
                } else {
                    if (!isset($_REQUEST[$param_name])) {
                        $_REQUEST[$param_name] = $param_val;
                    }
                }
            }
        }
        return true;
    }

    public static function clearConditions()
    {
        return self::$check_params = array();
    }

    public static function addContition($field_name, $field_type, $field_special_check = false, $flags = false)
    {
        $field = array();
        $field["name"] = mb_strtolower($field_name);
        $field["type"] = $field_type;
        if ($field_special_check != false)
        {
            $field["special_check"] = $field_special_check;
        }
        if ($flags != false)
        {
            if (is_array($flags) && @count($flags)>0)
            {
                $field["flags"] = $flags;
            }
            else
            {
                $field["flags"] = array($flags);
            }
        }
        return self::$check_params[] = $field;
    }

    public static function validate() {
        $result = true;
        if (@count(self::$check_params)>0) {
            foreach (self::$check_params as &$check_param) {
                $field_check = @isset(self::$params[$check_param["name"]]);

                if ($field_check) {
                    $check_param["value"] = self::$params[$check_param["name"]];
                    switch ($check_param["type"]) {
                        case "integer":
                            $field_check = ((string)intval($check_param["value"]) ==  $check_param["value"]);
                            $eval_func = "intval";
                            break;
                        case "float":
                            if (strlen($check_param["value"]) > 0) {
                                $check_param["value"] = str_replace(",", ".", $check_param["value"]);
                            }
                            $field_check = ((string)floatval($check_param["value"]) == $check_param["value"]);
                            $eval_func = "floatval";
                            break;
                        case "string":
                            $field_check = (strlen($check_param["value"]) > 0);
                            $eval_func = "strlen";
                            break;
                        case "array":
                            $field_check = (@is_array($check_param["value"]) && @count($check_param["value"]) >= 0);
                            $eval_func = "count";
                            break;
                            defaut:
                            $field_check = true;
                            break;
                    }
                }

                if ($field_check && isset($check_param["flags"]))
                {
                    if (is_array($check_param["flags"]) && @count($check_param["flags"])>0)
                    {
                        foreach ($check_param["flags"] as $flag)
                        {
                            switch ($flag)
                            {
                                case PARAMETER_CHECK_NON_ZERO:
                                case self::PARAMETER_CHECK_NON_ZERO:
                                    $field_cond = " != 0";
                                    break;
                                case PARAMETER_CHECK_ABOVE_ZERO:
                                case self::PARAMETER_CHECK_ABOVE_ZERO:
                                    $field_cond = " > 0";
                                    break;
                                case PARAMETER_CHECK_BELOW_ZERO:
                                case self::PARAMETER_CHECK_BELOW_ZERO:
                                    $field_cond = " < 0";
                                    break;
                                case PARAMETER_CHECK_ZERO_PLUS:
                                case self::PARAMETER_CHECK_ZERO_PLUS:
                                    $field_cond = " >= 0";
                                    break;
                                case PARAMETER_CHECK_ZERO_MINUS:
                                case self::PARAMETER_CHECK_ZERO_MINUS:
                                    $field_cond = " <= 0";
                                    break;
                                case PARAMETER_CHECK_ZERO:
                                case self::PARAMETER_CHECK_ZERO:
                                    $field_cond = " == 0";
                                    break;
                                case PARAMETER_CHECK_TRUE:
                                case self::PARAMETER_CHECK_TRUE:
                                    $field_cond = " === true";
                                    break;
                                case PARAMETER_CHECK_FALSE:
                                case self::PARAMETER_CHECK_FALSE:
                                    $field_cond = " === false";
                                    break;
                            }
                            $eval_data = "return ".$eval_func."(".$check_param["value"].")".$field_cond.";";
                            $field_check = eval($eval_data);
                        }
                    }
                }

                if (isset($check_param["special_check"]) && strlen($check_param["special_check"]) > 0)
                {
                    $field_check = \String\StringUtils::validate_string($check_param["special_check"],$check_param["value"]);
                }

                $check_param["result"] = $field_check;
                if (!$field_check)
                {
                    $result = false;
                }
            }
        }

        return $result;
    }

    public static function getValidationResults()
    {
        return self::$check_params;
    }

    public static function validateDirect($field_name, $field_type, $field_special_check = false, $flags = false)
    {
        self::clearConditions();
        switch ($field_type) {
            case "time":
                $field_type = "string";
                $field_special_check = "time";
                break;
            case "date":
                $field_type = "string";
                $field_special_check = "us_date";
                break;
            default: break;
        }
        self::addContition($field_name, $field_type, $field_special_check, $flags);
        return self::validate();
    }

    public static function hasParams()
    {
        $result = (is_array(self::$params) && @count(self::$params) > 0);
        return $result;
    }

    public static function hasParamsAnon()
    {
        $anon_arr = self::getParamsAnon();
        $result = (is_array($anon_arr) && @count($anon_arr) > 0);
        return $result;
    }
}
