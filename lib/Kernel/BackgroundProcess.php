<?php
namespace Kernel;

class BackgroundProcess {
    private static $command;
    private static $pid;

    public static function run($command, $output_file = "/dev/null") {
        $result = false;

        self::$command = $command;
        self::$pid = shell_exec(sprintf(
            '%s > %s 2>&1 & echo $!',
            self::$command,
            $output_file
        ));
        self::$pid = trim(self::$pid,"\n");
        $result = self::$pid;

        return self::$pid;
    }

    public static function isRunning($in_pid) {
        if ($in_pid === false) {
            $in_pid = self::$pid;
        }
        try {
            $result = shell_exec(sprintf('ps %d', $in_pid));
            if(count(preg_split("/\n/", $result)) > 2) {
                return true;
            }
        } catch(Exception $e) {}

        return false;
    }

    public static function getPid() {
        return self::pid;
    }
}
?>
