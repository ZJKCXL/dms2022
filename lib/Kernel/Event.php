<?php

namespace Kernel;

class Event
{

    private $listeners = array();

    public function __construct()
    {
        
    }

    public function addListener($event_id, $callback)
    {
        if (!isset($this->listeners[$event_id])) {
            $this->listeners[$event_id] = array();
        }

        if (!in_array($callback, $this->listeners)) {
            $this->listeners[] = $callback;
        }
    }
    
    

}
