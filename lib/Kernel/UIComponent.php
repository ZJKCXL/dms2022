<?php

namespace Kernel;

class UIComponent
{

    private $html;
    private $id, $name, $type, $value, $class;
    private $attributes = array();

    public function __construct($template_file)
    {
        if (@file_exists(BASE_DIR . "templates/ui_components/" . $template_file . ".html")) {
            $this->html = file_get_contents(BASE_DIR . "templates/ui_components/" . $template_file . ".html");
        }
    }

    public function setID($id)
    {
        $this->id = $id;
        return $this->setContent("id", $id);
    }

    public function getID()
    {
        return $this->id;
    }

    public function setAttribute($name, $value)
    {
        return $this->attributes[$name] = $value;
    }

    public function getAttribute($name)
    {
        if (isset($this->attributes[$name])) {
            return $this->attributes[$name];
        }

        return false;
    }

    public function setClass($class_name)
    {
        return $this->class .= " " . $class_name;
    }

    public function removeClass($class_name)
    {
        return $this->class = preg_replace("/(\s|)$class_name/", "", $this->class);
    }

    private function attributesToString()
    {
        $result = "";

        if (is_array($this->attributes)) {
            foreach ($this->attributes as $attribute_name => $attribute_value) {
                $result .= " $attribute_name=\"$attribute_value\"";
            }
        }
        
        return $result;
    }
    
    public function setContent($placeholder, $replace)
    {
        return $this->html = str_replace("<\$$placeholder\$>", $replace, $this->html);
    }

    public function getHTML()
    {
        $this->html = str_replace('<$attributes$>', $this->attributesToString(), $this->html);
        $this->html = str_replace('<$class$>', $this->class, $this->html);

        return preg_replace("/<\\$[A-Za-z_]+\\$>/", "", $this->html);
    }

    public function __toString()
    {
        return $this->getHTML();
    }
}
