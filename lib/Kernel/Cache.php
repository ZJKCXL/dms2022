<?php
namespace Kernel;

class Cache {
    private static $cache = array();

    private static function calculateHash($in_data) {
        $result = sha1(json_encode($in_data));
        return $result;
    }

    public static function isCached($in_data) {
        $hash = self::calculateHash($in_data);
        $result = isset(self::$cache[$hash]);
        return $result;
    }

    public static function getValue($in_data) {
        $result = self::$cache[self::calculateHash($in_data)];
        return $result;
    }

    public static function setValue($in_data, $in_value) {
        $result = (self::$cache[self::calculateHash($in_data)] = $in_value);
        return $result;
    }

    public static function getCount() {
        $result = @count(self::$cache);
        return $result;
    }

    public static function flushCache() {
        unset(self::$cache);
        self::$cache = array();
        return true;
    }

    public static function getCache() {
        return self::$cache;
    }

    public static function getSize() {
        return strlen(serialize(self::$cache));
    }
}
?>
