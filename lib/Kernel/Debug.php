<?php
namespace Kernel;

class Debug {

    const SLOW_QUERY_THRESHOLD = 0.1;

    private static $timers;
    private static $profiler;
    private static $vars = array();

    public static function getGITBranch($get_commit = false) {
        $result = false;

        $head = file_get_contents(BASE_DIR . '.git/HEAD');
        $head = trim(str_replace('ref: ', '', $head));

        if ($get_commit !== false) {
            $commit = file_get_contents(BASE_DIR . '.git/' . $head);
        }

        $head = substr($head, strrpos($head, '/') + 1);

        $result = $head;
        if ($get_commit !== false) {
            $result .= "#".$commit;
        }

        return $result;
    }

    public static function timerStart($name = false) {
        $result = false;

        $id = uniqid("TIMER",true);
        if ($id !== false) {
            self::$timers[$id]["start"] = microtime(true);
            if ($name != false) {
                self::$timers[$id]["name"] = $name;
            }
            $result = $id;
        }

        return $result;
    }

    public static function timerStop($timer_id) {
        $result = false;

        if ($timer_id && is_array(self::$timers) && count(self::$timers)) {
            $result = self::$timers[$timer_id]["end"] = microtime(true);
            self::$timers[$timer_id]["duration"] = self::$timers[$timer_id]["end"] - self::$timers[$timer_id]["start"];
        }

        return $result;
    }

    public static function timerStopAll() {
        $result = true;

        if (@count(self::$timers) > 0) {
            foreach (self::$timers as $id => &$timer) {
                if (!isset($timer["end"])) {
                    self::timerStop($id);
                }
            }
        }

        return $result;
    }

    public static function getTimer($timer_id) {
        $result = false;

        if (isset(self::$timers[$timer_id])) {
            $result = self::$timers[$timer_id];
        }

        return $result;
    }

    public static function timerGetDuration($timer_id) {
        $result = false;

        if (isset(self::$timers[$timer_id]["duration"])) {
            $result = $result = self::$timers[$timer_id]["duration"];
        }

        return $result;
    }

    public static function getTimers() {
        return self::$timers;
    }

    public static function profilerStart() {
        return self::$profiler = array();
    }

    public static function profilerRecordEvent($timer_id, $profiler_data) {
        $profile = array();
        $profile["timer"] = $timer_id;
        $profile["data"] = $profiler_data;

        return self::$profiler[] = $profile;
    }

    public static function profilerStop() {
        return \Kernel\Debug::timerStopAll();
    }

    public static function profilerGetData($add_timers) {
        if ($add_timers) {
            foreach (self::$profiler as &$profile_row) {
                $profile_row["duration"] = self::timerGetDuration($profile_row["timer"]);
            }
        }
        return self::$profiler;
    }

    public static function outputDebug($line_end = "<br />") {
        $result = false;
        $trace = debug_backtrace();

        $result = "[".$trace[@count($trace)-2]["file"]."][".$trace[@count($trace)-2]["line"]."]";
        if ($line_end !== false) {
            $result .= "<br />";
        }

        return $result;
    }

    public static function addVar($in_var_name, $in_var_value, $step = 0) {
        $result = true;

        $stamp = microtime(false);
        $back_trace = debug_backtrace();

        self::$vars[$stamp]["name"] = $in_var_name;
        self::$vars[$stamp]["val"] = $in_var_value;
        self::$vars[$stamp]["step"] = $step;
        self::$vars[$stamp]["file"] = str_replace(BASE_DIR, "", $back_trace[0]["file"]);
        self::$vars[$stamp]["line"] = $back_trace[0]["line"];

        return $result;
    }

    public static function addSQL($in_var_name, $in_sql, $step = 0) {
        $result = true;

        $stamp = microtime(false);
        $back_trace = debug_backtrace();

        self::$vars[$stamp]["name"] = $in_var_name;
        self::$vars[$stamp]["val"] = str_replace("\n","",nl2br(\SqlParser\Utils\Formatter::format($in_sql, array('type' => 'text', 'parts_newline' => false, 'clause_newline' => false))));
        self::$vars[$stamp]["step"] = $step;
        self::$vars[$stamp]["file"] = str_replace(BASE_DIR, "", $back_trace[0]["file"]);
        self::$vars[$stamp]["line"] = $back_trace[0]["line"];

        return $result;
    }

    public static function addVarComment($in_var_comment, $step = 0) {
        $result = true;

        $stamp = microtime(false);
        $back_trace = debug_backtrace();

        self::$vars[$stamp]["name"] = "comment";
        self::$vars[$stamp]["val"] = $in_var_comment;
        self::$vars[$stamp]["step"] = $step;
        self::$vars[$stamp]["file"] = str_replace(BASE_DIR, "", $back_trace[0]["file"]);
        self::$vars[$stamp]["line"] = $back_trace[0]["line"];

        return $result;
    }

    public static function dumpVars($do_print = true, $line_end = "<br />") {
        $result = true;

        if (@count(self::$vars)) {
            if ($do_print) {
                echo "<br /><br /><div class=\"alert alert-danger\" role=\"alert\">";
            }
            foreach (self::$vars as $var_stamp => $var_data) {
                $var_stamp_parts = explode(" ", $var_stamp);
                $var_stamp_parts[0] = substr($var_stamp_parts[0],1);
                $var_stamp = date("Y-m-d H:i:s",$var_stamp_parts[1]).$var_stamp_parts[0];
                if ($do_print) {
                    if ($var_data["name"] == "comment") {
                        echo "[".$var_stamp."]: ".str_repeat(".",$var_data["step"] * 3)." ".$var_data["val"].$line_end;
                    } else {
                        echo "[".$var_stamp."][".$var_data["file"]."][".$var_data["line"]."]: ".str_repeat(".",$var_data["step"] * 3)." variable [".$var_data["name"]."] value [".json_encode($var_data["val"])."]".$line_end;
                    }
                }
            }
            if ($do_print) {
                echo "</div>";
            }
        }

        return $result;
    }

    public static function dumpTimers($do_print = true, $line_end = "<br />") {
        $result = true;

        if (@count(self::$vars)) {
            if ($do_print) {
                echo "<br /><br /><div class=\"alert alert-danger\" role=\"alert\">";
            }
            foreach (self::$timers as $timer_id => $timer_data) {
                $var_stamp_parts = explode(" ", microtime());
                $var_stamp_parts[0] = substr($var_stamp_parts[0],1);
                $var_stamp = date("Y-m-d H:i:s",$var_stamp_parts[1]).$var_stamp_parts[0];
                if ($do_print) {
                    echo "[".$var_stamp."][".$timer_id."]: ".str_repeat(".",$var_data["step"] * 3)." timer [".$timer_data["name"]."] duration [".$timer_data["duration"]."]".$line_end;
                }
            }
            if ($do_print) {
                echo "</div>";
            }
        }

        return $result;
    }

    public static function clearVals() {
        $result = self::$vars = array();
    }
}
?>
