<?php
namespace Kernel;

class Auth {

    private static function setUserDataSession ($in_user_data) {
        $result = true;

        $result = $result && \Kernel\User::setUser($in_user_data["gid"]);
        $result = $result && \Kernel\User::setUserData($in_user_data);

        return $result;
    }

    private static function setUserDataCookie ($in_user_data) {

        $result = false;

        $cookie_data = $in_user_data["id"].";".$in_user_data["name"]." ".$in_user_data["surname"].";".$in_user_data["type_text"].";".crc32(str_rot13($in_user_data[""]."#".$timestamp)).";".$in_user_data["type"].";".$in_user_data["firm"];
        $log_cook=$cookie_data;
        $cookie_data = \String\Crypt::feistel_encrypt($cookie_data);

        \Kernel\Logger::logToFile("user [".$in_user_data["login"]."] has sucessfully logged in from [".$_SERVER['REMOTE_ADDR']."]. Cookie set as [".addslashes($log_cook)."], encrypted as [".addslashes($cookie_data)."]",\Kernel\Logger::LOG_ACCESS);

        @setcookie(COOKIE_USER_NAME,$cookie_data,0,COOKIE_PATH);

        $result = true;

        if (!isset($_COOKIE["eclipse_lang"]) || $_COOKIE["eclipse_lang"] != $in_user_data["lang"]) {
            setcookie("eclipse_lang", $in_user_data["lang"], null, COOKIE_PATH);
        }

        if ($in_user_data["type"] == \Application\User::TYPE_DISPONENT) {
            setcookie("dispo_tech_country",-1,null, COOKIE_PATH);
        }

        return $result;
    }

    public static function setAuthorizedUser($in_user_id) {
        $result = true;

        if (intval($in_user_id) > 0) {
            $user_data = \Application\User::getUserData($in_user_id, true);;
            if (\Kernel\Func::resultValidArr($user_data)) {
                if ($user_data["is_valid"] == 1 && $user_data["can_login"] == 1) {
                    $result = $result && self::setUserDataSession($user_data);
                    $result = $result && self::setUserDataCookie($user_data);
                }
            }
        }

        return $result;
    }

    public static function hasBasicAuth() {

        return (isset($_SERVER["PHP_AUTH_USER"]) && strlen($_SERVER["PHP_AUTH_USER"]) > 0);

    }

    public static function doBasicAuth() {
        $result = false;

        $user = false;
        if (isset($_SERVER["PHP_AUTH_USER"]) && strlen($_SERVER["PHP_AUTH_USER"]) > 0) {
            $user = trim(strip_tags($_SERVER["PHP_AUTH_USER"]));
        }
        $passwd = false;
        if (isset($_SERVER["PHP_AUTH_PW"]) && strlen($_SERVER["PHP_AUTH_PW"]) > 0) {
            $passwd = trim(strip_tags($_SERVER["PHP_AUTH_PW"]));
        }

        if ($user !== false && $passwd !== false) {
            $user_data = \DB\Auth::search(array("user" => $user),array("password", "desc"));
            if (\Kernel\Func::resultValidArr($user_data)) {
                if ($passwd == $user_data[0]["password"]) {
                    $result = true;
                    \Kernel\Logger::logToFile("basic auth request for user [".$user."] succesfull from [".$_SERVER['REMOTE_ADDR']."].",\Kernel\Logger::LOG_ACCESS);
                } else {
                    \Kernel\Logger::logToFile("invalid basic auth request for user [".$user."] tried password [".$passwd."] in from [".$_SERVER['REMOTE_ADDR']."]. passwords do not match",\Kernel\Logger::LOG_ACCESS);
                }
            } else {
                \Kernel\Logger::logToFile("invalid basic auth request for user [".$user."] tried password [".$passwd."] in from [".$_SERVER['REMOTE_ADDR']."]. user not know to system",\Kernel\Logger::LOG_ACCESS);
                \Kernel\Logger::logToFile("trying User table for [".$user."]",\Kernel\Logger::LOG_ACCESS);

                $user_data = \DB\User::search(array("login" => $user),array("password"));

                if (\Kernel\Func::resultValidArr($user_data)) {
                    $passwd = \String\Crypt::mysql_password($passwd);
                    if ($passwd !== false) {
                        $passwd = "*".mb_strtoupper($passwd);
                    }
                    if ($passwd == $user_data[0]["password"]) {
                        $result = true;
                        \Kernel\Logger::logToFile("basic auth request for user [".$user."] succesfull from [".$_SERVER['REMOTE_ADDR']."] using User table",\Kernel\Logger::LOG_ACCESS);
                    } else {
                        \Kernel\Logger::logToFile("invalid basic auth request for user [".$user."] tried password [".$passwd."] in from [".$_SERVER['REMOTE_ADDR']."]. passwords do not match using User table",\Kernel\Logger::LOG_ACCESS);
                    }
                } else {
                    \Kernel\Logger::logToFile("[".$user."] not found in User table",\Kernel\Logger::LOG_ACCESS);
                }
            }
        }


        return $result;
    }

}
?>
