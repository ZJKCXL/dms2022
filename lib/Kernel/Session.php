<?
namespace Kernel;

define("REQUEST_URI","request_uri");

class Session {

    const STATE = 'state';
    const SELF_URI = 'curr';
    const BACK_URI = 'back';

    public static function storeState($in_state)
    {
        $_SESSION[self::STATE] = $in_state;
    }

    public static function getState()
    {
        return $_SESSION[self::STATE];
    }

    public static function getRequestURI()
    {
        return $_SESSION[self::STATE]["request"]["request_uri"];
    }

    public static function storeSelfURI($in_self_uri) {
        $_SESSION[self::SELF_URI] = $in_self_uri;
    }

    public static function getSelfURI() {
        return $_SESSION[self::SELF_URI];
    }

    public static function storeBackURI($in_back_uri) {
        $_SESSION[self::BACK_URI] = $in_back_uri;
    }

    public static function getBackURI() {
        return $_SESSION[self::BACK_URI];
    }

    public static function storeApplURI($in_appl_domain, $in_appl_part , $in_appl_uri) {
        return $_SESSION[$in_appl_domain][$in_appl_part] = $in_appl_uri;
    }

    public static function getApplURI($in_appl_domain, $in_appl_part) {
        return $_SESSION[$in_appl_domain][$in_appl_part];
    }

    public static function storeData($in_key, $in_data) {
        return $_SESSION[$in_key] = $in_data;
    }

    public static function getData($in_key) {
        return $_SESSION[$in_key];
    }

    public static function clearData($in_key) {
        $_SESSION[$in_key] = NULL;
        unset($_SESSION[$in_key]);

        return true;
    }

    public static function setParam($in_param, $in_value) {
        return $_SESSION["params"][$in_param] = $in_value;
    }

    public static function getParam($in_param) {
        return $_SESSION["params"][$in_param];
    }
}

?>
