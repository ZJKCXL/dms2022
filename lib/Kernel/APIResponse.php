<?php
namespace Kernel;

class APIResponse {

    const RESULT_OK = "ok";
    const RESULT_ERR = "err";

    private $msg_id;
    private $msg_status;
    private $msg_result_code;
    private $msg_result_text;
    private $msg_result_target;
    private $msg_data;
    private $msg_debug;

    public function __construct() {
        $this->msg_id = \Kernel\Headers::generateMessageID();
    }

    public function setResult($in_status, $in_result_code = false, $in_result_text = false, $in_result_target = false) {
        $this->msg_status = $in_status;
        if ($in_result_code !== false) {
            $this->msg_result_code = $in_result_code;
        }
        if ($in_result_text !== false) {
            $this->msg_result_text = $in_result_text;
        }
        if ($in_result_target !== false) {
            $this->msg_result_target = $in_result_target;
        }
    }

    public function setPayload($in_data) {
        $this->msg_data = $in_data;
    }

    public function setDebugInfo($in_data) {
        $this->msg_debug = $in_data;
    }

    public function generate($json_encode = false) {
        $result = array();

        $result["id"] = $this->msg_id;
        \Kernel\Headers::generateHeader(\Kernel\Headers::HEADER_MESSAGE_ID, $this->msg_id);
        $result["status"] = $this->msg_status;
        if (isset($this->msg_result_code) || isset($this->msg_result_text) || isset($this->msg_result_target)) {
            if (isset($this->msg_result_code)) {
                $result["result"]["code"] = $this->msg_result_code;
            }
            if (isset($this->msg_result_text)) {
                $result["result"]["text"] = $this->msg_result_text;
            }
            if (isset($this->msg_result_target)) {
                $target = $this->msg_result_target;
                if (DEBUG == 1) {
                    \Kernel\Logger::logToFile("API response redirecting to ".$target,\Kernel\Logger::LOG_DEBUG,$this);
                }
                $result["result"]["target"] = $target;
            }
        }
        if (isset($this->msg_data)) {
            $result["data"] = $this->msg_data;
        }
        if (isset($this->msg_debug)) {
            $result["debug"] = $this->msg_debug;
        }

        if ($json_encode) {
            $result = json_encode($result);
        }

        return $result;
    }




}
?>
