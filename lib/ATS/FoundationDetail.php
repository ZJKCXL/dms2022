<?php
namespace ATS;


class FoundationDetail {
    const ENDPOINT = 'api_foundation_get';

    /**
    * funkce vraci DMS daily report
    *
    * @param int $in_collecton_id ID sbirky
    * @return array|false
    */

    public static function getDetail($in_foundation_id) {
        $result = false;
        $params = array();

        $params["item_id"] = $in_foundation_id;

        if (\Kernel\Func::resultValidArr($params)) {
            $res = \ATS\Request::doQuery(self::ENDPOINT, $params);
            if (\Kernel\Func::resultValidArr($res)) {
                if ($res["status"] == \ATS\Request::REPONSE_OK) {
                    $result = $res["data"];
                }
            }
        }

        return $result;
    }
}
?>
