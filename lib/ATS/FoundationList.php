<?php
namespace ATS;


class FoundationList {
    const ENDPOINT = 'api_foundation_list';

    /**
    * funkce vraci seznam nadaci
    *
    * @param int $in_offset odkud zacit
    * @param int $in_count pocet vysledku
    *
    * @return array|false
    */

    public static function getList($in_offset = 0, $in_count = 9999) {
        $result = false;
        $params = array();

        $params["offset"] = $in_offset;
        $params["count"] = $in_count;

        if (\Kernel\Func::resultValidArr($params)) {
            $res = \ATS\Request::doQuery(self::ENDPOINT, $params);
            if (\Kernel\Func::resultValidArr($res)) {
                if ($res["status"] == \ATS\Request::REPONSE_OK) {
                    $result = $res["data"];
                }
            }
        }

        return $result;
    }
}
?>
