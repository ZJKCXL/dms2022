<?php
namespace ATS;


class CollectionDetail {
    const ENDPOINT = 'api_collection_get';

    /**
    * funkce vraci DMS daily report
    *
    * @param int $in_collecton_id ID sbirky
    * @return array|false
    */

    public static function getDetail($in_collecton_id) {
        $result = false;
        $params = array();

        $params["item_id"] = $in_collecton_id;

        if (\Kernel\Func::resultValidArr($params)) {
            $res = \ATS\Request::doQuery(self::ENDPOINT, $params);
            if (\Kernel\Func::resultValidArr($res)) {
                if ($res["status"] == \ATS\Request::REPONSE_OK) {
                    $result = $res["data"];
                }
            }
        }

        return $result;
    }
}
?>
