<?php

if (!function_exists("import_request_variables")) {
    /**
     * @param false $scope
     */
    function import_request_variables($scope = false)
    {
        extract($_REQUEST, EXTR_REFS);
    }
}

if (!function_exists("eregi")) {
    /**
     * @param $pattern
     * @param $string
     * @param false $regs
     * @return false|int
     */
    function eregi($pattern, $string, &$regs = false)
    {
        return preg_match("/".$pattern."/", $string);
    }
}

 
/*
function convertPattern($pattern, $i)
{
    return \sprintf('/%s/%s', \addcslashes($pattern, '/'), $i ? 'i' : '');
}

function eregi_replace($pattern, $replacement, $string)
{
    return \preg_replace(convertPattern($pattern, true), $replacement, $string);
}
*/
 
if (!function_exists("mysql_query")) {

    /**
     * @param $server
     * @param $user_name
     * @param $password
     * @return false|mysqli
     */
    function mysql_connect($server, $user_name, $password)
    {
        $result = false;

        $link = @mysqli_connect($server, $user_name, $password);
        if ($link) {
            $GLOBALS["link"] = $link;
            $result = $link;
        }

        return $result;
    }

    /**
     * @param $in_db_name
     * @param null $in_link
     * @return bool
     */
    function mysql_select_db($in_db_name, $in_link = null)
    {
        if ($in_link === null) {
            $in_link = $GLOBALS["link"];
        }

        return mysqli_select_db($in_link, $in_db_name);
    }

    /**
     * @param $in_query
     * @param null $in_link
     * @return bool|mysqli_result
     */
    function mysql_query($in_query, $in_link = null)
    {
        if ($in_link === null) {
            $in_link = $GLOBALS["link"];
        }

        return mysqli_query($in_link, $in_query);
    }

    /**
     * @param $in_res
     * @return int
     */
    function mysql_num_rows($in_res)
    {
        return @mysqli_num_rows($in_res);
    }

    /**
     * @param $in_res
     * @param int $mode
     * @return array|null
     */
    function mysql_fetch_array($in_res, $mode = 3)
    {
        return @mysqli_fetch_array($in_res, $mode);
    }

    /**
     * @param $in_res
     * @param int $mode
     * @return array
     */
    function mysql_fetch_all($in_res, $mode = 3)
    {
        return @mysqli_fetch_all($in_res, $mode);
    }

    /**
     * @param $in_res
     * @return string[]|null
     */
    function mysql_fetch_assoc($in_res)
    {
        return @mysqli_fetch_assoc($in_res);
    }

    /**
     * @param $in_res
     * @return array|null
     */
    function mysql_fetch_row($in_res)
    {
        return @mysqli_fetch_row($in_res);
    }

    /**
     * @param null $in_link
     * @return int
     */
    function mysql_affected_rows($in_link = null)
    {
        if ($in_link === null) {
            $in_link = $GLOBALS["link"];
        }
        return @mysqli_affected_rows($in_link);
    }

    /**
     * @param null $in_link
     * @return int|string
     */
    function mysql_insert_id($in_link = null)
    {
        if ($in_link === null) {
            $in_link = $GLOBALS["link"];
        }
        return @mysqli_insert_id($in_link);
    }

    /**
     * @param null $in_link
     * @return string
     */
    function mysql_error($in_link = null)
    {
        if ($in_link === null) {
            $in_link = $GLOBALS["link"];
        }
        return @mysqli_connect_error($in_link);
    }

    /**
     * @param $resource
     * @param $row_number
     * @return bool
     */
    function mysql_data_seek($resource, $row_number)
    {
        return @mysqli_data_seek($resource, $row_number);
    }

    /**
     * @param $in_str
     * @param null $in_link
     * @return string
     */
    function mysql_real_escape_string($in_str, $in_link = null): string
    {
        if ($in_link === null) {
            $in_link = $GLOBALS["link"];
        }
        return @mysqli_real_escape_string($in_link, $in_str);
    }

    /**
     * @param $in_str
     * @return string
     */
    function mysql_escape_string($in_str, $in_link = null): string
    {
        if ($in_link === null) {
            $in_link = $GLOBALS["link"];
        }
        return @mysqli_real_escape_string($in_link, $in_str);
    }

    /**
     * @param null $in_link
     * @return bool
     */
    function mysql_close($in_link = null)
    {
        if ($in_link === null) {
            $in_link = $GLOBALS["link"];
        }
        return @mysqli_close($in_link);
    }

    /**
     * @param null $in_link
     * @return bool
     */
    function mysql_ping($in_link = null)
    {
        if ($in_link === null) {
            $in_link = $GLOBALS["link"];
        }
        return @mysqli_ping($in_link);
    }

    /**
     * @param $res
     */
    function mysql_free_result($res)
    {
        return @mysqli_free_result($res);
    }

    /**
     * @param null $in_link
     * @return int
     */
    function mysql_errno($in_link = null)
    {
        if ($in_link === null) {
            $in_link = $GLOBALS["link"];
        }
        return @mysqli_errno($in_link);
    }
}


