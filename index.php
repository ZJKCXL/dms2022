<?php
header("content-type: text/html; charset=utf-8"   );
 
$page = @$_REQUEST['page'];
$section = @$_REQUEST['section'];
$note = @$_REQUEST['note'];
$blabla = @$_REQUEST['blabla'];
include_once("./lib/globals.php");
include_once("config/conf.protected.php");
include_once("config/conf.php");
 
\Kernel\Kernel::parseState();
 
include "./sql/db.php";
include_once "./sql/polyfill.php";
include "./lib/pageswitcher.php";
include "./lib/lib.php";
include "./lib/seo.php";
include "./lib/pager.php";
include("./lib/user_functions.php");
include("./lib/cfd_user_functions.php");
include("./lib/StringUtils.php");
include "./cookies/cookiesControl.php"; 
$time = time();
 

//login
//echo $_REQUEST["login"];
if (strlen($_REQUEST["login"])>0 && strlen($_REQUEST["pass"]))
{

    if(user_login(trim($_REQUEST["login"]),trim($_REQUEST["pass"]))){
        header("Location: https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
    }
    else{
        //$loginresult = "<span class='red'>Nepřihlášen/a</span>";
        //die();
    }
}
//logount
if ((user_logged_in())&&($_GET[please]=='deleteme'))
{
    user_logout();
    header("Location: ".$tempdir."/");
    //header("Location: ../index.php");
}

if (strlen($_REQUEST["login2"])>0 && strlen($_REQUEST["pass2"]))
{

    if(user_login2(trim($_REQUEST["login2"]),trim($_REQUEST["pass2"]))){
      //  echo "ok";
      header("Location: https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
    }
    else{
        echo "ne";
        //die();
        //$loginresult = "<span class='red'>Nepřihlášen/a</span>";
    }
}
//logount
if ((user_logged_in2())&&($_GET[please]=='deleteme2'))
{
    user_logout2();
    // header("Location: https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
    header("Location: ../index.php");
}


//Media login

if (strlen($_REQUEST["cfdemail"])>0 && strlen($_REQUEST["pass"]))
{
    if(cfd_user_login(trim($_REQUEST["cfdemail"]),trim($_REQUEST["pass"]))){
        header("Location: https://www.darcovskasms.cz".$tempdir."/dispatcher.php?sendmeto=https://www.darcovskasms.cz".$tempdir."/prihlaseni-novinare.html");
        //header("https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
    }
    else{
        //$loginresult = "<span class='red'>Nepřihlášen/a</span>";
    }
}

//logount
if ($_GET[please]=='cfdeleteme')
{
    cfd_user_logout();
    //header("Location: https://www.darcovskasms.cz".$tempdir."/dispatcher.php?sendmeto=https://www.darcovskasms.cz".$tempdir."/cfd-login.html");
    header("Location: https://www.darcovskasms.cz");
}




?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <title><?php echo pageWTF(@$finalpage,@$page,@$finalsection,@$finalID,@$finalnewsid,'title'); ?></title>
    <meta name='description' content='<?php echo pageWTF(@$finalpage,@$page,@$finalsection,@$finalID,@$finalnewsid,'desc'); ?>' />
    <meta name='keywords' content='<?php echo pageWTF(@$finalpage,@$page,@$finalsection,@$finalID,@$finalnewsid,'keywords'); ?>' />
    <!-- Facebook -->
    <?php
    if(strlen(pageWTF(@$finalpage,@$page,@$finalsection,@$finalID,@$finalnewsid,'fbimg'))>0){
    ?><meta property="og:image" content="<?php echo pageWTF(@$finalpage,@$page,@$finalsection,@$finalID,@$finalnewsid,'fbimg'); ?>" />
    <?php
    }
    ?><meta property="og:title" content="<?php echo pageWTF(@$finalpage,@$page,@$finalsection,@$finalID,@$finalnewsid, 'title'); ?>"/>
    <meta property="og:url" content="<?php  echo 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>"/>
    <meta property='og:description' content='<?php echo pageWTF(@$finalpage,@$page,@$finalsection,@$finalID,@$finalnewsid, 'desc'); ?>' />
    <meta property="og:type" content="website" />
    <!-- End Facebook -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo $tempdir; ?>/favicons/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo $tempdir; ?>/favicons/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo $tempdir; ?>/favicons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo $tempdir; ?>/favicons/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo $tempdir; ?>/favicons/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo $tempdir; ?>/favicons/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo $tempdir; ?>/favicons/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo $tempdir; ?>/favicons/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $tempdir; ?>/favicons/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="<?php echo $tempdir; ?>/favicons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="<?php echo $tempdir; ?>/favicons/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="<?php echo $tempdir; ?>/favicons/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="<?php echo $tempdir; ?>/favicons/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="<?php echo $tempdir; ?>/favicons/manifest.json">
    <link rel="mask-icon" href="<?php echo $tempdir; ?>/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-TileImage" content="/mstile-144x144.png">
    <meta name="theme-color" content="#fff8ef">
    <meta name="google-site-verification" content="wo3m8yf6ocqWJXMQY49nIwQ5jfcW3ys0KxvUzkdfxeQ" />
    <link href="<?php echo $tempdir; ?>/css2017/parallax.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $tempdir; ?>/css2017/layout.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $tempdir; ?>/css2017/forms.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $tempdir; ?>/css2017/response.css" rel="stylesheet" type="text/css" />
    <?php echo pageWTF ($finalpage,$page,$finalsection,$finalID,$finalnewsid,'css'); ?>
    <meta name="robots" content="index,follow" />
    <script src="<?php echo $tempdir; ?>/js/jquery/jquery.js" type="text/javascript"></script>
    <script src="<?php echo $tempdir; ?>/js/ui/jquery-ui.js" type="text/javascript"  ></script>

    <script src="<?php echo $tempdir; ?>/js/slideshow2.js"   defer></script>
    <link rel="stylesheet" href="<?php echo $tempdir; ?>/font-awesome/css/font-awesome.min.css">
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,700,900|Roboto+Condensed:400,300,700&subset=latin,latin-ext,cyrillic' rel='stylesheet' type='text/css'>

    <?php
    if( $ishome == 1){
    }else{
    ?>
        <link href="<?php echo $tempdir; ?>/css2017/responsePage.css" rel="stylesheet" type="text/css" />
    <?php
    }
    ?>

</head>
<body>
 
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-311147-33"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  <?php if($cookieAnalytics != 1) { ?>
  gtag('consent', 'default', {'ad_storage': 'denied','analytics_storage': 'denied'})
  <?php } ?>
  gtag('js', new Date());
  gtag('config', 'UA-311147-33');
</script>

<?php if(100 == 1) { ?> 
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/cs_CZ/all.js#xfbml=1&appId=110250542326699";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk')); 
    </script> 
<?php }  ?> 

    <nav><div>

            <?php
            if( $ishome == 1){
            ?>
                <h1 id='logo'><a href='<?php echo $tempdir; ?>/'><span>DMS - Dárcovská SMS</span></a></h1>
                <?php
            }else{
                ?>
                <div id='logo'><a href='<?php echo $tempdir; ?>/'><span>DMS - Dárcovská SMS</span></a></div>
            <?php
            }
            ?>

            <ul id="menu">
                <?php
                $type = 0;
                include ('./inside/2016menu.php');
                ?>
            </ul>
            <span id='startmenu' title='MENU'></span>
            <ul class="mini-menu" id="mini-menu">
                <?php
                $type = 0;
                include ('./inside/2016menu.php');
                ?>
            </ul>
        </div>
    </nav>

    <div id='wrapper'>

        <div id='main'>

            <?php



            include ('./inside/content.php');
            ?>

        </div>

        <footer>
            <div>

                <span>
                    <h4>Kontakt:   </h4>


                    <?php
                    include ('./adminpages/microdata_footer.php');
                    ?>



                    <span id='newsearch2'>
                        <h4>Vyhledat projekt:   </h4>
                        <form action='/search.html' method='post' name='formsrch' id='formsrch'>
                            <fieldset>
                                <input type='text' name='search' id='srch' /><input type='submit' name='gosrch' value='Hledat' />
                            </fieldset>
                        </form>
                    </span>

                </span>



                <span>
                    <h4>Naše stránky</h4>
                    <span class='break logodown'><a href='http://www.donorsforum.cz'><img src='<?php echo $tempdir; ?>/images/fd.png'  alt='Fórum dárců' title='Fórum dárců' /></a><a href='http://www.donorsforum.cz'>Fórum dárců</a></span>
                    <span class='break logodown'><a href='http://www.darujspravne.cz'><img src='<?php echo $tempdir; ?>/images/dsp.png'  alt='Daruj správně' title='Daruj správně' /></a><a href='http://www.darujspravne.cz'>Daruj správně</a></span>
                    <span class='break logodown'><a href='http://www.cenyforadarcu.cz'><img src='<?php echo $tempdir; ?>/images/cfd.png'  alt='Ceny Fóra dárců' title='Ceny Fóra dárců' /></a><a href='http://www.cenyforadarcu.cz'>Ceny Fóra dárců</a></span>
                    <span class='break logodown'><a href='http://www.apms.cz/'><img src='<?php echo $tempdir; ?>/images/apms.png'  alt='APMS' title='APMS' /></a><a href='http://www.apms.cz/'>APMS</a></span>
                </span>

                <span class='right'>
                    <h4> Další info </h4>
                    <li><a href="javascript:void(0);" aria-label="View cookie settings" data-cc="c-settings">Vaše nastavení Cookies</a></li>
                    <?php    include ('./inside/menudown.php');     ?>
                    <h4> Sociální sitě </h4>
                    <a class='social fa fa-facebook-official' href='https://www.facebook.com/darujspravne/' alt='Facebook'><span>Facebook</span></a>
                    <a class='social fa fa-youtube-square' href='https://www.youtube.com/channel/UC0RwiWO-LGHTbYbqdvjJlNw' alt='YouTube'><span>YouTube</span></a>
                    <a class='social fa fa-twitter-square' href='https://www.twitter.com/@darujspravne' alt='Twitter'><span>Twitter</span></a>
                </span>

                <div class='mainclr'> </div>
                <p class='hp'> Portál Dárcovská SMS je realizován za finanční podpory Úřadu vlády České republiky a Rady vlády pro nestátní neziskové organizace.</p>
            </div>
        </footer>
    </div>
 
    <script src="/cookies/cookieconsent.js"></script>
    <script src="/cookies/cookieconsent-init.js"></script>
</body>
<?php
echo "<hr/>".$finalpage;
if(count($_POST)) {
	$newREQ = '';
	foreach ( $_REQUEST as $param => $value ) {
	$newREQ .=  $param . ": " .  $value . "\n";
	}
	$hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);
	$dataqueryLOG = "INSERT INTO  _spyLOGwww (`jason`,`time`,`timestamp`,`userid`,`name`,`infotxt`) VALUES ('".$newREQ."','".time()."', NOW( ),'".get_user_id()."','".$hostname."','".$info_text.$infotext."')";
	mysql_query($dataqueryLOG);
	}
?>
</html>
