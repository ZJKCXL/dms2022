<?php

if (!function_exists("import_request_variables")) {
    /**
     * @param false $scope
     */
    function import_request_variables($scope = false)
    {
        extract($_REQUEST, EXTR_REFS);
    }
}

if (!function_exists("eregi")) {
    /**
     * @param $pattern
     * @param $string
     * @param false $regs
     * @return false|int
     */
    function eregi($pattern, $string, &$regs = false)
    {
        return preg_match("/".$pattern."/", $string);
    }
}


    /**
     * @param $server
     * @param $user_name
     * @param $password
     * @return false|mysqli
     */
    function mysql_connect($server, $user_name, $password)
    {
        $result = false;

        $link = @mysqli_connect($server, $user_name, $password);
        if ($link) {
            $GLOBALS["link"] = $link;
            $result = $link;
        }

        return $result;
    }

    /**
     * @param $in_db_name
     * @param null $in_link
     * @return bool
     */
    function mysql_select_db($in_db_name, $in_link = null)
    {
        if ($in_link === null) {
            $in_link = $GLOBALS["link"];
        }

        return mysqli_select_db($in_link, $in_db_name);
    }

    /**
     * @param $in_query
     * @param null $in_link
     * @return bool|mysqli_result
     */
    function mysql_query($in_query, $in_link = null)
    {
        if ($in_link === null) {
            $in_link = $GLOBALS["link"];
        }

        return mysqli_query($in_link, $in_query);
    }

    /**
     * @param $in_res
     * @return int
     */
    function mysql_num_rows($in_res)
    {
        return @mysqli_num_rows($in_res);
    }

 /**
	     * @param $in_res
	          * @return int
		       */

    function mysql_numrows($in_res)
    {
	return @mysqli_num_rows($in_res);
    }

    /**
     * @param $in_res
     * @param int $mode
     * @return array|null
     */
    function mysql_fetch_array($in_res, $mode = 3)
    {
        return @mysqli_fetch_array($in_res, $mode);
    }

    /**
     * @param $in_res
     * @param int $mode
     * @return array
     */
    function mysql_fetch_all($in_res, $mode = 3)
    {
        return @mysqli_fetch_all($in_res, $mode);
    }

    /**
     * @param $in_res
     * @return string[]|null
     */
    function mysql_fetch_assoc($in_res)
    {
        return @mysqli_fetch_assoc($in_res);
    }

    /**
     * @param $in_res
     * @return array|null
     */
    function mysql_fetch_row($in_res)
    {
        return @mysqli_fetch_row($in_res);
    }

    /**
     * @param null $in_link
     * @return int
     */
    function mysql_affected_rows($in_link = null)
    {
        if ($in_link === null) {
            $in_link = $GLOBALS["link"];
        }
        return @mysqli_affected_rows($in_link);
    }

    /**
     * @param null $in_link
     * @return int|string
     */
    function mysql_insert_id($in_link = null)
    {
        if ($in_link === null) {
            $in_link = $GLOBALS["link"];
        }
        return @mysqli_insert_id($in_link);
    }

    /**
     * @param null $in_link
     * @return string
     */
    function mysql_error($in_link = null)
    {
        if ($in_link === null) {
            $in_link = $GLOBALS["link"];
        }
        return @mysqli_connect_error($in_link);
    }

    /**
     * @param $resource
     * @param $row_number
     * @return bool
     */
    function mysql_data_seek($resource, $row_number)
    {
        return @mysqli_data_seek($resource, $row_number);
    }




    /**
     * @param $in_str
     * @param null $in_link
     * @return string
     */
    function mysql_real_escape_string($in_str, $in_link = null): string
    {
        if ($in_link === null) {
            $in_link = $GLOBALS["link"];
        }
        return @mysqli_real_escape_string($in_link, $in_str);
    }

    /**
     * @param $in_str
     * @return string
     */
    function mysql_escape_string($in_str, $in_link = null): string
    {
        if ($in_link === null) {
            $in_link = $GLOBALS["link"];
        }
        return @mysqli_real_escape_string($in_link, $in_str);
    }

    /**
     * @param null $in_link
     w
     * @return bool
     */
    function mysql_close($in_link = null)
    {
        if ($in_link === null) {
            $in_link = $GLOBALS["link"];
        }
        return @mysqli_close($in_link);
    }

    /**
     * @param null $in_link
     * @return bool
     */
    function mysql_ping($in_link = null)
    {
        if ($in_link === null) {
            $in_link = $GLOBALS["link"];
        }
        return @mysqli_ping($in_link);
    }

    /**
     * @param $res
     */
    function mysql_free_result($res)
    {
        return @mysqli_free_result($res);
    }

    /**
     * @param null $in_link
     * @return int
     */
    function mysql_errno($in_link = null)
    {
        if ($in_link === null) {
            $in_link = $GLOBALS["link"];
        }
        return @mysqli_errno($in_link);
    }

    /**
     * @param $result
     * @param $row
     * @param $field
     * @return false|mixed|void
     */
    function mysql_result($result, $row, $field = 0)
    {
        if (!mysqli_data_seek($result, $row)) {
            return false;
        }

        $found = true;
        if (strpos($field, '.') !== false) {
            [$table, $name] = explode('.', $field);
            $i = 0;
            $found = false;
            mysqli_field_seek($result, 0);
            while ($column = mysqli_fetch_field($result)) {
                if ($column->table === $table && $column->name === $name) {
                    $field = $i;
                    $found = true;
                    break;
                }
                $i++;
            }
        }

        $row = mysql_fetch_array($result);
        if ($found && array_key_exists($field, $row)) {
            return $row[$field];
        }
    }

    function mysql_num_fields($res)
    {
        return @mysqli_num_fields($res);
    }
    function mysql_fetch_field($res)
    {
        return @mysqli_fetch_field($res);
    }

   
    