<?
$wizzard_text = "";

if (isset($_REQUEST["AnswerScale"]) && is_array($_REQUEST["AnswerScale"]))
{
	foreach ($_REQUEST["AnswerScale"] as $question_id => $answer)
	{
		$question_query = "Select questionText from tblquestions where questionID = ".$question_id;
		$question_res = @mysql_query($question_query);
		if ($question_res && @mysql_num_rows($question_res)>0)
		{
			$question_row = @mysql_fetch_array($question_res);
			if (strlen($question_row["questionText"]) > 0)
			{
				$wizzard_text .= $question_row["questionText"]." [skala 1-5]: ".$answer."\n";
			}
		}
	}
}

if (isset($_REQUEST["AnswerRadio"]) && is_array($_REQUEST["AnswerRadio"]))
{
	foreach ($_REQUEST["AnswerRadio"] as $question_id => $answer)
	{
		$question_query = "Select questionText, question1Answer".$answer." from tblquestions where questionID = ".$question_id;
		$question_res = @mysql_query($question_query);
		if ($question_res && @mysql_num_rows($question_res)>0)
		{
			$question_row = @mysql_fetch_array($question_res);
			if (strlen($question_row["question1Answer".$answer]) > 0)
			{
				$wizzard_text .= $question_row["questionText"]." : ".$question_row["question1Answer".$answer]."\n";
			}
		}
	}
}

if (isset($_REQUEST["AnswerCheck"]) && is_array($_REQUEST["AnswerCheck"]))
{
	foreach ($_REQUEST["AnswerCheck"] as $question_id => $answer)
	{
		$question_query = "Select questionText from tblquestions where questionID = ".$question_id;
		$question_res = @mysql_query($question_query);
		if ($question_res && @mysql_num_rows($question_res)>0)
		{
			$question_row = @mysql_fetch_array($question_res);
			if (strlen($question_row["questionText"]) > 0)
			{
				$wizzard_text .= $question_row["questionText"]." : ";
			}
		}
		$answers = array();
		if (is_array($answer))
		{
			foreach ($answer as $answer_id)
			{
				$answer_query = "Select question1Answer".$answer_id." from tblquestions where questionID = ".$question_id;
				$answer_res = @mysql_query($answer_query);
				if ($answer_res && @mysql_num_rows($answer_res)>0)
				{
					$answer_row = @mysql_fetch_array($answer_res);
					if (strlen($answer_row["question1Answer".$answer_id]) > 0)
					{
						$answers[] = $answer_row["question1Answer".$answer_id];
					}
				}
			}
		}
		$wizzard_text .= implode(",",$answers)."\n";
	}
}

if (isset($_REQUEST["AnswerText"]) && is_array($_REQUEST["AnswerText"]))
{
	foreach ($_REQUEST["AnswerText"] as $question_id => $answer)
	{
		$question_query = "Select questionText  from tblquestions where questionID = ".$question_id;
		$question_res = @mysql_query($question_query);
		if ($question_res && @mysql_num_rows($question_res)>0)
		{
			$question_row = @mysql_fetch_array($question_res);
			if (strlen($answer) > 0)
			{
				$wizzard_text .= $question_row["questionText"]." : ".$answer."\n";
			}
		}
	}
}



$wizzard_text = "==================================\nAsistovany dotaz\n\n".$wizzard_text;
$wizzard_text .= "==================================\n";

$cols = array();
$vals = array();

if (isset($_REQUEST["nadpis"]) && strlen($_REQUEST["nadpis"])>0)
{
	$cols[] = "questHeadline";
	$cols[] = "questHeadlineASCII";
	$cols[] = "questURL";
	$vals[] = "\"".@mysql_escape_string(strip_tags(trim($_REQUEST["nadpis"])))."\"";
	$vals[] = "\"".@mysql_escape_string(czechize(strip_tags(trim($_REQUEST["nadpis"]))))."\"";
	$vals[] = "\"".makeLinkFrom(strip_tags(trim($_REQUEST["nadpis"])))."\"";
}

if (isset($_REQUEST["dotaz"]) && strlen($_REQUEST["dotaz"])>0)
{
	$cols[] = "questText";
	$cols[] = "questTextASCII";
	$vals[] = "\"".@mysql_escape_string(strip_tags(trim($_REQUEST["dotaz"])))."\"";
	$vals[] = "\"".@mysql_escape_string(czechize(strip_tags(trim($_REQUEST["dotaz"]))))."\"";
}

if (isset($_REQUEST["poznamka"]) && strlen($_REQUEST["poznamka"])>0 || strlen($wizzard_text)>0)
{
	$cols[] = "questComment";
	$cols[] = "questCommentASCII";
	$vals[] = "\"".@mysql_escape_string($wizzard_text."\n".strip_tags(trim($_REQUEST["poznamka"])))."\"";
	$vals[] ="\"". @mysql_escape_string(czechize($wizzard_text)."\n".czechize(strip_tags(trim($_REQUEST["poznamka"]))))."\"";
}

if (isset($_REQUEST["nickname"]) && strlen($_REQUEST["nickname"])>0)
{
	$cols[] = "tmpnick";
	$vals[] = "\"".@mysql_escape_string(strip_tags(trim($_REQUEST["nickname"])))."\"";
}

if (isset($_REQUEST["email"]) && strlen($_REQUEST["email"])>0)
{
	$cols[] = "tmpemail";
	$vals[] = "\"".@mysql_escape_string(strip_tags(trim($_REQUEST["email"])))."\"";
}


$cols[] = "questPublic";
$vals[] = "0";
$cols[] = "questCode";
$sendmecode = $vals[] =  rand(99999,999999);
$cols[] = "questAddedWhen";
$vals[] = "NOW()";
$cols[] = "questAddedFromIP";
$vals[] = "\"".$_SERVER['REMOTE_ADDR']."\"";
$cols[] = "questAddedFromOriginHash";
$vals[] = "\"".get_user_origin_hash()."\"";
$cols[] = "assisted";
$vals[] = "1";

$ins_query = "Insert into tblquestion ";
$ins_query .= " (";
$ins_query .= implode(",",$cols);
$ins_query .= ") ";
$ins_query .= " VALUES (";
$ins_query .= implode(",",$vals);
$ins_query .= ")";

$ins_res = mysql_query($ins_query);
//echo mysql_error();
if ($ins_res && @mysql_affected_rows($link)>0)
{
	$errcode = 0;
}
else
{
	$errcode = -1;
}
$errcode = 0;
//echo $errcode;
//	header("Location: ../index.php?errcode=".$errcode);


?>
