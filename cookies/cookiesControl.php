<?php
function unset_cookie($name)
{
	$host = $_SERVER['HTTP_HOST'];
	$domain = explode(':', $host)[0];

	$uri = $_SERVER['REQUEST_URI'];
	$uri = rtrim(explode('?', $uri)[0], '/');

	if ($uri && !filter_var('file://' . $uri, FILTER_VALIDATE_URL)) {
		throw new Exception('invalid uri: ' . $uri);
	}

	$parts = explode('/', $uri);

	$cookiePath = '';
	foreach ($parts as $part) {
		$cookiePath = '/'.ltrim($cookiePath.'/'.$part, '//');

		setcookie($name, '', 1, $cookiePath);

		$_domain = $domain;
		do {
			setcookie($name, '', 1, $cookiePath, $_domain);    
		} 
		while (strpos($_domain, '.') !== false && $_domain = substr($_domain, 1 + strpos($_domain, '.')));
	}
}
$checkCookie = $_COOKIE['cc_cookie'];
if (strlen($checkCookie)>0)
{
	$exploded_cookie30 = explode(":",$checkCookie);
	$exploded_cookie40 = explode(",",$exploded_cookie30[1]);
    $exploded_cookie50 = str_replace(',"revision"','',$exploded_cookie30[1]);
	$exploded_cookie3 = explode(":",$checkCookie);
	$exploded_cookie4 = explode(",",$exploded_cookie3[3]);
	$exploded_cookie5 = str_replace('"','',$exploded_cookie4[0]);
	$exploded_cookie5 = str_replace(' \n','',$exploded_cookie5);
 
    
$query = "Update cookiesLog Set cookieLevel = '".$exploded_cookie50."' Where cookieIdentity = '".$exploded_cookie5."' And cookieLevel = '' ";
	$res =  mysql_query($query);	

	if(strpos($exploded_cookie50,'analytics') > 5) { $cookieAnalytics = 1; } else { 
		$cookieAnalytics = 0;
		unset_cookie('_ga');
		unset_cookie('_gid');
		unset_cookie('_gat_gtag_UA-311147-33');

	}
 
	if(strpos($exploded_cookie50,'ads') > 5) { $cookieMarketing = 1; } else { 
		$cookieMarketing = 0;
		unset_cookie('vuid');
		unset_cookie('external_referer');
		unset_cookie('guest_id_marketing');
		unset_cookie('eu_cn');
		unset_cookie('ct0');
		unset_cookie('auth_token');
		unset_cookie('ads_prefs');
		unset_cookie('dnt');
		unset_cookie('twid');
		unset_cookie('guest_id');
		unset_cookie('personalization_id');
		unset_cookie('_fbp');

	}
	
	 
}
?>