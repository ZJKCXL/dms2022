var web = 'https://www.darcovskasms.cz/' ;

window.addEventListener('load', function () {
    var expirationDays = 182;
    var expirationNecessary = undefined; //14;
    var cookieconsent = initCookieConsent();

    var logConsent = function logConsent(user_preferences) {
        var date = new Date();
        date.setTime(date.getTime() + (1000 * (expirationDays * 24 * 60 * 60)));

        var oldID = undefined;
        var data = cookieconsent.get('data');
        if (!!data)
            oldID = data.id;
          /*clearCookie('_gid','.topof.cz','/');
          clearCookie('_ga','.topof.cz','/');
          clearCookie('_gat_gtag_UA_44694781_5','.topof.cz','/');*/
        var data = {
            AcceptedCategories: user_preferences.accepted_categories,
            RejectedCategories: user_preferences.rejected_categories,
            Domain: "DMS",
            ClientCookieExpiration: date.toISOString(),
            ClientTimestamp: (new Date()).toISOString(),
            OldID: oldID
        };
        $.ajax(web+"cookies/cookiesLog.php", {
            data: JSON.stringify(data),           
            contentType: 'application/json',
            type: 'POST'
        })
            .fail(function () { })
            .done(function (data) {
                console.log(data);
                cookieconsent.set('data', {value: data});
            });
    }
    window.cookieconsent = cookieconsent;
    var request = new XMLHttpRequest();
    request.open('GET', web+'cookies/cookiesDate.php', false);
    request.send();
    var isOK = request.responseText;
    cookieconsent.run({

        page_scripts: true,
        auto_language: 'browser',
        autorun: true,
        delay: 0,
        autoclear_cookies: true,
        force_consent: false,
        cookie_expiration: expirationDays,
        cookie_necessary_only_expiration: expirationNecessary,

        theme_css: web+'cookies/cookies.css',

        gui_options: {
            consent_modal: {
                layout: 'box',               // box/cloud/bar 
                position: 'middle center',     // bottom/top + left/right/center
                transition: 'slide'             // zoom/slide
            },
            settings_modal: {
                layout: 'box',                 // box/bar
                transition: 'slide',            // zoom/slide
            }
        },

        onFirstAction: function (user_preferences, cookie) {
            logConsent(user_preferences);
        },

        onChange: function () {
            logConsent(cookieconsent.getUserPreferences());
        },

        languages: {
            'en': {
                consent_modal: {
                    title: "For the website to work properly - information about cookies",
                    description: 'We take a responsible approach not only to business, but also to the protection of your privacy. Therefore, we want to ask you for your consent to the processing of cookies on this website. However, we do not exaggerate their use and cookies are used only for the proper functioning of the website and to improve the user experience through statistics. By clicking the OK button, you agree to the storage of statistical cookies. You can adjust your preferences by clicking on Cookie settings, where you will also find information about individual cookie files.',
                    primary_btn: {
                        text: 'OK',
                        role: 'accept_all'  //'accept_selected' or 'accept_all'
                    },
                    secondary_btn: {
                        text: 'Cookies Prefences',
                        role: 'settings'   //'settings' or 'accept_necessary'
                    }
                },
                settings_modal: {
                    title: 'Cookies settings',
                    save_settings_btn: "Save preferences",
                    accept_all_btn: "Accept All",
                    close_btn_label: "Close",
                    cookie_table_headers: [
                        { col1: "Cookie" },
                        { col2: "Description" },

                    ],
                    blocks: [
                        {
                            title: "What are cookies good for?",
                            description: 'For the best experience of browsing our website, we use cookies, ie small files that are temporarily stored in the Internet browser of your device (computer, smartphone or tablet). We need your consent to process some cookies, thank you for giving us. Below you will find information about the cookies that this website uses.',
                        }, {
                            title: "Necessary cookies",
                            description: 'These cookies are necessary for the proper functioning of the website. They provide basic web functions, security and proper display on a computer or mobile phone. It is also used to store information about your preferences regarding cookies. Necessary cookies cannot be turned off, when disabling them in the browser, the site may not work properly.',
                            toggle: {
                                value: 'necessary',
                                enabled: true,
                                readonly: true
                            },
                            cookie_table: [
                                {
                                    col1: 'cc_cookie',
                                    col2: 'Your Cookies Preferences from this dialog.'
                                }
                            ]
                        }, {
                            title: "Statistical cookies",
                            description: 'Thanks to statistical cookies, we have an overview of the use of the website, so we can constantly improve it for you. For example, we know what sites are most visited, which buttons users click on, and how much time they spend on the web.',
                            toggle: {
                                value: 'analytics',
                                enabled: true,
                                readonly: false
                            },
                            cookie_table: [
                                {
                                    col1: '_ga/_ga*, _gid',
                                    col2: 'Google Analytics - store and count pageviews.',
                                },
                                {
                                    col1: '_gcl_au',
                                    col2: 'Google Tag Manager - Conversion linker funcionality',
                                }
                            ]
                        }


                        // , {
                        //   title: "Marketing cookies",
                        //   description: 'They are used to monitor a user\'s website preferences for the purpose of targeting advertising, ie displaying marketing and advertising messages (even on third-party sites) that may be of interest to the website visitor, in accordance with these preferences. Marketing cookies use the tools of external companies. These marketing cookies will be used only with your consent.',
                        //   toggle: {
                        //     value: 'ads',
                        //     enabled: false,
                        //     readonly: false
                        //   },
                        //   cookie_table: [
                        //     {
                        //       col1: '_fbp',
                        //       col2: 'Facebook Pixel -  display advertisements when either on Facebook or on a digital platform powered by Facebook advertising, after visiting the website.',
                        //     },
                        //   ]
                        // }
                    ]
                }
            },
            'cs': {
                consent_modal: {
                    title: "Aby web správně fungoval -  cookies",
                    description: 'Chceme vás požádat o souhlas se zpracováním souborů cookies na tomto webu. Používáme dva typy cookies. První slouží pouze pro správné fungování webu a musíte je přijmout. Kliknutím na tlačítko \"Vše OK\" souhlasíte kromě uložení těchto nutných cookies také s uložením statistických cookies. Své preference můžete upravit kliknutím na \"Nastavení cookies\", kde naleznete také informace o jednotlivých cookie souborech.' ,
                    
                    primary_btn: {
                        text: 'Vše OK',
                        role: 'accept_all'  //'accept_selected' or 'accept_all'
                    },
                    secondary_btn: {
                        text: 'Nastavení cookies',
                        role: 'settings'   //'settings' or 'accept_necessary'
                    }
                },
                settings_modal: {
                    title: 'Nastavení cookies',
                    save_settings_btn: "Uložit nastavení",
                    accept_all_btn: "Souhlasím se vším",
                    close_btn_label: "Zavřít",
                    cookie_table_headers: [
                        { col1: "Cookie" },
                        { col2: "Popis" },
                    ],
                    blocks: [
                        {
                            title: "K čemu jsou cookies dobré",
                            description: 'Pro co nejlepší zážitek z procházení našeho webu používáme soubory cookie, tj. malé soubory, které se dočasně ukládají v internetovém prohlížeči vašeho zařízení (počítači, chytrém telefonu či tabletu). Pro zpracování některých souborů cookies od vás potřebujeme souhlas, děkujeme že nám ho dáte. Níže naleznete informace o cookies, které tento web používá.  '+isOK,
                        }, 
                        {
                            title: "Nezbytné",
                            description: 'Tyto cookies jsou nezbytné pro správné fungování webu. Zajišťují základní funkce webu, bezpečnost a řádné zobrazování na počítači nebo na mobilu. Slouží také k ukládání informací o tom, jaké jsou vaše preference právě ohledně cookies. Nezbytné cookies není možné vypnout, při jejich zakázání v prohlížeči nemusejí stránky následně fungovat správně.',
                            toggle: {
                                value: 'necessary',
                                enabled: true,
                                readonly: true
                            },
                            cookie_table: [
                                {
                                    col1: 'cc_cookie',
                                    col2: 'Vaše nastavení cookies z tohoto dialogu'
                                },
                                {
                                    col1: 'reguserdms',
                                    col2: 'Ukládá se jen v případě  přihlášení organizace do systému DMS.',
                                }
                            ]
                        }, {
                            title: "Statistické cookies",
                            description: 'Díky statistickým cookies máme přehled o využití webu, a tak ho pro vás můžeme neustále vylepšovat. Například víme, jaké stránky jsou nejčastěji navštěvované, na která tlačítka uživatelé klikají a kolik času na webu stráví.',
                            toggle: {
                                value: 'analytics',
                                enabled: false,
                                readonly: false
                            },
                            cookie_table: [
                                {
                                    col1: '_ga/_ga*, _gid',
                                    col2: 'Google Analytics - ukládá a počítá počet zobrazení stránek a chování webu',
                                }/*,
                                 {
                                    col1: '_gcl_au',
                                    col2: 'Google Tag Manager - propojení konverzí',
                                }*/
                            ]
                        } 
                        
                    ]
                }
            }

        }
    });
    var field = 'consent';
    var url = window.location.href;
    var forceShowConsent = false;
    if (url.indexOf('?' + field + '=') != -1)
        forceShowConsent = true;
    else if (url.indexOf('&' + field + '=') != -1)
        forceShowConsent = true;
    if (forceShowConsent) {
        cookieconsent.showSettings();
    }
});