
<?php
//https://graph.facebook.com/oauth/access_token?type=client_cred&client_id=178997825456089&client_secret=a0fb8e45f4e63c1f28f98b9f3a465792
// curl -F 'access_token=178997825456089|tIv6ZnfU8t6gRPmP-Xzk243U-Mo' \
//     -F 'message=Hello, Arjun. I like this new API.' \
//$ch = curl_init("https://graph.facebook.com/178997825456089/feed");
/**************************************************************************************
$touid: Facebook Unique ID of an User. Set the variable to "me" if posting on logged in user's wall
$msg: The Message to be posted above the actual link post
$name: Title of the URL to be posted
$link: Direct (Full) URL of the Link to be posted
$description: A short description text about the post
$pic: Absolute URL of the accompanying image to be posted
$action_name & $action_link: Title & URL of the Action link for the post, see this image:
http://i.imgur.com/JCdGh.png

$facebook: The facebook object which can be obtained from the PHP-SDK
***************************************************************************************/

$msg = "ahoy";
$title = "test";
$uri = "https://graph.facebook.com/178997825456089/feed";
$desc = "desc";

$attachment =  array(
'message' => $msg,
'name' => $title,
'link' => $uri,
'description' => $desc,
'picture'=>$pic,
'actions' => json_encode(array('name' => $action_name,'link' => $action_link))
);

//Posting to the wall of an user whose Facebook User ID is known
try {
$result = $facebook->api('/178997825456089/feed', 'post', $attachment);
}
catch (FacebookApiException $e) {

//notify error

}

//Posting to the wall of the currently logged-in Facebook user
try {

$result = $facebook->api('/me/feed', 'post', $attachment);

} catch (FacebookApiException $e) {

//notify error

}
?>
