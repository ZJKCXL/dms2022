<?
$asc2uni = Array();

for($i=128;$i<256;$i++)
{
	$asc2uni[chr($i)] = "&#x".dechex($i).";";
}

function XMLStrFormat($str)
{
	global $asc2uni;
	$str = str_replace("&", "&amp;", $str);
	$str = str_replace("<", "&lt;", $str);
  $str = str_replace(">", "&gt;", $str);
  $str = str_replace('class="MsoNormal"', "", $str); 
	$str = str_replace("'", "&apos;", $str);
	$str = str_replace("\"", "&quot;", $str);
	$str = str_replace("\r", "", $str);
	//$str = strtr($str,$asc2uni);
	return $str;
}
function czechize($in_str)
{
    //Predpoklada ze kodovani na serveru je ISO-8859-2
    //jinak $in_str = mb_convert_encoding($in_str, "ISO-8859-2")

    return strtr($in_str, "\xe1\xe4\xe8\xef\xe9\xec\xed\xb5\xe5\xf2\xf3\xf6\xf5\xf4\xf8\xe0\xb9\xbb\xfa\xf9\xfc\xfb\xfd\xbe\xc1\xc4\xc8\xcf\xc9\xcc\xcd\xa5\xc5\xd2\xd3\xd6\xd5\xd4\xd8\xc0\xa9\xab\xda\xd9\xdc\xdb\xdd\xae",
    "aacdeeillnoooorrstuuuuyzAACDEEILLNOOOORRSTUUUUYZ");
}

function normalize($in_file_name)
{
    $in_file_name = str_replace(",","-",$in_file_name);
    $in_file_name = str_replace(".","-",$in_file_name);
    $in_file_name = str_replace(" ","-",$in_file_name);
    $in_file_name = str_replace("*","-",$in_file_name);
    $in_file_name = str_replace("?","",$in_file_name);
    $in_file_name = str_replace("/","_",$in_file_name);
    $in_file_name = str_replace("\\","_",$in_file_name);
    $in_file_name = czechize($in_file_name);
    return $in_file_name;
}


function makeLinkFrom($string){
    $string = strtolower($string);
    $string = normalize($string);
    $string = czechize($string);
    $string = str_replace("--","-",$string);    
    return $string;
}

header("Cache-Control: public, must-revalidate");
header('Content-Type: text/xml');
header('Content-Disposition: inline; filename=get_hotel.xml');
echo "<?xml version=\"1.0\"  encoding=\"UTF-8\" standalone=\"yes\" ?>\n";
echo "<!DOCTYPE dmsexport [";
echo "<!ELEMENT dmsexport (dms+)>";
echo "<!ELEMENT dms (dmstext, weburl, project*, organisation*, dmscategory*)>";
echo "<!ATTLIST dms id CDATA #IMPLIED>";
echo "<!ELEMENT dmstext (#PCDATA)>";
echo "<!ELEMENT weburl (#PCDATA)>";
echo "<!ELEMENT project (project_name*, project_category*, project_text*, project_logo*)>";
echo "<!ELEMENT project_name (#PCDATA)>";
echo "<!ELEMENT project_text (#PCDATA)>";
echo "<!ATTLIST project_text";
echo " encoded CDATA #IMPLIED>";
echo "<!ELEMENT project_logo (#PCDATA)>";
echo "<!ELEMENT dmscount (dms*,till*)>";
echo "<!ELEMENT org_name (#PCDATA)>";
echo "<!ELEMENT org_text (#PCDATA)>";
echo "<!ELEMENT organisation (org_name*,org_text*)>";
echo "<!ELEMENT org_name (#PCDATA)>";
echo "<!ELEMENT org_text (#PCDATA)>";
echo "<!ATTLIST org_text";
echo " encoded CDATA #IMPLIED>";
echo "<!ELEMENT dmscategory (overcategory*)>";
echo "<!ELEMENT overcategory (category*)>";
echo "<!ATTLIST overcategory id CDATA #IMPLIED>";
echo "<!ATTLIST overcategory name CDATA #IMPLIED>";
echo "<!ELEMENT category (#PCDATA)>";
echo "]>";

$link = @$link =mysql_connect("localhost","dms","p8eYm364sTUHFLLU");
if (!$link)
{
	exit;
}
@mysql_query("SET character_set_results=latin2",$link);
@mysql_query("SET character_set_connection=latin2",$link);
@mysql_query("SET character_set_client=latin2",$link);
$db = @mysql_select_db("dms2012");
if (!$db)
{
	exit;
}
//$ip = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
//$log_query = "Insert into tblXMLLog (LWho,LWhoIP) values (INET_ATON('".$ip."'),'".$ip."')";
//$log_res = @mysql_query($log_query);

$query = "SELECT *, project.description as prtext,  organization.description AS orgtext, collection.validFrom AS NEW, project.name AS prjname, project.id AS prid, organization.name AS orgname, GROUP_CONCAT( keyword.name  ORDER BY keyword.name SEPARATOR '</dmstext><dmstext>' ) AS keywords FROM collection INNER JOIN project ON project.id = collection.project_id INNER JOIN organization ON organization.id = project.organization_id INNER JOIN collectionKeyword ON collection.id = collectionKeyword.collection_id INNER JOIN keyword ON keyword.id = collectionKeyword.keyword_id  LEFT JOIN loga ON organization.id = loga.organizace WHERE CURDATE( ) >= collection.validFrom AND CURDATE( ) < collection.validTo GROUP BY collection.project_id ORDER BY prjname ";
$res = @mysql_query($query);
if ($res && @mysql_num_rows($res)>0)
{
	echo "<dmsexport>";
	while ($resarr = @mysql_fetch_array($res))
	{    
		echo "<dms id=\"".$resarr["prid"]."\">";	
		//echo "<dmstext>".XMLStrFormat(mb_convert_encoding($resarr["keywords"],"utf-8","iso-8859-2"))."</dmstext>";
		echo "<dmstext>";
		if(strlen($resarr['keywords'])<250) { echo $resarr['keywords']; }else{
    $resarr['keywords'] = substr($resarr['keywords'],0,249);
    $needle = strrpos ($resarr['keywords'],"</dmstext>");
    echo substr($resarr['keywords'],0,$needle);
     
    }
    echo "</dmstext>";
  	echo "<weburl>".XMLStrFormat("http://www.darcovskasms.cz/projekt-".mb_convert_encoding($resarr["prid"],"utf-8","iso-8859-2")."/". makeLinkFrom($resarr["prjname"]).".html")."</weburl>";		
    echo "<project>"	;
		echo "<project_name>".XMLStrFormat(mb_convert_encoding($resarr["prjname"],"utf-8","iso-8859-2"))."</project_name>";
		echo "<project_text encoded=\"cdata\"><![CDATA[".XMLStrFormat(mb_convert_encoding($resarr["prtext"],"utf-8","iso-8859-2"))."]]></project_text>";	
		echo "<project_logo>".XMLStrFormat("http://www.darcovskasms.cz/DNS_loga/".mb_convert_encoding($resarr["logo"],"utf-8","iso-8859-2"))."</project_logo>";		
    echo "</project>"	;
    echo "<dmscount>"	;    

    
        //prohibited last two month
        $akturok = date("Y");           
        $aktumonth = date("n");
        $aktumonth1 = date("n")-1;
        if($aktumonth<10){ $aktumonth = "0".strval($aktumonth); }
        if($aktumonth1<10){ $aktumonth1 = "0".strval($aktumonth1); }
        $prohib1 = $akturok."-".$aktumonth;
        $prohib2 = $akturok."-".$aktumonth1;
        $rok = $akturok;
        // query pro zjisteni ze kdy jsou na serveru skutecne nejnovejsi data - pro pripad, ze na serveru nejsou data z predminuleho mesice
        $mothquery2 = "Select period AS kolipa From monthlydms Where " ;
        $mothquery2 .= " period NOT LIKE '".$prohib1."' And period NOT LIKE '".$prohib2."'";
        $mothquery2 .= " Order by period DESC Limit 0,1";   
        $mothres2 =  mysql_query($mothquery2);
        if ($mothres2 && mysql_num_rows($mothres2)>0)
        {
        while ($mothrow2 = mysql_fetch_array($mothres2)){ 
        $aktuperiod = substr($mothrow2['kolipa'],5);
        }
        }
        $rok = $akturok;
        if($aktuperiod == '11') { $mesic = "listopadu"; $rok = $akturok-1; }
        if($aktuperiod == '12') { $mesic = "prosince"; $rok = $akturok-1;  }
        if($aktuperiod == '01') { $mesic = "ledna"; }
        if($aktuperiod == '02') { $mesic = "�nora"; }
        if($aktuperiod == '03') { $mesic = "b�ezna"; }
        if($aktuperiod == '04') { $mesic = "dubna"; }
        if($aktuperiod == '05') { $mesic = "kv�tna"; }
        if($aktuperiod == '06') { $mesic = "�ervna"; }
        if($aktuperiod == '07') { $mesic = "�ervence"; }
        if($aktuperiod == '08') { $mesic = "srpna"; }
        if($aktuperiod == '09') { $mesic = "z���"; }
        if($aktuperiod == '10') { $mesic = "��jna"; }
        $totaldms = 0;

 $preprojectquery = "SELECT *, project.description AS prdescription, collection.validFrom AS CFROM, collection.validTo AS CTO, project.id AS prid, organization.name AS orgname, project.name AS prname, keyword.id AS keywordid, keyword.name AS colkeyword
FROM collection, collectionKeyword, project, organization, keywordorganization, keyword
WHERE collection.project_id = project.id
AND collection.id = collectionKeyword.collection_id
AND collectionKeyword.keyword_id = keyword.id
AND organization.id = keywordorganization.organization_id
AND keyword.id = keywordorganization.keyword_id And project.id = ".$resarr['prid']." GROUP BY keyword.id ";
        $preprojectres = mysql_query($preprojectquery);

        if ($preprojectres && mysql_num_rows($preprojectres)>0)
        {
        while ($preprojectrow = @mysql_fetch_array($preprojectres)){
       
        // query pro dms jednotlivych hesel
        $mothquery = "Select sum(increment) AS kolipa From monthlydms Where keyword_id = ".$preprojectrow['keywordid'];   
        $CFROM = substr($preprojectrow['CFROM'],0,6).(substr($preprojectrow['CFROM'],6,1)-1);
        $CTO = substr($preprojectrow['CTO'],0,6).(substr($preprojectrow['CTO'],6,1)+1);        
         $mothquery .= " And period > '".$CFROM."' And period < '".$CTO."'";           
         
        $monthres =  mysql_query($mothquery);            
        if ($monthres && mysql_num_rows($monthres)>0)
        {
        while ($monthrow = mysql_fetch_array($monthres)){ 
        if($monthrow['kolipa']) {
         $totaldms = $totaldms + $monthrow['kolipa'];     
        }
        else {
        $totaldms = $totaldms + $monthrow['kolipa'];     
        }        
        }
        }
        }
        }   
 
		echo "<dms>".XMLStrFormat(mb_convert_encoding($totaldms,"utf-8","iso-8859-2"))."</dms>";
		echo "<till>".XMLStrFormat(mb_convert_encoding("do konce ". $mesic." ".$rok,"utf-8","iso-8859-2"))."</till>";		
    echo "</dmscount>"	;    
    echo "<organisation>"	;   
		echo "<org_name>".XMLStrFormat(mb_convert_encoding($resarr["orgname"],"utf-8","iso-8859-2"))."</org_name>";    		
		if($resarr["orgtext"]) { echo "<org_text encoded=\"cdata\"><![CDATA[".XMLStrFormat(mb_convert_encoding($resarr["orgtext"],"utf-8","iso-8859-2"))."]]></org_text>" ; }  		
    echo "</organisation>"	; 
	$catquery = "SELECT GROUP_CONCAT( category.name ORDER BY category.name SEPARATOR 'helpbreak' ) AS CATNAME, overcategory.name AS OVERCATNAME, overcategory.id AS OVERID, category.id AS CATID
FROM projectCategory, category, overcategory
WHERE project_id =".$resarr["prid"]."
AND category_id = category.id
AND overcategory_id = overcategory.id
GROUP BY overcategory.id";
	$catres =  @mysql_query($catquery);
  if ($catres && @mysql_num_rows($catres)>0)
     {
     echo "<dmscategory>"	;
    	while ($catresarr = @mysql_fetch_array($catres))
	    {
	    	echo "<overcategory id=\"".$catresarr["OVERID"]."\" name=\"".XMLStrFormat(mb_convert_encoding($catresarr["OVERCATNAME"],"utf-8","iso-8859-2"))."\">";
        $categories = explode("helpbreak",$catresarr["CATNAME"]);
        for($i = 0; $i < count($categories); $i++){
        echo "<category>".XMLStrFormat(mb_convert_encoding($categories[$i],"utf-8","utf-8"))."</category>";
        }
        echo "</overcategory>";  
	    }
	    echo "</dmscategory>"	;
   }     
		echo "</dms>";
	}
	echo "</dmsexport>";
}
?>
