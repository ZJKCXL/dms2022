var confirm_wrapper = {};
confirm_wrapper.message = JS_CONFIRM_WRAPPER_MSG;
confirm_wrapper.init = function (element)
{
	elements = document.getElementsByTagName('*');
	for (var i = 0; i < elements.length; i++)
	{
		if (class_handler.has(elements[i], 'confirm'))
		{
			if (elements[i].tagName.toLowerCase() == 'form')
			{
				event_handler.add(elements[i], 'submit', confirm_wrapper.ask);
			}
			else
			{
				event_handler.add(elements[i], 'click', confirm_wrapper.ask);
			}
		}
	}
};
confirm_wrapper.ask = function (event)
{
	if (!event.target)
	{
		event = event_handler.fix(event);
	}
	var element = event.target;

	if (confirm(confirm_wrapper.message))
	{
		switch (event.type)
		{
			case 'submit':
				element.submit();
				break;
			case 'click':
				break;
		}
	}
	else
	{
		event.preventDefault();
	}
};

event_handler.add(window, 'load', confirm_wrapper.init);

var show_hide_wrapper = {};
show_hide_wrapper.init = function (element)
{
	elements = document.getElementsByTagName('*');
	for (var i = 0; i < elements.length; i++)
	{
		if (class_handler.like(elements[i], 'show_hide_'))
		{
			event_handler.add(elements[i], 'click', show_hide_wrapper.click);
		}
	}
};
show_hide_wrapper.click = function (event)
{
	event = event_handler.fix(event);
	var element = event.target;

	var return_class = class_handler.like(element, 'show_hide_');
	while (return_class === false)
	{
		element = element.parentNode;

		var return_class = class_handler.like(element, 'show_hide_');
	}

	var look_for = return_class.substring(10, return_class.length);

	var container = document.getElementById('show_hide_container_'+look_for);
	if (container)
	{
		if (class_handler.has(container, 'hidden'))
		{
			class_handler.remove(container, 'hidden');
		}
		else
		{
			class_handler.add(container, 'hidden');
		}
	}
};

event_handler.add(window, 'load', show_hide_wrapper.init);