var dom_handler = {};
dom_handler.getNextSibling = function (element)
{
	if (element.nextSibling)
	{
		var sibling = element.nextSibling;
		while (sibling.nodeType != 1)
		{
			if (sibling.nextSibling)
			{
				sibling = sibling.nextSibling;
			}
			else
			{
				return false;
			}
		}
		return sibling;
	}

	return false;
};

dom_handler.insertAfter = function (insertNode, referenceNode)
{
	if (dom_handler.getNextSibling(referenceNode))
	{
		referenceNode.parentNode.insertBefore(insertNode, dom_handler.getNextSibling(referenceNode));
	}
	else
	{
		referenceNode.parentNode.appendChild(insertNode);
	}
};

dom_handler.getProperties = function (object)
{
	var keys = [];
	for (var key in object)
	{
		if (key != 'getProperties')
		{
			keys.push(key);
		}
	}

	return keys;
};

if (!Array.prototype.indexOf)
{
	Array.prototype.indexOf = function (searchElement)// /*, fromIndex */)
	{
		if (this === void 0 || this === null) throw new TypeError();

		var t = Object(this);
		var len = t.length >>> 0;

		if (len === 0)
		return -1;

		var n = 0;
		if (arguments.length > 0)
		{
			n = Number(arguments[1]);
			if (n !== n)
			{
				n = 0;
			}
			else if (n !== 0)// && n !== (1 / 0) && n !== -(1 / 0))
			{
				n = (n > 0 || -1) * Math.floor(Math.abs(n));
			}
		}

		if (n >= len)
		return -1;

		var k = n >= 0 ? n : Math.max(len - Math.abs(n), 0);

		for (; k < len; k++)
		{
			if (k in t && t[k] === searchElement)
			return k;
		}

		return -1;
	};
}