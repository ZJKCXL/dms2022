var window_handler = {};
window_handler.open = function (file, window_id, width, height, modal, dependable, raised, scrollbars, resizable, toolbar, location, statusbar, menubar)
{
	if (width > 0 && height == 0)
	{
		height = window.screen.availHeight - 50;
	}
	if (modal != 1)
	{
		modal = 0;
	}
	if (dependable != 1)
	{
		dependable = 0;
	}
	if (raised != 1)
	{
		raised = 0;
	}
	if (scrollbars != 1 && scrollbars != 2)
	{
		scrollbars = 0;
	}
	if (resizable != 1)
	{
		resizable = 0;
	}
	if (toolbar != 1)
	{
		toolbar = 0;
	}
	if (location != 1)
	{
		location = 0;
	}
	if (statusbar != 1)
	{
		statusbar = 0;
	}
	if (menubar != 1)
	{
		menubar = 0;
	}
	try
	{
		var wnd = window.open(file, window_id, 'width='+width+'px,height='+height+'px,scrollbars='+scrollbars+',modal='+modal+',dependable='+dependable+',alwaysRaised='+raised+',resizable='+resizable+',toolbar='+toolbar+',location='+location+',statusbar='+statusbar+',menubar='+menubar);
	}
	catch(ex) {}
	if (wnd)
	{
		var x = 5;
		var y = 0;
		var parent_window = wnd.opener;
		if (parent_window)
		{
			if (document.all)
			{
				x = (parent_window.screenLeft + 300);
				y = (parent_window.screenTop);
			}
			else
			{
				x = (parent_window.screenX + 300);
				y = (parent_window.screenY);
			}
			var is_first = parent_window.opener;
			if (typeof(is_first) !== 'object' || is_first == null)
			{
				x = 15;
			}
		}
		wnd.moveTo(x, y);
		return wnd;
	}
	else
	{
		return false;
	}
};

window_handler.close_self = function ()
{
	window.close();
};

window_handler.close = function (element)
{
	element.close();
};

window_handler.reload = function (element)
{
	element.location.reload();
};

window_handler.forward_reload = function (parent_window, child)
{
	window_handler.reload(parent_window);
	window_handler.close(child);
};
window_handler.resize_to_content = function()
{
	var isMSIE = (navigator.appName == "Microsoft Internet Explorer");
	var isOpera = (navigator.userAgent.indexOf("Opera") != -1);

	if (isOpera)
	return;

	if (isMSIE)
	{
		//var elm = window.document.body;
		var wrap = window.document.getElementById('wrapper');
		/*var width = elm.offsetWidth;
		var height = elm.offsetHeight;
		var dx = (elm.scrollWidth - width + 50);
		var dy = (elm.scrollHeight - height);*/
		dx = (wrap.offsetWidth + 40);
		dy = (wrap.offsetHeight + 80);
		try
		{
			if (dy > (window.opener.screen.availHeight - 30))
			{
				dy = (window.opener.screen.availHeight - 30);
			}
		} catch (ex) {}
		if (dx < 320)
		{
			dx = 320;
		}

		try { window.resizeTo(dx, dy); } catch (ex) {}
	}
	else
	{
		window.sizeToContent();
		if (window.innerWidth < 320)
		{
			window.resizeBy((320 - window.innerWidth), 0);
		}
		if (window.innerHeight > (window.opener.screen.availHeight - 30))
		{
			window.resizeBy(0, -30);
		}
		window.scrollTo(0, 0);
	}
};