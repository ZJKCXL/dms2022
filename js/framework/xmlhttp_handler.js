function xmlhttp_handler ()
{
	this.action = null;
	this.XML = null;
	this.commInterface = null;

	this.resetXMLHR();

	return true;
}

/* Check if the XMLHttpRequest object is available */
xmlhttp_handler.prototype.isAvailable = function ()
{
	if (this.commInterface == null)
	{
		return false;
	}

	return true;
};

/* Execute the action which has been associated with the completion of this object */
xmlhttp_handler.prototype.executeAction = function ()
{
	// If XMLHR object has finished retrieving the data
	if (this.commInterface.readyState == 4)
	{
		// If the data was retrieved successfully
		try
		{
			if (this.commInterface.status == 200)
			{
				var not_set = true
				while (not_set)
				{
					if (this.commInterface.requestXML != '')
					{
						this.responseText = this.commInterface.requestXML;
						not_set = false;
					}
				}
				if (this.action())
				{
					return true;
				}
			}
			// IE returns status = 0 on some occasions, so ignore
			else if (this.commInterface.status != 0)
			{
				alert("There was an error while retrieving the URL: " + this.commInterface.statusText);
			}
		}
		catch (error)
		{
		}
	}

	return true;
}
/* Return responseText */
xmlhttp_handler.prototype.getText = function ()
{
	return this.commInterface.responseText;
}
/* Return responseXML */
xmlhttp_handler.prototype.getXML = function ()
{
	return this.commInterface.responseXML;
}
/* Initialise XMLHR object and load URL */
xmlhttp_handler.prototype.loadURL = function (type, url, params, asynchronous)
{
	if (asynchronous)
	{
		asynchronous = true;
	}
	else
	{
		asynchronous = false;
	}
	if (type != 'GET' && type != 'POST')
	{
		return false;
	}
	else if (type == 'GET')
	{
		var first = true;
		for (var i = 0; i < params.length; i++)
		{
			if (first)
			{
				url += "?";
			}
			else
			{
				url += "&";
			}
			url += params[i][0]+'='+params[i][1];
			first = false;
		}
		var query = null;
	}
	if (type != '' && url != '' && query != '')
	{
		if (this.resetXMLHR())
		{
			this.commInterface.open(type, url, asynchronous);
			if (type == 'POST')
			{
				this.commInterface.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
				var query = "";
				for (var i = 0; i < params.length; i++)
				{
					if (i != 0)
					{
						query += "&";
					}
					var reg_exp = new RegExp('[\+]');
					try
					{
						if (params[i][1].search(reg_exp) != -1)
						{
							params[i][1] = params[i][1].replace(reg_exp, '%2B')
						}
					} catch (ex) {}
					query += params[i][0]+'='+params[i][1];

				}
				this.commInterface.send(query);
			}
			else
			{
				this.commInterface.send(query);
			}
			return true;
		}
	}
	else
	{
		return false;
	}
}
/* Turn off existing connections and create a new XMLHR object */
xmlhttp_handler.prototype.resetXMLHR = function()
{
	var self = this;

	if (this.commInterface != null && this.commInterface.readyState != 0 && this.commInterface.readyState != 4)
	{
		this.commInterface.abort();
	}

	try
	{
		this.commInterface = new XMLHttpRequest();
	}
	catch (error)
	{
		try
		{
			this.commInterface = new ActiveXObject("Microsoft.XMLHTTP");
		}
		catch (error)
		{
			return false;
		}
	}
	this.commInterface.onreadystatechange = function()
	{
		if (self.commInterface.readyState == 4)
		{
			if (self.executeAction())
			{
				return true;
			}
		}

	};

	return true;
}
/* Assign the function which will be executed once the XMLHR object finishes retrieving data */
xmlhttp_handler.prototype.setAction = function (actionFunction)
{
	this.action = actionFunction;

	return true;
}