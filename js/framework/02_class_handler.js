/************************************/
/* Element class handling functions */
/************************************/
var class_handler = {};
class_handler.get = function (element)
{
	if (element && element.tagName)
	{
		if (element.className)
		{
			if (element.className.replace)
			{
				var cl = element.className.replace(/\s+/g, ' ');
				return cl.split(' ');
			}
		}
	}
	return false;
};
class_handler.has = function (element, cl)
{
	var actual_class = class_handler.get(element);
	if (typeof(cl) == 'string')
	{
		for (var i = 0; i < actual_class.length; i++)
		{
			if (actual_class[i] == cl)
			{
				return true;
			}
		}
	}
	return false;
};
class_handler.add = function (element, cl)
{
	var actual_class = class_handler.get(element);
	if (typeof(cl) == 'string')
	{
		if (!class_handler.has(element, cl))
		{
			element.className += (actual_class.length > 0) ? ' ' + cl : cl;
		}
		return true;
	}
	return false;
};
class_handler.remove = function (element, cl)
{
	var actual_class = class_handler.get(element);
	if (typeof(cl) == 'string')
	{
		temp_class = '';
		for (var i = 0; i < actual_class.length; i++)
		{
			if (actual_class[i] != cl)
			{
				if (temp_class != '')
				{
					temp_class += ' ';
				}
				temp_class += actual_class[i];
			}
			element.className = temp_class;
		}
		return true;
	}
	return false;
};
class_handler.replace = function (element, old_class, new_class)
{
	var actual_class = class_handler.get(element);
	if ((typeof(old_class) == 'string') && (typeof(new_class) == 'string'))
	{
		temp_class = '';
		if (class_handler.has(element, new_class))
		{
			class_handler.remove(element, old_class);
		}
		else if (class_handler.has(element, old_class))
		{
			for (var i = 0; i < actual_class.length; i++)
			{
				if (temp_class != '')
				{
					temp_class += ' ';
				}
				temp_class += (actual_class[i] == old_class) ? new_class : actual_class[i];
			}
			element.className = temp_class;
		}
		else
		{
			class_handler.add(element, new_class);
		}
		return true;
	}
	return false;
};
class_handler.like = function (element, cl)
{
	var actual_class = class_handler.get(element);
	if (typeof(cl) == 'string')
	{
		for (var i = 0; i < actual_class.length; i++)
		{
			reg = new RegExp (cl);
			if (actual_class[i].search(reg) != -1)
			{
				return actual_class[i];
			}
		}
	}
	return false;
};
class_handler.getElementsByClass = function (cl)
{
	elements = [];

	var elm = document.getElementsByTagName('*');
	for (var i = 0; i < elm.length; i++)
	{
		if (class_handler.has(elm[i], cl))
		{
			elements[elements.length] = elm[i];
		}
	}

	return elements;
};