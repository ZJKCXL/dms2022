/****************************/
/* Event handling functions */
/****************************/
var event_handler = {};
event_handler.fix = function (event)
{
	if (typeof(event) == 'undefined') event = window.event;
	if (typeof(event.target) == 'undefined') event.target = event.srcElement;
	if (typeof(event.clientX) == 'undefined') event.clientX = event.offsetX;
	if (typeof(event.clientY) == 'undefined') event.clientY = event.offsetY;
	if (!event.preventDefault) event.preventDefault = function()
	{
		event.returnValue = false;
	}

	return event;
};
event_handler.add = function (object, event_type, function_name, use_capture)
{
	// Opera hack
	if (window.opera)
	{
		// Opera doesn't accept attaching events on object window, but accepts them on object document
		if (object == window)
		{
			if (!object.addEventListener && !object.attachEvent)
			{
				object = document;
			}
		}
	}
	if (object.addEventListener)
	{
		object.addEventListener(event_type, function_name, use_capture);
		return true;
	}
	else if (object.attachEvent)
	{
		var r = object.attachEvent('on'+event_type, function_name);
		return r;
	}
	else
	{
		object['on'+event_type] = function_name;
		return true;
	}
};
event_handler.remove = function (object, event_type, function_name, use_capture)
{
	// Opera hack
	if (window.opera)
	{
		// Opera doesn't accept detaching events on object window, but accepts them on object document
		if (object == window)
		{
			if (!object.addEventListener && !object.attachEvent)
			{
				object = document;
			}
		}
	}
	if (object.removeEventListener)
	{
		object.removeEventListener(event_type, function_name, use_capture);
		return true;
	}
	else if (object.detachEvent)
	{
		var r = object.detachEvent('on'+event_type, function_name);
		return r;
	}
	else
	{
		object['on'+event_type] = null;
		return true;
	}
};

event_handler.event = null;
event_handler.create = function (event_type, can_bubble, cancelable, view, detail, screenX, screenY, clientX, clientY
, ctrlKey, altKey, shiftKey, metaKey, button, related_target)
{
	switch (event_type)
	{
		case "click":
		/*evt.initMouseEvent("click", true, true, window,
    0, 0, 0, 0, 0, false, false, false, false, 0, null);*/
			var event = document.createEvent('MouseEvents');
			event.handler.event = event.initMouseEvent(event_type, can_bubble, cancelable, view,
					                     detail, screenX, screenY, clientX, clientY,
          					           ctrlKey, altKey, shiftKey, metaKey,
                    					 button, related_target);
			break;
	}
};
event_handler.dispatch = function (element)
{
	element.dispatchEvent(event.handler.event);
};