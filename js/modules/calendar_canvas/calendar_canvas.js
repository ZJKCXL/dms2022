var calendar_canvas = {};
calendar_canvas.start_x = 0;
calendar_canvas.start_y = 0;
calendar_canvas.current_object = null;
calendar_canvas.init = function ()
{
	$(document).bind("mousedown", calendar_canvas.mousedown);
	$(document).bind("mouseup", calendar_canvas.mouseup);
	
	$("#calendar_canvas").css("height", ($("body").innerHeight()-$("#calendar_canvas").offset().top)+"px");
	$("#calendar_controls").css("height", ($("body").innerHeight()-$("#calendar_controls").offset().top)+"px");
	$("#calendar_workspace").css("height", ($("body").innerHeight()-$("#calendar_workspace").offset().top)+"px");
};
calendar_canvas.mousedown = function (event)
{
	var $elm = $(event.target);
	while ($elm.parent().length > 0 && !$elm.hasClass("calendar_canvas_col"))
	{
		$elm = $elm.parent();
	}
	
	if ($elm.get(0).nodeName != "#document")
	{
		var $offset = $("#calendar_canvas").offset();
	
		calendar_canvas.start_x = event.pageX-$offset.left;
		calendar_canvas.start_y = event.pageY-$offset.top;
	
		var $obj = $("<div>").addClass("calendar_canvas_new_item");
	
		if (calendar_canvas.current_object == null)
		{
			calendar_canvas.current_object = $obj;
		}
	
		$("body").append($obj);
		$obj.offset({
			top: calendar_canvas.start_y,
			left: calendar_canvas.start_x
		});
	}
};
calendar_canvas.mousemove = function (event)
{
	if (calendar_canvas.start_x > 0)
	{
	}
};
calendar_canvas.mouseup = function (event)
{
	$(".calendar_canvas_new_item").remove();
};