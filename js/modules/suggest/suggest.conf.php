<?

header("Content-type: text/javascript; charset=iso-8859-2");

?>

/**
 * define class
 */

var suggest = {};

/**
 *	int hide_timeout	milliseconds after which hint box disappears
 */

suggest.hide_timeout = 5000;

/**
 *	int display_limit	limit for number of items to be displayed
 */

suggest.display_limit = 10;

/**
 *	obj value_element	element to store the chosen item value
 */

//suggest.value_element = null;

/**
 *	obj display_box_element	element to act as a container for suggested items
 *							- if null -> default container is attached to element which triggered the action
 */

suggest.display_box_element = null;

/**
 *	url that handles search and response
 */

suggest.handler_url = BASE_URL+'js/modules/suggest/suggest.run.php';