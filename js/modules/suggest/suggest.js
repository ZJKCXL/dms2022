suggest.timer = null;
suggest.type = null;
suggest.requester = null;
suggest.target_element = null;
suggest.init = function ()
{
	var inputs = document.getElementsByTagName('input');

	for (var i = 0; i < inputs.length; i++)
	{
		if (class_handler.like(inputs[i], 'suggest_'))
		{
			event_handler.add(inputs[i], 'keyup', suggest.filterKey, 0);
			event_handler.add(inputs[i], 'keydown', suggest.filterEnter, 0);
			inputs[i].setAttribute('autocomplete','off');
		}
	}
};
suggest.filterEnter = function (event)
{
	var event = event_handler.fix(event);

	if (event.keyCode == 13)//enter
	{
		event.cancelBubble = true;
		if (event.stopPropagation)
		{
			event.stopPropagation();
		}
		if (event.preventDefault)
		{
			event.preventDefault();
		}

		suggest.enter();

		return false;
	}
}
suggest.filterKey = function (event)
{
	var event = event_handler.fix(event);

	if (event.keyCode == 13)//enter
	{
		event.cancelBubble = true;
		if (event.stopPropagation)
		{
			event.stopPropagation();
		}
		if (event.preventDefault)
		{
			event.preventDefault();
		}

		suggest.enter();

		return false;
	}
	if (event.keyCode >= 37 && event.keyCode <= 40)//arrow keys
	{
		if (event.keyCode == 38)//up
		{
			suggest.moveUp();
		}
		if (event.keyCode == 40)//down
		{
			suggest.moveDown();
		}
		return false;
	}
	else if (event.keyCode >= 16 && event.keyCode <= 20)//wtf keys
	{
		return false;
	}
	else if (event.keyCode >= 33 && event.keyCode <= 37)//home, end..., arrow
	{
		return false;
	}
	else if (event.keyCode == 39 || event.keyCode == 45)//right arrow, insert
	{
		return false;
	}
	else if (event.keyCode == 9 || event.keyCode == 27)//tab, ESC
	{
		if (event.keyCode == 27)
		{
			if (event.preventDefault)
			{
				event.preventDefault();
			}
		}
		suggest.destroy();
		return false;
	}
	else
	{
		suggest.destroy();
		suggest.process(event);
	}
};
suggest.process = function (event)
{
	var element = event.target;

	if (element.value == '')
	{
		suggest.destroy();
		return false;
	}

	suggest.target_element = element;

	var temp_class = class_handler.like(element, 'suggest_');

	suggest.type = temp_class.substring(8, temp_class.length);

	param = [];
	var p = 0;

	param[p] = ['limit', suggest.display_limit];
	p++;

	param[p] = ['type', suggest.type];
	p++;

	param[p] = ['phrase', element.value];
	p++;

	suggest.requester = new xmlhttp_handler();
	suggest.requester.setAction(suggest.display);
	suggest.requester.loadURL('POST', suggest.handler_url, param, true);

	return false;
};
suggest.display = function ()
{
	var response = suggest.requester.getXML();

	var items = response.getElementsByTagName('item');

	if (items.length <= 0)
	{
		return false;
	}

	if (suggest.display_box_element != null)
	{
		suggest.display_box_element.parentNode.removeChild(suggest.display_box_element);
	}

	suggest.display_box_element = document.createElement('div');
	suggest.display_box_element.id = 'suggest_container';

	//dom_handler.insertAfter(suggest.display_box_element, suggest.target_element);

	if (document.body)
	{
		document.body.appendChild(suggest.display_box_element);
	}
	else if (document.documentElement)
	{
		document.documentElement.appendChild(suggest.display_box_element);
	}

	suggest.display_box_element.style.left = position.getOffsetX(suggest.target_element)+'px';
	suggest.display_box_element.style.top = (position.getOffsetY(suggest.target_element)+position.getOuterHeight(suggest.target_element, false))+'px';

	var list = document.createElement('ul');

	for (var i = 0; i < items.length; i++)
	{
		var temp_id = items[i].getElementsByTagName('id')[0].childNodes[0].nodeValue;
		var temp_display = items[i].getElementsByTagName('display')[0].childNodes[0].nodeValue;
		var clickable = items[i].getElementsByTagName('clickable')[0].childNodes[0].nodeValue;
		var temp_html = items[i].getElementsByTagName('html_display');
		suggest.value_element = document.getElementById(items[i].getElementsByTagName('value_element_id')[0].childNodes[0].nodeValue);

		var list_item = document.createElement('li');
		list_item.id = temp_id;

		event_handler.add(list_item, 'mouseover', suggest.itemOver);
		event_handler.add(list_item, 'mouseout', suggest.itemOut);
		if (clickable == 1)
		{
			event_handler.add(list_item, 'click', suggest.itemClick);
		}
		else
		{
			class_handler.add(list_item, 'noclick');
		}

		if (temp_html.length > 0)
		{
			temp_html = temp_html[0].firstChild.data;
			var tmp_div = document.createElement('div');
			tmp_div.innerHTML = temp_html;
			list_item.appendChild(tmp_div);
			list_item.title = temp_display;
		}
		else
		{
			var txt = document.createTextNode(temp_display);
			list_item.appendChild(txt);
		}

		list.appendChild(list_item);
	}

	suggest.display_box_element.appendChild(list);

	var div = document.createElement('div');
	class_handler.add(div, 'clear');
	suggest.display_box_element.appendChild(div);

	suggest.display_box_element.style.display = 'block';

	return false;
};
suggest.destroy = function ()
{
	if (suggest.display_box_element != null)
	{
		suggest.display_box_element.parentNode.removeChild(suggest.display_box_element);
	}

	suggest.timer = null;
	suggest.display_box_element = null;
	suggest.requester = null;
	suggest.target_element = null;
	suggest.value_element = null;

	return false;
};
suggest.moveUp = function ()
{
	var temp_ul = suggest.display_box_element.getElementsByTagName('ul');
	var items = temp_ul[0].getElementsByTagName('li');

	var selected = -1;

	for (var i = 0; i < items.length; i++)
	{
		if (class_handler.has(items[i], 'over'))
		{
			selected = i;
			class_handler.remove(items[i], 'over');
			break;
		}
	}

	selected--;

	if (selected < 0)
	{
		class_handler.add(items[items.length-1], 'over');
	}
	else
	{
		class_handler.add(items[selected], 'over');
	}

	return false;
};
suggest.moveDown = function ()
{
	var temp_ul = suggest.display_box_element.getElementsByTagName('ul');
	var items = temp_ul[0].getElementsByTagName('li');

	var selected = items.length;

	for (var i = 0; i < items.length; i++)
	{
		if (class_handler.has(items[i], 'over'))
		{
			selected = i;
			class_handler.remove(items[i], 'over');
			break;
		}
	}

	selected++;

	if (selected > (items.length-1))
	{
		class_handler.add(items[0], 'over');
	}
	else
	{
		class_handler.add(items[selected], 'over');
	}

	return false;
};
suggest.itemOver = function (event)
{
	var event = event_handler.fix(event);

	var element = event.target;

	while (element.tagName.toLowerCase() != 'li')
	{
		element = element.parentNode;
	}

	if (!class_handler.has(element, 'over'))
	{
		class_handler.add(element, 'over');
	}

	return false;
};
suggest.itemOut = function (event)
{
	var event = event_handler.fix(event);

	var element = event.target;

	while (element.tagName.toLowerCase() != 'li')
	{
		element = element.parentNode;
	}

	class_handler.remove(element, 'over');

	return false;
};
suggest.itemClick = function (event)
{
	var event = event_handler.fix(event);

	var element = event.target;

	while (element.tagName.toLowerCase() != 'li')
	{
		element = element.parentNode;
	}

	suggest.select(element.id);

	return false;
};
suggest.enter = function ()
{
	if (suggest.display_box_element != null)
	{
		var temp_ul = suggest.display_box_element.getElementsByTagName('ul');
		var items = temp_ul[0].getElementsByTagName('li');

		for (var i = 0; i < items.length; i++)
		{
			if (class_handler.has(items[i], 'over') && !class_handler.has(items[i], 'noclick'))
			{
				suggest.select(items[i].id);
			}
		}
	}

	return false;
};
suggest.select = function (element_id)
{
	suggest.value_element.value = element_id;

	var temp_element = document.getElementById(element_id);
	suggest.target_element.value = temp_element.title;

	suggest.destroy();

	return false;
};