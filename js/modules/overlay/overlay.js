var overlay = {};
overlay.img = BASE_URL+'images/icons/basic/24/Close.png';
overlay.requester = null;
overlay.init = function ()
{
	var temp_overlay = document.createElement('div');
	temp_overlay.id = 'overlay';

	var temp_content = document.createElement('div');
	temp_content.id = 'overlay_content';

	if (document.body)
	{
		document.body.appendChild(temp_overlay);
		document.body.appendChild(temp_content);
	}
	else if (document.documentElement)
	{
		document.documentElement.appendChild(temp_overlay);
		document.documentElement.appendChild(temp_content);
	}
}
overlay.build = function (url, overlay_width, overlay_height, params)
{
	var temp_content = document.getElementById('overlay_content');

	var pass_params = [];
	if (params.length > 0)
	{
		pass_params = params;
	}

	var total_height = position.getAvailableHeight();

	var margin_top = (total_height > overlay_height) ? -(overlay_height/2) : -(total_height/2);
	overlay_height = (total_height > overlay_height) ? overlay_height : total_height;

	margin_top = margin_top + position.getScrollY() - 15;

	overlay_width = overlay_width/(overlay_height/(overlay_height-20));

	var margin_left = overlay_width/2;

	temp_content.style.width = overlay_width+'px';
	temp_content.style.height = overlay_height+'px';
	temp_content.style.marginTop = '-'+overlay_height/2+'px';
	temp_content.style.marginLeft = '-'+margin_left+'px';

	overlay.requester = new xmlhttp_handler();
	overlay.requester.setAction(overlay.show);
	overlay.requester.loadURL('POST', url, pass_params, false);
	overlay.show();
};
overlay.show = function ()
{
	var temp_overlay = document.getElementById('overlay');
	var temp_content = document.getElementById('overlay_content');

	temp_content.innerHTML = '<!--[if lte IE 6.5]><iframe></iframe><![endif]-->';
	temp_content.innerHTML += '<img id="overlay_hide" src="'+overlay.img+'" />';
	temp_content.innerHTML += overlay.requester.getText();

	event_handler.add(document.getElementById('overlay_hide'), 'click', overlay.hide);

	temp_overlay.style.display = 'block';
	temp_overlay.style.height = position.getMaxHeight()+'px';
	temp_content.style.display = 'block';
};
overlay.hide = function ()
{
	var temp_overlay = document.getElementById('overlay');
	if (typeof(temp_overlay) != 'undefined')
	{
		var temp_content = document.getElementById('overlay_content');
		if (typeof(temp_content) != 'undefined')
		{
			temp_content.style.marginTop = '-1000px';
			temp_content.style.marginLeft = '-1000px';
			temp_content.style.display = 'none';
			temp_overlay.style.display = 'none';
		}
	}
};