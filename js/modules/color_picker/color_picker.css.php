<?

header("Content-type: text/css; charset=iso-8859-2");

$dir = substr($_SERVER['SCRIPT_FILENAME'], strlen($_SERVER['DOCUMENT_ROOT']));
$dir = substr($dir, 0, strpos($dir, "/", 1)+1);

include_once($_SERVER['DOCUMENT_ROOT'].$dir."config/conf.protected.php");
include_once($_SERVER['DOCUMENT_ROOT'].$dir."config/conf.php");
include_once(BASE_DIR."js/modules/color_picker/color_picker.colors.php");

?>
#color_picker_overlay
{
	position: absolute;
	display: none;
	padding: 10px;
	background: #fff;
	border: solid 6px #C0C0C0;
}
.color_table
{
	display: block;
	float: left;
	margin: 0.1em;
	padding: 0;
	width: 1em;
	height: 1em;
	text-decoration: none;
}
#color_selection
{
	float: left;
	margin: 0 0.3em 0 0;
	width: 2em;
	height: 1.2em;
	border: solid 1px #000;
}
<?
if (is_array($websafe_colors))
{
	foreach ($websafe_colors as $color)
	{
		echo ".bg_".$color."\n";
		echo "{\n";
		echo "	background: #".$color."\n";
		echo "}\n";
		echo ".color_".$color."\n";
		echo "{\n";
		echo "	color: #".$color.";\n";
		echo "}\n";
		echo ".border_color_".$color."\n";
		echo "{\n";
		echo "	border-color: #".$color.";\n";
		echo "}\n";
	}
}
?>