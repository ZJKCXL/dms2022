color_picker.color_table = BASE_URL+'js/modules/color_picker/color_picker.overlay.php';
color_picker.close_img = BASE_URL+'images/icons/basic/16/Close.png';
color_picker.invoked_by = null;
color_picker.requester = null;
color_picker.init = function ()
{
	var container = document.createElement('div');
	container.id = 'color_picker_overlay';

	if (document.body)
	{
		document.body.appendChild(container);
	}
	else if (document.documentElement)
	{
		document.documentElement.appendChild(container);
	}

	param = [];

	color_picker.requester = new xmlhttp_handler();
	color_picker.requester.loadURL('POST', color_picker.color_table, param, false);

	var result = color_picker.requester.getText();

	container.innerHTML = result;

	var anchors = document.getElementsByTagName('a');
	for (var i = 0; i < anchors.length; i++)
	{
		if (class_handler.has(anchors[i], 'color_table'))
		{
			event_handler.add(anchors[i], 'mouseover', color_picker.mouseover);
			event_handler.add(anchors[i], 'mouseout', color_picker.mouseout);
			event_handler.add(anchors[i], 'click', color_picker.click);
		}
	}

	var tmp_hide = document.getElementById('color_picker_hide');

	var img = document.createElement('img');
	img.src = color_picker.close_img;
	tmp_hide.appendChild(img);

	event_handler.add(tmp_hide, 'click', color_picker.close);

	var all_elements = document.getElementsByTagName('*');
	for (var i = 0; i < all_elements.length; i++)
	{
		if (class_handler.has(all_elements[i], 'color_picker'))
		{
			event_handler.add(all_elements[i], 'click', color_picker.show);
		}
	}
};
color_picker.show = function (event)
{
	event = event_handler.fix(event);
	var element = event.target;

	color_picker.invoked_by = element.id;

	var container = document.getElementById('color_picker_overlay');

	container.style.left = position.getPositionX(event)+'px';
	container.style.top = position.getPositionY(event)+'px';
	container.style.display = 'block';
};
color_picker.mouseover = function (event)
{
	event = event_handler.fix(event);
	var element = event.target;

	var container = document.getElementById('color_picker_overlay');
	if (container)
	{
	var temp_color_selection = document.getElementById('color_selection');
	temp_color_selection.style.background = '#'+element.id;

	var temp_color_code = document.getElementById('color_code');
	temp_color_code.value = '#'+element.id;
	}
};
color_picker.mouseout = function (event)
{
	event = event_handler.fix(event);
	var element = event.target;

	var container = document.getElementById('color_picker_overlay');
	if (container)
	{
	var temp_color_selection = document.getElementById('color_selection');
	temp_color_selection.style.background = '#fff';

	var temp_color_code = document.getElementById('color_code');
	temp_color_code.value = '';
	}
};
color_picker.click = function (event)
{
	event = event_handler.fix(event);

	if (event.preventDefault)
	{
		event.preventDefault();
	}

	var element = event.target;

	document.getElementById(color_picker.invoked_by).value = element.id;

	var tmp_select_actions = (dom_handler.getProperties(color_picker.select_action)) ? dom_handler.getProperties(color_picker.select_action) : 0;
	if (tmp_select_actions.length > 0)
	{
		var has_run = false;

		for (var i = 0; i < tmp_select_actions.length; i++)
		{
			if (color_picker.invoked_by.id == tmp_select_actions[i])
			{
				var tmp_functions = eval('color_picker.select_action.'+tmp_select_actions[i]);
				if (tmp_functions.length > 0)
				{
					for (var j = 0; j < tmp_functions.length; j++)
					{
						var tmp_class = tmp_functions[j].split('.');
						if (tmp_class.length > 0)
						{
							if (eval('typeof('+tmp_class[0]+')') == 'undefined')
							{
								continue;
							}
						}

						if (eval('typeof('+tmp_functions[j]+')') != 'undefined')
						{
							var tmp_run = eval(tmp_functions[j]);
							tmp_run();
						}
					}
				}

				has_run = true;
			}
		}

		if (!has_run)
		{
			var tmp_functions = color_picker.select_action.defaults;
			if (tmp_functions.length > 0)
			{
				for (var j = 0; j < tmp_functions.length; j++)
				{
					var tmp_class = tmp_functions[j].split('.');
					if (tmp_class.length > 0)
					{
						if (eval('typeof('+tmp_class[0]+')') == 'undefined')
						{
							continue;
						}
					}

					if (eval('typeof('+tmp_functions[j]+')') != 'undefined')
					{
						var tmp_run = eval(tmp_functions[j]);
						tmp_run();
					}
				}
			}
		}
	}

	color_picker.close();
};
color_picker.close = function ()
{
	var container = document.getElementById('color_picker_overlay');

	container.style.display = 'none';

	color_picker.invoked_by = null;
};