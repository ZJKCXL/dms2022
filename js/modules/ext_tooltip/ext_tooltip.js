var ext_tooltip = {};
ext_tooltip.posX = null;
ext_tooltip.posY = null;
ext_tooltip.tempTitle = null;
ext_tooltip.init = function ()
{
	var temp_tooltip = document.createElement('div');
	temp_tooltip.id = 'tooltip';

	if (document.body)
	{
		document.body.appendChild(temp_tooltip);
	}
	else if (document.documentElement)
	{
		document.documentElement.appendChild(temp_tooltip);
	}

	var all_elements = document.getElementsByTagName('*');
	for (var i = 0; i < all_elements.length; i++)
	{
		if (class_handler.has(all_elements[i], 'tooltip'))
		{
			ext_tooltip.hook(all_elements[i]);
		}
	}
};
ext_tooltip.hook = function (element)
{
	if (element.title.length > 0)
	{
		event_handler.remove(element, 'mouseover', ext_tooltip.mouseover);
		event_handler.remove(element, 'mousemove', ext_tooltip.mousemove);
		event_handler.remove(element, 'mouseout', ext_tooltip.mouseout);

		event_handler.add(element, 'mouseover', ext_tooltip.mouseover);
		event_handler.add(element, 'mousemove', ext_tooltip.mousemove);
		event_handler.add(element, 'mouseout', ext_tooltip.mouseout);
	}
}
ext_tooltip.mouseover = function (event)
{
	if (!event.target)
	{
		var event = event_handler.fix(event);
	}
	if (event.preventDefault)
	{
		event.preventDefault();
	}
	var element = event.target;
	while (!class_handler.has(element, 'tooltip'))
	{
		element = element.parentNode;
	}

	ext_tooltip.tempTitle = element.title;
	element.title = '';

	ext_tooltip.posX = position.getPositionX(event);
	ext_tooltip.posY = position.getPositionY(event);

	var div = document.getElementById('tooltip');
	div.innerHTML = ext_tooltip.tempTitle;

	if ((ext_tooltip.posY+div.offsetHeight) > (position.getAvailableHeight()+position.getScrollY()))
	{
		div.style.top = ((position.getAvailableHeight()+position.getScrollY())-div.offsetHeight)+'px';
	}
	else
	{
		div.style.top = (ext_tooltip.posY+20)+'px';
	}
	if ((ext_tooltip.posX+div.offsetWidth) > (position.getAvailableWidth()+position.getScrollX()))
	{
		div.style.left = ((position.getAvailableWidth()+position.getScrollX())-div.offsetWidth)+'px';
	}
	else
	{
		div.style.left = (ext_tooltip.posX+20)+'px';
	}

	div.style.display = 'block';
};
ext_tooltip.mousemove = function (event)
{
	if (!event.target)
	{
		var event = event_handler.fix(event);
	}
	if (event.preventDefault)
	{
		event.preventDefault();
	}
	var element = event.target;
	while (!class_handler.has(element, 'tooltip'))
	{
		element = element.parentNode;
	}

	ext_tooltip.posX = position.getPositionX(event);
	ext_tooltip.posY = position.getPositionY(event);

	var div = document.getElementById('tooltip');

	if ((ext_tooltip.posY+div.offsetHeight) > (position.getAvailableHeight()+position.getScrollY()))
	{
		div.style.top = ((position.getAvailableHeight()+position.getScrollY())-div.offsetHeight)+'px';
	}
	else
	{
		div.style.top = (ext_tooltip.posY+20)+'px';
	}
	if ((ext_tooltip.posX+div.offsetWidth) > (position.getAvailableWidth()+position.getScrollX()))
	{
		div.style.left = ((position.getAvailableWidth()+position.getScrollX())-div.offsetWidth)+'px';
	}
	else
	{
		div.style.left = (ext_tooltip.posX+20)+'px';
	}

	div.style.display = 'block';
};
ext_tooltip.mouseout = function (event)
{
	if (!event.target)
	{
		var event = event_handler.fix(event);
	}
	if (event.preventDefault)
	{
		event.preventDefault();
	}
	var element = event.target;
	while (!class_handler.has(element, 'tooltip'))
	{
		element = element.parentNode;
	}

	element.title = ext_tooltip.tempTitle;
	ext_tooltip.tempTitle = null;

	var div = document.getElementById('tooltip');

	div.style.display = 'none';
	div.style.top =  '0px';
	div.style.left = '-3000px';
};