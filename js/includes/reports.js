$(document).ready(function () {
	$(".invokeFilter").bind("click", function () {
		var param = {};
		param.url = BASE_URL+"overlays/reports.php";
		param.data = {
			"lang": LANGUAGE
		};
		
		$.extend(param.data, $("#frm_paramHolder").serializeJSON());

		$.fn.overlay("show", param);
		
		$.fn.tooltip("init");
		if (typeof(form_handler) != "undefined" && typeof(form_handler.init) != "undefined") form_handler.init();
		if (typeof(calendar) != "undefined" && typeof(calendar.init) != "undefined") calendar.init();
	});
	
	$(".filterShow").bind("click", function () {
		var container = $(this).parents(".filterContainer");
		if ($("img", container).attr("src").match(/Expand\.png/))
		{
			$("img", container).attr("src", $("img", container).attr("src").replace(/Expand\.png/, "Collapse.png"));
		}
		else
		{
			$("img", container).attr("src", $("img", container).attr("src").replace(/Collapse\.png/, "Expand.png"));
		}
		$(".padding", container).slideToggle();
	});
	
	$(".graphPrev").bind("click", function (event) {
		charts.move(1, event);
	});
	$(".graphNext").bind("click", function (event) {
		charts.move(0, event);
	});
	
	$(".exportExcel").bind("click", function (event) {
		charts.exportExcel(event);
	});
	
	$(".graphSwitch, .top3, .top3Plus, .all").bind("click", function (event) {
		charts.switchView(event);
	});
});

var charts = {};
charts.items = {};
charts.options = {};
charts.settings = {
	chartArea: {
		left: 20,
		top: 20,
		width: "90%",
		height: "90%"
	},
	backgroundColor: "#F4F8FA",
	height: 250,
	title: ""
};
charts.draw = function (elementID, graphType, settings, data) {
	var useSettings = {};
	$.extend(useSettings, charts.settings);
	$.extend(useSettings, settings);
	
	var drawType = "";
	switch (graphType)
	{
		default:
		case "pie":
			drawType = "PieChart";
			break;
		case "column":
			drawType = "ColumnChart";
			break;
		case "combo":
			drawType = "ComboChart";
			break;
		case "area":
			drawType = "AreaChart";
			break;
		case "line":
			drawType = "LineChart";
			break;
		case "bar":
			drawType = "BarChart";
			break;
		case "geo":
			drawType = "GeoChart";
			break;
	}
	
	eval("var chart = new google.visualization."+drawType+"($(\"#"+elementID+"\").get(0));");
	$("#"+elementID).data("chart", chart);
	chart.draw(data, useSettings);
	
	return chart;
};
charts.move = function (prev, event) {
	event = $.event.fix(event);
	
	var chart = $(".chart", $(event.target).parents(".graf"));
	
	if ($(chart).data("seriesLength") && parseInt($(chart).data("seriesLength")) > 0)
	{
		if ((prev && $(chart).data("options").hAxis.viewWindow.min-$(chart).data("maxValues") >= 0)
		|| (!prev && $(chart).data("options").hAxis.viewWindow.max+$(chart).data("maxValues")
			<= Math.ceil($(chart).data("seriesLength")/$(chart).data("maxValues"))*$(chart).data("maxValues")))
		{
			$(chart).data("options").hAxis.viewWindow.min += (!prev) ? $(chart).data("maxValues") : -$(chart).data("maxValues");
			$(chart).data("options").hAxis.viewWindow.max += (!prev) ? $(chart).data("maxValues") : -$(chart).data("maxValues");
		}
	}
		
	var useSettings = {};
	$.extend(useSettings, charts.settings);
	$.extend(useSettings, $(chart).data("options"));
	
	$(chart).data("chart").draw($(chart).data("data"), useSettings);
};
charts.exportExcel = function (event) {
	event = $.event.fix(event);
	
	var element = event.target;
	if (!$(element).hasClass("graf"))
	{
		element = $(element).parents(".graf");
	}
	
	element = $(".chart", element);
	
	var data = $("#frm_paramHolder").serialize();
	data += "&elementID="+$(element).attr("id");
	
	window.location.replace(BASE_URL+"scripts/graphExcelExport.php?"+data);
	
	return;
};
charts.switchView = function (event) {
	event = $.event.fix(event);
	if (event.preventDefault) event.preventDefault();
	
	var element = event.target;
	if (element.tagName.toLowerCase() != "a")
	{
		element = $(element).parents("a");
	}
	
	if ($(element).attr("rel") == "sentimentChart")
	{
		if ($(element).hasClass("graphSwitchCurves"))
		{
			$(".graphDesc p", $("#sentimentChart").parents(".graph")).html(TXT_REPORTS_SENTIMENT_EXPLANATION);
			charts.draw("dateChart", "area", charts.options.datesCurves, charts.items.datesCurves);
		}
		else if ($(element).hasClass("graphSwitchAvg"))
		{
			$(".graphDesc p", $("#sentimentChart").parents(".graph")).html(TXT_REPORTS_SENTIMENT_AVG_EXPLANATION);
			charts.draw("dateChart", "area", charts.options.datesAvg, charts.items.datesAvg);
		}
		else if ($(element).hasClass("graphSwitchMomentum"))
		{
			$(".graphDesc p", $("#sentimentChart").parents(".graph")).html(TXT_REPORTS_SENTIMENT_CHART_EXPLANATION);
			charts.draw("dateChart", "column", charts.options.sentimentMomentumDefault, charts.items.sentimentMomentumDefault);
		}
		else
		{
			$(".graphDesc p", $("#sentimentChart").parents(".graph")).html(TXT_REPORTS_SENTIMENT_EXPLANATION);
			charts.draw("dateChart", "column", charts.options.datesDefault, charts.items.datesDefault);
		}
		
		$("a", $(element).parents("h3")).removeClass("active");
		$(element).addClass("active");
	}
	else if ($(element).attr("rel") == "topicChart" || $(element).attr("rel") == "hubChart")
	{
		if ($(element).attr("rel") == "topicChart")
		{
			var displayOptions = charts.options.topicsDefault;
			displayOptions = ($(element).hasClass("top3Plus")) && !$(element).hasClass("active") ? charts.options.topicsTopPlus : displayOptions;
			
			var displayData = charts.items.topicsDefault;
			displayData = ($(element).hasClass("top3")) && !$(element).hasClass("active") ? charts.items.topicsTop : displayData;
			displayData = ($(element).hasClass("top3Plus")) && !$(element).hasClass("active") ? charts.items.topicsTopPlus : displayData;
		}
		else
		{
			var displayOptions = charts.options.hubsDefault;
			displayOptions = ($(element).hasClass("top3Plus")) && !$(element).hasClass("active") ? charts.options.hubsTopPlus : displayOptions;
			
			var displayData = charts.items.hubsDefault;
			displayData = ($(element).hasClass("top3")) && !$(element).hasClass("active") ? charts.items.hubsTop : displayData;
			displayData = ($(element).hasClass("top3Plus")) && !$(element).hasClass("active") ? charts.items.hubsTopPlus : displayData;
		}
		
		charts.draw($(element).attr("rel"), "pie", displayOptions, displayData);
		
		if (!$(element).hasClass("active"))
		{
			$("a", $(element).parents("h3")).removeClass("active");
			$(element).addClass("active");
		}
		else
		{
			$("a", $(element).parents("h3")).removeClass("active");
		}
	}
	else if ($(element).attr("rel") == "pageFansComposite")
	{
		var graphType = "area";
		
		if ($(element).attr("rel") == "pageFansComposite")
		{
			var displayOptions = charts.options.pageFansCompositeDefault;
			displayOptions = ($(element).hasClass("graphSwitchLine")) && !$(element).hasClass("active") ? charts.options.pageFansCompositeLine : displayOptions;
			displayOptions = ($(element).hasClass("graphSwitchColumns")) && !$(element).hasClass("active") ? charts.options.pageFansCompositeColumns : displayOptions;
			
			var displayData = charts.items.pageFansCompositeDefault;
			displayData = ($(element).hasClass("graphSwitchLine")) && !$(element).hasClass("active") ? charts.items.pageFansCompositeLine : displayData;
			displayData = ($(element).hasClass("graphSwitchColumns")) && !$(element).hasClass("active") ? charts.items.pageFansCompositeLine : displayData;
			
			graphType = ($(element).hasClass("graphSwitchColumns")) && !$(element).hasClass("active") ? "combo" : graphType;
		}
		
		charts.draw($(element).attr("rel"), graphType, displayOptions, displayData);
		
		if (!$(element).hasClass("active"))
		{
			$("a", $(element).parents("h3")).removeClass("active");
			$(element).addClass("active");
		}
		else
		{
			$("a", $(element).parents("h3")).removeClass("active");
		}
	}
	else if ($(element).attr("rel") == "pageFansGeo")
	{
		if ($(element).attr("rel") == "pageFansGeo")
		{
			var displayOptions = charts.options.pageFansGeoDefault;
			displayOptions = ($(element).hasClass("graphSwitchWorld")) && !$(element).hasClass("active") ? charts.options.pageFansGeoWorld : displayOptions;
			
			var displayData = charts.items.pageFansGeoDefault;
			displayData = ($(element).hasClass("graphSwitchWorld")) && !$(element).hasClass("active") ? charts.items.pageFansGeoWorld : displayData;
		}
		
		var tmpChart = charts.draw($(element).attr("rel"), "geo", displayOptions, displayData);
		
		if (($(element).hasClass("graphSwitchWorld")) && !$(element).hasClass("active"))
		{
			google.visualization.events.addListener(tmpChart, "regionClick", function (event) {
				var tmpOptions = {};
				$.extend(tmpOptions, charts.options.pageFansGeoDefault);
				$.extend(tmpOptions, {
					region: event.region
				});
				
				charts.draw($(element).attr("rel"), "geo", tmpOptions, charts.items.pageFansGeoDefault)
				
				$("a[rel=pageFansGeo]").removeClass("active");
			});
		}
		
		if (!$(element).hasClass("active"))
		{
			$("a", $(element).parents("h3")).removeClass("active");
			$(element).addClass("active");
		}
		else
		{
			$("a", $(element).parents("h3")).removeClass("active");
		}
	}
	else if ($(element).attr("rel") == "pageFansDemographics")
	{
		if ($(element).attr("rel") == "pageFansDemographics")
		{
			var displayOptions = charts.options.pageFansDemographicsDefault;
			//displayOptions = ($(element).hasClass("graphSwitchMen")) && !$(element).hasClass("active") ? charts.options.pageFansDemographicsMen : displayOptions;
			displayOptions = ($(element).hasClass("graphSwitchWomen")) && !$(element).hasClass("active") ? charts.options.pageFansDemographicsWomen : displayOptions;
			
			var displayData = charts.items.pageFansDemographicsDefault;
			displayData = ($(element).hasClass("graphSwitchMen")) && !$(element).hasClass("active") ? charts.items.pageFansDemographicsMen : displayData;
			displayData = ($(element).hasClass("graphSwitchWomen")) && !$(element).hasClass("active") ? charts.items.pageFansDemographicsWomen : displayData;
		}
		
		charts.draw($(element).attr("rel"), "bar", displayOptions, displayData);
		
		if (!$(element).hasClass("active"))
		{
			$("a", $(element).parents("h3")).removeClass("active");
			$(element).addClass("active");
		}
		else
		{
			$("a", $(element).parents("h3")).removeClass("active");
		}
	}
};