var ZClipBound = [];

$(document).ready(function () {
	$("#instanceDisplayedID").bind("change", function () {
		$(this).parents("form").submit();
	});
	
	if ($("#work").length > 0)
	{
		$("#gooddataIframe").height(($(window).height()-$("#work").offset().top-$("#work p.info").outerHeight(true)-($("#work").offset().top-$("#main").offset().top)*2-4)+"px");
	}
	
	$(".menuCollapse").bind("click", function () {
		var tmpA = this;
		
		var imgSrc = BASE_URL+"images/icons/basic/24/";
		imgSrc += $("img", this).attr("src").match(/Arrow_Left/i) ? "Arrow_Right.png" : "Arrow_Left.png";
		
		var newWidth = "120px";
		var newMainWidth = ($("#main").width()-96)+"px";
		var newMainMargin = "0px";
		var cookieValue = "open";
		if ($("img", this).attr("src").match(/Arrow_Left/i))
		{
			newWidth = "24px";
			newMainWidth = ($("#main").width()+96)+"px";
			newMainMargin = "-96px";
			cookieValue = "closed";
		}
		
		if (cookieValue == "open")
		{
			$("#menu ul.menu-main").css("visibility", "visible");
		}
		else
		{
			$("#menu ul.menu-main").css("visibility", "hidden");
		}
		
		$.fn.cookies("set", {
			name: "menuState",
			value: cookieValue,
			expires: (3600*24*7)
		});
		
		$("#menu").animate({width: newWidth}, "fast",
			function () {
				$("img", tmpA).attr("src", imgSrc);
			}
		);
		$("#main").animate({
			"margin-left": newMainMargin,
			width: newMainWidth
		}, "fast");
	});
});
$(window).bind("load", function () {
	$("a[target=_blank]").bind("click", function (event) {
		event = $.event.fix(event);
		
		if (event.preventDefault) event.preventDefault();
	});
	$("a[target=_blank]").bind("mouseenter", function () {
		if (ZClipBound.indexOf(this) != -1)
		{
			return;
		}
		$(this).zclip({
			path: "include/ZeroClipboard10.swf",
			copy: function () {
				return $(this).attr("href");
			},
			afterCopy: function () {
				$.fn.statusBar("fadeWith", {html: JS_COPIED_TO_CLIPBOARD});
			}
		});
		
		ZClipBound.push(this);
	});
});