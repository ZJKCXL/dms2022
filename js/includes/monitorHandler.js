var monitorHandler = {};
monitorHandler.scrollDuration = 200; //milliseconds
monitorHandler.init = function () {
	$(".hubAdd, .hubMod").unbind("click");
	$(".hubAdd, .hubMod").bind("click", monitorHandler.hubOverlay);
	
	$(".pageAdd, .pageMod").unbind("click");
	$(".pageAdd, .pageMod").bind("click", monitorHandler.pageOverlay);
	
	$(".sessionAdd").unbind("click");
	$(".sessionAdd").bind("click", monitorHandler.sessionAdd);
	
	$(".postsAdd").unbind("click");
	$(".postsAdd").bind("click", monitorHandler.postsAdd);
	
	$(".notifyAdmins").unbind("click");
	$(".notifyAdmins").bind("click", monitorHandler.notifyAdmins);
	
	$("a[target=_blank]").unbind("click");
	$("a[target=_blank]").bind("click", function (event) {
		event = $.event.fix(event);
		if (event.preventDefault) event.preventDefault();
		
		return false;
	});
	
	if (typeof(calendar) != "undefined" && calendar.img)
	{
		calendar.img = BASE_URL+'images/icons/basic/16/Calendar.png';
	}
};
monitorHandler.initPosts = function () {
	$(".healthPlus, .healthMinus").unbind("click");
	$(".healthPlus, .healthMinus").bind("click", monitorHandler.healthClick);
	
	$(".topicAdd").unbind("click");
	$(".topicAdd").bind("click", monitorHandler.topicAdd);
	
	$(".heartIcon").unbind("click");
	$(".heartIcon").bind("click", monitorHandler.proximityClick);
	
	$(".intensityOK, .intensitySevere, .intensityCritical").unbind("click");
	$(".intensityOK, .intensitySevere, .intensityCritical").bind("click", monitorHandler.intensityClick);
	
	$(".textareaShow").unbind("click");
	$(".textareaShow").bind("click", function (event) {
		event = $.event.fix(event);
		if (event.preventDefault) event.preventDefault();
		
		$("textarea", $(this).parents(".postBlock")).toggle();
		$(".postBodyContainer", $(this).parents(".postBlock")).toggle();
	});
	
	$("a[target=_blank]").unbind("click");
	$("a[target=_blank]").bind("click", function (event) {
		event = $.event.fix(event);
		if (event.preventDefault) event.preventDefault();
		
		return false;
	});
	
	$(".loadMore").bind("click", function (event) {
		event = $.event.fix(event);
		if (event.preventDefault) event.preventDefault();
		
		$(this).text(TXT_MONITORING_LOADING_MORE);
		
		monitorHandler.showHide(this);
	});
	
	$("select[name^=topicID]").bind("focus", function () {
		$(this).data({prevVal: $(this).val()});
	});
	$("select[name^=topicID]").bind("change", function () {
		var newVal = $(this).val();
		
		if ($(this).data("prevVal") == -1)
		{
			$("select", $(this).parents(".monitoredPage")).each(function () {
				if ($(this).val() == -1)
				{
					$(this).val(newVal);
				}
			});
		}
	});
	
	$("input[name^=timeAt]").unbind("blur", timeHandler.normalizeTime);
	$("input[name^=timeAt]").bind("blur", timeHandler.normalizeTime);
	
	$("input[name^=firstPostTimeAt]").unbind("blur", timeHandler.normalizeTime);
	$("input[name^=firstPostTimeAt]").bind("blur", timeHandler.normalizeTime);
	
	$(".postBlock").unbind("mouseover mouseout", monitorHandler.highlightPost);
	$(".postBlock").bind("mouseover mouseout", monitorHandler.highlightPost);
	
	$(".markForDeletion, .deletePostDo").bind("click", monitorHandler.postDelete);
};
monitorHandler.bindZClip = function (selector) {
	$(selector).bind("click", function (event) {
		event = $.event.fix(event);
		
		if (event.preventDefault) event.preventDefault();
	});
	$(selector).bind("mouseenter", function () {
		if (ZClipBound.indexOf(this) != -1)
		{
			return;
		}
		$(this).zclip({
			path: "include/ZeroClipboard10.swf",
			copy: function () {
				return $(this).attr("href");
			},
			afterCopy: function () {
				$.fn.statusBar("fadeWith", {html: JS_COPIED_TO_CLIPBOARD});
			}
		});
		
		ZClipBound.push(this);
	});
};
monitorHandler.healthClick = function (event) {
	event = $.event.fix(event);
	if (event.preventDefault) event.preventDefault();
	
	var anchor = $(event.target);
	while ($(anchor).get(0).nodeName.toLowerCase() != "a")
	{
		anchor = $(anchor).parents("a");
	}
	
	var tmpPost = $(event.target);
	while (!$(tmpPost).hasClass("postBlock"))
	{
		tmpPost = $(tmpPost).parents(".postBlock");
	}
	
	if ($(anchor).hasClass("healthPlus"))
	{
		if (parseInt($("[name^=postHealth]", $(tmpPost)).val()+1) <= 1)
		{
			$("[name^=postHealth]", $(tmpPost)).val(parseInt($("[name^=postHealth]", $(tmpPost)).val())+1);
		}
	}
	else
	{
		if ($("[name^=postHealth]", $(tmpPost)).val()-1 >= -1)
		{
			$("[name^=postHealth]", $(tmpPost)).val(parseInt($("[name^=postHealth]", $(tmpPost)).val())-1);
		}
	}
	
	var flagTitle = TXT_POST_HEALTH+": ";
	var src = BASE_URL+"images/icons/editing_controls/16/";
	switch (parseInt($("[name^=postHealth]", $(tmpPost)).val()))
	{
		case -1:
			src += "Emoticon_Angry.png";
			flagTitle += TXT_POST_FLAG_RED;
			break;
		case 0:
			src += "Emoticon_Tense.png";
			flagTitle += TXT_POST_FLAG_BLUE;
			break;
		case 1:
			src += "Emoticon_Smile.png";
			flagTitle += TXT_POST_FLAG_GREEN;
			break;
	}
	
	$(".postHealthIMG", $(tmpPost)).attr({
		src: src,
		title: flagTitle
	});
	
	if (parseInt($("[name^=postHealth]", $(tmpPost)).val()) <= 0)
	{
		$(".healthPlus img", $(tmpPost)).attr("src", BASE_URL+"images/icons/commerce/16/Rate_Thumbs_Up.png");
		if (parseInt($("[name^=postHealth]", $(tmpPost)).val()) < 0)
		{
			$(".healthMinus img", $(tmpPost)).attr("src", BASE_URL+"images/icons/honeypot/16/Rate_Thumbs_Down_BW.png");
		}
	}
	if (parseInt($("[name^=postHealth]", $(tmpPost)).val()) >= 0)
	{
		$(".healthMinus img", $(tmpPost)).attr("src", BASE_URL+"images/icons/commerce/16/Rate_Thumbs_Down.png");
		if (parseInt($("[name^=postHealth]", $(tmpPost)).val()) > 0)
		{
			$(".healthPlus img", $(tmpPost)).attr("src", BASE_URL+"images/icons/honeypot/16/Rate_Thumbs_Up_BW.png");
		}
	}
};
monitorHandler.proximityClick = function (event) {
	event = $.event.fix(event);
	if (event.preventDefault) event.preventDefault();
	
	var anchor = $(event.target);
	while ($(anchor).get(0).nodeName.toLowerCase() != "a")
	{
		anchor = $(anchor).parents("a");
	}
	
	var tmpPost = $(event.target);
	while (!$(tmpPost).hasClass("postBlock"))
	{
		tmpPost = $(tmpPost).parents(".postBlock");
	}
	
	if ($(anchor).hasClass("heartIcon"))
	{
		var src = BASE_URL+"images/icons/honeypot/16/Heart.png";
		
		var newval = Math.abs(parseInt($("[name^=postProximity]", $(tmpPost)).val())-1);
		$("[name^=postProximity]", $(tmpPost)).val(newval);
		
		if (newval == 0)
		{
			src = BASE_URL+"images/icons/honeypot/16/Heart_BW.png";
		}
		
		var flagTitle = newval == 1 ? TXT_POST_PROXIMITY_TRUE : TXT_POST_PROXIMITY_FALSE;

		$(".heartIcon img", $(tmpPost)).attr("src", src);
		$.fn.tooltip("hide");
		$(".heartIcon", $(tmpPost)).attr("title", flagTitle);
	}
};
monitorHandler.intensityClick = function (event) {
	event = $.event.fix(event);
	if (event.preventDefault) event.preventDefault();
	
	var anchor = $(event.target);
	while ($(anchor).get(0).nodeName.toLowerCase() != "a")
	{
		anchor = $(anchor).parents("a");
	}
	
	var tmpPost = $(event.target);
	while (!$(tmpPost).hasClass("postBlock"))
	{
		tmpPost = $(tmpPost).parents(".postBlock");
	}
	
	$(".intensityOK img", $(tmpPost)).attr("src", BASE_URL+"images/icons/honeypot/16/Sun.png");
	$(".intensitySevere img", $(tmpPost)).attr("src", BASE_URL+"images/icons/honeypot/16/Cloud.png");
	$(".intensityCritical img", $(tmpPost)).attr("src", BASE_URL+"images/icons/honeypot/16/Rain.png");
	
	var newval = $("[name^=postIntensity]", $(tmpPost)).val();
	
	var intensityDesatureateSelector = "";
	var intensityHighlightSelector = "";
	if ($(anchor).hasClass("intensityOK"))
	{
		newval = 1;
		intensityDesatureateSelector = ".intensitySevere img, .intensityCritical img";
		intensityHighlightSelector = ".intensityOK img";
		
		$(".intensityOK img", $(tmpPost)).attr("src", BASE_URL+"images/icons/honeypot/16/Sun.png");
		$(".intensitySevere img", $(tmpPost)).attr("src", BASE_URL+"images/icons/honeypot/16/Cloud_BW.png");
		$(".intensityCritical img", $(tmpPost)).attr("src", BASE_URL+"images/icons/honeypot/16/Rain_BW.png");
	}
	else if ($(anchor).hasClass("intensitySevere"))
	{
		newval = 2;
		intensityDesatureateSelector = ".intensityOK img, .intensityCritical img";
		intensityHighlightSelector = ".intensitySevere img";
		
		$(".intensityOK img", $(tmpPost)).attr("src", BASE_URL+"images/icons/honeypot/16/Sun_BW.png");
		$(".intensitySevere img", $(tmpPost)).attr("src", BASE_URL+"images/icons/honeypot/16/Cloud.png");
		$(".intensityCritical img", $(tmpPost)).attr("src", BASE_URL+"images/icons/honeypot/16/Rain_BW.png");
	}
	else if ($(anchor).hasClass("intensityCritical"))
	{
		newval = 3;
		intensityDesatureateSelector = ".intensityOK img, .intensitySevere img";
		intensityHighlightSelector = ".intensityCritical img";
		
		$(".intensityOK img", $(tmpPost)).attr("src", BASE_URL+"images/icons/honeypot/16/Sun_BW.png");
		$(".intensitySevere img", $(tmpPost)).attr("src", BASE_URL+"images/icons/honeypot/16/Cloud_BW.png");
		$(".intensityCritical img", $(tmpPost)).attr("src", BASE_URL+"images/icons/honeypot/16/Rain.png");
	}
	
	$(intensityDesatureateSelector, $(tmpPost)).css({border: 0})
	$(intensityHighlightSelector, $(tmpPost)).css({border: "solid 1px #000"});
	
	$("[name^=postIntensity]", $(tmpPost)).val(newval);
};
monitorHandler.topicAdd = function (event) {
	event = $.event.fix(event);
	if (event.preventDefault) event.preventDefault();
	
	var anchor = $(event.target);
	while ($(anchor).get(0).nodeName.toLowerCase() != "a")
	{
		anchor = $(anchor).parents("a");
	}
	
	var params = {};
	params.async = 0;
	params.url = BASE_URL+"overlays/topicHandler.php";
	params.data = {};
	$.fn.overlay("show", params);
	
	$("#save").bind("click", function() {
		monitorHandler.topicSave($(anchor), $("#frm_topicHandler").serializeJSON());
	});
};
monitorHandler.topicSave = function (anchor, formData) {
	var params = {};
	params.url = BASE_URL+"scripts/topicHandlerAJAX.php";
	params.data = formData;
	
	var tr = $(anchor).parents("tr");
	
	selectVals = [];
	$("[name^=topicID] option:selected").each(function () {
		selectVals[selectVals.length] = $(this).val();
	});
	
	var select = $("[name^=topicID]", $(tr));
	
	$.ajax({
		type: "POST",
		dataType: "json",
		async: true,
		url: params.url,
		cache: false,
		data: params.data,
		success: function (response) {
			var html = ""
			for (var i = 0; i < response.data.topics.length; i++)
			{
				html += "<option value=\""+response.data.topics[i].id+"\">"+response.data.topics[i].topicNameCZ+"</option>";
			}

			var i = 0;
			$("[name^=topicID]").each(function () {
				$(this).html($(html).clone());
				
				$(this).val(selectVals[i]);
				i++;
			});
			
			$(select).val(response.data.selectedID);
		}
	});
	
	$.fn.overlay("hide");
};
monitorHandler.hubOverlay = function (event) {
	event = $.event.fix(event);
	if (event.preventDefault) event.preventDefault();
	
	var element = $(event.target);
	if ($(element).get(0).tagName.toLowerCase() != "a")
	{
		element = $(element).parents("a");
	}
	
	var hubID = $(element).hasClass("hubMod") ? $(element).parents(".monitoredHub").attr("id").match(/[0-9]+/)[0] : null;
	
	var params = {};
	params.async = 0;
	params.url = BASE_URL+"overlays/hubHandler.php";
	params.data = {
		hubID: hubID
	};
	$.fn.overlay("show", params);
	$.fn.tooltip("init");
	
	$("#newHubURL").bind("keyup", function (event) {
		monitorHandler.hubCheckExists(event)
	});
	
	$("#save").bind("click", function () {
		monitorHandler.hubSave($("#frm_hubHandler").serializeJSON());
	});
};
monitorHandler.hubCheckExists = function (event) {
	event = $.event.fix(event);
	if (event.preventDefault) event.preventDefault();
	
	if ($(event.target).val().length <= 0)
	{
		$("#existingHubsList").hide();
		$("#existingHubsList tbody tr").remove();
		
		params = {};
		params.resize = "XY";
		$.fn.overlay("resize", params);
				
		return;
	}
	
	$.ajax({
		type: "POST",
		dataType: "json",
		async: true,
		url: BASE_URL+"scripts/checkURLExists.php",
		cache: false,
		data: {
			checkURL: $(event.target).val(),
			checkWhere: "hub"
		},
		success: function (response) {
			if (response.data && response.data.length > 0)
			{
				var html = "";
				
				for (var i = 0; i < response.data.length; i++)
				{
					html += "<tr>";
					html += "<td class=\"";
					html += (i+1)%2 == 0 ? "even " : "";
					html += "\">";
					html += response.data[i].hubName;
					html += "</td>";
					html += "<td class=\"";
					html += (i+1)%2 == 0 ? "even " : "";
					html += "\">";
					html += "<a href=\""+response.data[i].hubURL+"\" target=\"_blank\">"+response.data[i].hubURL+"</a>";
					html += "</td>";
					html += "</tr>";
				}
				
				$("#existingHubsList tbody").html(html);
				$("#existingHubsList").show();
				
				monitorHandler.bindZClip("#existingHubsList tbody a[target=_blank]");
				
				params = {};
				params.resize = "XY";
				$.fn.overlay("resize", params);
			}
			else
			{
				$("#existingHubsList").hide();
				$("#existingHubsList tbody tr").remove();
			}
		}
	});
};
monitorHandler.hubSave = function (formData) {
	var params = {};
	params.url = BASE_URL+"scripts/hubHandlerAJAX.php";
	params.data = formData;
	
	$.ajax({
		type: "POST",
		dataType: "json",
		async: true,
		url: params.url,
		cache: false,
		data: params.data,
		success: function (response) {
			if (response.data.action == "mod")
			{
				monitorHandler.hubUpdate(response.data);
			}
			else
			{
				monitorHandler.hubAppend(response.data);
			}
		}
	});
	
	monitorHandler.postponedSave = false;
	
	$.fn.overlay("hide");
};
monitorHandler.hubUpdate = function (param) {
	var tmpName = param.hubName != null ? param.hubName : param.hubURL;
	var displayName = tmpName.substring(0, 80);
	displayName += tmpName.length > 80 ? "..." : "";
	
	$("#hubID"+param.id+" h3 a[target=_blank]").attr("href", param.hubURL);
	$("#hubID"+param.id+" h3 a[target=_blank]").text(displayName);
};
monitorHandler.hubAppend = function (param) {
	var isAdmin = false;
	$.ajax({
		type: "POST",
		dataType: "json",
		async: false,
		url: BASE_URL+"scripts/checkUserPrivilege.php",
		cache: false,
		data: {
			userID: $("#userID").val(),
			userLevel: 128
		},
		success: function (response) {
			isAdmin = response.data.hasPrivilege;
		}
	});
	
	var html = "<div id=\"hubID"+param.id+"\" class=\"monitoredHub\">";
	html += "<h3>";
	html += "<a href=\"#\"><img src=\""+BASE_URL+"images/icons/information_management/24/Collapse.png\" /></a> ";
	html += "<a href=\""+param.hubURL+"\" target=\"_blank\">"+param.hubName+"</a> ";
	html += "<a href=\"#\" class=\"pageAdd tooltip\" title=\""+TXT_PAGE_ADD+"\"><img src=\""+BASE_URL+"images/icons/information_management/16/Documents_x2_Add.png\" /></a> ";
	if (isAdmin)
	{
		html += "<a href=\"#\" class=\"pageMod tooltip\" title=\""+TXT_PAGE_MOD+"\"><img src=\""+BASE_URL+"images/icons/information_management/16/Documents_x2_Edit.png\" /></a> ";
	}
	html += "</h3>";
	html += "<div class=\"pagesContainer\"></div>";
	html += "</div>";
	
	$("#hubID"+param.id+"a:first").bind("click", monitorHandler.showHide);
	
	$(html).appendTo($("#monitoringWorkspace"));
	
	monitorHandler.init();
	
	monitorHandler.bindZClip("#hubID"+param.id+" a[target=_blank]");
	
	$.fn.tooltip("init");
};
monitorHandler.pageOverlay = function (event) {
	event = $.event.fix(event);
	if (event.preventDefault) event.preventDefault();
	
	var element = $(event.target);
	if ($(element).get(0).tagName.toLowerCase() != "a")
	{
		element = $(element).parents("a");
	}
	
	var hub = $(element).parents(".monitoredHub");
	
	var pageID = $(element).hasClass("pageMod") ? $(element).parents(".monitoredPage").attr("id").match(/[0-9]+/)[0] : null;
	
	var params = {};
	params.async = 0;
	params.url = BASE_URL+"overlays/pageHandler.php";
	params.data = {
		hubID: $(hub).attr("id").match(/[0-9]+/)[0],
		pageID: pageID
	};
	$.fn.overlay("show", params);
	$.fn.tooltip("init");
	
	$("#newPageURL").bind("keyup", function (event) {
		monitorHandler.pageCheckExists(event)
	});

	$("#save").bind("click", function () {
		monitorHandler.pageSave($("#frm_pageHandler").serializeJSON());
	});
};
monitorHandler.pageCheckExists = function (event) {
	event = $.event.fix(event);
	if (event.preventDefault) event.preventDefault();
	
	if ($(event.target).val().length <= 0)
	{
		$("#existingPagesList").hide();
		$("#existingPagesList tbody tr").remove();
		
		params = {};
		params.resize = "XY";
		$.fn.overlay("resize", params);
				
		return;
	}
	
	$.ajax({
		type: "POST",
		dataType: "json",
		async: true,
		url: BASE_URL+"scripts/checkURLExists.php",
		cache: false,
		data: {
			checkURL: $(event.target).val(),
			checkWhere: "page"
		},
		success: function (response) {
			if (response.data && response.data.length > 0)
			{
				var html = "";
				
				for (var i = 0; i < response.data.length; i++)
				{
					var fullTitle = "";
					var title = "";
					var fullURL = "";
					var url = "";
					if (response.data[i].pageTitle != null)
					{
						fullTitle = response.data[i].pageTitle;
						title = fullTitle.substring(0, 50);
						title += fullTitle.length > 50 ? "..." : "";
					}
					
					fullURL = response.data[i].pageURL;
					url = fullURL.substring(0, 50);
					url += fullURL.length > 50 ? "..." : "";
					
					html += "<tr>";
					html += "<td title=\""+fullTitle+"\" class=\"";
					html += (i+1)%2 == 0 ? "even " : "";
					html += "tooltip\">";
					html += title;
					html += "</td>";
					html += "<td title=\""+fullURL+"\" class=\"";
					html += (i+1)%2 == 0 ? "even " : "";
					html += "tooltip\">";
					html += "<a href=\""+fullURL+"\" target=\"_blank\">"+url+"</a>";
					html += "</td>";
					html += "</tr>";
				}
				
				$("#existingPagesList tbody").html(html);
				$("#existingPagesList").show();
				
				monitorHandler.bindZClip("#existingPagesList tbody a[target=_blank]");
				$.fn.tooltip("init");
			}
			else
			{
				$("#existingPagesList").hide();
				$("#existingPagesList tbody tr").remove();
			}
		
			params = {};
			params.resize = "XY";
			$.fn.overlay("resize", params);
		}
	});
};
monitorHandler.pageSave = function (formData) {
	var params = {};
	params.url = BASE_URL+"scripts/pageHandlerAJAX.php";
	params.data = formData;
	
	$.ajax({
		type: "POST",
		dataType: "json",
		async: true,
		url: params.url,
		cache: false,
		data: params.data,
		success: function (response) {
			if (response.data.action == "mod")
			{
				monitorHandler.pageUpdate(response.data);
			}
			else
			{
				monitorHandler.pageAppend(response.data);
			}
		}
	});
	
	monitorHandler.postponedSave = false;
	
	$.fn.overlay("hide");
};
monitorHandler.pageUpdate = function (param) {
	var tmpName = param.pageTitle != null ? param.pageTitle : param.pageURL;
	var displayName = tmpName.substring(0, 80);
	displayName += tmpName.length > 80 ? "..." : "";

	$("#pageID"+param.id+" h5 a[target=_blank]").attr("href", param.pageURL);
	$("#pageID"+param.id+" h5 a[target=_blank]").text(displayName);
};
monitorHandler.pageAppend = function (param) {
	var isAdmin = false;
	$.ajax({
		type: "POST",
		dataType: "json",
		async: false,
		url: BASE_URL+"scripts/checkUserPrivilege.php",
		cache: false,
		data: {
			userID: $("#userID").val(),
			userLevel: 128
		},
		success: function (response) {
			isAdmin = response.data.hasPrivilege;
		}
	});
	
	var tmpText = param.pageTitle != null ? param.pageTitle : param.pageURL;
	var displayText = tmpText.substring(0, 80);
	displayText += tmpText.length > 80 ? "..." : "";
	
	var displayLastPostDate = "";
	var showNewCount = false;
	var newCount = 0;
	var newClass = "postCountNoNew";
	var newCountHint = TXT_POST_NEW_COUNT_NONE;
	if (param.lastPostDate != null)
	{
		displayLastPostDate = date_handler.convert(param.lastPostDate.substring(0, 10), "Y-m-d", DATE_FORMAT);
		displayLastPostDate += param.lastPostDate.substring(10, 16);
		
		showNewCount = true;
		
		newCount = new Number(param.lastPostCount).valueOf();
		newClass = newCount > 0 ? "postCountNew" : newClass;
		newCountHint = newCount > 0 ? TXT_POST_NEW_HAS+"<br /><br />"+TXT_POST_NEW_LAST_POST+" "+displayLastPostDate : newCountHint;
	}
	
	var html = "";
	html += "<div id=\"pageID"+param.id+"\" class=\"monitoredPage\">";
	html += "<h5>";
	html += "<a><img src=\""+BASE_URL+"images/icons/information_management/24/Collapse.png\" /></a> ";
	if (isAdmin)
	{
		html += "<a href=\"#\" class=\"pageMod tooltip\" title=\""+TXT_PAGE_MOD+"\"><img src=\""+BASE_URL+"images/icons/information_management/16/Document_Blank_Edit.png\" /></a> ";
	}
	html += "<a href=\""+BASE_URL+"index.php?r=5&pageID="+param.pageID+"\" class=\"tooltip\" title=\""+TXT_PAGE_DETAIL+"\"><img src=\""+BASE_URL+"images/icons/information_management/16/Read_More.png\" /></a> ";
	html += "<a href=\"#\" class=\"sessionAdd tooltip\" title=\""+TXT_POST_GROUP_ADD+"\"><img src=\""+BASE_URL+"images/icons/basic/16/Comment_Add.png\" /></a> ";
	html += "<a href=\"#\" class=\"postsAdd tooltip\" title=\""+TXT_SESSION_ADD+"\"><img src=\""+BASE_URL+"images/icons/information_management/16/Conversation_Add.png\" /></a> ";
	html += "<a href=\""+param.pageURL+"\" title=\""+param.pageURL+"\" target=\"_blank\">"+displayText+"</a> ";
	html += "<span class=\"floatRight inlineBlock tooltip dummy\" title=\""+TXT_MONITORING_LAST_POST+"\">&nbsp;</span>";
	html += "<span class=\"inlineBlock floatRight tooltip postCount\" title=\""+TXT_POST_GROUP_COUNT+"\">0</span>";
	if (showNewCount)
	{
		html += "<span class=\"inlineBlock floatRight tooltip "+newClass+"\" title=\""+newCountHint+"\">"+newCount+"</span>";
	}
	html += "</h5>";
	html += "</div>";
	
	$(html).appendTo($("#hubID"+param.hubID+" .pagesContainer"));
	
	$("#pageID"+param.id+"a:first").bind("click", monitorHandler.showHide);
	
	monitorHandler.init();
	
	monitorHandler.bindZClip("#pageID"+param.id+" a[target=_blank]");
	
	$.fn.tooltip("init");
};
monitorHandler.postsAdd = function (event) {
	monitorHandler.sessionAdd(event, 1);
};
monitorHandler.sessionAdd = function (event, grouped) {
	event = $.event.fix(event);
	if (event.preventDefault) event.preventDefault();
	
	var grouped = grouped == 1 ? 1 : 0;
	
	var groupedClass = [];
	groupedClass.push("postBlock");
	
	var rowsAdd = 1;
	
	var page = $(event.target).parents(".monitoredPage");
	/*if (parseInt($("input[name^=sessionPage]", $(page)).val()) > 0)
	{
		rowsAdd = parseInt($("input[name^=sessionPage]", $(page)).val());
	}*/
	
	if (grouped)
	{
		rowsAdd = 1;
		
		groupedClass.push("grouped");
		var countElem = $("<div>").addClass("postCount floatLeft")
		.append(
			$("<input>").addClass("required tooltip")
			.attr({
				type: "text",
				size: 3,
				name: "postCount[]",
				title: TXT_POST_GROUP_COUNT
			})
		);
	}
	else
	{
		var countElem = $("<input>")
		.attr({
			type: "hidden",
			name: "postCount[]",
			value: 1
		});
	}

	var canPostText = false;
	$.ajax({
		type: "POST",
		dataType: "json",
		async: false,
		url: BASE_URL+"scripts/checkUserPrivilege.php",
		cache: false,
		data: {
			userID: $("#userID").val(),
			canDo: "postText"
		},
		success: function (response) {
			canPostText = response.data.hasPrivilege;
		}
	});
	
	var data = {};
	data.hubID = $(page).parents(".monitoredHub").attr("id").substring(5, $(page).parents(".monitoredHub").attr("id").length);
	data.pageID = $(page).attr("id").substring(6, $(page).attr("id").length);
	
	var html = "";
	
	var re = new RegExp("\<%BASE_URL%\>", "g");
	
	var topicsHTML = "";
	$.ajax({
		type: "POST",
		dataType: "json",
		async: false,
		url: BASE_URL+"scripts/topicGetAJAX.php",
		cache: false,
		data: {},
		success: function (response) {
			for (var i = 0; i < response.data.length; i++)
			{
				topicsHTML += "<option value=\""+response.data[i].id+"\">"+response.data[i].topicNameCZ+"</option>";
			}
		}
	});
	
	/*var tmpLength = 0;
	$("[name^=postCount]", $(page)).each(function () {
		tmpLength += new Number($(this).val()).valueOf();
	});*/
	
	var tmpLengthAll = $(".postBlock").length;
	$.get(BASE_URL+"pages/postTemplate.html?time=1", function (response) {
		var tmpDate = new Date();
		var tmpToday = tmpDate.getUTCFullYear()+"-"+(tmpDate.getUTCMonth()+1)+"-"+tmpDate.getUTCDate();
		var j = 0;
		for (var i = 0; i < rowsAdd; i++)
		{
			//var substForI = tmpLength+i+1;
			var substForI = "";
			var inputType = "hidden";
			var dateFromRequired = false;
			var dateFromValue = "";
			var dateBR = "";
			if (grouped)
			{
				substForI = "";
				inputType = "text";
				dateFromRequired = " required";
				dateFromValue = date_handler.convert(tmpToday, "Y-m-d", DATE_FORMAT);
				dateBR = "<br />";
			}
			
			substForI += $('<div>').append($(countElem).clone()).remove().html();
			
			var html = response;
			html = html.replace("<%CHILD_WRAPPER_CLASS%>", "");
			html = html.replace("<%CHILD_WRAPPER_STYLE%>", "");
			html = html.replace("<%GROUP_CLASS%>", groupedClass.join(" "));
			html = html.replace("<%GROUP_STYLE%>", "");
			html = html.replace("<%I%>", substForI);
			html = html.replace("<%J%>", tmpLengthAll+i+1);
			html = html.replace("<%J%>", tmpLengthAll+i+1);
			html = html.replace("<%MD5CHECKSUM%>", "");
			html = html.replace("<%HUB_ID%>", data.hubID);
			html = html.replace("<%PAGE_ID%>", data.pageID);
			html = html.replace("<%POST_ID%>", "");
			html = html.replace("<%POST_HEALTH%>", 0);
			html = html.replace("<%POST_PROXIMITY%>", 0);
			html = html.replace("<%POST_INTENSITY%>", 1);
			html = html.replace("<%HEART_TITLE%>", TXT_POST_PROXIMITY_FALSE);
			html = html.replace("<%INTENSITY_OK%>", TXT_POST_INTENSITY_OK);
			html = html.replace("<%INTENSITY_SEVERE%>", TXT_POST_INTENSITY_SEVERE);
			html = html.replace("<%INTENSITY_CRITICAL%>", TXT_POST_INTENSITY_CRITICAL);
			html = html.replace(re, BASE_URL);
			html = html.replace("<%FLAG%>", "Emoticon_Tense.png");
			html = html.replace("<%DATE_FROM_HIDDEN%>", inputType);
			html = html.replace("<%DATE_FROM_HIDDEN%>", inputType);
			html = html.replace("<%DATE_FROM_VALUE%>", dateFromValue);
			html = html.replace("<%DATE_FROM_REQUIRED%>", dateFromRequired);
			html = html.replace("<%DATE_FROM_REQUIRED%>", dateFromRequired);
			html = html.replace("<%TIME_FROM_VALUE%>", "");
			html = html.replace("<%DATE_BR%>", dateBR);
			html = html.replace("<%DATE_BR%>", dateBR);
			html = html.replace("<%TOOLTIP_DATE%>", TXT_MONITORING_DATE);
			html = html.replace("<%TOOLTIP_TIME%>", TXT_MONITORING_TIME);
			html = html.replace("<%DATE_AT%>", date_handler.convert(tmpToday, "Y-m-d", DATE_FORMAT));
			html = html.replace("<%TIME_AT%>", "");
			html = html.replace("<%TOPICS%>", topicsHTML);
			html = html.replace("<%POSTED_BY_USER%>", "");
			html = html.replace("<%POSTER_INFO%>", "");
			var textareaToggle = "";
			if (!canPostText)
			{
				html = html.replace("<%TEXT_TD%>", "<td><input type=\"hidden\" name=\"postBody[]\" />");
			}
			else
			{
				if (!grouped)
				{
					textareaToggle = "<a href=\"#\" class=\"textareaShow tooltip\" title=\""+TXT_MONITORING_SHOWHIDE_TEXT+"\"><img src=\""+BASE_URL+"images/icons/basic/16/Notepad_Remove.png\" /></a>";
					html = html.replace("<%TEXT_TD%>", "<td><div class=\"postBodyContainer\"><textarea name=\"postBody[]\" rows=\"3\" cols=\"48\" class=\"tooltip\" title=\""+TXT_MONITORING_TEXTAREA_TITLE+"\"></textarea></div></td>");
				}
				else
				{
					html = html.replace("<%TEXT_TD%>", "<td><input type=\"hidden\" name=\"postBody[]\" />");
				}
			}
			html = html.replace("<%THUMBS_UP%>", TXT_POST_THUMB_UP);
			html = html.replace("<%THUMBS_DOWN%>", TXT_POST_THUMB_DOWN);
			html = html.replace("<%FLAG_EXPLAIN%>", TXT_POST_HEALTH+": "+TXT_POST_FLAG_BLUE);
			html = html.replace("<%TOPIC_ADD%>", TXT_POST_TOPIC_ADD);
			html = html.replace("<%DELETE_A_CLASS%>", "deletePostNew");
			html = html.replace("<%DELETE_A_TOOLTIP%>", BTN_DELETE);
			html = html.replace("<%RATE_UP_IMG%>", BASE_URL+"images/icons/commerce/16/Rate_Thumbs_Up.png");
			html = html.replace("<%RATE_DOWN_IMG%>", BASE_URL+"images/icons/commerce/16/Rate_Thumbs_Down.png");
			html = html.replace("<%HEART_IMG%>", "Heart_BW");
			html = html.replace("<%SUN_IMG%>", "Sun");
			html = html.replace("<%SUN_STYLE%>", "");
			html = html.replace("<%CLOUD_IMG%>", "Cloud_BW");
			html = html.replace("<%CLOUD_STYLE%>", "");
			html = html.replace("<%RAIN_IMG%>", "Rain_BW");
			html = html.replace("<%RAIN_STYLE%>", "");
			html = html.replace("<%TEXTAREA_TOGGLE%>", textareaToggle);
			html = html.replace("<%NEW_POST%>", "");
			
			$(html).insertAfter($("h5", $(page)));
		}
		
		$(".postBlock:first .healthPlus, .postBlock:first .healthMinus", $(page)).bind("click", monitorHandler.healthClick);
		$(".postBlock:first .topicAdd", $(page)).bind("click", monitorHandler.topicAdd);
		$(".postBlock:first .deletePostNew", $(page)).bind("click", monitorHandler.postDelete);
		$(".postBlock:first .heartIcon", $(page)).bind("click", monitorHandler.proximityClick);
		$(".postBlock:first .intensityOK, .postBlock:first .intensitySevere, .postBlock:first .intensityCritical", $(page)).bind("click", monitorHandler.intensityClick);
		
		$(".postBlock:first .intensityOK img", $(page)).css({border: "solid 1px #000"});
		
		$(".postBlock:first .textareaShow", $(page)).bind("click", function (event) {
			event = $.event.fix(event);
			if (event.preventDefault) event.preventDefault();
		
			$("textarea", $(this).parents(".postBlock")).toggle();
			$(".postBodyContainer", $(this).parents(".postBlock")).toggle();
		});
			
		$.fn.tooltip("init");
		if (typeof(form_handler) != "undefined" && typeof(form_handler.init) != "undefined") form_handler.init();
		if (typeof(calendar) != "undefined" && typeof(calendar.init) != "undefined") calendar.init();
		
		if (grouped)
		{
			$(".postBlock:first input[name^=postCount]:first", $(page)).focus();
		}
		else
		{
			$(".postBlock:first input[name^=timeAt]:first", $(page)).focus();
		}
		
		$(".postBlock:first input[name^=timeAt]:first").unbind("blur", timeHandler.normalizeTime);
		$(".postBlock:first input[name^=timeAt]:first").bind("blur", timeHandler.normalizeTime);
		
		$(".postBlock:first input[name^=firstPostTimeAt]:first").unbind("blur", timeHandler.normalizeTime);
		$(".postBlock:first input[name^=firstPostTimeAt]:first").bind("blur", timeHandler.normalizeTime);
		
		$(".postBlock:first").unbind("mouseover mouseout", monitorHandler.highlightPost);
		$(".postBlock:first").bind("mouseover mouseout", monitorHandler.highlightPost);
	});
};
monitorHandler.postDelete = function (event) {
	event = $.event.fix(event);
	if (event.preventDefault) event.preventDefault();
	
	var element = $(event.target);
	
	if ($(element).get(0).tagName.toLowerCase() != "a")
	{
		element = $(element).parents("a");
	}
	
	if ($(element).hasClass("deletePostNew"))
	{
		$(element).parents(".postBlock").remove();
	}
	else
	{		
		var action = $(element).hasClass("deletePostDo") ? "delete" : "mark";
		var postID = $("input[name^=postID]", $(element).parents(".postBlock")).val();
		
		if (action == "mark")
		{
			if ($(".deleteReasonContainer", $(element).parents(".postBlock")).length <= 0)
			{
				$("<div>").addClass("deleteReasonContainer inlineBlock")
				.html("<input name=\"deleteReason\" class=\"tooltip\" title=\""+TXT_MONITORING_DELETE_REASON+"\" /><a href=\"#\" class=\"deleteConfirm tooltip\" title=\""+BTN_CONFIRM+"\"><img src=\""+BASE_URL+"\images/icons/basic/16/Confirm.png\" /></a>")
				.insertAfter($(element));
				
				$(".deleteReasonContainer input[name=deleteReason]").focus();
				
				$(".deleteReasonContainer input[name=deleteReason]").bind("keyup", function (event) {
					event = $.event.fix(event);
					
					if (event.keyCode == KEY_CODE_ENTER)
					{
						if (event.preventDefault) event.preventDefault();
						if (event.preventBubble) event.preventBubble();
					
						monitorHandler.postMark(postID, $("input[name=deleteReason]", $(element).parents(".postBlock")).val());
					}
				});
			
				$(".deleteConfirm").bind("click", function (event) {
					event = $.event.fix(event);
					if (event.preventDefault) event.preventDefault();
					
					monitorHandler.postMark(postID, $("input[name=deleteReason]", $(element).parents(".postBlock")).val());
				});
				
				$.fn.tooltip("init");
			}
		}
		else
		{
			var params = {};
			params.async = 0;
			params.url = BASE_URL+"overlays/confirmDelete.php";
			params.data = {
				"action": action,
				"postID": postID
			};
			$.fn.overlay("show", params);
		}
	}
	
	$.fn.tooltip("hide");
};
monitorHandler.postMark = function (postID, reason) {
	$.ajax({
		type: "POST",
		dataType: "json",
		async: true,
		url: BASE_URL+"scripts/postDeleteAJAX.php",
		cache: false,
		data: {
			"action": "mark",
			"postID": postID,
			"reason": reason
		},
		success: function (response) {
			if (response.status == "ok")
			{
				$.fn.statusBar("fadeWith", {html: JS_TXT_POST_MARKED});
				
				$(".postBlock input[name^=postID]").each(function () {
					if ($(this).val() == postID)
					{
						$(".newPostMarker", $(this).parents(".postBlock")).remove();
						$(".markForDeletion", $(this).parents(".postBlock")).remove();
						$(".deleteReasonContainer", $(this).parents(".postBlock")).remove();
						
						$(this).parents(".postBlock").removeClass("newPost");
						$(this).parents(".postBlock").addClass("markedForDeletion");
							
						var html = "<span class=\"markedForDeletionMarker tooltip\" title=\""+TXT_MONITORING_DELETE_REASON+": "+reason+"\">"+TXT_MONITORING_DELETE_POST_MARKER+"</span>";
						$(html).insertBefore($("input[name^=md5sum]", $(this).parents(".postBlock")));
						
						$.fn.tooltip("init");
					}
				});
			}
			else
			{
				$.fn.statusBar("showWith", {html: JS_TXT_POST_NOT_DELETED});
			}
		}
	});
};
monitorHandler.notifyAdmins = function (event) {
	event = $.event.fix(event);
	if (event.preventDefault) event.preventDefault();
	
	var params = {};
	params.async = 0;
	params.url = BASE_URL+"overlays/notifyAdmins.php";
	params.data = {};
	$.fn.overlay("show", params);
	
	$("#save").bind("click", function () {
		$.ajax({
			type: "POST",
			dataType: "json",
			async: true,
			url: BASE_URL+"scripts/notifyAdmins.php",
			cache: false,
			data: $("#frm_notifyAdmins").serializeJSON(),
			success: function (response) {
				if (response.status == "ok")
				{
					$.fn.statusBar("fadeWith", {html: JS_TXT_NOTIFY_OK});
				}
				else
				{
					$.fn.statusBar("showWith", {html: JS_TXT_NOTIFY_FAILED});
				}
			}
		});
	});
};
monitorHandler.highlightPost = function (event) {
	event = $.event.fix(event);
	var tmpElement = $(event.target);
	
	if (!$(tmpElement).hasClass("postBlock"))
	{
		tmpElement = tmpElement.parents(".postBlock");
	}
	
	if (event.type == "mouseover") {
		$(tmpElement).addClass("postBlockHighlighted");
	}
	else
	{
		$(tmpElement).removeClass("postBlockHighlighted");
	}
};

//////////////////
monitorHandler.showHide = function (event) {
	if (event.nodeType == 1)
	{
		element = $(event).parents(".monitoredPage");
	}
	else
	{
		event = $.event.fix(event);
	
		var anchor = $(event.target);
		while ($(anchor).get(0).nodeName.toLowerCase() != "a")
		{
			anchor = $(anchor).parents("a");
		}
		
		var element = $(anchor).parent().parent(".monitoredHub");
		if ($(element).length <= 0)
		{
			element = $(anchor).parent().parent(".monitoredPage");
			if ($(element).length <= 0)
			{
				element = $(anchor).parent().parent(".monitoredSession");
			}
		}
	}
	
	var re = new RegExp("Collapse");
	if (event.nodeType != 1)
	{
		if ($(element).hasClass("monitoredSession"))
		{
			$("table tbody", $(element)).toggle();
		}
		else if ($(element).hasClass("monitoredPage"))
		{
			$(".postBlock", $(element)).toggle();
		}
		else
		{
			$(".pagesContainer", $(element)).toggle();
		}
		
		if ($("img", $(anchor)).attr("src").match(re))
		{
			$(".loadMore", $(element)).remove();
			$("img", $(anchor)).attr("src", $("img", $(anchor)).attr("src").replace("Collapse", "Expand"));
		}
		else
		{
			$("img", $(anchor)).attr("src", $("img", $(anchor)).attr("src").replace("Expand", "Collapse"));
		}
	}
	
	if (event.nodeType == 1 || $("img", $(anchor)).attr("src").match(re))
	{
		if ($(element).hasClass("monitoredPage"))
		{
			var params = {};
			params.url = BASE_URL+"scripts/getPosts.php";
			params.data = {
				pageID: $(element).attr("id").substring(6, $(element).attr("id").length)
			};
			
			var tmpForm = $("#frm_paramHolder");
			if (tmpForm.length > 0)
			{
				var tmpValues = $("#frm_paramHolder").serializeJSON();
				$.extend(params.data, tmpValues);
				console.log(params.data);
			}
				
			var postCountSum = 0;
			$("input[name^=postCount]", $(element)).each(function () {
				postCountSum += parseInt($(this).val());
			});
				
			if (event.nodeType == 1)
			{
				$.extend(params.data, {
					offset: $(event).attr("rel"),
					postCountOffset: parseInt(postCountSum)
				});
			}

			$.ajax({
				type: "POST",
				dataType: "html",
				async: true,
				url: params.url,
				cache: false,
				data: params.data,
				success: function (response) {
					if (event.nodeType != 1)
					{
						$(".postBlock", $(element)).remove();
					}
					var anchor = $(".loadMore", $(element)).attr("rel");
					$(".loadMore", $(element)).remove();
					$(element).append(response);

					if (typeof(form_handler) != "undefined" && typeof(form_handler.init) != "undefined") form_handler.init();
					if (typeof(calendar) != "undefined" && typeof(calendar.init) != "undefined") calendar.init();
					
					monitorHandler.initPosts();
						
					$.fn.tooltip("init");
						
					if (event.nodeType == 1)
					{
						var tmpScrollStart = parseInt($("#monitoringWorkspace").scrollTop());
						var tmpScrollEnd = parseInt($("#monitoringWorkspace").scrollTop()+$($(".postBlock", $(element)).get(anchor)).position().top-$("#monitoringWorkspace").position().top);
						
						var amount = (tmpScrollEnd-tmpScrollStart)/monitorHandler.scrollDuration*10;

						monitorHandler.scroll(element, anchor, amount);
					}
				}
			});
		}
	}
};
monitorHandler.scroll = function (element, anchor, amount) {
	var tmpScrollStart = parseInt($("#monitoringWorkspace").scrollTop());
	var tmpScrollEnd = parseInt($("#monitoringWorkspace").scrollTop()+$($(".postBlock", $(element)).get(anchor)).position().top-$("#monitoringWorkspace").position().top);

	if (tmpScrollStart+amount < tmpScrollEnd)
	{
		$("#monitoringWorkspace").scrollTop(tmpScrollStart+amount);
		setTimeout(function () {monitorHandler.scroll(element, anchor, amount)}, 10);
	}
	else
	{
		$("#monitoringWorkspace").scrollTop(tmpScrollEnd);
	}
};

$(document).ready(function () {
	monitorHandler.init();
	
	$(".monitoredHub h3 a:first-child").each(function () {
		$(this).bind("click", monitorHandler.showHide);
	});
	
	$(".monitoredPage h5 a:first-child").each(function () {
		$(this).bind("click", monitorHandler.showHide);
	});
	
	$(".filterShow").bind("click", function () {
		var container = $(this).parents(".filterContainer");
		if ($("img", container).attr("src").match(/Expand\.png/))
		{
			$("img", container).attr("src", $("img", container).attr("src").replace(/Expand\.png/, "Collapse.png"));
		}
		else
		{
			$("img", container).attr("src", $("img", container).attr("src").replace(/Collapse\.png/, "Expand.png"));
		}
		$(".padding", container).slideToggle();
	});
	
	var workspaceHeight = ($(window).height()-$("#work").offset().top-$("#monitoringControls").outerHeight(true)-($("#work").offset().top-$("#main").offset().top)*3-10);
	if ($(".filterContainer").length > 0)
	{
		workspaceHeight -= ($("#monitoringWorkspace").offset().top-$(".filterContainer").offset().top+30);
	}
	else
	{
		workspaceHeight -= ($("#work h2").outerHeight(true));
	}
	
	$("#monitoringWorkspace").height(workspaceHeight+"px");
});