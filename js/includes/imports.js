$(document).ready(function () {
	$("#showImports").bind("click", function () {
		event = $.event.fix(event);
		if (event.preventDefault) event.preventDefault();
			
		var param = {};
		param.url = BASE_URL+"overlays/imports.php";
		param.data = {
			"lang": LANGUAGE
		};
			
		$.fn.overlay("show", param);
		/*$(".tabs").tabs({
			activate: function(event, ui) {
				event = $.event.fix(event);
			
				if ($(ui.newPanel).attr("id") == "tabs-4")
				{
					$("input[type=submit]", $(this)).css("display", "none");
					$("input[type=submit]", $(ui.newPanel)).css("display", "");
				}
				else
				{
					$("input[type=submit]", $(this)).css("display", "");
				}
			
				var params = {};
				params.resize = "D";
				$.fn.overlay("resize", params);
			}
		});*/
		imports.init();
	});
});

var imports = {};
imports.init = function () {
	$("#hubID").bind("change", function () {
		var params = {};
		params.url = BASE_URL+"scripts/getPages.php";
		params.data = $("#frm_imports").serializeJSON();
	
		$.ajax({
			type: "POST",
			dataType: "json",
			async: true,
			url: params.url,
			cache: false,
			data: params.data,
			success: function (response) {
				var html = ""
				for (var i = 0; i < response.length; i++)
				{
					html += "<option value=\""+response[i].id+"\">"+response[i].pageURL+"</option>";
				}

				$("#pageID").html(html);
			}
		});
	});
};