$(document).ready(function () {
	$(".adminBudgetNew").bind("click", function (event) {
		event = $.event.fix(event);
		if (event.preventDefault) event.preventDefault();
			
		var param = {};
		param.url = BASE_URL+"overlays/budgetHandler.php";
		param.data = {
			"act": "new",
			"lang": LANGUAGE
		};
			
		$.fn.overlay("show", param);
		$(".tabs").tabs();
		budgetHandler.init();
	});
	
	$(".adminBudgetMod").bind("click", function (event) {
		event = $.event.fix(event);
		if (event.preventDefault) event.preventDefault();
			
		var param = {};
		param.url = BASE_URL+"overlays/budgetHandler.php";
		param.data = {
			"id": $(this).attr("id").substring(6, $(this).attr("id").length),
			"act": "mod",
			"lang": LANGUAGE
		};
			
		$.fn.overlay("show", param);
		$(".tabs").tabs();
		budgetHandler.init();
	});
});

var budgetHandler = {};
budgetHandler.init = function ()
{
};