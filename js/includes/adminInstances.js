$(document).ready(function () {
	$(".adminInstanceNew").bind("click", function (event) {
		event = $.event.fix(event);
		if (event.preventDefault) event.preventDefault();
			
		var param = {};
		param.url = BASE_URL+"overlays/instanceHandler.php";
		param.data = {
			"act": "new",
			"lang": LANGUAGE
		};
			
		$.fn.overlay("show", param);
		/*$(".tabs").tabs({
			activate: function(event, ui) {
				event = $.event.fix(event);
			
				if ($(ui.newPanel).attr("id") == "tabs-2")
				{
					$("input[type=submit]", $(this)).css("display", "none");
					$("input[type=submit]", $(ui.newPanel)).css("display", "");
				}
				else
				{
					$("input[type=submit]", $(this)).css("display", "");
				}
			
				var params = {};
				params.resize = "XY";
				$.fn.overlay("resize", params);
			}
		});*/
	});
	
	$(".adminInstanceMod").bind("click", function (event) {
		event = $.event.fix(event);
		if (event.preventDefault) event.preventDefault();
			
		var param = {};
		param.url = BASE_URL+"overlays/instanceHandler.php";
		param.data = {
			"id": $(this).attr("id").substring(8, $(this).attr("id").length),
			"act": "mod",
			"lang": LANGUAGE
		};
			
		$.fn.overlay("show", param);
		$(".tabs").tabs({
			activate: function(event, ui) {
				event = $.event.fix(event);
			
				if ($(ui.newPanel).attr("id") == "tabs-2")
				{
					$("input[type=submit]", $(this)).css("display", "none");
					$("input[type=submit]", $(ui.newPanel)).css("display", "");
				}
				else
				{
					$("input[type=submit]", $(this)).css("display", "");
				}
			
				var params = {};
				params.resize = "XY";
				$.fn.overlay("resize", params);
			}
		});
	});
});