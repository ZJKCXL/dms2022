(function( $ ) {
	var methods = {
		show: function (params) {
			var is_async = params.async == 1 ? true : false;
			$.ajax({
				type: "POST",
				dataType: "json",
				async: is_async,
				url: params.url,
				cache: false,
				data: params.data,
				success: function (response) {
					if (response.data.css && response.data.css.length > 0)
					{
						for (var i = 0; i < response.data.css.length; i++)
						{
							if ($("link[href='"+response.data.css[i]+"']").length <= 0)
							{
								$("head", document)
								.append(
									$("<link>").attr({
										rel: "stylesheet",
										type: "text/css",
										media: "screen",
										href: response.data.css[i]
									})
								);
							}
						}
					}
					
					if (response.data.js && response.data.js.length > 0)
					{
						for (var i = 0; i < response.data.js.length; i++)
						{
							if ($("script[src='"+response.data.js[i]+"']").length <= 0)
							{
								$.getScript(response.data.js[i]);
							}
						}
					}

					$("body", document)
					.append(
						$("<div>").attr("id", "overlay")
					)
					.append(
						$("<div>").attr("id", "overlayContent")
						.html("<!--[if lte IE 6.5]><iframe></iframe><![endif]-->"+response.data.html)
					);
					
					var tmp_width = $("#overlayContent").width();
					var tmp_height = $("#overlayContent").height()+12;
					
					if ($(".tabs").length > 0)
					{
						tmp_height += 10;
					}
					
					if ($("#overlayContent").outerWidth() > $(window).width())
					{
						tmp_width = $(window).width()-($("#overlayContent").outerWidth(true)-tmp_width)*2;
					}
					
					$("#overlayContent").css({
						width: tmp_width+"px",
						height: tmp_height+"px"
					});
					
					if ($("#overlayContent").outerHeight() > $(window).height())
					{
						tmp_height = $(window).height()-($("#overlayContent").outerHeight(true)-tmp_height)*2;
						$("#overlayContent").css({
							height: tmp_height+"px"
						});
					}
					
					var margin_top = $("#overlayContent").outerHeight(true);
					
					if (params.displayTop)
					{
						margin_top = $(window).height()-24;
					}
					
					$("#overlayContent").css({
						"margin-left": "-"+($("#overlayContent").outerWidth(true)/2)+"px",
						"margin-top": "-"+(margin_top/2)+"px"
					});
					
					$("body", document).append(
						$("<img>")
						.attr("id", "overlayHide")
						.attr("src", BASE_URL+"images/icons/basic/24/Close.png")
						.css({
							"margin-left": ($("#overlayContent").outerWidth(true)-12)+"px",
							"margin-top": "-"+(margin_top/2+12)+"px"
						})
						.bind("click", function () {
							$.fn.overlay("hide");
						})
					);
					
					$("#overlay").fadeIn("fast");
					$("#overlayContent").fadeIn("fast");
					
					if (form_handler)
					{
						if (form_handler.init) form_handler.init();
					}
					return true;
				}
			});
		},
		resize: function (params) {
			if (params.resize == "X" || params.resize == "XY")
			{
				$("#overlayContent").css({
					width: "auto"
				});
			}
		
			if (params.resize == "Y" || params.resize == "XY" || params.resize == "D")
			{
				$("#overlayContent").css({
					height: "auto"
				});
			}
			
			var tmp_width = $("#overlayContent").width();
			var tmp_height = $("#overlayContent").height();

			if (params.resize == "X" || params.resize == "XY")
			{	
				if ($("#overlayContent").outerWidth() > $(window).width())
				{
					tmp_width = $(window).width()-Math.abs($("#overlayContent").outerWidth(true)-tmp_width)/2;
				}
				
				$("#overlayContent").css({
					width: tmp_width+"px"
				});
				
				$("#overlayContent").css({
					"margin-left": "-"+(tmp_width/2)+"px"
				});
				
				$("#overlayHide").css({
					"margin-left": (tmp_width/2-10)+"px"
				});
			}
			
			if (params.resize == "Y" || params.resize == "XY" || params.resize == "D")
			{
				if (params.resize != "D")
				{
					if ($("#overlayContent").outerHeight() > $(window).height())
					{
						tmp_height = $(window).height()-Math.abs($("#overlayContent").outerHeight(true)-tmp_height)/2;
					}
				
					$("#overlayContent").css({
						"margin-top": "-"+(tmp_height/2)+"px"
					});
				
					$("#overlayHide").css({
						"margin-top": "-"+(tmp_height/2+10)+"px"
					});
				}
				else
				{
					if ($("#overlayContent").outerHeight() > $(window).height()/2)
					{
						tmp_height = $(window).height()-24;
					}
				}
				
				$("#overlayContent").css({
					height: tmp_height+"px"
				});
			}
			
			return true;
		},
		hide: function () {
			$("#overlayContent").fadeOut("fast", function () {
				$("#overlayContent").remove();
			});
			$("#overlay").fadeOut("fast", function () {
				$("#overlay").remove();
			});
			$("#overlayHide").fadeOut("fast", function () {
				$("#overlayHide").remove();
			});
			
			return true;
		}
	};
	$.fn.overlay = function (method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		}
		else if (typeof method === "object" || !method) {
			return methods.init.apply(this, arguments);
		}
		else {
			$.error("Method "+method+" does not exist on jQuery.overlay");
		}
	};
})( jQuery );