(function( $ ) {
	$.fn.serializeJSON = function() {
		var json = {};
		jQuery.map($(this).serializeArray(), function(n, i) {
			if (json[n['name']])
			{
				if (typeof(json[n['name']]) != "object")
				{
					var tmp = [json[n['name']]];
					
					json[n['name']] = tmp;
				}
				
				json[n['name']].push(n['value']);
			}
			else
			{
				json[n['name']] = n['value'];
			}
		});
		return json;
	};
})( jQuery );