<?

header("Content-type: text/javascript; charset=utf-8");

$expires = 3600*24*7;

header("Pragma: public");
header("Cache-Control: maxage=".$expires);
header("Expires: ".gmdate("D, d M Y H:i:s", time()+$expires)." GMT");

include_once("./conf.php");
if (@file_exists("./../lang/".$_REQUEST["lang"].".php")) include_once("./../lang/".$_REQUEST["lang"].".php");

$constants = get_defined_constants(true);
$constants = $constants["user"];

foreach ($constants as $constant_name => $constant_value)
{
	$constant_value = str_replace("'", "\'", $constant_value);
	$constant_value = preg_replace("/\n/", "", $constant_value);
	print "var $constant_name = '".$constant_value."';";
}

include_once("./conf.protected.php");

?>