<?

/**
 * Define website variables
 */

$conflen=strlen("config");
$B=substr(__FILE__,0,strrpos(__FILE__,"/"));
$A=substr($_SERVER["DOCUMENT_ROOT"], strrpos($_SERVER["DOCUMENT_ROOT"], $_SERVER["PHP_SELF"]));
$C=substr($B,strlen($A));
$posconf=strlen($C)-$conflen-1;
$D=substr($C,1,$posconf);
$host="http".($_SERVER["HTTPS"] ? "s" : "")."://".$_SERVER['SERVER_NAME']."/".$D;

define("BASE_URL", $host);

set_include_path(get_include_path().PATH_SEPARATOR.LIB_DIR);
require_once("autoloader.php");

define("COOKIE_PATH", "/".substr($D, 0, strlen($D)-1));
//define("COOKIE_PATH", "/");
define("COOKIE_DOMAIN", $_SERVER['SERVER_NAME']);
define("COOKIE_USER_NAME", "ezmonitor_app_user");
define("COOKIE_SU_USER_NAME", "ezmonitor_app_su_user");
define("COOKIE_USER_EXPIRATION", null);

define("LANG_DEFAULT", "cz");

/**
 * define language
 */
define("LANGUAGE", LANG_DEFAULT);

define("APP_CHARSET", "utf-8");
define("APP_LOCALE", "cs_CZ");
define("APP_LOCALE_LANG", "cs");
define("PASSWORD_MINLEN", 6);

define("REPORT_TRESHOLD_DAYS", 30);
define("REPORT_TRESHOLD_MONTHS", 120);

define("JS_CONFIRM_WRAPPER_MSG", "Opravdu chcete tuto akci provést?");

define("FB_APP_ID", "461585277188608");

define("FB_PERIOD_DAY", 86400);
define("FB_PERIOD_WEEK", 604800);
define("FB_PERIOD_MONTH", 2592000);
define("FB_PERIOD_LIFETIME", 0);

define("API_URL_AIAIOO","http://www.aiaioo.com/apis/vaksent/");
define("API_URL_MASHAPE","http://text-processing.com/api/sentiment/");
define("API_URL_REPUSTATE","http://api.repustate.com/v2/%KEY%/score.json");
define("API_URL_GOOGLE_TRANSLATE","https://www.googleapis.com/language/translate/v2");

$calendar = array(
	"monthLong" => array(
		"cz" => array(
			1 => "Leden",
			2 => "Únor",
			3 => "Březen",
			4 => "Duben",
			5 => "Květen",
			6 => "Červen",
			7 => "Červenec",
			8 => "Srpen",
			9 => "Září",
			10 => "Říjen",
			11 => "Listopad",
			12 => "Prosinec"
		)
	),
	"dayShort" => array(
		"cz" => array(
			1 => "Po",
			2 => "Út",
			3 => "St",
			4 => "Čt",
			5 => "Pá",
			6 => "So",
			7 => "Ne"
		)
	)
);

define("KEY_CODE_ENTER",13);
define("KEY_CODE_ARROW_UP",38);
define("KEY_CODE_ARROW_DOWN",40);
define("KEY_CODE_ARROW_LEFT",37);
define("KEY_CODE_ARROW_RIGHT",39);
define("KEY_CODE_ESC",27);
define("KEY_CODE_SPACE",32);
define("KEY_CODE_CTRL",17);
define("KEY_CODE_ALT",18);
define("KEY_CODE_TAB",9);
define("KEY_CODE_SHIFT",16);
define("KEY_CODE_CAPS_LOCK",20);
define("KEY_CODE_WINDOWS",91);
define("KEY_CODE_WINDOWS_OPTION",93);
define("KEY_CODE_BACKSPACE",8);
define("KEY_CODE_HOME",36);
define("KEY_CODE_END",35);
define("KEY_CODE_INS",45);
define("KEY_CODE_DEL",46);
define("KEY_CODE_PAGE_UP",33);
define("KEY_CODE_PAGE_DOWN",34);
define("KEY_CODE_NUM_LOCK",144);
define("KEY_CODE_F1",112);
define("KEY_CODE_F2",113);
define("KEY_CODE_F3",114);
define("KEY_CODE_F4",115);
define("KEY_CODE_F5",116);
define("KEY_CODE_F6",117);
define("KEY_CODE_F7",118);
define("KEY_CODE_F8",119);
define("KEY_CODE_F9",120);
define("KEY_CODE_F10",121);
define("KEY_CODE_F11",122);
define("KEY_CODE_F12",123);
define("KEY_CODE_SCROLL_LOCK",145);
define("KEY_CODE_PAUSE_BREAK",19);

?>
