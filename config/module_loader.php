<?

/**
 * Add modules you wish to load
 * Template: object, event, module
 */

$modules = array();
//$modules["window"]["load"]["overlay"] = "";
//$modules["window"]["load"]["suggest"] = "";
$modules["window"]["load"]["calendar"] = "";
//$modules["window"]["load"]["color_picker"] = "";
$modules["window"]["load"]["prevent"] = "";
$modules["window"]["load"]["row_highlight"] = "";
$modules["window"]["load"]["dragdrop"] = "";

/**
 * DO NOT ALTER BEYOND HERE
 */

foreach ($modules as $object => $set)
{
	foreach ($set as $event => $module_subset)
	{
		foreach ($module_subset as $module => $params)
		{
			if (@file_exists(BASE_DIR."js/modules/".$module."/".$module.".conf.php"))
			{
				echo "<script type=\"text/javascript\" src=\"".BASE_URL."js/modules/".$module."/".$module.".conf.php$params\"></script>\n";
			}

			if (@file_exists(BASE_DIR."js/modules/".$module."/".$module.".js"))
			{
				echo "<script type=\"text/javascript\" src=\"".BASE_URL."js/modules/".$module."/".$module.".js\"></script>\n";
			}

			if (@file_exists(BASE_DIR."js/modules/".$module."/".$module.".php"))
			{
				echo "<script type=\"text/javascript\" src=\"".BASE_URL."js/modules/".$module."/".$module.".php$params\"></script>\n";
			}

			if (@file_exists(BASE_DIR."js/modules/".$module."/".$module.".css"))
			{
				echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"".BASE_URL."js/modules/".$module."/".$module.".css\"  media=\"screen\" />\n";
			}
			else if (@file_exists(BASE_DIR."js/modules/".$module."/".$module.".css.php"))
			{
				echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"".BASE_URL."js/modules/".$module."/".$module.".css.php\"  media=\"screen\" />\n";
			}
		}
	}
}

echo "<script type=\"text/javascript\">\n";
foreach ($modules as $object => $set)
{
	foreach ($set as $event => $module_subset)
	{
		foreach ($module_subset as $module => $params)
		{
			echo "if (typeof(".$module.") != \"undefined\")\n";
			echo "{\n";
			echo "	if (".$module.".init)";
			echo "	{\n";
			echo (strlen($event) > 0) ? "		event_handler.add(".$object.", '$event', ".$module.".init);\n" : $module.".init();";
			echo "	}\n";
			echo "}\n";
		}
	}
}
echo "</script>\n";

?>