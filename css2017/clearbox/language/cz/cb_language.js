//                  
// 	ClearBox Language File (JavaScript)
//

var

	CB_NavTextPrv='p�edchoz�',				// text of previous image
	CB_NavTextNxt='dal��',					// text of next image
	CB_NavTextFull='full size',				// text of original size (only at pictures)
	CB_NavTextOpen='v nov�m okn�',		// text of open in a new browser window
	CB_NavTextDL='download',				// text of download picture or any other content
	CB_NavTextClose='zav��t',			// text of close CB
	CB_NavTextStart='start slideshow',			// text of start slideshow
	CB_NavTextStop='stop slideshow',			// text of stop slideshow
	CB_NavTextRotR='rotate image right by 90 degrees',	// text of rotation right
	CB_NavTextRotL='rotate image left by 90 degrees'	// text of rotation left

;
